FROM node:10-alpine
WORKDIR /node_app
COPY package*.json ./
RUN npm install
RUN npm install pm2 -g

# ENV PORT=10001
# ENV DATABASE_HOST=localhost
# ENV DATABASE_PASSWORD=1234
# ENV DATABASE_USER=root
# ENV DATABASE_NAME=marketCloser
# ENV DATABASE_PCEX_HOST=18.136.80.202
# ENV DATABASE_PCEX_PASSWORD=Hit4man47#0102
# ENV DATABASE_PCEX_USER=accounts
# ENV DATABASE_PCEX_NAME=pcex_live

# ENV PCEX_BASE_URL=http://localhost:3333
# ENV PCEX_PRIVATE_KEY=aaaafdsjhfjsf4875jddje38dnkjskshd837rjfndkf98r3ukfnkdj34urewndskljero3irkejf
# ENV PCEX_PUBLIC_KEY=jhsdaswidwcmnkwjkdlwkdslklkdalkskjdxmlwjeiu390219308248-21930274-293-284-35433

# EXPOSE 5005
COPY  . .
RUN npm run build
COPY './src/.env' './dist/'  

CMD [ "pm2-runtime","dist/index.js" ]

# docker build -t fcn_api_image .
# docker run -it --rm  --name=fcn_api_container --network="host" fcn_api_image 