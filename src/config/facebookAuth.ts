import passport from "passport";
import * as FbStrategy from 'passport-facebook';
import {serverConfig} from '../init/config';

export function doAuth(passpor:passport.PassportStatic)
{
    passport.serializeUser((user:any, done:any) => {
        done(null, user);
    });
    passport.deserializeUser((user:any, done:any) => {
        done(null, user);
    });
    let facebookOptions:FbStrategy.StrategyOption={
        clientID: serverConfig.oAuthConfig.FACEBOOK_CLIENT_ID,
        clientSecret: serverConfig.oAuthConfig.FACEBOOK_CLIENT_SECRET,
        callbackURL:"http://localhost:5005/api/login/facebook/callback",
        enableProof:true,
        profileFields: ['id', 'displayName', 'emails']
    }
    passport.use(new FbStrategy.Strategy(facebookOptions,
        (token:string, refreshToken:string, profile:FbStrategy.Profile, done:any) => {      
            return done(null, {
                profile: profile,
                token:token                
            });
        }));
};