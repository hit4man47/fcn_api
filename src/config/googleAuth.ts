import * as  GoogleStrategy from 'passport-google-oauth20';
import {PassportStatic} from 'passport';

import {serverConfig} from '../init/config';

 export function doAuth(passport:PassportStatic) {
    passport.serializeUser((user:any, done: any) => {
        done(null, user);
    });
    passport.deserializeUser((user: any, done: any) => {
        done(null, user);
    });
    
    let googleOptions:GoogleStrategy.StrategyOptions = {
            clientID:serverConfig.oAuthConfig.GOOGLE_CLIENT_ID,
            clientSecret:serverConfig.oAuthConfig.GOOGLE_CLIENT_SECRET,
            callbackURL:"http://localhost:5005/api/login/google/callback",
            //accessType: 'offline'
        }
  
    passport.use(
        new GoogleStrategy.Strategy(googleOptions,
                (accessToken:string,refreshToken:string,profile:GoogleStrategy.Profile,done:GoogleStrategy.VerifyCallback) => {
                        return done(undefined, {token: accessToken,profile: profile});                    
                    }
            )
    );
}