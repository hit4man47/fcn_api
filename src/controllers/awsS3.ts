
import * as fs from 'fs';
import * as path from 'path';
import {ERROR_CODES } from '../utils/constants';
import * as express from 'express';
import * as mime from 'mime';
import * as jimp from 'jimp';
import * as requestResponseHandler from '../init/requestResponseHandler';
import { MyExpressRequest, U } from '../types/customTypes';
import { ImageDimen } from '../types/NewTypes';
import * as dotenv from 'dotenv';
import * as AWS from 'aws-sdk';  
var x = __dirname + "/../.env";
dotenv.config({ path: x });
import {BUCKET } from '../utils/constants'
import { CostExplorer } from 'aws-sdk';
import {UploadFolderRequest} from '../../src/types/customTypes'
import {checkBucketExists, keyUploadingUtils} from '../utils/fileUpload';
import {serverConfig} from '../init/config'


//configuring the AWS environment
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: BUCKET.BUCKET_REGION,
    apiVersion: '2006-03-01'  // This is API version of AWS, (not ours). Don't change it, unless not using new version of the AWS API
});


// create s3 instance
const s3 = new AWS.S3()


// const createBucketParams : AWS.S3.CreateBucketRequest= {
//     Bucket: BUCKET.BUCKET_NAME,
//     ACL: 'public-read',
//     CreateBucketConfiguration: 
//     {    
//         LocationConstraint: BUCKET.BUCKET_REGION 
//     }
    
// }

const checkBucketParams : AWS.S3.HeadBucketRequest = {
    Bucket: BUCKET.BUCKET_NAME
};





export function uploadFolder(input : UploadFolderRequest) : Promise<any>
{
    return new Promise(async (resolve, reject) =>
    {
        try
        {
            if(!checkBucketExists(checkBucketParams) )
            {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_HIDDEN_FROM_USER, "Bucket not exist");
                return;
            }

            let {folderFullPath, folderName} = input;
        

            let arr: Array<string> = [];
            let files = fs.readdirSync(folderFullPath);
          

            for(let file of files)
            {                
                let srcFilePath = path.join(folderFullPath , file);
                let copyOfFile = fs.readFileSync(srcFilePath);
                let uploadObjectParams :AWS.S3.PutObjectRequest =  {
                    Bucket: BUCKET.BUCKET_NAME,
                    Key: `assets/${folderName}/${file}`,
                    Body: copyOfFile,
                    ACL: "public-read",
                    ContentType: mime.getType(file) as string
                }

                let s3Path = await keyUploadingUtils(uploadObjectParams);
                // arr.push(s3Path)
                arr.push(uploadObjectParams.Key)

            }
            resolve({ message: `LIST OF FILES UPLOADED`,data: arr, status: 200 });
            return;   
        }
        catch(e)
        {
            reject(e);
        }
    });
}





