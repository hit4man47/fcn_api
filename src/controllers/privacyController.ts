
import PrivacyModel from '../models/Privacy'
import * as express from 'express'
import * as requestResponseHandler from '../init/requestResponseHandler';
import { validationResult, Result } from 'express-validator';
import { CONSTANTS } from '../utils/constants';

export const getPrivacy = (req: express.Request, res: express.Response) => {

    PrivacyModel.findOne({ 'privacyId': 1 }).select('-_id content').lean().exec((err, result) => {
        if (err) {
            requestResponseHandler.sendErrorResponse(res, err);
        } else {
            requestResponseHandler.sendSuccessResponse(res, "Data fetched successfully", result);
        }
    });
}

export const updatePrivacy = async (req: express.Request, res: express.Response) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            requestResponseHandler.sendErrorResponse(res, { status: CONSTANTS.ERROR_DATABASE_ERROR, "error": errors.array() });
        } else {
            const updateData = await PrivacyModel.updateOne({ 'privacyId': 1 }, { 'content': req.body.content.trim() }, { upsert: true });

            requestResponseHandler.sendSuccessResponse(res, "Privacy updated successfully", 200);
        }
    } catch (e) {
        requestResponseHandler.sendErrorResponse(res, e);
    }
}       
