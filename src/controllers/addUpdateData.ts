import { ApiAddUpdateData } from "../types/addUpdateData";
import * as addDataModel from '../models/AddUpdateData';
import { CONSTANTS, SUB_CATEGORY_ID, DocStatus, DATATYPE, ERROR_CODES, ADD_UPDATE_DATA_MESSAGES, LOGIN_ERROR, SIZES_FOR_MIGRATE_IMAGES } from "../utils/constants";
import { jwtTypes } from "../routes/api/Auth";
import { AddUpdateStatus } from "../utils/constants";
import { getAllCategory, getCategoryByName, GetCategoryWhereFilters, getDefaultCategory } from "../models/Category";
import { GetAllCategorySortFilters, Category, AddCategoryInput } from "../models/Category";
import { AddUpdateData, GetAllDataOutput, addAuthor, AddAuthor, getDefaultAuthor } from "../models/AddUpdateData";
import { getData, getCategoryData, getTags } from '../utils/updateCatefories'
import * as fs from 'fs';
import * as path from 'path';
import moment = require("moment");
var request = require('request');
import * as tags from '../models/tags'
import * as mime from 'mime';
import {getImagesMimeTypes, keyUploadingUtils, imageResizer, getImagesMimeCustom} from '../utils/fileUpload';
import {ImageResizeConstants, MIGRATE_DATA_QUALITY_PERCENT} from "../utils/constants"
import {changePathFromS3} from '../utils/getImagePathBySize';
import {BUCKET} from '../utils/constants'





const si = require('systeminformation');

export function createOrUpdateData(input: ApiAddUpdateData.CreateOrUpdateDataRequest, payload: jwtTypes.LOGIN_PAYLOAD): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { tags, title, description, category, seo, doc, subCategory, label, section, id, file, video, noOfView, timeSpend, status, type, url, author, shortDescription, thumbnail } = input;


            let dataSchema: addDataModel.AddTDataInput | addDataModel.UpdateDataInfo =
            {
                tags, title, description, seo, doc, category, subCategory, label, section, file, video, noOfView, timeSpend, status, type, url, author, shortDescription, thumbnail
            }

            let data: addDataModel.AddUpdateData.AddUpdateDataExposedSchema | null = null;

            let isDataNew: boolean;

            if (id) {
                isDataNew = false;

                data = await addDataModel.getData(id, input.type);
                if (!data) {
                    global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Data Not Found");
                    return;
                }
                data = await addDataModel.updateDataInfo(id, dataSchema, input.type);
                if (!data) {
                    global.rejector(reject, ERROR_CODES.FAIL_UPDATE_DB, ADD_UPDATE_DATA_MESSAGES.FAIL_UPDATE_DB);
                    return;
                }
            }
            else {
                isDataNew = true;
                data = await addDataModel.addData(dataSchema, "ADMIN", input.type);
                if (!data) {
                    global.rejector(reject, ERROR_CODES.FAIL_UPDATE_DB, ADD_UPDATE_DATA_MESSAGES.FAIL_UPDATE_DB);
                    return;
                }
            }

            let message;
            if (isDataNew)
                message = `New Data Created`;
            else {

                message = `Data Updated  ${input.type}`;
            }


            resolve({ message: message, data: {} });
        }
        catch (e) {
            reject(e);
        }
    });
}


interface getbyIdData {
    mainPost: AddUpdateData.AddUpdateDataExposedSchema,
    trending: Array<AddUpdateData.AddUpdateDataExposedSchema>,
    editorChoice: Array<AddUpdateData.AddUpdateDataExposedSchema>,
    relatedCategory: Array<Category.CategoryExposedSchema>,//Array<ApiAddUpdateData.GetRelatedCategoryResponse>,
    relatedPost: Array<AddUpdateData.AddUpdateDataExposedSchema>
}

export function getNewsDetailById(input: ApiAddUpdateData.GetDataByIdRequest, payload: jwtTypes.LOGIN_PAYLOAD, ipAddress: any): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { _id, type } = input;
            let data: any = await addDataModel.getData(_id, type);
            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Data Not Found");
                return;
            }

            let result: getbyIdData = {
                mainPost: data,
                trending: [],
                editorChoice: [],
                relatedCategory: [],
                relatedPost: []
            }

            if (input.pageData === 1) {

                let relatedCategory: ApiAddUpdateData.GetRelatedCategory = {
                    whereFilters: {
                        _id: "", parentId: String(data.category._id), status: 0, name: "", categoryName: ""
                    },
                    sortFilters: {
                        _id: 0, parentId: 0, status: 0, time: 0, name: 0, order: 0
                    },
                    pageNumber: 1,
                    pageSize: 2
                }
                let { categoryList } = await getAllCategory(1, 500, relatedCategory.whereFilters, relatedCategory.sortFilters);
                result.relatedCategory = categoryList;

                let trending: ApiAddUpdateData.GetAllDataRequest = {
                    type: input.type,
                    whereFilters: {
                        category: "", status: 0, subCategory: "", title: "", label: ['TRENDING'], section: [], _id: "", categoryName: "", subCategoryName: "", url: ""
                    },
                    sortFilters: { title: 0, status: 0, timeSpend: 0, createdDate: -1, category: 0, subCategory: 0, noOfView: 0 },
                    pageNumber: 1,
                    pageSize: 4
                }

                let t = await getAllData(trending);
                result.trending = t.data.dataList;



                let relatedPost: ApiAddUpdateData.GetAllDataRequest = {
                    type: input.type,
                    whereFilters: {
                        category: "", status: 0, subCategory: "", title: data.title, label: [], section: [], _id: "", categoryName: "", subCategoryName: "", url: ""
                    },
                    sortFilters: { title: 0, status: 0, timeSpend: 0, createdDate: -1, category: 0, subCategory: 0, noOfView: 0 },
                    pageNumber: 1,
                    pageSize: 5
                }
                let r = await getAllData(relatedPost);
                result.relatedPost = r.data.dataList;

                let editorChoice: ApiAddUpdateData.GetAllDataRequest = {
                    type: input.type,
                    whereFilters: {
                        category: "", status: 0, subCategory: "", title: "", label: ["EDITOR'S CHOICE"], section: [], _id: "", categoryName: "", subCategoryName: "", url: ""
                    },
                    sortFilters: { title: 0, status: 0, timeSpend: 0, createdDate: -1, category: 0, subCategory: 0, noOfView: 0 },
                    pageNumber: 1,
                    pageSize: 3
                }

                let e = await getAllData(editorChoice);
                result.editorChoice = e.data.dataList;

            }

            const sys_info = await si.osInfo();
            let addImpressionData: ApiAddUpdateData.AddImpressionRequest = {
                ipAddress: ipAddress,
                newsId: data._id,
                type: input.type,
                noOfView: 1,
                device: sys_info.platform,
                viewDate: Date.now()
            }
            if (data.noOfView && data.noOfView > 0) {
                addImpressionData.noOfView = data.noOfView + 1;
            }
            let impdata = await addDataModel.addImpression(addImpressionData);

            let updateviewData: any = {
                noOfView: addImpressionData.noOfView
            }
            let updateView = addDataModel.updateDataInfo(data._id, updateviewData, input.type);

            resolve({ message: "Data Fetched", data: { ...result } });
        }
        catch (e) {
            reject(e);
        }
    });
}

export function getUniqueUrlData(input: ApiAddUpdateData.Url, ipAddress: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let data = await addDataModel.getUniqueUrlData(input.url);
            

            console.log("data==", data);
            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "No Data Found with this url");
                return;
            }
            // console.log("data ::",data)
            let result: getbyIdData = {
                mainPost: data,
                trending: [],
                editorChoice: [],
                relatedCategory: [],
                relatedPost: []
            }
            if (input.pageData === 1) {

                let relatedCategory: ApiAddUpdateData.GetRelatedCategory = {
                    whereFilters: {
                        _id: "", parentId: String(data.category._id), status: 0, name: "", categoryName: ""
                    },
                    sortFilters: {
                        _id: 0, parentId: 0, status: 0, time: 0, name: 0, order: 0
                    },
                    pageNumber: 1,
                    pageSize: 2
                }
                let { categoryList } = await getAllCategory(1, 500, relatedCategory.whereFilters, relatedCategory.sortFilters);
                result.relatedCategory = categoryList;

                let trending: ApiAddUpdateData.GetAllDataRequest = {
                    type: input.type,
                    whereFilters: {
                        category: "", status: 0, subCategory: "", title: "", label: ['TRENDING'], section: [], _id: "", categoryName: "", subCategoryName: "", url: ""
                    },
                    sortFilters: { title: 0, status: 0, timeSpend: 0, createdDate: -1, category: 0, subCategory: 0, noOfView: 0 },
                    pageNumber: 1,
                    pageSize: 4
                }

                let t = await getAllData(trending);
                result.trending = t.data.dataList;


                let relatedPost: ApiAddUpdateData.GetAllDataRequest = {
                    type: input.type,
                    whereFilters: {
                        category: "", status: 0, subCategory: "", title: { $ne: data.title } as unknown as string, label: [], section: [], _id: "", categoryName: data.category.name, subCategoryName: "", url: ""
                    },
                    sortFilters: { title: 0, status: 0, timeSpend: 0, createdDate: -1, category: 0, subCategory: 0, noOfView: 0 },
                    pageNumber: 1,
                    pageSize: 5
                }
                let r = await getAllData(relatedPost);
                
                let res =  r.data.dataList;

                console.log("typeof===", typeof res);
                for (const data of res) {
                    
                    const imageMimeTypes = getImagesMimeTypes();

                    let mimeType = mime.getType(data.file) ;
  
                    if(imageMimeTypes.indexOf(mimeType || '')>=0)
                    {
                        const u: string = data.file;
                        const lastIndexOfDot = u.lastIndexOf('.');
                        const choosenDimen = ImageResizeConstants.NEWS_IMAGE.SMALL2; // ImageResizeConstants.ADD_OR_UPDATE.RELATED_POST;
                        const newUrl = `${u.substr(0,lastIndexOfDot)}_${choosenDimen.width}x${choosenDimen.height}${u.substr(lastIndexOfDot)}`;
                        data.file = newUrl;
                    }
    
                }
            
                // result.relatedPost = r.data.dataList;
                result.relatedPost  = res;
                let editorChoice: ApiAddUpdateData.GetAllDataRequest = {
                    type: input.type,
                    whereFilters: {
                        category: "", status: 0, subCategory: "", title: "", label: ["EDITORS CHOICE"], section: [], _id: "", categoryName: "", subCategoryName: "", url: ""
                    },
                    sortFilters: { title: 0, status: 0, timeSpend: 0, createdDate: -1, category: 0, subCategory: 0, noOfView: 0 },
                    pageNumber: 1,
                    pageSize: 3
                }

                let e = await getAllData(editorChoice);
                 res = e.data.dataList;

                for (const data of res) {
                    
                    const imageMimeTypes = getImagesMimeTypes();

                    let mimeType = mime.getType(data.file) ;
  
                    if(imageMimeTypes.indexOf(mimeType || '')>=0)
                    {
                        const u: string = data.file;
                        const lastIndexOfDot = u.lastIndexOf('.');
                        const choosenDimen = ImageResizeConstants.NEWS_IMAGE.SMALL1;  // ADD_OR_UPDATE.EDITOR_CHOICE
                        const newUrl = `${u.substr(0,lastIndexOfDot)}_${choosenDimen.width}x${choosenDimen.height}${u.substr(lastIndexOfDot)}`;
                        data.file = newUrl;
                    }
    
                }

                result.editorChoice  = res;
                const sys_info = await si.osInfo();
                let addImpressionData: ApiAddUpdateData.AddImpressionRequest = {
                    ipAddress: ipAddress,
                    newsId: data._id,
                    type: input.type,
                    noOfView: 1,
                    device: sys_info.platform,
                    viewDate: Date.now()
                }

                if (data.noOfView && data.noOfView > 0) {
                    addImpressionData.noOfView = data.noOfView + 1;
                }
                let impdata = await addDataModel.addImpression(addImpressionData);

                let updateviewData: any = {
                    noOfView: addImpressionData.noOfView
                }
                // console.log("1111111111111", updateviewData);
                let updateView = addDataModel.updateDataInfo(data._id, updateviewData, input.type);

            }

            resolve({ data: result, message: "Data Fetched" })
        }
        catch (error) {
            reject(error)
        }
    })
}
export function getDataById(input: ApiAddUpdateData.GetDataByIdRequest, payload: jwtTypes.LOGIN_PAYLOAD, ipAddress: any): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { _id, type } = input;
            let data: any = await addDataModel.getData(_id, type);
            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Data Not Found");
                return;
            }
            resolve({ message: "Data Fetched", data: data });
        }
        catch (e) {
            reject(e);
        }
    });
}
export function getAllData(input: ApiAddUpdateData.GetAllDataRequest): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { category, subCategory, categoryName, subCategoryName, title } = input.whereFilters;

            let categorydata;
            if (categoryName) {
                let categoryNames: GetCategoryWhereFilters = {
                    categoryName: categoryName,
                    parentId: "0"
                }
                categorydata = await getCategoryByName(categoryNames);
                input.whereFilters.category = String(categorydata[0]._id)
            }
            if (subCategoryName) {

                if (categorydata && categorydata.length === 1) {
                    let categoryName1: GetCategoryWhereFilters = {
                        categoryName: subCategoryName,
                        parentId: String(categorydata[0]._id)
                    }
                    let subCategory = await getCategoryByName(categoryName1);
                    if (subCategory && subCategory.length === 1) {
                        input.whereFilters.subCategory = String(subCategory[0]._id)
                    }
                }
            }

            let { pageNumber, pageSize } = input;
            let { dataList, count } = await addDataModel.getAllData(input.pageNumber, input.pageSize, input.whereFilters, input.sortFilters, input.type);
           //  console.log("count :", count);

           console.log("dataList===", dataList);
           
            let data: ApiAddUpdateData.GetAllDataResponse =
            {
                pageNumber,
                pageSize,
                count,
                dataList
            }

            resolve({ message: "Data Fetched", data });
        }
        catch (e) {
            reject(e);
        }
    });
}

export function changeStatus(input: ApiAddUpdateData.ChangeStatus, payload: jwtTypes.LOGIN_PAYLOAD): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { id, type, status } = input;

            let updatData = addDataModel.updateStatus(id, status, type);

            resolve({ message: "status updated", data: { updatData } });
        }
        catch (e) {
            reject(e);
        }
    })
}

export function getRelatedCategory(input: ApiAddUpdateData.GetRelatedCategory): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let { _id, parentId, categoryName, status } = input.whereFilters;

            let categorydata;
            if (categoryName) {
                let categoryNames: GetCategoryWhereFilters = {
                    categoryName: categoryName,
                    parentId: "0"
                }
                categorydata = await getCategoryByName(categoryNames);
                input.whereFilters.parentId = String(categorydata[0]._id)
            }

            let { categoryList } = await getAllCategory(1, 100, input.whereFilters, input.sortFilters);

            let isDataFound = false;
            if (categoryList.length > 0) {
                isDataFound = true;
            }

            let inputs: ApiAddUpdateData.GetAllDataRequest = {
                type: "news",
                whereFilters: {
                    category: "", status: 0, subCategory: "", title: "", label: [], section: [], _id: "", categoryName: "", subCategoryName: "", url: ""
                },
                sortFilters: { title: 0, status: 0, timeSpend: 0, createdDate: -1, category: 0, subCategory: 0, noOfView: 0 },
                pageNumber: input.pageNumber ? input.pageNumber : 1,
                pageSize: input.pageSize ? input.pageSize : 1
            }
            var RelatedCategoryData: Array<ApiAddUpdateData.GetRelatedCategoryResponse> = [];

            for (let i = 0; i < categoryList.length; i++) {
                inputs.whereFilters.subCategory = categoryList[i]._id


                let { dataList } = await addDataModel.getAllData(input.pageNumber, input.pageSize, inputs.whereFilters, inputs.sortFilters, inputs.type);
                let data: ApiAddUpdateData.GetRelatedCategoryResponse =
                {
                    category: categoryList[i].name,
                    dataList
                }
                RelatedCategoryData.push(data);
            }
            let message;
            if (isDataFound)
                message = "Related Category Data Fetched";
            else {
                message = "Related Category Data not found";
            }
            resolve({ message: message, data: { RelatedCategoryData } });
        }
        catch (e) {
            reject(e);
        }
    });
}
export function getDataType(_id: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let type = await addDataModel.getDataType(_id)
            resolve(type);
        }
        catch (e) {
            reject(e);
        }
    });
}

export function getDataTypeUsingUrl(url: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let data = await addDataModel.getDataTypeUsingUrl(url)
            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "No Data Found with this url");
                return;
            }
            resolve(data);
        }
        catch (e) {
            reject(e);
        }
    });
}

//function to downlaod image from url. Save Image Locally
// function downloadImage(uri: string, filename: string): Promise<boolean> {
//     return new Promise( (resolve, reject) => {
//         try {
//             request.head(uri, function (err: any, res: any, body: any) {
//                 if (err) {
//                     console.log("escapte ", uri, "filename", filename)
//                     console.log("Image Download failed", err)
//                     resolve(false);
//                     return;
//                 }
//                 console.log('content-type:', res.headers['content-type']);
//                 console.log('content-length:', res.headers['content-length']);

//                 console.log("uri::", uri);
//                 console.log("filename::", filename);
                
            
//                 request(uri).pipe(fs.createWriteStream(filename)).on('close', async() => {

//                     /**
//                      * 
//                      */
//                     let key = `assets/news/${uri}`;
//                     console.log("key==", key);
                    
//                     let uploadObjectParams :AWS.S3.PutObjectRequest =  {
//                         Bucket: BUCKET.BUCKET_NAME,
//                         Key: key,
//                         Body: filename,// readFileContent,
//                         ACL: "public-read",
//                         ContentType: mime.getType(uri) as string
//                     }
//                     let s3Path = await keyUploadingUtils(uploadObjectParams); 
//                      /**
//                       * 
//                       */
//                     console.log("Image Downloaded", s3Path)
//                     resolve(true);
//                 });
//             });
//         } catch (error) {
//             console.log(false);
//             reject(error);
//         }
//     });
// };


// Save Image to s3 directly

function downloadImage(uri: string, filename: string): Promise<boolean> {
    return new Promise( (resolve, reject) => {
        try {
            request.head(uri, function (err: any, res: any, body: any) {
                if (err) {
                   // console.log("escapte ", uri, "filename", filename)
                   // console.log("Image Download failed", err)
                    resolve(false);
                    return;
                }
            //    console.log('content-type:', res.headers['content-type']);
             //   console.log('content-length:', res.headers['content-length']);

              //  console.log("uri::", uri);
              //  console.log("filename::", filename);
                
                

                request(uri).pipe(fs.createWriteStream(filename)).on('close', async() => {
                    
                    let new_uri = uri.substring(61)

                    let key = `assets/news/${new_uri}`;

                    let new_file = fs.readFileSync(filename);

                                   
                    let uploadObjectParams :AWS.S3.PutObjectRequest =  {
                        Bucket: BUCKET.BUCKET_NAME,
                        Key: key,
                        Body: new_file,// filename,// readFileContent,
                        ACL: "public-read",
                        ContentType: mime.getType(uri) as string
                    }
                    let s3Path = await keyUploadingUtils(uploadObjectParams); 
                
                    console.log("mime.getType(uri)==", mime.getType(uri));
                    const imageMimeTypes = getImagesMimeCustom();
                    const mimeType = mime.getType(uri) as string;

                    console.log("mimeType==", mimeType);
                    if(imageMimeTypes.indexOf(mimeType || '')>=0)
                    {
                        let res = await imageResizer(filename, SIZES_FOR_MIGRATE_IMAGES, MIGRATE_DATA_QUALITY_PERCENT.QUALITY, "news");
                        console.log("Res===", res);
                    }
                    if(!s3Path)
                    {
                        global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Error when uploaing file to S3");
                        return;
                    }
                    console.log("Image Downloaded", s3Path)
                    resolve(true);
                });
            });
        } catch (error) {
            console.log(false);
            reject(error);
        }
    });
};



export function getPostData(input: ApiAddUpdateData.queryParameter): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            const wdStatusMap: Map<string, number> = new Map();

            let response, filePath, uri
            let arrResponse: Array<any> = [];
            let insertObj: Array<AddUpdateData.AddDataInNewsSchema> = [];
            const tagsMap: Map<number, string> = new Map();

            let getTags = await tags.getAllTags()
            for (const t of getTags) {

                tagsMap.set(t.sqlId, t.name)
            }

            // for (const [sqlId, tag] of tagsMap) {
            //     console.log("tags", tag, sqlId)
            // }

            const catMap: Map<string, string> = new Map();
            let cat = await getDefaultCategory();
            for (const c of cat) {
                catMap.set(c.name, c._id)
            }

            // for (const [_id, name] of tagsMap) {
            //     console.log("category", _id, name)
            // }

            // let authorId=await getDefaultAuthor();
            //   console.log("get autor",authorId);

            //console.log("categoryId", JSON.stringify(catMap.get("BLOCKCHAIN")) )

            let seo: AddUpdateData.SEO = {
                title: "",
                metaDescription: "",
                canonical: "",
                metaTag: ""
            }
            let doc: AddUpdateData.DOC = {
                visibility: DocStatus.PUBLIC,
                publish: Date.now(),
            }
            let type = DATATYPE.NEWS

            let pageNumber = 1;
            let totalCount = 0;
            
            resolve({ data: [], message: "Will be done soon.........." });

            while (true)  //while condition for 1-5 pages is (pageNumber<6) and for only 6th page it (pageNumber==6) 
            {

                let postData = await getData(input.per_page, pageNumber++);
                console.log("postData ::", postData.length)
                if (!postData || postData.length == 0) {
                    console.log("ALL DONE");
                    break;
                }


            let destinationPath = path.join(__dirname, '../../assets/news');  //creating  destination path for download
              

                for (let post of postData) {

                   
                    ///image
                    let check = post._embedded ? post._embedded['wp:featuredmedia'] ? post._embedded['wp:featuredmedia'][0].source_url ? post._embedded['wp:featuredmedia'][0].source_url : undefined : undefined : undefined
                    if (check == undefined) {
                        filePath = ""
                    }
                    else {
                        uri = check.substring(61);
                        // filePath = escape("assets/news/" + uri)
                        console.log("uri===",uri);
                        
                        filePath = escape("assets/news/" + uri)
                        // try {
                        //     let success: boolean = await downloadImage(encodeURI(check), path.join(destinationPath, encodeURI(uri)));

                        //     if (!success) {
                        //         filePath = "";
                        //     }

                        // } catch (error) {
                        //     reject(error)
                        // }
                    }

                    let arrTag: Array<string> = [];
                    for (let t of post["tags"]) {
                        let tg = tagsMap.get(t);
                        arrTag.push(String(tg))

                    }

                    let subCat = "";
                    let cat = "";
                    for (const c of post.categories) {
                        switch (c) {
                            case 7:
                            case 5:
                            case 9:
                                cat = removeQuotes(JSON.stringify(catMap.get("BLOCKCHAIN")));

                                break;
                            case 8:
                            case 4:
                            case 10:
                                cat = removeQuotes(JSON.stringify(catMap.get("INDUSTRY")));

                                break;
                            case 6:
                                cat = removeQuotes(JSON.stringify(catMap.get("ANALYSIS")));

                                break;
                            default:
                                cat = removeQuotes(JSON.stringify(catMap.get("WATCH & LEARN")));

                                type = DATATYPE.PRESS_RELEASE
                                break;
                        }

                        if (c == 8 || c == 4) {
                            subCat = SUB_CATEGORY_ID.CRYPTOCURRENCY;
                        }
                        else if (c == 7) {
                            subCat = SUB_CATEGORY_ID.TECHNOLOGY
                        }
                        else if (c == 6) {
                            subCat = SUB_CATEGORY_ID.CRYPTO_EXCHANGE_COMPARISON
                        }
                        else if (c == 9) {
                            subCat = SUB_CATEGORY_ID.NEWS
                        }
                        else if (c == 10) {
                            subCat = SUB_CATEGORY_ID.FINANCE
                        }
                        else if (c == 5) {
                            subCat = SUB_CATEGORY_ID.USE_CASE
                        }

                        else {
                            subCat = SUB_CATEGORY_ID.PRESS_RELEASE
                            type = DATATYPE.PRESS_RELEASE
                        }
                    }

                    let newObj: AddUpdateData.AddDataInNewsSchema = {
                        mySqlId: post.id,
                        title: post.title.rendered,
                        status: AddUpdateStatus.PUBLIC,
                        description: post.content.rendered,
                        shortDescription: post.excerpt.rendered,
                        seo: seo,
                        doc: doc,
                        category: cat as string,
                        subCategory: subCat,
                        label: [],
                        tags: arrTag,
                        section: [],
                        file: filePath as string,
                        thumbnail: "",
                        video: "",
                        createdDate: Date.parse(post.date),
                        updatedDate: Date.parse(post.modified),
                        noOfView: 10,
                        timeSpend: 0,
                        addedBy: "Fintech Crypto News",
                        type: type,
                        url: post.slug,
                        author: "5e0bfd9a9956775e26bc551a"
                    }

                    //check whether the url is unique or not
                    //  let checkUniqueUrl=await addDataModel.checkUniqueUrl(post.slug);
                    // if(checkUniqueUrl==false)
                    // {
                    insertObj.push(newObj);

                   // console.log("insertObj===::::::test::::", insertObj);
                    // }

                }
                console.log(`PAGE NO:${pageNumber - 1} Total:${insertObj.length}`);
            }
            console.log("length of all data", insertObj.length);
            response = await addDataModel.addInNews(insertObj);
           // resolve({ data: { response }, message: "data uploaded" });
        }
        catch (error) {
            console.log("error found", error)
            reject(error)
        }
    })
}

const removeQuotes = (str: string) => str.replace(/"/g, "");
/*
export function getPostData(input:ApiAddUpdateData.queryParameter):Promise<NodeJS.ApiResponseType>
{
    return new Promise(async(resolve,reject)=>{
        try 
        {
           
              let response,filePath,uri
              let arrResponse:Array<any>=[];
              let insertObj:Array<AddUpdateData.AddDataInNewsSchema>=[];
              let pageNumber=1;
              let totalCount=0;
              while(pageNumber<=5)
              {
                let getResponse= await getData(input.per_page,pageNumber++);

                if(!getResponse || getResponse.length==0)
                {
                    console.log("ALL DONE");
                    break;
                }

              //let getResponse= await  getData(input.per_page,input.page)
              let destinationPath = path.join(__dirname, '../../assets/news');  //creating  destination path for download
              console.log("destima::",destinationPath)
              let url= getResponse[0]._embedded['wp:featuredmedia'][0].source_url;
             
            //   let result=await getDefaultAuthor();
            //   console.log("get autor",result);
       
           
              //  Starting loop for inserting datainside database
              for(let i=0;i<getResponse.length;i++)
              {
                console.log("first")
                let check=getResponse[i]._embedded['wp:featuredmedia'][0].source_url;
                console.log("check ::",check)
                
                if(check==undefined)
                {
                    filePath=""
                }
                else
                {
                     uri=check.substring(61);
                    filePath="assets/news/"+uri
                }
               
              
                // console.log("check path ::",check)
                // console.log("file path :::",filePath)
                // console.log("destination path::",destinationPath)
                console.log("i***************************************************************",i);
                console.log("check url::",check)
               
                
              try
              {
                    let success:boolean=  await downloadImage(check,  path.join(destinationPath,uri) );

                    if(!success)
                    {
                        console.log("***********************************",success)
                        console.log("old FilePath",filePath)
                        filePath="";
                        console.log("new FilePath ==",filePath)
                    }                  
                  
              } catch (error) {
                
                reject(error)
              }
             
                 
                for(let j=0;j<getResponse[i].categories.length;j++)
                  {
                      let cat
                      let type= DATATYPE.NEWS
                      switch(getResponse[i].categories[j])
                      {
                          case 7:
                            case 5:
                                case 9:
                                    cat="5de0137687d28c19b9836d17";
                                    break;
                          case 8:
                              case 4:
                                  case 10:
                                    cat="5de0139387d28c19b9836d18";
                                    break;
                          case 6:
                            cat="5de802ec8841061450e7bc44";
                            break;
                          default:
                            cat="5dee938a11ba5c59e0488f2d";
                            type=DATATYPE.PRESS_RELEASE
                            break;            
                      }

                      let subCat
                        if(getResponse[i].categories[j]==8||getResponse[i].categories[j]==4)
                        {
                            subCat=SUB_CATEGORY_ID.CRYPTOCURRENCY;
                        }
                        else if(getResponse[i].categories[j]==7)
                        {
                            subCat=SUB_CATEGORY_ID.TECHNOLOGY
                        }
                        else if(getResponse[i].categories[j]==6)
                        {
                            subCat=SUB_CATEGORY_ID.CRYPTO_EXCHANGE_COMPARISON
                        }
                        else if(getResponse[i].categories[j]==9)
                        {
                            subCat=SUB_CATEGORY_ID.NEWS
                        }
                        else if(getResponse[i].categories[j]==10)
                        {
                            subCat=SUB_CATEGORY_ID.FINANCE
                        }
                        else if(getResponse[i].categories[j]==5)
                        {
                            subCat=SUB_CATEGORY_ID.USE_CASE
                        }

                        else 
                        {
                            subCat=SUB_CATEGORY_ID.PRESS_RELEASE
                            type=DATATYPE.PRESS_RELEASE
                        }

                     let getTags=await tags.getTagsBysqlId(getResponse[i].tags)
                     let tagsId:Array<string>=[];
                     for (const tags of getTags) {
                        tagsId.push(tags._id)
                     }
                      let seo:AddUpdateData.SEO={
                        title: "",
                        metaDescription:"",
                        canonical: "",
                        metaTag: ""
                      }
                      let doc:AddUpdateData.DOC={
                        visibility:DocStatus.PUBLIC,
                        publish:Date.now(),
                      }
                     let newObj:AddUpdateData.AddDataInNewsSchema={
                        mySqlId: getResponse[i].id,
                        title:getResponse[i].title.rendered,                                    
                        status: 1,                                              //getResponse[i].status,
                        description: getResponse[i].content.rendered,
                        shortDescription:getResponse[i].excerpt.rendered,
                        seo: seo,
                        doc: doc,
                        category: cat ,
                        subCategory: subCat,
                        label: [],
                        tags:tagsId,                                                    // getResponse[i].tags,
                        section:[],
                        file: filePath as string,
                        video: "",
                        createdDate: Date.parse(getResponse[i].date) ,
                        updatedDate: Date.parse(getResponse[i].modified),
                        noOfView: 10,
                        timeSpend:0,
                        addedBy: "Fintech Crypto News",
                        type:type,
                        url: getResponse[i].slug,
                        author:"5e0bfd9a9956775e26bc551a"
                     }
                     arrResponse.push(newObj);
                     response= await addDataModel.addInNews(newObj);
                   
                    //  
                  }
                  
              }}
  //response= await addDataModel.addInNews(insertObj);
  
//   console.log("dddddata",arrResponse)
//              console.log("dddddata",arrResponse.length)
             resolve({data:arrResponse ,message:"data fetched"}); 
    }
        catch (error) {
            console.log("error found",error)
            reject(error)
        }
    })
}
*/

interface WordPressData {
    id: number,
    status: string,
    type: string,
    viewCount: number,
    link: string,
    title: string,
    date: string
}

export function beautifulSoup(pageSize: number): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let wordPressDataList: Array<WordPressData> = [];
            const wdTypesMap: Map<string, number> = new Map();
            const wdStatusMap: Map<string, number> = new Map();


            resolve({ data: [], message: "Will be done soon.........." });
            let pageNumber = 1;
            let totalCount = 0;
            while (true) {
                let data = await getData(pageSize, pageNumber++);

                if (!data || data.length == 0) {
                    console.log("ALL DONE");
                    break;
                }

                for (let d of data) {
                    const wd: WordPressData =
                    {
                        type: d.type,
                        link: d.link,
                        id: d.id,
                        status: d.status,
                        title: (d.title).rendered,
                        date: d.date,
                        viewCount: (d["_links"] && d["_links"]["version-history"] && d["_links"]["version-history"][0]) ? d["_links"]["version-history"][0]["count"] : -5
                    }

                    wdTypesMap.set(wd.type, (wdTypesMap.get(wd.type) || 0) + 1);
                    wdStatusMap.set(wd.status, (wdStatusMap.get(wd.status) || 0) + 1);

                    wordPressDataList.push(wd);
                    console.log(wd);
                }
                console.log(`PAGE NO:${pageNumber - 1} Total:${wordPressDataList.length}`);
            }

            console.log(wdStatusMap);
            for (let [status, count] of wdStatusMap) {
                console.log({ status, count });
            }
            console.log(wdTypesMap);
            for (let [type, count] of wdTypesMap) {
                console.log({ type, count });
            }


            wordPressDataList.sort((a, b) => {
                if (a > b) {
                    return -1;
                }
                if (b > a) {
                    return 1;
                }
                return 0;
            })
            for (let wd of wordPressDataList)
                console.log(wd.title);


        }
        catch (error) {
            console.log(error);
            reject(error)
        }
    })
}

// mySqlId:number,
//     title: string,
//     status: AddUpdateStatus,
//     description: string,
//     seo: SEO,//
//     doc: DOC,//----
//     category: string,
//     subCategory: string,
//     label: Array<string>,
//     tags: Array<string>,
//     section: Array<string>,//
//     file: string,
//     video: string,
//     createdDate: number,
//     updatedDate: number,
//     noOfView: number,
//     timeSpend: number,//
//     addedBy: string    //


interface WordPressData1 {
    id: number,
    count: string,
    name: string,

}

export function getCategory(pageSize: number): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let wordPressDataList: Array<WordPressData1> = [];
            const wdNameMap: Map<string, number> = new Map();
            const wdCountMap: Map<string, number> = new Map();



            let pageNumber = 1;
            let totalCount = 0;
            while (true) {
                let data = await getCategoryData(pageSize, pageNumber++);

                if (!data || data.length == 0) {
                    console.log("ALL DONE");
                    break;
                }

                for (let d of data) {
                    const wd: WordPressData1 =
                    {
                        id: d.id,
                        count: d.count,
                        name: d.name,

                    }

                    wordPressDataList.push(wd);
                    console.log(wd);
                }
                console.log(`PAGE NO:${pageNumber - 1} Total:${wordPressDataList.length}`);
            }

            resolve({ data: wordPressDataList, message: "category getch.........." });
        }
        catch (error) {
            console.log(error);
            reject(error)
        }
    })
}


interface WordPressTags {
    name: string,
    sqlId: string,
    count: number,
    addedBy: string,
    timeStamp: number,

}

export function getAndUpdateTags(pageSize: number): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let wordPressDataList: Array<WordPressTags> = [];
            let pageNumber = 1;
            let totalCount = 0;
            while (true) {
                let data = await getTags(pageSize, pageNumber++);

                if (!data || data.length == 0) {
                    console.log("ALL DONE");
                    break;
                }

                for (let d of data) {
                    const wd: WordPressTags =
                    {
                        name: d.name,
                        sqlId: d.id,
                        count: d.count,
                        addedBy: "admin",
                        timeStamp: Date.now()
                    }

                    wordPressDataList.push(wd);
                    console.log(wd);
                }
                console.log(`PAGE NO:${pageNumber - 1} Total:${wordPressDataList.length}`);
            }

            await tags.TagsModel.insertMany(wordPressDataList);

            resolve({ data: wordPressDataList, message: "tags updated.........." });

        }
        catch (error) {
            console.log(error);
            reject(error)
        }
    })
}
