import { serverConfig } from '../init/config';
import { ERROR_CODES, COMMON_TOKEN_MESSAGE } from '../utils/constants';
import * as bcryptUtils from '../utils/bcryptUtils'//import * as userModel from '../models/userDBModel';
import * as userModel from '../models/ClientUser';
import * as randomStringUtils from "../utils/randomStringUtils";
import { LOGIN_PAYLOAD } from '../utils/jwtTypes';
import * as BlackListUtils from '../utils/BlackListUtils';

const ObjectId = require('mongoose').Types.ObjectId;

import * as jwt from '../routes/api/Auth'
// import { user } from 'routes/api/user';
// // import { currentPasswordData } from './DB/MySql/userDBModel';

export interface PayloadData {
    userName: string,
    emailId: string,
    userId: string,
    source: number,
    role: string,
    permission: string

}

//change current password of user
export function changeUserPassword(payload: LOGIN_PAYLOAD, password: ChangePassword.PasswordRequest): Promise<NodeJS.ApiResponseType> {

    return new Promise(async (resolve, reject) => {
        try {
            var currentPassword = await userModel.getUserPasswordDB(payload.emailId);

            if (!currentPassword) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "This emailId does't exist");
                return;
            }

            const checkPasswordExist = await bcryptUtils.checkHash(password.currentPassword, currentPassword.passwordHash);

            if (!checkPasswordExist) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "incorrect password!");
                return;
            }

            const checkIFPasswordSame = await bcryptUtils.checkHash(password.newPassword, currentPassword.passwordHash);

            if (checkIFPasswordSame) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "current password and New password are Same !");
                return;
            }

            //create password hash
            const passwordHash: string = await bcryptUtils.getHash(password.newPassword);

            let updatePasswordObj =
            {
                newPassword: passwordHash,
                userName: payload.userName,
                emailId: payload.emailId
            }

            const updatepassResult = await userModel.changePasswordDB(updatePasswordObj);


            if (updatepassResult) {

                const newPayload: PayloadData = {
                    userId: payload.userId,
                    userName: payload.userName,
                    emailId: payload.emailId,
                    source: payload.source,
                    role: payload.role,
                    permission: payload.permission
                }


                const newToken = await jwt.generatJWTToken(newPayload, jwt.TOKEN_TYPE.ACCESS);
                let currentTimeStamp = Date.now();
                BlackListUtils.enterChangePassInBlackListMap(payload.userId, currentTimeStamp);

                console.log("New payload data", newPayload);
                console.log("newToken", newToken);

               
                let  refresh_token = await randomStringUtils.genrateRefreshToken();
                console.log("refresh token from inside if condition",refresh_token)
               let refreshTokenExpireTime = Date.now();
               let result1 = await userModel.updateRefreshTokenDB(refresh_token, payload.userId, refreshTokenExpireTime);
               if (!result1) {
                 global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_TOKEN_MESSAGE.CANNOT_UPDATE);
                 return;
               }
               let resetData:VerifyAccount.VerifyTokenResponse=
               {
                   access_token:newToken.token as string,
                   refresh_token:refresh_token
               }
                resolve({ data: resetData, message: "Password Changed successfully ..." });
    
            } else {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, "update password error");
            }
        }

        catch (e) {
            reject(e);
        }
    });
}



//change current password of Admin
export function changeAdminPassword(token: string, input: ChangePassword.AdminChangePassword): Promise<NodeJS.ApiResponseType> {

    return new Promise(async (resolve, reject) => {
        try {
            //verify the payload or token         
            if (!ObjectId.isValid(input.id)) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "invalid Id");
                return;
            }


            var DetailById = await userModel.getUserByIdDB(input.id);
            if (!DetailById) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "Details not found");
                return;
            }

            const checkIFPasswordSame = await bcryptUtils.checkHash(input.newPassword, DetailById.passwordHashh);

            if (checkIFPasswordSame) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "current password and New password are Same !");
                return;
            }

            //create password hash
            const passwordHash: string = await bcryptUtils.getHash(input.newPassword);
            let updatePasswordObj =
            {
                newPassword: passwordHash,
                userName: DetailById.userName,
                emailId: DetailById.emailId
            }

            const updatepassResult = await userModel.changePasswordDB(updatePasswordObj);


            if (updatepassResult) {


                resolve({ data: updatepassResult, message: "Password Changed successfully ..." });

            } else {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, "update password error");
            }
        }

        catch (e) {
            reject(e);
        }
    });
}

