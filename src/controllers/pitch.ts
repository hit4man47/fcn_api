import { ApiPitch } from "../types/pitch";
import * as pitchModel from '../models/pitch';
import { CONSTANTS, USER_ROLES,PitchStatus } from "../utils/constants";
import { jwtTypes } from "../routes/api/Auth";

export function createPitch(input: ApiPitch.CreatePitchRequest): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { type,title,description,releaseData,metaDescription,bodyAndimage , summary,url ,subCategory,name,emailId,contactNumber ,companyName,message} = input;

            let pitchSchema: pitchModel.AddPitchInput  =
            {
                type,title,description,releaseData,metaDescription,bodyAndimage , summary,url ,subCategory,name,emailId,contactNumber ,companyName,message
            }

            
            let data: pitchModel.Pitch.PitchExposedSchema | null = null;

                data = await pitchModel.addPitch(pitchSchema);
                if (!data) {
                    global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Failed to add Pitch in DB");
                    return;
                }
     

            resolve({ message:`New pitch work Added`, data });
        }
        catch (e) {
            reject(e);
        }
    });
}


export function getAllPitch(input: ApiPitch.GetAllPitchRequest): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { pageNumber, pageSize } = input;
            let { PitchList, hasMore } = await pitchModel.getAllPitch(input.pageNumber, input.pageSize, input.whereFilters, input.sortFilters);

            let data: ApiPitch.GetAllPitchResponse =
            {
                pageNumber,
                pageSize,
                hasMore,
                PitchList
            }

            resolve({ message: "Pitch work Fetched", data });
        }
        catch (e) {
            reject(e);
        }
    });
}



export function DeletePitchWork(input: ApiPitch.UpdatePitchRequest,user:string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let data:pitchModel.Pitch.PitchExposedSchema | null = null;
            data = await pitchModel.updatePitchWork(input);
            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Failed to update pitch work in DB");
                return;
            }

        
            resolve({ message: "Pitch work Updated", data });
        }
        catch (e) {
            reject(e);
        }
    });
}