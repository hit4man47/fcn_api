import { serverConfig } from '../init/config';
import { CONSTANTS, EMAIL_STATUSES, ERROR_CODES, PHONE_STATUSES } from '../utils/constants';
import * as jwt from '../routes/api/Auth';
import * as userModel from '../models/ClientUser';
import * as emailer from '../views/emailers';
import {VerifyAccountTokenType,ResetPasswordStatus} from './verifyAccount'
import {insertIntoPasswordChange} from '../models/ClientUser'


interface JWT_USER_FORGETPASSWORD_PAYLOAD {
    userId:string,
    userName: string,
    ipAddress: string,
    emailId: string,
    phoneNumber: string,
    role:string
}


export function forgetPassword(input: ForgetPassword.ForgetPasswordRequest, ipAddress: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await userModel.getUserDatailsByEmailOrUserNameDB(input.userNameOrEmailId);

            if (!result) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "user does not exists");
            }
                 
                
                if (result.emailIdStatus == EMAIL_STATUSES.NOT_VERIFIED)
                {
                    global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, " email id is not verifeid");
                     return
                  } 
                  let userForgetPasswordObject:any={
                    ipAdrres:ipAddress,
                    userId:result._id,
                    status:ResetPasswordStatus.PENDING,
                    type:VerifyAccountTokenType.RESET_PASSWORD
                  }
                 let response = await insertIntoPasswordChange(userForgetPasswordObject)
                 if(response== undefined||null)
                 {
                    global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, " can not add into database");
                     return
                 }
             
                    let payload:VerifyAccount.TokenInterface =
                    {
                        emailId:input.userNameOrEmailId,
                        resetPwdId:response._id,
                        type:VerifyAccountTokenType.RESET_PASSWORD,
                        isPasswordSet:false,
                        title:"RESET PASSWORD",
                        infoMessage:"GENERATE NEW PASSWORD"
                        
                    }
                    const {token,hasError,errorMessage} = await jwt.generatJWTToken(payload,jwt.TOKEN_TYPE.VERIFY_ACCOUNT);
                    if(hasError)
                    {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, errorMessage as string);
                        return
                    }               
                    const verificationUrl = `${serverConfig.PRODUCTION_URLS.USER_FORGETPASSWORD_VERIFICATION_BASE_URL}?token=${token}`;
    
                    console.log("VerificationUrl: ", verificationUrl);
                    console.log("EmailId: ",result.emailId);
  
                    let  forgetPasswordEmailObj: ClientUser.ForgetPasswordEmail = {
                        forgetPasswordLink: verificationUrl,
                        clientName: result.name,
                        contactUsLink:serverConfig.URLS.CONTACT_US_URL
                    }


                    let subject=emailer.SUBJECTS.FORGET_PASSWORD_EMAIL;
                    let sendmail = await emailer.sendHtml(result.emailId, subject ,emailer.forgetPassword.getHtml(subject, forgetPasswordEmailObj));


                    if (sendmail) {
                        resolve({ message: "Forget password email has been sent to " + result.emailId, data: {} });
                    }
                
                
            }
         catch (e) {
            reject(e);
        }
    })
}

//verify the user after sending the mail
// export function verifyUser(input: ForgetPassword.VerifyUserRequest,payload:any): Promise<NodeJS.ApiResponseType> {
//     return new Promise(async (resolve, reject) => {
//         try {
//             //verify the payload to check if the user is same       
//             let user = await userModel.getUserByIdDB(payload.userId);
          
//             if (!user) {
//                 global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "user does not exists");
//                 return;
//             }
//             resolve({ message: "user verified", data:user.emailId });
//         }

//         catch (e) {
//             reject(e);
//         }
//     })
// }

// //after verifying the user change the password
// export function resetPassword(input: ForgetPassword.ResetPasswordRequest,payload:any): Promise<NodeJS.ApiResponseType> {
//     return new Promise(async (resolve, reject) => {
//         try {
//             let userId1 = payload.userId;
            
//             let user = await userModel.getUserByIdDB(payload.userId);
          
//             if (!user) {
//                 global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "user does not exists");
//                 return;
//             }
//             let passwordUpdated = await userModel.updatePasswordDB(input.password, userId1);
//             if (!passwordUpdated) {
//                 global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "password cannot be changed");
//                 return;
//             }

//             else {
//                 resolve({ message: "password changed successfully", data: {} });
//             }
//         }

//         catch (e) {
//             reject(e);
//         }
//     })
// }

