
//import * as userModel from "../models/userDBModel";
import * as clientModel from "../models/ClientUser";
import { ERROR_CODES, LOGIN_ERROR, NOT_VERIFIED, COMMON_DATA_BASE_ERROR } from "../utils/constants";
import * as jwt from '../routes/api/Auth';
import * as emailer from '../views/emailers';
import { serverConfig } from '../init/config';
import {VerifyAccountTokenType} from './verifyAccount'
import {  REGISTRATION_ERROR } from "../utils/constants";
import { loginHistory } from "./login";

const ObjectId = require('mongoose').Types.ObjectId;



export function updateUserDetails(input: ClientUser.UpdateUserDetails, userId: string, ipAddress: string): Promise<NodeJS.ApiResponseType> {

    return new Promise(async (resolve, reject) => {
        try 
        {             
            let checkUserById = await clientModel.getUserByIdDB(userId)
            if (!checkUserById) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "NO SUCH USER EXISTS");
                return;
            }
            let updateUserData: ClientUser.updateUserData = {
                name: input.name,
                emailId: checkUserById.emailId,
                phoneNumber: input.phoneNumber,
                updateTimeStamp: Date.now(),
                emailIdStatus: checkUserById.emailIdStatus,
                phoneNumberStatus: checkUserById.phoneNumberStatus,
            };
            if (checkUserById.emailId !== input.emailId) {
                let checkUserByEmail = await clientModel.getClientUserBYEmailId(input.emailId);
                if (checkUserByEmail) {
                    global.rejector(reject, ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE, "This EmailId already exist!");
                    return;
                }
                let insertData:UPDATE.USER_CHANGE_LOG={
                    oldEmailId:checkUserById.emailId,
                    newEmailId: input.emailId,
                    ipAdrres: ipAddress,
                }

                let insertDataIntoUserChangeLog= await clientModel.insertIntoUserChangeLog(insertData);
                if(!insertDataIntoUserChangeLog)
                {
                    global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "CAN NOT BE INSERTED INTO DATABASE!");
                    return;
                }
                let payload: VerifyAccount.TokenInterface =
                {
                    emailId: input.emailId,
                    resetPwdId: userId,
                    type: VerifyAccountTokenType.RESET_EMAIL,
                    isPasswordSet: true,
                    title: "PLEASE VERIFY YOUR NEW EMAIL ADDRESS",
                    infoMessage: "PLEASE ENTER YOUR PASSWORD TO VERIFY YOUR EMAIL ADDRESS"
                }
                const token = await jwt.generatJWTToken(payload,jwt.TOKEN_TYPE.VERIFY_ACCOUNT);

                const verificationUrl = serverConfig.PRODUCTION_URLS.EMAIL_VERIFICATION_BASE_URL + token.token;
               
                // console.log("VerificationUrl: ", verificationUrl);
                // console.log("EmailId: ", input.emailId);

                let emailVerificationLinkObj: ClientUser.EmailVerificationEmail =
                {
                    emailVerficationUrl: verificationUrl,
                    clientName: input.name,
                }
                // TemplateEmailVerificationLink
                let subject = emailer.SUBJECTS.REGISTER_EMAIL_VERIFICATION;
                let sendmail = await emailer.sendHtml(input.emailId, subject, emailer.registerEmailVerification.getHtml(subject, emailVerificationLinkObj));

                if (!sendmail || sendmail == undefined) {
                    global.rejector(reject, ERROR_CODES.ERROR_CANNOT_FULLFILL_REQUEST, REGISTRATION_ERROR.CANNOT_SEND_EMAIL);
                    return;
                }
                resolve({ message: "Verification email has been sent to " + input.emailId, data: {} });
                return
               // updateUserData.emailIdStatus = EMAIL_STATUSES.NOT_VERIFIED;

            }
          
            let client = await clientModel.updateUserData(userId, updateUserData);
            if (client) {

                let loginHistoryControllerData = await loginHistory(client._id, ipAddress);
                let payload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD = {
                    userId: client._id,
                    userName: client.userName,
                    source: client.source,
                    emailId: client.emailId,
                    role: client.role,
                    ipaddress: ipAddress,
                    name: client.name,
                    permission: client.permission,
                    phoneNumber: client.phoneNumber,
                    loginHistoryId: loginHistoryControllerData._id
                };
                //creates the  acces token
                const access_token = await jwt.generatJWTToken(payload, jwt.TOKEN_TYPE.ACCESS);
                 console.log("data os user :",client)
                let outputData: UPDATE.UPDATE_RESPONSE = {
                   // client: client,
                    access_token: access_token.token as string
                }
                ///////////////////////////////////////////////////////////////////////////////////

                resolve({ message: "SUCCESSFULLY UPDATED", data: outputData });
            }
            resolve({message:"NO SUCH USER EXISTS",data:null});
        }
        catch (error) {
            reject(error);
        }
    });
}


