import * as ContactModel from '../models/Contact'
import { ERROR_CODES, SUBSCRIPTION_STATUS, CONTACT_CODE_MESSAGES, COMMON_DATA_BASE_ERROR } from '../utils/constants';
import { serverConfig } from '../init/config';
import * as emailer from '../views/emailers';


export function contactUs(input: FCN.CreateContactRequest, ipAddress: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let contactUsObj = {
                name: input.name,
                emailId: input.emailId,
                subject: input.subject,
                message: input.message,
                reason: input.reason,
                ipAddress,
                addedOn: Date.now()
            }

            let contactUsMailSubject = emailer.SUBJECTS.THANKS_FOR_CONTACTING_EMAIL;

            let adminContactUsMail = await emailer.sendHtml(serverConfig.ADMIN_EMAIL.CONTACT_US, contactUsMailSubject, emailer.thanksForContactingEmail.getHtml(contactUsMailSubject, contactUsObj));
            if (!adminContactUsMail) {
                resolve({ data: adminContactUsMail, message: CONTACT_CODE_MESSAGES.MAIL_SENDING_ERROR })
                return;
            }

            let contactUsMail = await emailer.sendHtml(input.emailId, contactUsMailSubject, emailer.thanksForContactingEmail.getHtml(contactUsMailSubject, contactUsObj));
            if (!contactUsMail) {
                resolve({ data: contactUsMail, message: CONTACT_CODE_MESSAGES.MAIL_SENDING_ERROR })
                return;
            }

            let contactUsData = await ContactModel.createContactUsModel(contactUsObj);
            if (!contactUsData) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_DATA_BASE_ERROR.CAN_NOT_BE_ADDED_INTO_DATABASE);
                return;
            }

            else {
                resolve({ message: CONTACT_CODE_MESSAGES.THANKYOU_FOR_CONTACTING_US, data: {} });
            }


        } catch (error) {
            reject(error);
        }
    })
}

export function subscribeWithUs(input: FCN.CreateSubscribeWithUsRequest, ipAddress: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let emailId = await ContactModel.checkIfEmailIdExistsModel(input.emailId);
            if (emailId) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, CONTACT_CODE_MESSAGES.ALREADY_SUBSCRIBED);
                return;
            }

            let subscribeWithUsObj = {
                emailId: input.emailId,
                ipAddress,
                addedOn: Date.now(),
                subscriptionStatus: SUBSCRIPTION_STATUS.VERIFIED
            }
            let subject = emailer.SUBJECTS.THANKS_FOR_SUBSCRIBING_WITH_US_EMAIL;

            let adminSubscribeWithUsMail = await emailer.sendHtml(serverConfig.ADMIN_EMAIL.SUBSCRIBE_WITH_US, subject, emailer.thanksForSubscribingWithUsEmail.getHtml(subject, subscribeWithUsObj));
            if (!adminSubscribeWithUsMail) {
                resolve({ data: adminSubscribeWithUsMail, message: CONTACT_CODE_MESSAGES.MAIL_SENDING_ERROR })
                return;
            }

            let subscribeWithUsMail = await emailer.sendHtml(input.emailId, subject, emailer.thanksForSubscribingWithUsEmail.getHtml(subject, subscribeWithUsObj));
            if (!subscribeWithUsMail) {
                resolve({ data: subscribeWithUsMail, message: CONTACT_CODE_MESSAGES.MAIL_SENDING_ERROR })
                return;
            }

            let subscribeWithUsData = await ContactModel.createSubscribeWithUsModel(subscribeWithUsObj);
            if (!subscribeWithUsData) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_DATA_BASE_ERROR.CAN_NOT_BE_ADDED_INTO_DATABASE);
                return;
            }

            else {
                resolve({ message: CONTACT_CODE_MESSAGES.THANKYOU_FOR_SUBSCRIBING_WITH_US, data: {} });
            }

        } catch (error) {
            reject(error);
        }
    })
}

export function getContactWithUsDetails(input: FCN.GetAllContactUsDetailsRequest): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let getContactWithUsDetailsData = await ContactModel.getContactWithUsDetailsModel(input);
            if (!getContactWithUsDetailsData) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_DATA_BASE_ERROR.INTERNAL_DATABASE_ERROR);
                return;
            }
            resolve({ message: CONTACT_CODE_MESSAGES.SUCCESS, data: getContactWithUsDetailsData });

        } catch (error) {
            reject(error);
        }
    })
}

export function getSubscribeWithUsDetails(input: FCN.GetAllSubscribeWithUsDetailsRequest): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let getSubscribeWithUsDetailsData = await ContactModel.getSubscribeWithUsDetailsModel(input);
            if (!getSubscribeWithUsDetailsData) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_DATA_BASE_ERROR.INTERNAL_DATABASE_ERROR);
                return;
            }
            resolve({ message: CONTACT_CODE_MESSAGES.SUCCESS, data: getSubscribeWithUsDetailsData });

        } catch (error) {
            reject(error);
        }
    })
}
