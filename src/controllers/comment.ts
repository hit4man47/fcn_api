import * as CommentModel from '../models/comment';
import { ERROR_CODES, NEWSROOM} from "../utils/constants";
import * as constants from "../utils/constants";
import { LOGIN_PAYLOAD } from "utils/jwtTypes";


export function subComment(input: COMMENT.QUERYDATA, commentId: any, payload?: LOGIN_PAYLOAD): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let QueryData: COMMENT.AddSubcomment = {
                name: input.name,
                emailId: input.emailId,
                newsId: commentId.newsId,
                commentType: constants.COMMENT_TYPES.SUBCOMMENT,
                comment: input.comment
            }

            let newsComment = await CommentModel.addSubcomment(QueryData)
            if (!newsComment) {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, NEWSROOM.NEWS_QUERY_ERROR);
                return;
            }
            await CommentModel.updateSubcomment(commentId.commentId, newsComment._id);


            resolve({ data: {}, message: constants.COMMENT.SUBCOMMENT_CREATED, status: 200 })
        }
        catch (error) {
            reject(error)
        }
    })
}

export function addComment(input: COMMENT.QUERYDATA, newsId: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let QueryData: COMMENT.AddNewsQuery = {
                name: input.name,
                emailId: input.emailId,
                newsId: newsId,
                comment: input.comment,
            }

            let newsComment= await CommentModel.addComment(QueryData)
            if (!newsComment) {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, NEWSROOM.NEWS_QUERY_ERROR);
                return;
            }

            resolve({ data:{}, message: "SUCCESS", status: 200 })
        }
        catch (error) {
            reject(error)
        }
    })
}
export function getComment(input: COMMENT.NEWSQUERYID): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let getCommentData = await CommentModel.getNewsQuery(input);
            if (!getCommentData) {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, NEWSROOM.DETAILS_FETCH_ERROR);
                return;
            }
            resolve({ data: getCommentData, message: "SUCCESS" })
        } catch (error) {
            reject(error)
        }
    })
}
export function getNewsComment(input: COMMENT.newsComment): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let getCommentData = await CommentModel.getNewsComment(input);
            if (!getCommentData) {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, NEWSROOM.DETAILS_FETCH_ERROR);
                return;
            }
            resolve({ data: getCommentData, message: "SUCCESS" })
        } catch (error) {
            reject(error)
        }
    })
}

export function DeleteComment(input: COMMENT.deleteComent): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let comments=await CommentModel.getCommentById(input.id);
            
            if (comments && comments.subComment !== null) {
                let deleteSubcomment = await CommentModel.deleteNewsCommentById(comments.subComment);
            }
            let getNewsQueryData = await CommentModel.deleteNewsComment(input.id);
            if (!getNewsQueryData) {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, NEWSROOM.ALREADY_DELETE);
                return;
            }
            resolve({ data: {}, message: constants.NEWSROOM.DELETE_SUCCESS })
        } catch (error) {
            reject(error)
        }
    })
}


export function updateCommentStatus(input: COMMENT.CommentUpdate): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let updateCommentStatus = await CommentModel.updateCommentStatus(input);
            if (!updateCommentStatus) {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, NEWSROOM.ALREADY_DELETE);
                return;
            }
            resolve({ data: updateCommentStatus, message: "comment " + input.status + " succesfully" })
        } catch (error) {
            reject(error)
        }
    })
}

