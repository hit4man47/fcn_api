import * as Constants from '../utils/constants';

export function login(loginObj: Admin.LoginRequest): Promise<NodeJS.ApiResponseType> {
    return new Promise((resolve, reject) => {
        if (loginObj.name == loginObj.password) {
            resolve({ message: "success", data: { name: "Vishwas", emailId: "vishwassingh910@gmail.com" } })
        }
        else {
            let errResponse: NodeJS.ApiErrorType =
            {
                status: Constants.ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER,
                message: "Invalid Credentials"
            }
            reject(errResponse);
        }
    });
}

export function saveImageUrl(imageUrl: any): Promise<any> {
    return new Promise((resolve, reject) => {
        try {
            let uploaded = Constants.IMAGE_URL_STATUS.NOT_UPLOADED;
            let url;
            let fileName;

            let urlPath = Constants.CONSTANTS.BASE_URL;

            if (imageUrl && imageUrl.length > 0) {
                url = urlPath + imageUrl[0].dbpath;
                uploaded = Constants.IMAGE_URL_STATUS.UPLOADED;
                fileName = imageUrl[0].name
            }

            resolve({ fileName, uploaded, url });

        } catch (error) {
            reject(error);
        }

    });
}

