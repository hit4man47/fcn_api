
import * as clientUser from '../controllers/clientUser';
import { ERROR_CODES, USER_ROLES, TYPES, STATUS, PERMISSION, COMMON_DATA_BASE_ERROR, REGISTRATION_ERROR } from '../utils/constants';
import * as bcrypt from '../utils/bcryptUtils';
import * as jwt from '../routes/api/Auth';
import * as permissions from '../utils/permissionManager'
import { CONSTANTS, EMAIL_STATUSES, PHONE_STATUSES, ACCOUNT_SOURCE } from '../utils/constants';
import { serverConfig } from '../init/config';
// import * as nodemailer from '../utils/nodeMailer'

import passport = require('passport');
import * as request from 'https';

import * as emailer from '../views/emailers';
import { loginHistory } from './login';

const path = require('path');
const fs = require('fs');



interface JWT_EMAIL_VERIFICATION_PAYLOAD {
    clientUserId: string
}


function autoVerifyEmail(url: string) {
    return new Promise(function (resolve, reject) {
        request.get(url, function () {
            resolve();
        })
    })
}



//verify the email and add him to the main user table
//user is now registered
export function verifyEmail(input: ClientUser.VerifyEmailRequest): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await clientUser.verifyEmailId(input);
            if (result == null || result == undefined) {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, COMMON_DATA_BASE_ERROR.CAN_NOT_BE_ADDED_INTO_DATABASE);
                return;
            }

            let subject = emailer.SUBJECTS.REGISTER_SUCCESS_EMAIL;
            let sendmail = await emailer.sendHtml(result.emailId, subject, emailer.registerSuccessEmail.getHtml(subject, { clientName: result.name }));

            // if(sendmail)
            // console.log("==>>>>0",result)


            resolve({ message: "Verifiy Sucessfully ", data: result.emailId });

        }
        catch (error) {
            reject(error)
        }
    });
}


export function verifyEmails(input: ClientUser.VerifyEmailRequest): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await clientUser.verifyEmails(input);
            resolve({ message: "Verifiy Sucessfully ", data: result });
        }
        catch (error) {
            reject(error)
        }
    });
}



// Created By Amir Khan dhoom machaale
// create user by google after login by google




export function createUserByGoogle(input: Register.CreateUserOAuthRequest, ipAddress: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let permission = await permissions.getDefaultPermissions(USER_ROLES.CLIENT)
            let emailIdExist = await clientUser.isEmailId(input.emailId);

            if (emailIdExist) {
                // validate user when login 2nd or more times
                let user = await clientUser.getClientUser(input.emailId);

                if (user) {

                    if (user.source === ACCOUNT_SOURCE.GOOGLE) {

                        let data: number = user.userId + CONSTANTS.BASE_URL_VERIFICATION_PK;

                        let loginHistoryControllerData = await loginHistory(user._id, ipAddress);
                        let payload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD =
                        {
                            userId: user.userId,
                            userName: user.userName,
                            source: ACCOUNT_SOURCE.GOOGLE,
                            emailId: user.emailId,
                            ipaddress: input.ipAddress,
                            role: USER_ROLES.CLIENT,
                            permission: permission,
                            name: user.name,
                            phoneNumber: user.phoneNumber,
                            loginHistoryId: loginHistoryControllerData._id


                        }
                        const token = await jwt.generatJWTToken(payload, jwt.TOKEN_TYPE.ACCESS);
                        resolve({ message: "Already Google User Exits", data: token });
                    }
                }

                global.rejector(reject, ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE, "EmailId already exist!");
                return;
            }

            let userName = input.emailId.replace(/^(.+)@(.+)$/g, '$1');
            let userNameExist = await clientUser.isUserName(userName);

            if (userNameExist) {

                userName = getUniqueUserName(input.name);
                let userNameExist = await clientUser.isUserName(userName);

                if (userNameExist) {
                    global.rejector(reject, ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE, REGISTRATION_ERROR.USERNAME_EXISTS_ERROR);
                    return;
                }
            }



            let InputClientUserData: ClientUser.ClientUserDataRequest = {
                name: input.name,
                countryCode: "+91",
                emailId: input.emailId,
                phoneNumber: "",
                designation: "",
                userName: userName,
                password: "",
                avatar: "",
                role: USER_ROLES.CLIENT
            }

            let rows = await clientUser.createClientUser(InputClientUserData, input.ipAddress, null);
            let userID = (rows as any).id;
            let data: number = userID + CONSTANTS.BASE_URL_VERIFICATION_PK;
            let loginHistoryControllerData = await loginHistory(userID, ipAddress);
            let payload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD =
            {
                userId: userID,
                userName: userName,
                source: ACCOUNT_SOURCE.GOOGLE,
                emailId: InputClientUserData.emailId,
                ipaddress: input.ipAddress,
                role: USER_ROLES.CLIENT,
                permission: permission,
                name: input.name,
                phoneNumber: "",
                loginHistoryId: loginHistoryControllerData._id
            }
            const token = await jwt.generatJWTToken(payload, jwt.TOKEN_TYPE.ACCESS);

            resolve({ message: "Create User By Google", data: token });
        }
        catch (error) {
            reject(error)
        }
    });
}



// Created By Amir Khan
// create user by google after login by google

export function createUserByFaceBook(input: Register.CreateUserOAuthRequest, ipAddress: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let emailIdExist = await clientUser.isEmailId(input.emailId);
            let permission = await permissions.getDefaultPermissions(USER_ROLES.CLIENT)

            if (emailIdExist) {
                // validate user when login 2nd or more times
                let user = await clientUser.getClientUser(input.emailId);

                if (user) {

                    if (user.source === ACCOUNT_SOURCE.FACEBOOK) {

                        let data: number = user.userId + CONSTANTS.BASE_URL_VERIFICATION_PK;
                        let loginHistoryControllerData = await loginHistory(user._id, ipAddress);
                        let payload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD =
                        {
                            userId: user.userId,
                            userName: user.userName,
                            source: ACCOUNT_SOURCE.GOOGLE,
                            emailId: user.emailId,
                            ipaddress: input.ipAddress,
                            role: USER_ROLES.CLIENT,
                            permission: permission,
                            name: user.name,
                            phoneNumber: user.phoneNumber,
                            loginHistoryId: loginHistoryControllerData._id
                        }
                        const token = await jwt.generatJWTToken(payload, jwt.TOKEN_TYPE.ACCESS);
                        resolve({ message: "Already FaceBook User Exits", data: token });
                    }
                }

                global.rejector(reject, ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE, REGISTRATION_ERROR.EMAIL_EXISTS_ERROR);
                return;
            }

            let userName = input.emailId.replace(/^(.+)@(.+)$/g, '$1');
            let userNameExist = await clientUser.isUserName(userName);

            if (userNameExist) {

                userName = getUniqueUserName(input.name);
                let userNameExist = await clientUser.isUserName(userName);

                if (userNameExist) {
                    global.rejector(reject, ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE, "UserName already exist!");
                    return;
                }
            }


            let InputClientUserData: ClientUser.ClientUserDataRequest = {
                name: input.name,
                countryCode: "+91",
                emailId: input.emailId,
                phoneNumber: "",
                designation: "",
                userName: userName,
                password: "",
                avatar: "",
                role: USER_ROLES.CLIENT
            }
            let rows = await clientUser.createClientUser(InputClientUserData, input.ipAddress, null);
            let userID = (rows as any).insertId;
            let data: string = userID + CONSTANTS.BASE_URL_VERIFICATION_PK;
            let loginHistoryControllerData = await loginHistory(userID, ipAddress);
            let payload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD =
            {
                userId: data,
                userName: userName,
                source: ACCOUNT_SOURCE.GOOGLE,
                emailId: InputClientUserData.emailId,
                ipaddress: input.ipAddress,
                role: USER_ROLES.CLIENT,
                permission: permission,
                name: input.name,
                phoneNumber: "",
                loginHistoryId: loginHistoryControllerData._id

            }
            const token = await jwt.generatJWTToken(payload, jwt.TOKEN_TYPE.ACCESS);

            resolve({ message: "Create User By Facebook", data: token });
        }
        catch (error) {
            reject(error)
        }
    });
}




function getUniqueUserName(name: string) {
    var a = name.split(' ');
    var name = a[0];
    if (name.length > 16) {
        name = name.slice(0, 15);
    }
    var d = Date.now();
    return name + d;
}

function alphanumeric_unique() {
    return Math.random().toString(36).split('').filter(function (value, index, self) {
        return self.indexOf(value) === index;
    }).join('').substr(2, 16);
}