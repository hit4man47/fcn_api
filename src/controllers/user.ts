
import * as userModel from '../models/ClientUser';

export function checkUserNameExist(input: User.CheckUserNameRequest): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let userNameExist = await userModel.checkUserNameExistDB(input.userName);

            if (userNameExist) {
                resolve({ message: "exist", data: { userNameExist } });
            }
            else {
                resolve({ message: "Not Exist", data: { userNameExist } });
            }
        }
        catch (e) {
            reject(e)
        }
    });
}

export function checkEmailExist(input: User.CheckEmailIdRequest): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let email = await userModel.checkEmailExistDB(input.emailId);

            if (email) {
                resolve({ message: "exist", data: { email } });
            }
            else {
                resolve({ message: "Not Exist", data: { email } });
            }
        }
        catch (e) {
            reject(e)
        }
    });
}

