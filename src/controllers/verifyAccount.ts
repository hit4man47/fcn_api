import { ERROR_CODES, EMAIL_STATUSES } from '../utils/constants';
import * as jwt from '../routes/api/Auth';
import * as userModel from '../models/ClientUser';
import * as  clientUser from '../models/ClientUser'
import { TOKEN_TYPE, generatJWTToken } from '../routes/api/Auth';
import * as bcrypt from '../utils/bcryptUtils';
import * as randomStringUtils from "../utils/randomStringUtils";
import { COMMON_TOKEN_MESSAGE } from "../utils/constants";
import { loginHistory } from './login';
import { user } from 'routes/api/user';
import {updateParties} from '../controllers/clientUser'



export enum VerifyAccountTokenType {
    EMAIL_ID_VERIFICATION = "EMAIL_ID_VERIFICATION",
    ADMIN_EMAIL_ID_VERIFICATION = "ADMIN_EMAIL_ID_VERIFICATION",
    RESET_PASSWORD = "RESET_PASSWORD",
    RESET_EMAIL = "RESET_EMAIL_ID"
}
export enum ResetPasswordStatus {
    PENDING = "PENDING",
    SUCCESS = "SUCCESS",
    ERROR = "ERROR"
}

export function checkToken(payload: VerifyAccount.TokenInterface, ipAddress: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let userExistInMainDB = await clientUser.checkUserExist(payload.emailId, userModel.UserTableType.MAIN);

            switch (payload.type) {
                case VerifyAccountTokenType.EMAIL_ID_VERIFICATION:
                case VerifyAccountTokenType.ADMIN_EMAIL_ID_VERIFICATION:
                    let data: VerifyAccount.CheckTokenResponse;
                    if (userExistInMainDB) {
                        data =
                            {
                                canProceed: false,
                                isPasswordSet: payload.isPasswordSet,
                                title: payload.title,
                                infoMessage: "Your account has already been Verified"
                            }

                        resolve({ message: "", data });
                    }
                    else {
                        data =
                            {
                                canProceed: true,
                                isPasswordSet: payload.isPasswordSet,
                                title: payload.title,
                                infoMessage: payload.infoMessage
                            }
                        resolve({ message: "", data });
                    }
                    //resolve({ message: "", data });
                    break;
                case VerifyAccountTokenType.RESET_PASSWORD:
                    let resetData: VerifyAccount.CheckTokenResponse;
                    let status = await clientUser.getResetPasswordRequestStatus(payload.resetPwdId as string);
                    console.log("status :", status)
                    console.log("payload :", payload)
                    if (!status || status != ResetPasswordStatus.PENDING) {
                        console.log("error in this 1")
                        resetData =
                            {
                                canProceed: false,
                                isPasswordSet: payload.isPasswordSet,
                                title: payload.title,
                                infoMessage: "No such password request found"
                            }
                        resolve({ message: "", data: resetData });
                    }
                    else {
                        console.log("111111111111111111111", payload.resetPwdId, payload.title, payload.infoMessage)
                        resetData =
                            {
                                canProceed: true,
                                isPasswordSet: payload.isPasswordSet,
                                title: payload.title,
                                infoMessage: payload.infoMessage
                            }
                        console.log("error in this 2", resetData);
                        resolve({ message: "", data: resetData });
                    }
                    console.log(resetData);

                    break;

                case VerifyAccountTokenType.RESET_EMAIL:
                    let resetEmailData: VerifyAccount.CheckTokenResponse;
                    if (userExistInMainDB) {
                        resetEmailData =
                            {
                                canProceed: false,
                                isPasswordSet: payload.isPasswordSet,
                                title: payload.title,
                                infoMessage: "Your account has already been Verified"
                            }

                        resolve({ message: "", data: resetEmailData });
                    }
                    else {
                        resetEmailData =
                            {
                                canProceed: true,
                                isPasswordSet: payload.isPasswordSet,
                                title: payload.title,
                                infoMessage: payload.infoMessage
                            }
                        resolve({ message: "", data: resetEmailData });
                    }

                    break;
            }
        } catch (error) {
            reject(error)
        }

    })
}

export function verifyToken(payload: VerifyAccount.TokenInterface, password: string, ipAddress: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            console.log("payload==>>")
            let userExistInMainDB = await clientUser.checkUserExist(payload.emailId, userModel.UserTableType.MAIN);
            console.log("++++++++", userExistInMainDB)
            switch (payload.type) {
                case VerifyAccountTokenType.EMAIL_ID_VERIFICATION:
                case VerifyAccountTokenType.ADMIN_EMAIL_ID_VERIFICATION:

                    let data: VerifyAccount.VerifyTokenResponse;
                    if (userExistInMainDB) {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "This user already exist");
                        return;
                    }
                    else {
                        let userDetails = await clientUser.getUserById(payload.tempUserId as string, userModel.UserTableType.TEMP);
                        console.log("userDetails :::", userDetails)
                        if (!userDetails) {
                            global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "NO Such user exist");
                            return;
                        }
                        console.log("if 1")

                        if (payload.type === VerifyAccountTokenType.EMAIL_ID_VERIFICATION) {
                            let passwordMatch = await bcrypt.checkHash(password, userDetails.passwordHash);
                            if (!passwordMatch) {
                                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "invalid password");
                                return;
                            }
                            console.log("if 2")
                        }
                        else {
                            userDetails.createTimeStamp = Date.now(),
                                userDetails.passwordHash = await bcrypt.getHash(password);
                            console.log("else in else 3")
                        }


                        userDetails.upadteTimeStamp = Date.now();
                        console.log("if 4")
                        let newUserData: ClientUser.InputClientUserData = {
                            userName: userDetails.userName,
                            name: userDetails.name,
                            countryCode: userDetails.countryCode,
                            phoneNumber: userDetails.phoneNumber,
                            passwordHash: userDetails.passwordHash,
                            emailId: userDetails.emailId,
                            emailIdStatus: EMAIL_STATUSES.VERIFIED,
                            createTimeStamp: userDetails.createTimeStamp,
                            phoneNumberStatus: userDetails.phoneNumberStatus,
                            source: userDetails.source,
                            role: userDetails.role,
                            createBy: userDetails.createBy,
                            status: userDetails.status,
                            refreshToken: userDetails.refreshToken,
                            refreshTokenExpiry: userDetails.refreshTokenExpiry,
                            allowGoogle: userDetails.allowGoogle,
                            allowFacebook: userDetails.allowFacebook,
                            permission: userDetails.permission,
                            updateTimeStamp: Date.now(),
                            ipAddrress: userDetails.ipAddress
                        }

                        let userObject = await clientUser.insertClientUserData(newUserData);
                        
                       let userIdInParties=await updateParties(userObject.emailId);
                    //    if(!userIdInParties)
                    //    {

                    //    }
                        console.log("if 5", userObject)
                        let loginHistoryControllerData = await loginHistory(userObject._id, ipAddress);
                        let newPayload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD =
                        {
                            userId: userObject._id,
                            userName: userObject.userName,
                            source: userObject.source,
                            emailId: userObject.emailId,
                            role: userObject.role,
                            ipaddress: ipAddress,
                            name: userObject.name,
                            permission: userObject.permission,
                            phoneNumber: userObject.phoneNumber,
                            loginHistoryId: loginHistoryControllerData._id
                        }

                        // move userDetails to main table and CREATE LOGIN JWT and send it in response 
                        let { hasError, errorMessage, token } = await generatJWTToken(newPayload, TOKEN_TYPE.ACCESS);
                        if (hasError) {
                            global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, errorMessage as string);
                            return;
                        }
                        let refresh_token = await randomStringUtils.genrateRefreshToken();
                        console.log("refresh token from inside if condition", refresh_token)
                        let refreshTokenExpireTime = Date.now();
                        let result1 = await clientUser.updateRefreshTokenDB(refresh_token, userObject._id, refreshTokenExpireTime);
                        if (!result1) {
                            global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_TOKEN_MESSAGE.CANNOT_UPDATE);
                            return;
                        }

                        data =
                            {
                                access_token: token as string,
                                refresh_token: refresh_token
                            }
                        resolve({ message: "Success", data });
                    }
                    break;

                case VerifyAccountTokenType.RESET_PASSWORD:
                    let resetData: VerifyAccount.VerifyTokenResponse;

                    if (!userExistInMainDB) {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "This user does not exist");
                        return;
                    }
                    let status = await clientUser.getResetPasswordRequestStatus(payload.resetPwdId as string);
                    if (!status || status != ResetPasswordStatus.PENDING) {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "No such password request found");
                        return;
                    }
                    console.log("payload in verify function:========", payload)
                    let userObject = await clientUser.getUserByEmailId(payload.emailId, userModel.UserTableType.MAIN);
                    console.log("1")
                    userObject.upadteTimeStamp = Date.now();
                    userObject.passwordHash = await bcrypt.getHash(password);
                    console.log("userObject :", userObject)
                    await clientUser.updateUserData(userObject._id, userObject);
                    await clientUser.updateResetPasswordRequestStatus(payload.resetPwdId as string, ResetPasswordStatus.SUCCESS, ipAddress);

                    let loginHistoryControllerData = await loginHistory(userObject._id, ipAddress);
                    let newPayload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD =
                    {
                        userId: userObject._id,
                        userName: userObject.userName,
                        source: userObject.source,
                        emailId: userObject.emailId,
                        role: userObject.role,
                        ipaddress: ipAddress,
                        name: userObject.name,
                        permission: userObject.permission,
                        phoneNumber: userObject.phoneNumber,
                        loginHistoryId: loginHistoryControllerData._id
                    }

                    let { hasError, errorMessage, token } = await generatJWTToken(newPayload, TOKEN_TYPE.ACCESS);
                    if (hasError) {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, errorMessage as string);
                        return;
                    }
                    let refresh_token = await randomStringUtils.genrateRefreshToken();
                    console.log("refresh token from inside if condition", refresh_token)
                    let refreshTokenExpireTime = Date.now();
                    let result1 = await clientUser.updateRefreshTokenDB(refresh_token, userObject._id, refreshTokenExpireTime);
                    if (!result1) {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_TOKEN_MESSAGE.CANNOT_UPDATE);
                        return;
                    }

                    resetData =
                        {
                            access_token: token as string,
                            refresh_token: refresh_token
                        }
                    console.log("data :", resetData)
                    resolve({ message: "Success", data: resetData });

                    break;
                case VerifyAccountTokenType.RESET_EMAIL:
                    let resetEmailData: VerifyAccount.VerifyTokenResponse;

                    if (userExistInMainDB) {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "A USER WITH THIS EMAIL ADDRESS ALREADY EXISTS");
                        return;
                    }
                    let userDetails = await clientUser.getUserById(payload.resetPwdId as string, userModel.UserTableType.MAIN);
                    console.log("userDetails :::", userDetails)
                    if (!userDetails) {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "NO SUCH USER EXISTS ");
                        return;
                    }
                    if (payload.type === VerifyAccountTokenType.RESET_EMAIL) {
                        let passwordMatch = await bcrypt.checkHash(password, userDetails.passwordHash);
                        if (!passwordMatch) {
                            global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "invalid password");
                            return;
                        }
                    }
                    let updateUserChangeLog= await clientUser.updateChangeEmailStatus(payload.emailId)
                    if(!updateUserChangeLog)
                    {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "CANNOT BE INSERTED INTO USERCHANGELOG");
                        return;
                    }
                    let checkUpdate = await clientUser.updateEmailId(userDetails._id, payload.emailId)
                    if (!checkUpdate) {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "CANNOT PERFORM USER REQUEST DUE TO INTERNAL SERVER ERROR ");
                        return;
                    }
                
                    let loginHistoryControllerDataUpdate = await loginHistory(userDetails._id, ipAddress);
                    let updatedPayload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD =
                    {
                        userId: userDetails._id,
                        userName: userDetails.userName,
                        source: userDetails.source,
                        emailId: payload.emailId,
                        role: userDetails.role,
                        ipaddress: ipAddress,
                        name: userDetails.name,
                        permission: userDetails.permission,
                        phoneNumber: userDetails.phoneNumber,
                        loginHistoryId: loginHistoryControllerDataUpdate._id
                    }

                    // move userDetails to main table and CREATE LOGIN JWT and send it in response 
                    let tokenResult = await generatJWTToken(updatedPayload, TOKEN_TYPE.ACCESS);
                    if (tokenResult.hasError) {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, errorMessage as string);
                        return;
                    }
                    let refresh_token_updated = await randomStringUtils.genrateRefreshToken();
                    console.log("refresh token from inside if condition", refresh_token_updated)
                    let refreshTokenExpireTimeNew = Date.now();
                    let result2 = await clientUser.updateRefreshTokenDB(refresh_token_updated, userDetails._id, refreshTokenExpireTimeNew);
                    if (!result2) {
                        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_TOKEN_MESSAGE.CANNOT_UPDATE);
                        return;
                    }
                    resetEmailData =
                    {
                        access_token: tokenResult.token as string,
                        refresh_token: refresh_token_updated
                    }
                console.log("data :", resetEmailData)
                resolve({ message: "Success", data: resetEmailData });
                    break;

            }

        } catch (error) {
            reject(error)
        }

    })
}

