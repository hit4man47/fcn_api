import * as express from "express";
import * as requestHandler from '../init/requestHandler';
import * as responseHandler from '../init/responseHandler';
import mongoose from "mongoose";
import * as check from '../libs/checkLib';
import * as constants from '../utils/constants'
import { AboutModel, CountriesModel } from '../models/About'
import TeamModel from '../models/Team'
import { resolve } from "path";
import * as fs from 'fs';
import { moduleConstants, permissonLevelsConstants } from "../utils/permissionManager";
import { deletefiles } from '../utils/fileUpload';
import {getImagesMimeTypes} from '../utils/fileUpload';
import * as mime from 'mime';
import {ImageResizeConstants} from "../utils/constants"
import { ImageDimen } from "types/NewTypes";
import {changePathFromS3} from '../utils/getImagePathBySize';






export const getCountries = async (req: express.Request, res: express.Response) => {
    try {

        const result = await CountriesModel.find().select('-_id').lean();

        res.setHeader('X-FRAME-OPTIONS', 'SAMEORIGIN');
        responseHandler.sendSuccessResponse(res, { message: "About fetched successfully", data: result, status: 200 });

    } catch (e) {
        responseHandler.sendErrorResponse(res, e);
    }
}
export const getAbout = async (req: express.Request, res: express.Response) => {
    try {
        const result = await AboutModel.findOne({ 'aboutId': 1 }).select('-_id -__v').lean();
       let teamResult = await TeamModel.find({ status: constants.TEAM_STATUS.ACTIVE }).select('-_id -__v').lean();

       console.log("teamResult===::::1", teamResult);
       console.log("result===:::2", result);
       // let updateResultVal = teamResult;
    
       let updateResultVal = await changePathFromS3(teamResult,ImageResizeConstants.ABOUT_TEAM.MEDIUM, "photoUrl");

       teamResult = updateResultVal;   
      
       
        const output = {
            content: result,
            team: teamResult
        }
        responseHandler.sendSuccessResponse(res, { message: "About fetched successfully", data: output, status: 200 });

    } catch (e) {
        console.log('Error on get aboutUs data', e)
        responseHandler.sendErrorResponse(res, e);
    }
}
export const updateAboutUs = async (req: express.Request, res: express.Response) => {
    try {
        let u: any = {};

        u['addedOn'] = Date.now();
        if (req.body.content) {
            u['content'] = req.body.content.trim()
        }
        if (req.body.why) {
            u['quote'] = req.body.why.trim()
        }
        if (req.body.author) {
            u['author'] = req.body.author.trim()
        }
        if (req.body.youTubeLink) {
            u['youTubeLink'] = req.body.youTubeLink.trim()
        }
        const updateData = await AboutModel.updateOne({ 'aboutId': 1 }, u, { upsert: true });
        responseHandler.sendSuccessResponse(res, { message: "About us updated successfully", data: {}, status: 200 });

    } catch (e) {
        responseHandler.sendErrorResponse(res, e);
    }
}

export const addTeamMember = async (req: express.Request, res: express.Response) => {
    try {

        const newObj = new TeamModel({
            name: req.body.name,
            designation: req.body.designation,
            message: req.body.message,
            photoUrl: 'x'
        });
        const result = await newObj.save();
        responseHandler.sendSuccessResponse(res, { message: "Team member added successfully", data: result, status: 200 });
    } catch (e) {
        responseHandler.sendErrorResponse(res, e);
    }
}


export function addTeam(input: Team.ITeamRequest, image: any): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            
            input.photoUrl = ""
            if(image && image.length > 0)
            {
                input.photoUrl = image[0].dbpath 
            }
            let { name,photoUrl,designation,message,profiles} = input;

            let u: any = {};
           
            if (name)
                u["name"] = name;
            if (photoUrl)
                u["photoUrl"] = photoUrl;
            if (designation)
                u["designation"] = designation;
            if (message)
                u["message"] = message;
            if (profiles)
                u["profiles"] = profiles;


            const newObj = new TeamModel(u);
            const result = await newObj.save();
            resolve({ message: "Team added successfully", data: result });
        }
        catch (error) 
        {
            reject(error);
        }
    });
}

export function getTeamByTeamId(teamId: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let team = await TeamModel.findOne({ '_id': teamId })
            console.log("team===", team);
            
            let teamData = await changePathFromS3(Array(team),ImageResizeConstants.ABOUT_TEAM.SMALL, "photoUrl");

            // console.log
            // ("team===", JSON.parse(team));
            resolve({ message: "Team get successfully", data: teamData[0] });
        } catch (error) {
            reject(error);
        }

    });
}

/*

 let team = await TeamModel.findOne({ '_id': teamId })
            console.log("team===", team);
            
            team: String = await changePathFromS3(Array(team),ImageResizeConstants.ABOUT_TEAM.SMALL, "photoUrl");

            console.log
            ("team===", JSON.parse(team));
            resolve({ message: "Team get successfully", data: team });

*/



export const getDataTable = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {

        console.log(req.body)
        let perPage = req.body.length;
        let page = req.body.length * req.body.start;
        // creating find query.

        let findQuery = {};
        let orderColumn = req.body.order[0].column;
        let sort = {};
        const dir = req.body.order[0].dir || 'desc';

        if (orderColumn === 0) {
            orderColumn = 'name';
        } else if (orderColumn === 1) {
            orderColumn = 'designation';
        } else if (orderColumn === 2) {
            orderColumn = 'message';
        }

        if (orderColumn) {
            sort = {
                [orderColumn]: dir
            };
        }
        console.log(sort);

        if (!check.isEmpty(req.body.search.value)) {
            findQuery = { 'name': { '$regex': req.body.search.value.trim(), '$options': 'i' } };
        }


        const count = await TeamModel.countDocuments(findQuery).exec();

        const data = await TeamModel.find(findQuery).sort(sort).limit(perPage).skip(req.body.start).lean().exec();


        let objToSend = {
            draw: req.body.draw,
            recordsTotal: count,
            recordsFiltered: count,
            data: data
        };
        const x: NodeJS.ApiResponseType =
        {
            data: objToSend,
            status: 200,
            message: "team data fetched successfully",
        }

        responseHandler.sendSuccessResponse(res, x);

    } catch (e) {

        const x: NodeJS.ApiErrorType = {
            status: 500,
            message: "Some error occurred " + e,
            extraError: e
        }
        responseHandler.sendErrorResponse(res, x);
    }
}

export function getTeam(): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            const team = await TeamModel.find({"status":constants.TEAM_STATUS.ACTIVE});
            console.log("team====", team);
            resolve({ message: "Teams fetch Successfully", data: team });
        } catch (error) {
            reject(error);
        }
    });
}

//         let result = await aboutController.updateTeam(input, req.params.id, image);

export function updateTeam(input: Team.ITeamRequest, Id: string, image: any): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            const team: any = await TeamModel.findOne({ '_id': Id }).lean().exec();

            if (!team) {
                deletefiles(image);
                global.rejector(reject, 404, "Team not found");
                return;
            }
            if(image && image.length > 0)
            {
                input.photoUrl = image[0].dbpath;
                fs.unlink(team.photoUrl, (err) => {
                    if (err) {
                        console.log("images not Deleted");
                    }
                    console.log("images Deleted");
                });
            }
            else
            {
                input.photoUrl = team.photoUrl;
            }

            let { name,photoUrl,designation,message,profiles} = input;

            let u: any = {};
           
            if (name)
                u["name"] = name;
            if (photoUrl)
                u["photoUrl"] = photoUrl;
            if (designation)
                u["designation"] = designation;
            if (message)
                u["message"] = message;
            if (profiles)
                u["profiles"] = profiles;

            const result = await TeamModel.findOneAndUpdate({ '_id': Id }, u, { new: true }).exec();
            resolve({ message: "Team updated successfully", data: result });
        } 
        catch (error) 
        {
            deletefiles(image);
            reject(error);
        }

    });
}


export function deleteTeam(teamId: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
             TeamModel.findOneAndUpdate({ '_id': teamId }, { 'status': constants.TEAM_STATUS.INACTIVE, updateOn: Date.now() }, { upsert: false, new: true }, (err: any, doc: any) => {
                if (err) {
                    reject(err);
                }
                if (doc) {
                    resolve({ message: "Team deleted successfully", data: doc });
                } else {
                    resolve({ message: "team not found", data: {} });
                }


            });

        } catch (error) {
            reject(error);
        }

    });
}

