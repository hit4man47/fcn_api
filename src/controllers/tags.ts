import { ApiCategory } from "../types/Category";
import * as tagesModel from '../models/tags';
import { CONSTANTS, USER_ROLES,CategoryStatus, TagsStatus } from "../utils/constants";
import { jwtTypes } from "../routes/api/Auth";
import { ApiTages } from "../types/tages";

export function createOrUpdateTags(input: ApiTages.CreateOrUpdateTagesRequest, userName:string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let {   _id ,name, count,sqlId} = input;

            let tagesSchema: tagesModel.AddTagesInput | tagesModel.UpdateTagesInfo =
            {
                 name,count,sqlId
            }

            
            let data: tagesModel.Tages.TagesExposedSchema | null = null;

            let isTagesNew: boolean;
            if (_id) {
                isTagesNew = false;

                data = await tagesModel.getTags(_id);
                if (!data) {
                    global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Tages Not Found");
                    return;
                }

                data = await tagesModel.updateTagsInfo(_id, tagesSchema);
                if (!data) {
                    global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Failed to update Category in DB");
                    return;
                }

          }
            else {
                isTagesNew = true;
                data = await tagesModel.addTags(tagesSchema, userName);
                if (!data) {
                    global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Failed to add Category in DB");
                    return;
                } 
            }

            data.name = name;
     
        
            let message;
            if (isTagesNew)
                message = `New Tags Created ${input.name}`;
            else {

                message = `Tags Updated ${input.name}`;
            }
            
            resolve({ message:message, data });
        }
        catch (e) {
            reject(e);
        }
    });
}


export function getAllTages(input: ApiTages.GetAllTagesRequest, payload: jwtTypes.LOGIN_PAYLOAD): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { pageNumber, pageSize } = input;
            let { dataList, count } = await tagesModel.getAllTages(input.pageNumber, input.pageSize, input.whereFilters, input.sortFilters);

            let data: ApiTages.GetAllTagesResponse=
            {
                pageNumber,
                pageSize,
                count,
                dataList
            }

            resolve({ message: "Tags Fetched", data });
        }
        catch (e) {
            reject(e);
        }
    });
}
export function deleteTag(input: ApiTages.DeleteTags, payload: jwtTypes.LOGIN_PAYLOAD): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { tagId} = input;
            let data = await tagesModel.updateTagStatus(tagId,TagsStatus.DELETE);
            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Tag Not Found");
                return;
            }

            resolve({ message: "Tag Deleted", data });
        }
        catch (e) {
            reject(e);
        }
    });
}


