import * as clientUser from '../models/ClientUser';
import * as responseHandler from '../init/responseHandler';
import mongoose, { mongo } from 'mongoose';
import * as permissions from '../utils/permissionManager'
import * as jwt from '../routes/api/Auth'
import * as bcryptUtils from '../utils/bcryptUtils'
import { serverConfig } from '../init/config';
import * as emailer from '../views/emailers';
import { VerifyAccountTokenType } from './verifyAccount'
import express from 'express';
const ObjectId = require('mongoose').Types.ObjectId;
import { ERROR_CODES, STATUS, EMAIL_STATUSES, PHONE_STATUSES, ACCOUNT_SOURCE, TYPES, COMMON_DATA_BASE_ERROR, CONSTANTS, USER_ROLES, REGISTRATION_ERROR, JWT_ERROR } from "../utils/constants";


interface JWT_EMAIL_VERIFICATION_PAYLOAD {
    clientUserId: string
}

export function createClientUser(input: ClientUser.ClientUserDataRequest, ipAddress: string, createBy: string | null, business_Id?: string, payloadData?: any): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let password = null;
            let role = input.role;
            if (input.role == USER_ROLES.CLIENT || input.role == USER_ROLES.BUSINESS_MANAGER) {
                password = await bcryptUtils.getHash(input.password);
            }
            var newUserName = input.userName
            if (newUserName == null) {
                newUserName = input.emailId;
            }
            let checkInput: ClientUser.CHECKUNIQUESNESS = {
                emailId: input.emailId,
                role: role,
                newUserName: newUserName,
                phoneNumber: input.phoneNumber
            }
            let tokenType = VerifyAccountTokenType.ADMIN_EMAIL_ID_VERIFICATION
            let passwordSet = false
            let titleName = "VERIFY YOUR ACCOUNT"
            let message = "please create your password"
            if (input.role == USER_ROLES.CLIENT || input.role == USER_ROLES.BUSINESS_MANAGER) {
                tokenType = VerifyAccountTokenType.EMAIL_ID_VERIFICATION
                passwordSet = true
                titleName = " VERIFY YOUR ACCOUNT"
                message = "enter your password to  verify your account"
            }
            let inputStatus = await checkUniquesness(checkInput)
            //console.log("input status:",inputStatus)
            if (inputStatus == null) {
                let permission = permissions.getDefaultPermissions(input.role as USER_ROLES);
                console.log("from controller", input.role)
                console.log("permission returned :", permission)
                console.log("===>>>>", typeof (input.role as USER_ROLES))
                let InputClientUserData: ClientUser.InputClientUserData = {
                    userName: newUserName,
                    name: input.name,
                    countryCode: input.countryCode,
                    phoneNumber: input.phoneNumber,
                    passwordHash: password,
                    emailId: input.emailId,
                    emailIdStatus: EMAIL_STATUSES.NOT_VERIFIED,
                    createTimeStamp: Date.now(),
                    updateTimeStamp: Date.now(),
                    phoneNumberStatus: PHONE_STATUSES.NOT_VERIFIED,
                    source: ACCOUNT_SOURCE.LOCAL,
                    business_Id: business_Id,
                    role: role,
                    createBy: null,// payloadData.userId,
                    status: STATUS.NOT_ACTIVE,
                    refreshToken: "",
                    refreshTokenExpiry: 0,
                    allowGoogle: false,
                    allowFacebook: false,
                    ipAddrress: ipAddress,
                    permission: permission
                }

                //INSERTIND DATA INSIDE OUR TEMPARAY CLIENT TABLE
                let insertUser = await clientUser.insertIntoTempClientUserData(InputClientUserData)

                let payload: VerifyAccount.TokenInterface = {
                    emailId: input.emailId,
                    tempUserId: insertUser._id,
                    type: tokenType,
                    isPasswordSet: passwordSet,
                    title: titleName,
                    infoMessage: message
                }

                //CREATING A VERIFICATION TOKEN TO BE SEND ON EMAIL
                const token = await jwt.generatJWTToken(payload, jwt.TOKEN_TYPE.VERIFY_ACCOUNT);
                console.log("***********", token)
                if (token.errorMessage == null) {
                    const verificationUrl = serverConfig.PRODUCTION_URLS.EMAIL_VERIFICATION_BASE_URL + token.token;
                    let emailVerificationLinkObj: ClientUser.EmailVerificationEmail = {
                        emailVerficationUrl: verificationUrl,
                        clientName: input.name,
                    }
                    console.log("===========", verificationUrl)
                    //console.log("=", token)
                    let subject = emailer.SUBJECTS.REGISTER_EMAIL_VERIFICATION;
                    let sendmail = await emailer.sendHtml(input.emailId, subject, emailer.registerEmailVerification.getHtml(subject, emailVerificationLinkObj));

                    if (!sendmail || sendmail == undefined) {
                        global.rejector(reject, ERROR_CODES.ERROR_CANNOT_FULLFILL_REQUEST, REGISTRATION_ERROR.CANNOT_SEND_EMAIL);
                        return;
                    }
                    resolve({ message: "Verification email has been sent to " + input.emailId, data: {} });
                }
                else {
                    global.rejector(reject, ERROR_CODES.ERROR_CANNOT_FULLFILL_REQUEST, "can not create token");
                    return;
                }
            }
            else {
                console.log("else condition")
                global.rejector(reject, ERROR_CODES.ERROR_CANNOT_FULLFILL_REQUEST, REGISTRATION_ERROR.CANNOT_SEND_EMAIL);
                return;
            }
            //TO PROVIDE DEFAULT PERMISSION TO OUR USER


        } catch (error) {
            reject(error);
        }
    });
}


export function updateParties(emailId: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            console.log("emaillllllllllllllll",emailId)
            let user = await clientUser.getClientUserBYEmailId(emailId);
            console.log("user",user)
            resolve(null);
        }
        catch (error) {
            reject(error);
        }
    });
}



function getPassword(): string {
    return (Math.random()).toString(36).substr(2, 8);
}

// verify token Controller of business manager User
export function verifyEmailId(input: ClientUser.VerifyEmailRequest): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let payload: jwt.TokenVerificationResult = await jwt.verifyJWTToken(input.token, jwt.TOKEN_TYPE.VERIFY_ACCOUNT);
            console.log("in cont", payload)
            if (payload == undefined || payload == null) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, JWT_ERROR.PAYLOAD_NULL);
                return;
            }
            console.log("===>>>>  payload :", payload)
            //GET DATA FROM THE DATABASE USING THE USERID WE GET FROM THE TOKEN PAYLOAD
            let userData = await clientUser.getTempClientDataDB(payload.payload.tempUserId)
            if (!userData) {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, REGISTRATION_ERROR.NO_USERDATA_IN_DB);
                return;
            }
            let checkInput: ClientUser.CHECKUNIQUESNESS = {
                emailId: userData.emailId,
                role: userData.role,
                newUserName: userData.newUserName,
                phoneNumber: userData.phoneNumber
            }
            // CHECK IF EMAIL EXISTS IN OUR MAIN CLIENT TABLE
            let inputStatus = await checkUniquesness(checkInput)
            console.log("input status:", inputStatus)
            console.log("====>>>>  userData : ", userData)
            if (inputStatus == null) {
                let InputClientUserData: ClientUser.InputClientUserData = {
                    userName: userData.userName,
                    name: userData.name,
                    countryCode: userData.countryCode,
                    phoneNumber: userData.phoneNumber,
                    passwordHash: userData.passwordHash,
                    emailId: userData.emailId,
                    emailIdStatus: EMAIL_STATUSES.VERIFIED,
                    createTimeStamp: userData.createTimeStamp,
                    updateTimeStamp: userData.updateTimeStamp,
                    phoneNumberStatus: userData.phoneNumberStatus,
                    source: userData.source,
                    business_Id: userData.business_Id,
                    role: userData.role,
                    designation: userData.designation,
                    createBy: userData.createBy,
                    status: userData.status,
                    refreshToken: userData.refreshToken,
                    refreshTokenExpiry: userData.refreshTokenExpiry,
                    allowGoogle: userData.allowGoogle,
                    allowFacebook: userData.allowFacebook,
                    ipAddrress: userData.ipAddrress,
                    permission: userData.permission
                }
                //INSERTING DATA INSIDE OUR MAIN CLIENT TABLE
                let insertUser = await clientUser.insertClientUserData(InputClientUserData);
                console.log("::=====::================================")
                console.log("::=====:::", insertUser)
                console.log("::=====::================================")
                
                resolve(insertUser)
            }
            else {
                global.rejector(reject, ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE, COMMON_DATA_BASE_ERROR.CAN_NOT_BE_ADDED_INTO_DATABASE);
                return;
            }

        } catch (error) {
            reject(error);
        }
    });
}

// verify token Controller  changepassword,forget
export function verifyEmails(input: ClientUser.VerifyEmailRequest): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            console.log("input token :", input.token)
            let payload: jwt.TokenVerificationResult = await jwt.verifyJWTToken(input.token, jwt.TOKEN_TYPE.VERIFY_ACCOUNT);
            if (payload.hasError) {
                global.rejector(reject, jwt.ERROR_CODES.TOKEN_INVALID, JWT_ERROR.INVALID_JSONWEB_TOKEN);
                return;
            }
            if (payload) {
                console.log("payload :", payload)
                let client = await clientUser.updateEmailStatus(payload.payload.resetPwdId, EMAIL_STATUSES.VERIFIED);
                console.log("client data at verify :", client)
                let subject = emailer.SUBJECTS.REGISTER_SUCCESS_EMAIL;
                let sendmail = await emailer.sendHtml(client.emailId, subject, emailer.registerSuccessEmail.getHtml(subject, { clientName: client.name }));

                if (sendmail) {
                    console.log("Email Verified mail sent successfully!!")
                }
                else {
                    console.log("Issue in sending Email Verification Mail!!")

                }
                resolve(client);

            }
        } catch (error) {
            reject(error);
        }
    });
}


export function isEmailId(input: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let client = await clientUser.getClientUserBYEmailId(input);
            if (client) {
                resolve(true);
            }
            resolve(false);

        } catch (error) {
            reject(error);
        }
    });
}


export function isUserName(input: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let client = await clientUser.getClientUserBYUserName(input);
            if (client) {
                resolve(true);
            }
            resolve(false);

        } catch (error) {
            reject(error);
        }
    });
}


export function isPhoneNumber(input: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let client = await clientUser.getClientUserBYPhoneNumber(input);
            if (client) {
                resolve(true);
            }
            resolve(false);

        } catch (error) {
            reject(error);
        }
    });
}

export function getUserDetailsByUserId(input: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            console.log("input from params :", input)
            let result = await clientUser.getUserByIdDB(input)
            if (!result || result == undefined) {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR, COMMON_DATA_BASE_ERROR.NO_SUCH_USER_EXISTS);
                return;
            }
            let userObject: ClientUser.OutputUserIdData = {
                _id: result._id,
                userName: result.userName,
                name: result.name,
                countryCode: result.countryCode,
                phoneNumber: result.phoneNumber,
                emailId: result.emailId,
                phoneNumberStatus: result.phoneNumberStatus,
                source: result.source,
                role: result.role,
                status: result.status,
                permission: result.permission
            }
            resolve({ data: userObject, message: "User Details are as Follows" })
            return
        }
        catch (error) {
            reject(error)
        }

    })
}

export function getClientUser(input: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let client = await clientUser.getUserDatailsByEmailOrUserNameDB(input);
            if (client) {
                resolve(client);
            }
            resolve(null);

        } catch (error) {
            reject(error);
        }
    });
}

export function getTokenData(input: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let payload: jwt.TokenVerificationResult = await jwt.verifyJWTToken(input, jwt.TOKEN_TYPE.VERIFY_ACCOUNT)
            if (!payload) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "This User Name or Email Id does not exists");
                return;
            }
            let result = await clientUser.getUserByIdDB(payload.payload.userId)
            if (!result || result == null) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "This User Name or Email Id does not exists");
                return;
            }
            resolve({ data: result, message: "The details of User are as follows" })

        }
        catch (error) {
            reject(error)
        }
    })
}

export function updateClientDetails(input: ClientUser.UpdateClientUserData, userId: ClientUser.GETUSERIDPARAM, ipAddress: string): Promise<NodeJS.ApiResponseType> {

    return new Promise(async (resolve, reject) => {
        try {

            let checkId = await clientUser.getUserByIdDB(userId.id)

            if (!checkId || checkId == undefined) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "NO SUCH USER WITH THIS USERID PRESENT");
                return;
            }
            if (checkId.emailId !== input.emailId) {
                let checkUserByEmail = await clientUser.getClientUserBYEmailId(input.emailId);
                if (checkUserByEmail) {
                    global.rejector(reject, ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE, "This EmailId already exist!");
                    return;
                }
                let insertData: UPDATE.USER_CHANGE_LOG = {
                    oldEmailId: checkId.emailId,
                    newEmailId: input.emailId,
                    ipAdrres: ipAddress,
                }

                let insertDataIntoUserChangeLog = await clientUser.insertIntoUserChangeLog(insertData);
                if (!insertDataIntoUserChangeLog) {
                    global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "CAN NOT BE INSERTED INTO DATABASE!");
                    return;
                }
                let payload: VerifyAccount.TokenInterface =
                {
                    emailId: input.emailId,
                    resetPwdId: userId.id,
                    type: VerifyAccountTokenType.RESET_EMAIL,
                    isPasswordSet: true,
                    title: "PLEASE VERIFY YOUR NEW EMAIL ADDRESS",
                    infoMessage: "PLEASE ENTER YOUR PASSWORD TO VERIFY YOUR EMAIL ADDRESS"
                }
                const token = await jwt.generatJWTToken(payload, jwt.TOKEN_TYPE.VERIFY_ACCOUNT);

                const verificationUrl = serverConfig.PRODUCTION_URLS.EMAIL_VERIFICATION_BASE_URL + token.token;

                // console.log("VerificationUrl: ", verificationUrl);
                // console.log("EmailId: ", input.emailId);

                let emailVerificationLinkObj: ClientUser.EmailVerificationEmail =
                {
                    emailVerficationUrl: verificationUrl,
                    clientName: input.name,
                }
                // TemplateEmailVerificationLink
                let subject = emailer.SUBJECTS.REGISTER_EMAIL_VERIFICATION;
                let sendmail = await emailer.sendHtml(input.emailId, subject, emailer.registerEmailVerification.getHtml(subject, emailVerificationLinkObj));

                if (!sendmail || sendmail == undefined) {
                    global.rejector(reject, ERROR_CODES.ERROR_CANNOT_FULLFILL_REQUEST, REGISTRATION_ERROR.CANNOT_SEND_EMAIL);
                    return;
                }
                resolve({ message: "Verification email has been sent to " + input.emailId, data: {} });
                return
                // updateUserData.emailIdStatus = EMAIL_STATUSES.NOT_VERIFIED;

            }
            let client = await clientUser.updateClientUserData(userId.id, input);
            if (client) {
                resolve({ data: null, message: "USER DETAILS HAS BEEN SUCCESSFULLY UPDATED" });
            }
            resolve({ data: null, message: "CAN NOT UPDATE USER DATA DUE TO SOME INTERNAL SERVER ISSUES" });
        }
        catch (error) {
            reject(error);
        }
    });
}

export function updatePermissionOfUser(input: ClientUser.UpdateUserPermission, payload: any): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            console.log("input ===========>>>>>>>>>>>>>>>>> :", input)

            let checkUserExists = await clientUser.getUserByIdDB(input.userId)
            if (!checkUserExists) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "NO such user exists");
                return;
            }
            if (checkUserExists.role == USER_ROLES.CLIENT || checkUserExists.role == USER_ROLES.BUSINESS_MANAGER) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "CAN NOT UPDATE PERMISSION FOR THIS TYPE OF USER");
                return;
            }
            console.log("user role from datadddhg", checkUserExists.role)
            let validate = await permissions.validateNewPermissions(input.newPermission, checkUserExists.role as USER_ROLES)
            console.log("validator :", validate)

            if (!validate) {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "CAN NOT ASSIGN THESE PERMISSIONS");
                return;
            }
            let updateIntoDb = await clientUser.updateUserPermission(input, checkUserExists.role);
            resolve({ message: "PERMISSION SUCCESSFULLY UPDATED", data: updateIntoDb });
            return

        }
        catch (error) {
            reject(error)
        }


    })
}

export function getAdminUsers(param: User.GET_USER_INPUT): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            console.log("param", param)
            const result = await clientUser.getAdminUsers(param);

            const objToSend = {
                // draw: param.draw,
                count: result.count,
                //  recordsFiltered: result.count,
                users: result.data
            };

            console.log('\n\n\n\n\n\n\n\n ' + objToSend);

            resolve({ message: "data found", data: objToSend });
        }
        catch (e) {
            reject(e)
        }
    });
}

export const getAdminUser = async (req: express.Request, res: express.Response, cb: any) => {
    try {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            const result = await clientUser.ClientUserModel.findOne({ '_id': req.params.id, type: 'ADMIN' }).exec();
            if (!result) {
                responseHandler.sendSuccessResponse(res, { data: null, message: 'No data found', status: 204 });
            } else {
                responseHandler.sendSuccessResponse(res, { data: result, message: 'data fetched' });
            }
        } else {
            global.rejector(cb, ERROR_CODES.INVALID_REQUEST, 'Invalid Id');
        }
    }
    catch (e) {
        // global.rejector(cb, ERROR_CODES.DATABASE_ERROR, e, e);
        responseHandler.sendErrorResponse(res, { message: e, status: ERROR_CODES.DATABASE_ERROR, extraError: e })
    }
}

export function checkUniquesness(input: ClientUser.CHECKUNIQUESNESS): Promise<null> {
    return new Promise(async (resolve, reject) => {
        try {
            let checkEmailExits = await clientUser.checkEmailExistDB(input.emailId)
            if (checkEmailExits) {
                console.log("inside new function email")
                global.rejector(reject, ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE, REGISTRATION_ERROR.EMAIL_EXISTS_ERROR);
                return;
            }
            //CHECK IF USERNAME IS ALREADY EXISTS IN OUR DATABASE
            let userNameExists = await clientUser.checkUserNameExistDB(input.newUserName)
            if (userNameExists) {
                console.log("inside new function username")

                global.rejector(reject, ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE, REGISTRATION_ERROR.USERNAME_EXISTS_ERROR);
                return;
            }
            //CHECK IF PHONE NUMBER IS ALREADY EXISTS IN OUR DATABASE
            // if (input.role == USER_ROLES.CLIENT || input.role == USER_ROLES.BUSINESS_MANAGER) {
            //     let clt3 = await clientUser.getClientUserBYPhoneNumber(input.phoneNumber);
            //     if (clt3) {
            //         console.log("inside new function phonenumber")

            //         global.rejector(reject, ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE, REGISTRATION_ERROR.PHONE_NUMBR_EXISTS);
            //         return;
            //     }
            // }

            resolve(null)
        }
        catch (error) {
            reject(error)
        }
    })
}