import * as clientModel from "../models/ClientUser";
import { LOGOUT } from "../utils/constants";
import * as BlackListUtils from '../utils/BlackListUtils';

export function logout(payload: any, ipAddress: string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {

            let logoutObj = {
                loginStatus: false,
                logoutStatus: true,
                logoutTimeStamp: Date.now(),
                logoutIpAddress: ipAddress
            }
            await clientModel.logoutModel(payload.loginHistoryId, logoutObj);

            BlackListUtils.enterUserloginIdInBlackListMap(payload.loginHistoryId, payload.userId, logoutObj.logoutTimeStamp);

            resolve({ data: {}, message: LOGOUT.SUCCESSFULL_LOGOUT });

        } catch (error) {
            reject(error);
        }
    })
}