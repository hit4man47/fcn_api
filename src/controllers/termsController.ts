
import TermsModel from '../models/TermsOfUse'
import * as express from 'express'
import * as requestResponseHandler from '../init/requestResponseHandler';
import { validationResult, Result } from 'express-validator';
import { CONSTANTS } from '../utils/constants';

export const getTermsOfUse = (req: express.Request, res: express.Response) => {

    TermsModel.findOne({ 'termsId': 1 }).select('-_id content').lean().exec((err, result) => {
        if (err) {
            requestResponseHandler.sendErrorResponse(res, err);
        } else {
            requestResponseHandler.sendSuccessResponse(res, "Data fetched successfully", result);
        }
    });
    
}

export const updateTermsOfUse = async (req: express.Request, res: express.Response) => {

    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            requestResponseHandler.sendErrorResponse(res, { status: CONSTANTS.ERROR_DATABASE_ERROR, "error": errors.array() });
        } else {
            const updateData = await TermsModel.updateOne({ 'termsId': 1 }, { 'content': req.body.content.trim() }, { upsert: true });

            requestResponseHandler.sendSuccessResponse(res, "Terms of use updated successfully", 200);
        }
    } catch (e) {
        requestResponseHandler.sendErrorResponse(res, e);
    }
}       
