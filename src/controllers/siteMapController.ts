import * as siteMapModel from '../models/siteMap';
import { ERROR_CODES } from "../utils/constants";
import fs from 'fs';


export function pushAllDataToXml(): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await siteMapModel.getAllData();
            if (!data) {
                global.rejector(reject, ERROR_CODES.DATABASE_ERROR);
                return;
            }
            let urls: Array<SiteMap.UrlData> = [];

            data.forEach(function (data: any) {
                urls.push({ loc: data.url, lastmod: data.updatedDate, changefreq: "monthly" });
            });

            let xmlBody = getXmlBody(urls);

            let urlPath = 'src/sitemap.xml';

            fs.writeFileSync(urlPath, xmlBody.toString());

            resolve({ message: "sitemap.xml file created successfully", data: {} });

        }
        catch (e) {
            reject(e)
        }
    })
}

export function getXmlBody(input: Array<SiteMap.UrlData>) {
    let xmlBody = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`;

    let urlBody = ``;

    for (let item of input) {
        urlBody += `
        <url>
            <loc>https://www.fintechcryptonews.com/${item.loc}</loc>
            <lastmod>${item.lastmod}</lastmod>
            <changefreq>${item.changefreq}</changefreq>
        </url>`
    }

    xmlBody += `${urlBody}
    </urlset>`;
    return xmlBody;
}