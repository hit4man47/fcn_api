//import * as userModel from "../models/userDBModel";
import * as clientModel from "../models/ClientUser";
import * as clientUser from '../controllers/clientUser';
import { ERROR_CODES } from "../utils/constants";
import * as bcrypt from "../utils/bcryptUtils";
import * as jwt from "../routes/api/Auth";
import * as randomStringUtils from "../utils/randomStringUtils";
import { CONSTANTS, EMAIL_STATUSES, PHONE_STATUSES, ACCOUNT_SOURCE, COMMON_TOKEN_MESSAGE, LOGIN_ERROR } from "../utils/constants";
import { serverConfig } from "../init/config";
const ObjectId = require('mongoose').Types.ObjectId;




interface JWT_LOGIN_VERIFICATION_REFRESH_PAYLOAD {
  userId: string; //(10^7)
  userName: string;
  source: number;
  emailId: string;
  ipAdrres: string;
  role: string,
  permission: string
}

interface LOGIN_RESPONSE_TOKENS {
  access_token: string;
  refresh_token: string;
}
interface LOGIN_RESPONSE {
  name: string;
  access_token: string;
  refresh_token?: string;
}


export function login(input: LoginAuthentication.LoginRequest, ipAddress: string): Promise<NodeJS.ApiResponseType> {
  return new Promise(async (resolve, reject) => {
    try {
      let userObject = await clientModel.getUserDatailsByEmailOrUserNameDB(input.userNameoremailId);
      console.log("===>>>.userobject", userObject)
      if (!userObject) {
        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, LOGIN_ERROR.EMAILID_USERNAME_NOT_FOUND);
        return;
      }
      if (userObject.emailIdStatus == EMAIL_STATUSES.NOT_VERIFIED) {
        if (userObject.phoneNumberStatus == PHONE_STATUSES.NOT_VERIFIED) {
          global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, LOGIN_ERROR.EMAILID_OR_PHONE_NUMBER_NOT_VERIFIED);
        }
      }
      const check = await bcrypt.checkHash(input.password, userObject.passwordHash);

      if (!check) {
        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, LOGIN_ERROR.INCORRECT_PASSWORD);
        return;
      }
      //login history
      let loginHistoryControllerData = await loginHistory(userObject._id, ipAddress);
      console.log("2222222222222", loginHistoryControllerData);

      //creates the payload for access token
      let payload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD = {
        userId: userObject._id,
        userName: userObject.userName,
        source: userObject.source,
        emailId: userObject.emailId,
        role: userObject.role,
        ipaddress: ipAddress,
        name: userObject.name,
        permission: userObject.permission,
        phoneNumber: userObject.phoneNumber,
        loginHistoryId: loginHistoryControllerData._id,
      };
      console.log("refresh token old from db:",userObject.refreshToken)
      //creates the  acces token
      const access_token = await jwt.generatJWTToken(payload,jwt.TOKEN_TYPE.ACCESS);
      let refresh_token=userObject.refreshToken
      // creates the refresh token
      let userObj = await clientModel.validateRefreshTokenDB(userObject.refreshToken, Date.now());
      console.log("refresh token from validate :",userObj)
      if (!userObj|| userObj.refresh_token == "") 
      {
      
         refresh_token = await randomStringUtils.genrateRefreshToken();
         console.log("refresh token from inside if condition",refresh_token)
        let refreshTokenExpireTime = Date.now();
        let result1 = await clientModel.updateRefreshTokenDB(refresh_token, userObject._id, refreshTokenExpireTime);
        if (!result1) {
          global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_TOKEN_MESSAGE.CANNOT_UPDATE);
          return;
        }
      }

      console.log("refresh token checking which one:",refresh_token)
      // console.log("date now time :",Date.now())
      // console.log("refresh expiry time :",CONSTANTS.JWT_REFRESH_TOKEN_EXPIRY_SEC)
      // console.log("==>>>>",Date.now()+CONSTANTS.JWT_REFRESH_TOKEN_EXPIRY_SEC*1000)
     
      // const loginUrl=serverConfig.URLS.LOGIN_VERIFICATION_BASE_URL+"/"+token;
      const result: LOGIN_RESPONSE = {
        access_token: access_token.token as string,
        name: userObject.name,
        refresh_token:refresh_token
      };

      console.log(result);
      resolve({ data: result, message: "User is now logged in" });
    } catch (error) {
      reject(error);
    }
  });
}



export function refreshTokenValidator(input: string, ipAddress: string, time: number): Promise<NodeJS.ApiResponseType> {
  return new Promise(async (resolve, reject) => {

    try {
      let userObj = await clientModel.validateRefreshTokenDB(input, time);
      if (!userObj) {
        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_TOKEN_MESSAGE.INVALID_TOKEN);
        return;
      }
      let loginHistoryControllerData = await loginHistory(userObj._id, ipAddress);

      let accesspayload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD = {

        userName: userObj.userName,
        emailId: userObj.emailId,
        source: userObj.source,
        userId: userObj._id,
        role: userObj.role,
        permission: userObj.permission,
        name: userObj.name,
        ipaddress: ipAddress,
        phoneNumber: userObj.phoneNumber,
        loginHistoryId: loginHistoryControllerData._id
      };

      let newtoken: LOGIN_RESPONSE_TOKENS = await generateNewTokens(accesspayload, ipAddress);
      if (newtoken) {
        console.log("new jwt token created");
        resolve({ data: newtoken, message: COMMON_TOKEN_MESSAGE.TOKEN_CREATED });
      }

    } catch (error) {
      reject(error);
    }
  });
}






export function generateNewTokens(payload: LoginAuthentication.JWT_LOGIN_VERIFICATION_PAYLOAD, ipAddress: string): Promise<LOGIN_RESPONSE_TOKENS> {
  //Promise<NodeJS.ApiResponseType>
  return new Promise(async (resolve, reject) => {
    try {

      let newAccessToken = await jwt.generatJWTToken(payload, jwt.TOKEN_TYPE.ACCESS)
      let newRefreshToken: string = await randomStringUtils.genrateRefreshToken();

      let generatedTokens: LOGIN_RESPONSE_TOKENS = {
        access_token: newAccessToken.token as string,
        refresh_token: newRefreshToken
      };

      let result = await clientModel.updateRefreshTokenDB(generatedTokens.refresh_token, payload.userId, Date.now());
      if (!result) {
        global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, COMMON_TOKEN_MESSAGE.CANNOT_UPDATE);
        return;
      }
      resolve(generatedTokens);
    } catch (error) {
      reject(error);
    }
  });
}


export function loginHistory(userId: string, ipAddress: string): Promise<LoginAuthentication.LoginHistoryData> {
  return new Promise(async (resolve, reject) => {
    try {
      let loginStatus = true;
      let logoutStatus = false;
      let loginTimeStamp = Date.now();
      let logoutTimeStamp = 0;


      let loginHistoryObject: LoginAuthentication.LoginHistoryInput = {
        userId: userId,
        loginIpAddress: ipAddress,
        loginStatus,
        logoutStatus,
        loginTimeStamp,
        logoutTimeStamp,

      }

      let loginHistoryData = await clientModel.createLoginHistoryModel(loginHistoryObject);

      resolve(loginHistoryData);
    } catch (error) {
      reject(error);
    }
  })
}

//******************** this is the controller for refresh token  ********************** */


