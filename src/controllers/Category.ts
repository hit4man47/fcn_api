import { ApiCategory } from "../types/Category";
import * as categoryModel from '../models/Category';
import { CONSTANTS, USER_ROLES,CategoryStatus } from "../utils/constants";
import { jwtTypes } from "../routes/api/Auth";
import { GetCategoryWhereFilters, getCategoryByName } from "../models/Category";
import { getInputBody } from "init/requestResponseHandler";

export function createOrUpdateCategory(input: ApiCategory.CreateOrUpdateCategoryRequest, userName:string): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { description, parentId, name, order,  _id ,label} = input;

            let categorySchema: categoryModel.AddCategoryInput | categoryModel.UpdateCategoryInfo =
            {
                description, parentId, name, order,label
            }
         
                 //check Category name already exist  
                 if(parentId === "0" && name){
                  
                    let categoryNames:GetCategoryWhereFilters={
                        categoryName:name,
                        parentId:parentId
                    }
                    let categorydata = await getCategoryByName(categoryNames);
                    if (categorydata && categorydata.length > 0) {
                        if(_id && categorydata[0]._id == _id){}else{
                        global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Category Name " + name+" Already Exist");
                        return;
                        }
                    }
                }
                //check subCategory name already exist
                else if(parentId && name)
                {
                    let categoryName1:GetCategoryWhereFilters={
                        categoryName:name,
                        parentId:parentId
                    }
                    let subCategory = await getCategoryByName(categoryName1);
                 
                    if (subCategory && subCategory.length > 0) {
                        if(_id && subCategory[0]._id == _id){}else{
                        global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "subCategory Name " + name + " Already Exist");
                        return;
                    }}
                }
            let data: categoryModel.Category.CategoryExposedSchema | null = null;

            let isCategoryNew: boolean;
            if (_id) {



                isCategoryNew = false;
                data = await categoryModel.getCategory(_id);
                if (!data) {
                    global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Category Not Found");
                    return;
                }

                data = await categoryModel.updateCategoryInfo(_id, categorySchema);
                if (!data) {
                    global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Failed to update Category in DB");
                    return;
                }

          }
            else {

                isCategoryNew = true;
                data = await categoryModel.addCategory(categorySchema, userName);
                if (!data) {
                    global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Failed to add Category in DB");
                    return;
                }
            }
            data.name = name;
            let message;
            if (isCategoryNew)
                message = `New Category Created`;
            else {

                message = `Category Updated  ${input.name}`;
            }


            resolve({ message:message, data });
        }
        catch (e) {
            reject(e);
        }
    });
}

export function getCategoryById(input: ApiCategory.GetCategoryByIdRequest, payload: jwtTypes.LOGIN_PAYLOAD): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { categoryId} = input;
            let data = await categoryModel.getCategory(categoryId);
            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Category Not Found");
                return;
            }
            resolve({ message: "Category Fetched", data });
        }
        catch (e) {
            reject(e);
        }
    });
}

export function getAllCategory(input: ApiCategory.GetAllCategoryRequest, payload: jwtTypes.LOGIN_PAYLOAD): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { pageNumber, pageSize } = input;
            let { categoryList, count } = await categoryModel.getAllCategory(input.pageNumber, input.pageSize, input.whereFilters, input.sortFilters);

            let data: ApiCategory.GetAllCategoryResponse =
            {
                pageNumber,
                pageSize,
                count,
                categoryList
            }

            resolve({ message: "Category Fetched", data });
        }
        catch (e) {
            reject(e);
        }
    });
}

export function deleteCategory(input: ApiCategory.GetCategoryByIdRequest, payload: jwtTypes.LOGIN_PAYLOAD): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { categoryId} = input;
            let data = await categoryModel.updateCategoryStatus(categoryId,CategoryStatus.DELETE);
            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Category Not Found");
                return;
            }

            resolve({ message: "Category Fetched", data });
        }
        catch (e) {
            reject(e);
        }
    });
}



export function getLabelByCategoryId(input: ApiCategory.GetLabelByCategoryId, payload: jwtTypes.LOGIN_PAYLOAD): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
           
            let data = await categoryModel.getLabelByCategoryId(input.categoryId);
            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Labels Not Found");
                return;
            }
            resolve({ message: "Labels Fetched", data });
        }
        catch (e) {
            reject(e);
        }
    });
}

export function addLabel(input: ApiCategory.AddLabel): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            
            let data = await categoryModel.AddLabel(input);
            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "fail to add label");
                return;
            }

            let categorySchema: categoryModel.UpdateCategoryInfo =
            {
                description:"", parentId:"", name:"",order:0,label:input.name
            }

            let result = await categoryModel.updateCategoryInfo(input.categoryId, categorySchema);
            if (!result) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Failed to update Category in DB");
                return;
            }
            
            resolve({ message: "label added", data });
        }
        catch (e) {
            reject(e);
        }
    });
}

/*
export function getCategoryByParentId(input: ApiCategory.GetCategoryByParentIdRequest, payload: jwtTypes.LOGIN_PAYLOAD): Promise<NodeJS.ApiResponseType> {
    return new Promise(async (resolve, reject) => {
        try {
            let { parentId} = input;
            let data = await categoryModel.getCategory(parentId);
            let { categoryList, hasMore } = await categoryModel.getAllCategory(input.pageNumber, input.pageSize, input.whereFilters, input.sortFilters);

            if (!data) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "Category Not Found");
                return;
            }

            resolve({ message: "Category Fetched", data });
        }
        catch (e) {
            reject(e);
        }
    });
}*/