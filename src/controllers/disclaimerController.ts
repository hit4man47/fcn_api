
import * as express from 'express';
import * as requestResponseHandler from '../init/requestResponseHandler';
import { CONSTANTS } from '../utils/constants';

import { validationResult, Result } from 'express-validator';
import DisclaimerModel from '../models/Disclaimer'
import shortid from "shortid";


export const getDisclaimer = async (req: express.Request, res: express.Response) => {
    console.log('getDisclaimer');
    try {
        const result = await DisclaimerModel.findOne();

        console.log('result ', result)
        requestResponseHandler.sendSuccessResponse(res, "Disclaimer mil gaya", result, 200);

    } catch (e) {
        requestResponseHandler.sendErrorResponse(res, e);
    }

    // res.json({"message": "Don e"});
}
export const postDisclaimer = async (req: express.Request, res: express.Response) => {
    try {
        const newObj = new DisclaimerModel({
            disclaimerId: shortid.generate(),
            content: req.body.content
        });
        const result = await newObj.save();
        requestResponseHandler.sendSuccessResponse(res, "disclaimer added successfully", result, 200);
    } catch (e) {
        requestResponseHandler.sendErrorResponse(res, e);
    }
}

export const updateDisclaimer = async (req: express.Request, res: express.Response) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            requestResponseHandler.sendErrorResponse(res, { status: CONSTANTS.ERROR_DATABASE_ERROR, "error": errors.array() });
        } else {
            const updateData = await DisclaimerModel.updateOne({ 'disclaimerId': 1 }, { 'content': req.body.content.trim() }, { upsert: true });

            requestResponseHandler.sendSuccessResponse(res, "Disclaimer updated successfully", updateData, 200);
        }
    } catch (e) {
        requestResponseHandler.sendErrorResponse(res, e);
    }
}