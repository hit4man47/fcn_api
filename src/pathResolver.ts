import * as path from 'path';
import fs from 'fs';
export const PATHS=
{
    //GIT_REPO_PATH:path.resolve(__dirname,'../../','GitTemplateRepositories')
}

export function createPathsIfNotExist()
{
    for(let i in PATHS)
    {
        if(PATHS.hasOwnProperty(i))
        {
            fs.mkdirSync((PATHS as any)[i], { recursive: true });
        }
    }
}

