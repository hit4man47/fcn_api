import { Application } from "express-serve-static-core";
import * as  bodyParser from 'body-parser';
import * as cors from 'cors';
import { MyExpressRequest } from "../types/customTypes";
import {getIpAddress} from "../routes/api/index";
var cookieParser = require('cookie-parser')
export function setUpMiddleWares(app: Application) {
    app.get('', function (req, res) {
        res.status(500).send("OOPS!!!You are lost.")
    })

    app.use(cors.default({ origin: '*' }));
    app.use(bodyParser.json({ 'limit': '5MB' }));
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());

    app.use((req, res, next) => {
        console.log("binding ip ", req.url);
        (req as MyExpressRequest).extendedData =
            {
                ipAddress: getIpAddress(req),
                incomingTime: Date.now(),
                uploadData: undefined,
                payload: undefined
            };

        next();
    })



}