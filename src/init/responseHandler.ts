import * as express from 'express';
import { ERROR_CODES, CONSTANTS } from "../utils/constants";
import { serverConfig } from '../init/config'
import * as jwt from '../routes/api/Auth'

export function sendSuccessResponse(res: express.Response, response: NodeJS.ApiResponseType) {
    response.status = 200;
    console.log("SUCCESS:", response);
    sendResonse(res, response);
}

export function sendErrorResponse(res: express.Response, e: NodeJS.ApiErrorType) {
    console.log("ERROR:", e);
    var response: NodeJS.ApiResponseType = { status: 0, message: "", data: null, extraError: null };

    if (e && e.status) {
        switch (e.status) {
            case ERROR_CODES.ACCESS_DENIED:
                response.status = e.status;
                response.message = "Access Denied (Invalid permission)!!!";
                break;

            case ERROR_CODES.DATABASE_ERROR:
                response.status = ERROR_CODES.DATABASE_ERROR;
                response.message = "Unknown Error(402)";
                break;

            case ERROR_CODES.INVALID_UPLOADING:
                response.status = e.status;
                response.message = e.message || 'Error Uploading File(s)';
                break;

            case ERROR_CODES.INVALID_REQUEST:
                response.status = ERROR_CODES.INVALID_REQUEST;
                response.message = "InValid Request";
                break;

            case ERROR_CODES.ERROR_UNKNOWN_HIDDEN_FROM_USER:
                response.status = ERROR_CODES.ERROR_UNKNOWN_HIDDEN_FROM_USER;
                response.message = "Unknown Error (4xx)";
                break;

            case ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER:
                response.status = ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER;
                if (e.message)
                    response.message = e.message;
                break;

            case ERROR_CODES.INVALID_KYC_ID:
                response.status = ERROR_CODES.INVALID_KYC_ID;
                break;

            case ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE:
                response.status = ERROR_CODES.DATABASE_DUPLICATE_ERROR_CODE;
                if (e.message) {
                    response.message = e.message;
                }
                break;

            case ERROR_CODES.ERROR_CANNOT_FULLFILL_REQUEST:
                response.status = ERROR_CODES.ERROR_CANNOT_FULLFILL_REQUEST;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case ERROR_CODES.SESSION_ERRORR:
                response.status = ERROR_CODES.SESSION_ERRORR;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case jwt.ERROR_CODES.TOKEN_EXPIRED:
                response.status = jwt.ERROR_CODES.TOKEN_EXPIRED;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case jwt.ERROR_CODES.TOKEN_INVALID:
                response.status = jwt.ERROR_CODES.TOKEN_INVALID;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case ERROR_CODES.INVALID_CONTRACT_ID:
                response.status = ERROR_CODES.INVALID_CONTRACT_ID;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case ERROR_CODES.TEMPLATE_NOT_FOUND:
                response.status = ERROR_CODES.TEMPLATE_NOT_FOUND;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case ERROR_CODES.INVALID_COST:
                response.status = ERROR_CODES.INVALID_COST;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case ERROR_CODES.ORDER_REQUEST_FAILED:
                response.status = ERROR_CODES.ORDER_REQUEST_FAILED;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case ERROR_CODES.PAYMENT_REQUEST_FAILED:
                response.status = ERROR_CODES.PAYMENT_REQUEST_FAILED;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case ERROR_CODES.ORDER_DATA_NOT_FOUND:
                response.status = ERROR_CODES.ORDER_DATA_NOT_FOUND;
                if (e.message) {
                    response.message = e.message;
                }
                break;

            case ERROR_CODES.CONTRACT_STATUS_UPDATE_ERROR:
                response.status = ERROR_CODES.CONTRACT_STATUS_UPDATE_ERROR;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case ERROR_CODES.USER_NOT_FOUND:
                response.status = ERROR_CODES.USER_NOT_FOUND;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case jwt.ERROR_CODES.Token_NOT_FOUND:
                response.status = jwt.ERROR_CODES.Token_NOT_FOUND;
                if (e.message) {
                    response.message = e.message;
                }
                break;
            case ERROR_CODES.USER_TEMPLATE_UPDATE_ERROR:
                response.status = ERROR_CODES.USER_TEMPLATE_UPDATE_ERROR;
                if (e.message) {
                    response.message = e.message;
                }
                break;
                case ERROR_CODES.INVALID_STATUS:
                    response.status = ERROR_CODES.INVALID_STATUS;
                    if (e.message) {
                        response.message = e.message;
                    }
                    break;

            case ERROR_CODES.INVALID_MONGOOSE_OBJECTID:
                response.status = ERROR_CODES.INVALID_MONGOOSE_OBJECTID;
                if(e.message) {
                    response.message = e.message
                }
                break;
         

            case ERROR_CODES.KYC_ID_NOT_EXIST:
                response.status = ERROR_CODES.KYC_ID_NOT_EXIST;
                if(e.message) {
                    response.message = e.message
                }
                break;
                case CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER:
                    response.status = 400;
                    if(e.message)
                    {
                        response.message = e.message;
                    }            
                    break;
                    case ERROR_CODES.FAIL_UPDATE_DB:
                        response.status = ERROR_CODES.INVALID_MONGOOSE_OBJECTID;
                    if(e.message)
                    {
                        response.message = e.message;
                    }            
                    break;
                    
                
            default:
                response.status = 500;
                response.message = "Unknown Error";
                break;
        }
    }
    else {
        response.status = 409;
        response.message = "Unexpected error";
    }

    if (e.message && (!response.message || response.message == "")) {
        response.message = e.message;
    }

    if (e && !serverConfig.isProduction) {
        response.extraError = e.extraError;
        response.extraMessage = e.message;
    }
    sendResonse(res, response);
}

export interface ApiResponseLogin {
    status?: number,
    message: string,
    extraError?: any,
    extraMessage?: any
}

function sendResonse(res: express.Response, body: NodeJS.ApiResponseType) {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    // res.setHeader("access-token",body.data);
    res.setHeader('Access-Control-Expose-Headers', 'access_token');
    // res.setHeader('Access-Control-Expose-Headers','refresh_token');
    res.send(body);

}


