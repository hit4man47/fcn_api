
import { serverConfig } from './config';
import { Request, Response } from 'express'
import { CONSTANTS } from '../utils/constants';

function getIpAddress(req: any): string {
    //return "dummyIp";
    return req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection.socket ? req.connection.socket.remoteAddress : null);
}



export function attachIpAddress(req: Request, inputBody: any) {
    inputBody.ipAddress = getIpAddress(req);
}

export function getInputBody(req: Request): any {

    var body: any = {};
    switch (req.method) {
        case 'POST':
            body = req.body;
            break;
        case 'PUT':
            body = req.body;
            break;

        case 'GET':
            body = req.query;
            break;
    }

    body.ipAddress = getIpAddress(req);
    return body;
}

export function sendSuccessResponse(res: Response, message: string, data: any, status: number = 200) {
    console.log("SUCCESS:", message, data);
    let response: NodeJS.ApiResponse = { status: status, message: message, data: data, extraError: null }

    sendResponse(res, response);
}

export function sendErrorResponse(res: Response, e: any) {
    console.log("ERROR:", e.error);
    let response: NodeJS.ApiResponse = { status: 0, message: "", data: {}, extraError: null };

    if (e && e.status) {
        switch (e.status) {
            case CONSTANTS.ERROR_DATABASE_ERROR:
                response['status'] = 402;
                response.message = "Unknown Error(402)";
                break;

            case CONSTANTS.ERROR_SESSION_ERROR:
                response.status = 406;
                response.message = "Session Expired";
                break;

            case CONSTANTS.ERROR_UNKNOWN:
                response.status = 400;
                response.message = e.error;
                break;

            case CONSTANTS.ERROR_UNKNOWN_HIDDEN_FROM_USER:
                response.status = 400;
                response.message = "Unknown Error (4xx)";
                if (serverConfig.serverType.TESTING) {
                    response.extraError = e.error;//FOR TESTING ENVIRONEMT ONLY
                }

                break;


            case CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER:
                response.status = 400;
                response.message = e.error;
                break;

            case CONSTANTS.ERROR_MISSING_POST_PARAMS:
                response.status = 401;
                response.message = "Missing Post Params";
                break;

            case CONSTANTS.ERROR_PERMISSION_DENIED:
                response.status = 400;
                response.message = "Permission Denied!!!";
                break;
        
            case CONSTANTS.ERROR_INPUT_ERROR:
                response.status = 455;
                response.message = "Unknown Error (455)";
                console.log("1");
                if (serverConfig.serverType.TESTING) {
                    console.log("12");
                    response.extraError = e.error;//FOR TESTING ENVIRONEMT ONLY
                }
                break;


            default:
                response.status = 400;
                response.message = "Unknown Error";
                break;
        }
    }
    else {
        //unhandled error 
        //runtime error

        response.status = 409;
        response.message = "Unexpected error";
        if (serverConfig.serverType.TESTING) {
            response.extraError = e;//FOR TESTING ENVIRONEMT ONLY
        }

    }

    sendResponse(res, response);
}


function sendResponse(res: Response, body: any) {
    res.setHeader('Content-Type', 'application/json');
    //res.setHeader('Content-Type', '*');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Expose-Header', '*');
    //res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Methods', 'POST');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.send(body);
}