import * as dotenv from 'dotenv';
var x = __dirname + "/../.env";
import {BUCKET} from '../utils/constants'
dotenv.config({ path: x });

let serverConfig =
{
  port: parseInt(process.env.PORT as string) || 5005,
  host: process.env.HOST || '0.0.0.0',
  // database: { 'uri': 'mongodb+srv://sanjayg:pancap1234@cluster0-una63.mongodb.net/test?retryWrites=true&w=majority' },
  database:{ 'uri': 'mongodb://root:root@127.0.0.1:27017/fcn?authSource=admin' },
  sendGridAPIKey: process.env.SENDGRID_API_KEY ? process.env.SENDGRID_API_KEY : '',
  isProduction: process.env.NODE_ENV === 'production',
  serverType:
  {
    LIVE: process.env.PORT === '5005',
    TESTING: process.env.PORT === '5005'
  },

  keys:
  {
    JWT_ACCESS: "SKDNKANSDNASD",
    JWT_EMAIL_VERIFICATION: "SKDNKANSDNASD",
    JWT_USER_FORGETPASSWORD_VERIFICATION: "SKDNKANQWASWD",
    JWT_REQUEST_SIGNATURE_VERIFICATION: "ITSFORTHEREQUESTQWRT",
    JWT_DEFAULT: "KMSDFJSKFSKNFKSDBNFK",
    JWT_VERIFY_ACCOUNT: "SLALANSLASLJALSLANSLNALSNLANS"
  },
  URLS:
  {
    URL: "http://18.140.120.37",
    LOGIN_VERIFICATION_BASE_URL: "http://18.140.120.37:5005/api/login/verifyLogin",
    EMAIL_VERIFICATION_BASE_URL: "http://18.140.120.37:5005/api/register/verifyEmail",
    REQUEST_SIGNATURE_EMAIL_BASE_URL: "http://18.140.120.37:5005/api/requestSignature/verifyUser",
    BUSINESS_VERIFICATION_BASE_URL: "http://18.140.120.37:5005/api/business/verifyEmail/",
    BASE_URL: "http://18.140.120.37:5005",
    VERIFY_ACCOUNT_BASE_URL: "http://18.140.120.37:3000/verifyAccount",
    USER_EMAIL_VERIFICATION_BASE_URL: "http://18.140.120.37:5005/api/register/verifyEmailId",
    CONTACT_US_URL: "http://18.140.120.37:5005",
    USER_FORGETPASSWORD_VERIFICATION_BASE_URL: "http://18.140.120.37:3000/verifyAccount/checkToken"

  },
  PRODUCTION_URLS: {
    EMAIL_VERIFICATION_BASE_URL: "http://localhost:4200/verifyAccount/checkToken?token=",
    BASE_URL: "http://18.140.120.37:5005",
    BASE_URL_SIGNIN_PAGE: "http://18.140.120.37:3000/signin",
    USER_FORGETPASSWORD_VERIFICATION_BASE_URL: "http://18.140.120.37:3000/verifyAccount/checkToken",
  },
  ADMIN_EMAIL:
  {
    REQUEST_CUSTOMIZATION_EMAIL: 'arun@panaeshacapital.com',
    CONTACT_US: 'arun@panaeshacapital.com',
    PARTNER_WITH_US: 'arun@panaeshacapital.com',
    SUBSCRIBE_WITH_US: 'arun@panaeshacapital.com'
  },

  oAuthConfig:
  {

    GOOGLE_CLIENT_ID: (process.env.GOOGLE_CLIENT_ID as string),
    GOOGLE_CLIENT_SECRET: (process.env.GOOGLE_CLIENT_SECRET as string),

    FACEBOOK_CLIENT_SECRET: (process.env.FACEBOOK_CLIENT_SECRET as string),
    FACEBOOK_CLIENT_ID: (process.env.FACEBOOK_CLIENT_ID as string)

  },
  smtpServer:
  {
    "host": process.env.SMTP_HOST ? process.env.SMTP_HOST : '',
    "port": process.env.SMTP_PORT ? parseInt(process.env.SMTP_PORT) : 25,
    "user": process.env.SMTP_USER,
    "password": process.env.SMTP_PASSWORD,
    "sender":
    {
      admin: process.env.SMTP_SENDER_ADMIN
    },
  },

  mailToLawyer: {
    lawyerName: "asif",
    emailId: "mdasifwr@gmail.com",
    data: "my name is xyz ",
    msg: "I am lawyer we can help you",

  },

  
  BUCKET_PATH_PREFIX: {
    BUCKET_PATH_PREFIX :  `https://${BUCKET.BUCKET_NAME}.${BUCKET.BUCKET_REGION}.amazonaws.com/` // https://feeliumcryptonews.s3.ap-south-1.amazonaws.com/
  }

  


};

export { serverConfig };

