import * as express from 'express';
import { validationResult } from 'express-validator';
import {ERROR_CODES, USER_ROLES} from '../../../utils/constants'
import {getPayload} from '../index'
import { jwtTypes } from '../../../routes/api/Auth';
import { hasPermission, moduleConstants, permissonLevelsConstants} from  '../../../utils/permissionManager';

export function hasValidationError(req:express.Request,next:Function):boolean
{
    var error = validationResult(req);
    if (!error.isEmpty())
    {
        let resError: NodeJS.ApiErrorType = { status: ERROR_CODES.INVALID_REQUEST,message:"",extraError: error.array() };
        next(resError);
    }
    return !error.isEmpty();
}

export function isAccessDenied(req:express.Request,next:Function,module:number,permissonLevel:number,allowedRoles?:USER_ROLES[]):boolean
{
    let payload = getPayload(req) as jwtTypes.LOGIN_PAYLOAD;
    console.log("payload:",payload)
    let accessDenied=!hasPermission(module,permissonLevel,payload?payload.permission:"");
    console.log("perrmssion result :",accessDenied)
    let invalidRole= allowedRoles ? allowedRoles.indexOf(payload.role)==-1:false;

    if(accessDenied || invalidRole)
    {
        let resError: NodeJS.ApiErrorType = { status: ERROR_CODES.ACCESS_DENIED};
        next(resError);
    }
    return accessDenied || invalidRole;
}