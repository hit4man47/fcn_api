import * as express from 'express';
import {hasValidationError,isAccessDenied} from './RequestValidator';

export function getBody(req: express.Request) {
    return req.body;
}

export function getParams(req: express.Request): any {
    return req.params;
}

export function getQuery(req: express.Request) {
    return req.query;
}
export function getCookies(req: express.Request) {
    return req.cookies;
}

export function getAccessToken(req:express.Request):string | null
{
    if(req && req.headers && (req.headers as any).access_token)
    {
        return (req.headers as any).access_token;
    }
   return null;
}

export function getRefreshToken(req:express.Request):string | null
{
    if(req && req.headers && (req.headers as any).refresh_token)
    {
        return (req.headers as any).refresh_token;
    }
   return null;
}
export function getPayload(req:express.Request):any
{
    return (req as any).payload;
}
export function uploaddata(req:express.Request):any
{
    return (req as any).extendedData.uploadData;
}
export {hasValidationError,isAccessDenied};