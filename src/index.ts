'use strict';
import * as express from 'express';
import { serverConfig } from './init/config';
import * as http from 'http';
import { setUpMiddleWares } from './init/middlewares';
import { startRoutes } from './routes';
import swaggerUi from 'swagger-ui-express'
import * as swaggerDocument from './swagger.json'
import mongoose from 'mongoose';
import * as fs from 'fs';
import * as path from 'path';



let app: express.Application = express.default();
app.set('views', './src/views');
app.use('/assets', express.static(path.join(__dirname, '../assets')));
app.use('/src/assets', express.static('src/views/emailers/img'))
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
let server: http.Server = app.listen(serverConfig.port, serverConfig.host);
server.on('error', (error: NodeJS.ErrnoException) => {
    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EADDRINUSE':
            console.error(serverConfig.port + ' is already in use,exiting process now');
            process.exit(1);
            break;
        default:
            throw error;
    }
})

server.on('listening', () => {
    console.info(`>>> 🌎 Server started on  http://${serverConfig.host}:${serverConfig.port}/`);
})


global.rejector = function (reject: Function, status: number, error?: string, errorDetails?: any) {
    let e: NodeJS.ApiErrorType = { status, message: error, extraError: errorDetails }
    reject(e);
}

var port = serverConfig.port;
app.set('views', './src/views');
function onListening() {
    console.log('Server is listening on ' + port);
    mongoose.connect(serverConfig.database.uri, { useCreateIndex: true, useNewUrlParser: true, useFindAndModify: false });
}

onListening();

/**
 * database connection settings
 */
mongoose.connection.on('error', function (err) {
    console.log('database connection error');
    console.log(err)
    process.exit(1)
}); // end mongoose connection error

import { loadAllCache } from './init/cacheLoader';
import { createPathsIfNotExist } from './pathResolver';
mongoose.connection.on('open', function (err) {
    if (err) {
        console.log("database error");
        console.log(err);
    } else {
        console.log("database connection open success");
        loadAllCache().then(() => {
            setUpMiddleWares(app);
            startRoutes(app);
            const schemas = new Array();
            mongoose.modelNames().forEach(function (modelName) {
                schemas.push(mongoose.model(modelName));
            })
        }).catch((err) => {
            console.log(err);
        });
    }
});

createPathsIfNotExist();