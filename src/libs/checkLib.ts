
let trim = (x: any) => {
  let value = String(x)
  return value.replace(/^\s+|\s+$/gm, '')
}
export let isEmpty = (value: any) => {
  if (value === null || value === undefined || trim(value) === '' || value.length === 0) {
    return true
  } else {
    return false
  }
}
