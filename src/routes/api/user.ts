import * as express from 'express';
//import * as exampleController from '../../controllers/example';
import {  param } from 'express-validator';
import * as responseHandler from '../../init/responseHandler';
import * as user from '../../controllers/user';
import * as clientUser from '../../controllers/clientUser';
import * as requestHandler from '../../init/requestHandler';


let router = express.Router();



router.get('/getUserByName/:userName',
    [
        param('userName').exists().withMessage("not found").isString().withMessage("invalid userName").isAlphanumeric().withMessage("Should be Alphanumeric"),
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if(requestHandler.hasValidationError(req,next))
            {
                return;
            }  
            let input = requestHandler.getParams(req) as User.CheckUserNameRequest;
            let r = await user.checkUserNameExist(input);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    })

router.get('/getUserByEmailId/:emailId',
    [
        param('emailId').exists().withMessage("not found").isString().withMessage("invalid userName").isEmail().withMessage("Invalid EmailId"),
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if(requestHandler.hasValidationError(req,next))
            {
                return;
            }  
            let input = requestHandler.getParams(req) as User.CheckEmailIdRequest;
            let r = await user.checkEmailExist(input);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    })

router.get('/get/all',
 async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
        if (requestHandler.hasValidationError(req, next)) {
            return;
        }
        let input = requestHandler.getQuery(req) as User.GET_USER_INPUT;
        let r =  await clientUser.getAdminUsers(input);
        responseHandler.sendSuccessResponse(res, r);
    }
    catch (e) {
        next(e);
    }
})

export { router as user }