
import * as express from 'express';
import { checkAccessTokenMiddleware } from './Auth/accessManager';
import { body} from 'express-validator';
import { ERROR_CODES, USER_ROLES, COMMON_ROUTES_VALIDATION, REGISTRATION_ERROR } from "../../utils/constants";
import * as responseHandler from '../../init/responseHandler';
import * as requestHandler from '../../init/requestHandler';
import * as clientuser from '../../controllers/clientUser';
import { permissonLevelsConstants, moduleConstants } from '../../utils/permissionManager'
import * as clientUser from '../../controllers/clientUser';
import { REGISTRATION_VALIDATION, REGISTRATION_VALIDATION_MIN_MAX_LIMIT } from './utils/validationConstant'

let router = express.Router();

// Normal User
router.post('/createUser',
    [
        body('name').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(REGISTRATION_VALIDATION.NAME).trim().isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.NAME_MIN_LIMIT, max: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.NAME_MAX_LIMIT }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('userName').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(REGISTRATION_VALIDATION.USER_NAME).trim()
            .isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.USER_NAME_MIN_LIMIT, max: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.USER_NAME_MAX_LIMIT }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('emailId').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isEmail().withMessage(REGISTRATION_VALIDATION.EMAIL_ID).isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.EMAILID_MIN_LIMIT, max: 256 }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('countryCode').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(REGISTRATION_VALIDATION.COUNTRY_CODE).trim().isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.COUNTRY_CODE_MIN_LIMIT, max: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.COUNTRY_CODE_MAX_LIMIT }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('phoneNumber').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isNumeric().withMessage(REGISTRATION_VALIDATION.PHONE_NUMBER).trim().isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.PHONE_NUMBER_MIN_LIMIT, max: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.PHONE_NUMBER_MAX_LIMIT }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('password').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(REGISTRATION_VALIDATION.PASSWORD).trim().isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.PASSWORD_MIN_LIMIT, max: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.PASSWORD_MAX_LIMIT }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH)
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {

            if (requestHandler.hasValidationError(req, next)) {
                return;
            }
            
            let input = requestHandler.getBody(req) as ClientUser.ClientUserDataRequest;
            input.avatar = ""
            input.designation = ""
            input.role = USER_ROLES.CLIENT
            let { ipAddress } = (res as any);
            let r = await clientUser.createClientUser(input, ipAddress, null);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    }
)
// Admin User
router.post('/adminUser',
    [
        body('name').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(REGISTRATION_VALIDATION.NAME).isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.NAME_MIN_LIMIT, max: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.NAME_MAX_LIMIT }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('userName').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(REGISTRATION_VALIDATION.USER_NAME)
            .isLength({ min: 6, max: 16 }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('emailId').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(REGISTRATION_VALIDATION.EMAIL_ID)
            .isEmail().withMessage("invalid email").isLength({ min: 6, max: 256 }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('phoneNumber').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(REGISTRATION_VALIDATION.PASSWORD).isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.PASSWORD_MIN_LIMIT, max: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.PASSWORD_MAX_LIMIT }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('role').exists().withMessage(COMMON_ROUTES_VALIDATION).custom(
            function validateUserRole(role: string): boolean {
                let ok = false;
                for (let key in USER_ROLES) {
                    if (USER_ROLES.hasOwnProperty(key)) {
                        if (role == (USER_ROLES as any)[key]) {
                            ok = true;
                            return true
                            //break;
                        }
                    }
                }
                return false;
            }
        ).withMessage("invalid role")], checkAccessTokenMiddleware,
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }
            let input = requestHandler.getBody(req) as ClientUser.ClientUserDataRequest;
            if(input.role==USER_ROLES.CLIENT || input.role==USER_ROLES.BUSINESS_MANAGER)
            {
           res.status(ERROR_CODES.ERROR_UNKNOWN_HIDDEN_FROM_USER).send({message:"CAN NOT CREATE CLIENT TYPES OF USER FROM HERE"})
           return
            }
            const ipAddress: string = (req as any).ipAddress;
            let payload = await requestHandler.getPayload(req)
            console.log("somthing :  ****  ", "++++", payload)
            if (requestHandler.isAccessDenied(req, next, moduleConstants.USERS, permissonLevelsConstants.ADD,[USER_ROLES.ADMIN])) {
                return;
            }
            let r = await clientuser.createClientUser(input, ipAddress, null);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    }
)
//update AdminUser Details by id
// router.put('/adminUser/:id',
//     [
//         body('name').exists().withMessage("not found").isString().withMessage("invalid name").isLength({ min: 3, max: 512 }).withMessage("invalid length"),
//         body('userName').exists().withMessage("not found").isString().withMessage("invalid userName")
//             .isLength({ min: 6, max: 16 }).withMessage("invalid length"),
//         body('emailId').exists().withMessage("not found").isString().withMessage("invalid email")
//             .isEmail().withMessage("invalid email").isLength({ min: 6, max: 256 }).withMessage("invalid length"),
//         body('password').exists().withMessage("not found").isString().withMessage("invalid password").isLength({ min: 6 }).withMessage("invalid length"),
//         body('role').exists().withMessage("not found").isString().withMessage("invalid role").isLength({ min: 3, max: 512 }).withMessage("invalid length"),
//         body('permission').exists().withMessage("not found")

//     ],
//     async (req: express.Request, res: express.Response, next: express.NextFunction) => {
//         try {
//             if (requestHandler.hasValidationError(req, next)) {
//                 return;
//             }

//             let input = requestHandler.getBody(req) as ClientUser.ClientUserDataRequest;
//             const ipAddress: string = (req as any).ipAddress;

//             let permission = requestHandler.getBody(req).permission;

//             let inputPermissions: string = permission

//             let r = await clientuser.createClientUser(input, ipAddress, null);
//             responseHandler.sendSuccessResponse(res, r);
//         }
//         catch (e) {
//             next(e);
//         }
//     })


//get AdminUser Detail by id
// router.get('/adminUser/:id', clientUser.getAdminUser)

//EmailVerification redirect 
// router.get('/verifyEmail/:token',
//     [
//         param('token').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(COMMON_ROUTES_VALIDATION.INVALID_TOKEN)
//     ],
//     async (req: express.Request, res: express.Response, next: express.NextFunction) => {
//         try {
//             if (requestHandler.hasValidationError(req, next)) {
//                 return;
//             }

//             let input = requestHandler.getParams(req) as ClientUser.VerifyEmailRequest;
//             // let result=await clientUser.verifyEmailId(input);
//             let r = await register.verifyEmail(input);
//             // if(r==undefined || r==null)
//             // {
//             //     res.status(ERROR_CODES.DATABASE_ERROR).send({message:"can not be inserted into database error from routes"})
//             // }
//             res.redirect(configs.serverConfig.PRODUCTION_URLS.BASE_URL_SIGNIN_PAGE)
//         }
//         catch (error) {
//             next(error);
//         }
//     })



//EmailVerification redirect changePassword,updateDetails
// router.get('/verifyEmailId/:token',
//     [
//         param('token').exists().withMessage("not found").isString().withMessage('invalid token')
//     ],
//     async (req: express.Request, res: express.Response, next: express.NextFunction) => {
//         try {
//             if (requestHandler.hasValidationError(req, next)) {
//                 return;
//             }

//             let input = requestHandler.getParams(req) as ClientUser.VerifyEmailRequest;
//             console.log("sfsfs", input)
//             let r = await register.verifyEmails(input);
//             responseHandler.sendSuccessResponse(res, r);
//         }
//         catch (error) {
//             next(error);
//         }
//     })


export { router as registerRouter };