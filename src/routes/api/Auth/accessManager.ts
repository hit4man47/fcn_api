import * as express from 'express';
import * as authUtils from './index';
import * as requestHandler from '../../../init/requestHandler';
import * as responseHandler from '../../../init/responseHandler';
import * as BlackListUtils from '../../../utils/BlackListUtils';



export let checkAccessTokenMiddleware = async (req: express.Request, res: express.Response, next: Function) => {
    try {
        let { errorMessage, hasError, payload, errCode } = await authUtils.verifyJWTToken(requestHandler.getAccessToken(req), authUtils.TOKEN_TYPE.ACCESS);
        if (hasError) {
            let resError: NodeJS.ApiErrorType = { status: errCode, message: errorMessage as string };
            responseHandler.sendErrorResponse(res, resError);
            return;
        }
        else {
            let loginHistoryId = payload.loginHistoryId;
            let userId = payload.userId;
            let JWTCreatedTimeStamp = payload.iat;
            let logoutBlackList = BlackListUtils.checkIfUserExistsInBlackList(loginHistoryId, userId);
            let changePasswordBlackList = BlackListUtils.checkIfUserExistsChangePasswordList(userId, JWTCreatedTimeStamp);
            if (logoutBlackList && changePasswordBlackList) {
                let resError: NodeJS.ApiErrorType = { status: errCode, message: errorMessage as string };
                responseHandler.sendErrorResponse(res, resError);
                return;
            }
            (req as any).payload = payload;
            next();
        }
    }
    catch (e) {
        responseHandler.sendErrorResponse(res, e);
    }
}