import * as jwt from 'jsonwebtoken';
import {serverConfig} from '../../../init/config';
import * as jwtTypes from '../../../utils/jwtTypes' 
import * as accessManager from './accessManager';

export enum TOKEN_TYPE
{
    ACCESS="Access",
    VERIFY_ACCOUNT="VERIFYACCOUNT",
    SIGNATURE_REQUEST="SIGNATURE_REUEST",
    CONTRACT_REQUEST="CONTRACT_REQUEST"

}

enum TOKEN_EXPIRY_TIME
{
    ACCESS=2*3600,
    VERIFY_ACCOUNT=1*360*10,
    DEFAULT=24*3600*30
}

export enum ERROR_CODES
{
    TOKEN_EXPIRED=599,
    TOKEN_INVALID=511,
    Token_NOT_FOUND=513,
    DEFAULT=512,
    NOT_FOUND=513,
}
export enum ERROR_MESSAGES
{
    TOKEN_EXPIRED="Token Expired",
    TOKEN_INVALID="Invalid Token",
    DEFAULT="Unknown Error (302)",
    NOT_FOUND="Token not found",    
}
export interface CONTRACT_REQUEST {
    fileName: string,
    ipAddress: string
}
export function getTokenInfo(tokenType:TOKEN_TYPE):{secretKey:string,expiryTimeStamp:number}
{
    let expiryTimeStamp:number;
    let secretKey:string;
    switch(tokenType)
    {
        case TOKEN_TYPE.ACCESS:
            secretKey=serverConfig.keys.JWT_ACCESS;
            expiryTimeStamp=TOKEN_EXPIRY_TIME.ACCESS;
            break;
        case TOKEN_TYPE.VERIFY_ACCOUNT:
            secretKey= serverConfig.keys.JWT_VERIFY_ACCOUNT;
            expiryTimeStamp=TOKEN_EXPIRY_TIME.VERIFY_ACCOUNT;
            break;
        default:
            secretKey= serverConfig.keys.JWT_DEFAULT;
            expiryTimeStamp=TOKEN_EXPIRY_TIME.DEFAULT;
            break;
    }

    return {expiryTimeStamp,secretKey};
}


export interface TokenVerificationResult
{
    hasError:boolean,
    errorMessage:ERROR_MESSAGES|null,
    payload:any,
    errCode:number
}

/***
 * it will never reject,no unhandled error will be thrown
 */

export function verifyJWTToken(token: string|null,tokenType:TOKEN_TYPE): Promise<TokenVerificationResult>
{
    return new Promise((resolve, reject) =>
    {
        if(!token)
        {
            resolve({hasError:true,errorMessage:ERROR_MESSAGES.NOT_FOUND,payload:null,errCode:ERROR_CODES.NOT_FOUND});
            return;
        }
        else
        {
            jwt.verify(token, getTokenInfo(tokenType).secretKey, function (err: jwt.VerifyErrors, payload:any)
            {
                if(err || !payload)
                {
                    if(err && err instanceof jwt.TokenExpiredError)
                    {
                        resolve({hasError:true,errorMessage:ERROR_MESSAGES.TOKEN_EXPIRED,payload:null,errCode:ERROR_CODES.TOKEN_EXPIRED});
                    }
                    else
                    {
                        resolve({hasError:true,errorMessage:ERROR_MESSAGES.TOKEN_INVALID,payload:null,errCode:ERROR_CODES.TOKEN_INVALID});
                    }
                }
                else
                {
                    resolve({hasError:false,errorMessage:null,payload,errCode:0});
                }
            });
        }
       
    });
}

interface TokenGenerationResult
{
    hasError:boolean,
    errorMessage:ERROR_MESSAGES|null,
    token:string|null
    errCode:number
}

/**
 * 
 * @param payload 
 * @param expiryTimeInMillis if <=0 NEVER EXPIRE
 */
export function generatJWTToken(payload:any,tokenType:TOKEN_TYPE):Promise<TokenGenerationResult>
{
    return new Promise((resolve, reject) =>
    {
        let signOptions={};
        let tokenInfo=getTokenInfo(tokenType);
        if(tokenInfo.expiryTimeStamp>0)
        {
            signOptions={expiresIn:tokenInfo.expiryTimeStamp};
        }
        jwt.sign(payload,tokenInfo.secretKey, signOptions, function (err, token)
        {
            if (err || !token)
            {
                resolve({hasError:true,errorMessage:ERROR_MESSAGES.DEFAULT,token:null,errCode:ERROR_CODES.DEFAULT})
            }
            else
            {
                resolve({hasError:false,errorMessage:null,token,errCode:0})
            }
        })
    });
}

export {jwtTypes,accessManager};