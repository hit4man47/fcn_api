
import * as express from 'express';
import { body, ValidationChain,param} from 'express-validator';
import * as responseHandler from '../../init/responseHandler';
import * as requestHandler from '../../init/requestHandler';
import { checkAccessTokenMiddleware } from './Auth/accessManager';
import * as Tags from '../../controllers/tags';
import { TAGES_VALIDATION_MIN_MAX_LIMIT } from './utils/validationConstant'
import { ApiCategory } from '../../types/Category';
import { jwtTypes } from './Auth';
import mongoose, { Mongoose } from 'mongoose';
import { ApiTages } from 'types/tages';

let router = express.Router();


function basicInfoValidations():never
{
    let v:ValidationChain[]=
    [
        
        body('name').exists().withMessage("NOT_FOUND").isString().withMessage("").trim().isLength(
            {
                min: TAGES_VALIDATION_MIN_MAX_LIMIT.TAGES_NAME_MIN_LIMIT,
                max: TAGES_VALIDATION_MIN_MAX_LIMIT.TAGES_NAME_MAX_LIMIT
            })
            .withMessage("INVALID_LENGTH"),
       
    ];

    return v as never;
}


function paginationValidations():never
{
    let v:ValidationChain[]=
    [
        body('sortFilters').exists().withMessage("Not exist").custom((sortFilters)=>
        {
            let ok=typeof sortFilters === "object";
            for(let i in sortFilters)
            {
                if(sortFilters.hasOwnProperty(i))
                {
                    let v=(sortFilters as any)[i];
                    ok= ok && (v==0 || v==1 || v==-1); 
                }
            }
            return ok;
        }).withMessage("Invalid"),
        body('whereFilters').exists().withMessage("Not exist").custom((whereFilters)=>
        {
            let ok=typeof whereFilters === "object";
            return ok;
        }).withMessage("Invalid"),

        body('pageNumber').exists().withMessage("Not exist").isNumeric().withMessage('Not a Number').isInt({min:1}),
        body('pageSize').exists().withMessage("Not exist").isNumeric().withMessage('Not a Number').isInt({min:0,max:200})
    ];

    return v as never;
}


router.post('/CreateOrUpdateTages',
    [
       body("_id").custom((_id)=>
       {
           if(_id)
           {
            return mongoose.Types.ObjectId.isValid(_id);
           }
           else
           {
            return true;
           }
       }).withMessage("INVALID")
    ].concat(basicInfoValidations()),
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try 
        {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }

            let input = requestHandler.getBody(req) as ApiTages.CreateOrUpdateTagesRequest;
            //let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);
         
            let r = await Tags.createOrUpdateTags(input,"Admin");
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });



    router.post('/getAll',paginationValidations(),
    async (req: express.Request, res: express.Response, next: express.NextFunction) =>
    {
        try
        {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }

            let input = requestHandler.getBody(req) as ApiTages.GetAllTagesRequest;
            let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

            let r = await Tags.getAllTages(input, payload);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e)
        {
            next(e);
        }
    });

    router.delete('/:tagId',
    [
        param("tagId").exists().withMessage("Not exist").isString().withMessage("").trim().isMongoId().withMessage("Invalid"),
      
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) =>
    {
        try
        {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }

            let input = requestHandler.getParams(req) as ApiTages.DeleteTags;
            let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

            let r = await Tags.deleteTag(input, payload);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e)
        {
            next(e);
        }
    }); 


export { router as tages};


