
import * as express from 'express';
import * as contactController from '../../controllers/contactController';
import { body, validationResult } from 'express-validator';

import * as requestHandler from '../../init/requestHandler';
import * as responseHandler from '../../init/responseHandler';
import { ERROR_CODES, COMMON_ROUTES_VALIDATION } from '../../utils/constants';
import { REGISTRATION_VALIDATION, REGISTRATION_VALIDATION_MIN_MAX_LIMIT, CONTACT_VALIDATION } from './utils/validationConstant';
import { checkAccessTokenMiddleware } from './Auth/accessManager';

const router = express.Router();

router.post('/contactUs',
    [
        body('name').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(REGISTRATION_VALIDATION.NAME).isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.NAME_MIN_LIMIT, max: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.NAME_MAX_LIMIT }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('emailId').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isEmail().withMessage(REGISTRATION_VALIDATION.EMAIL_ID).isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.EMAILID_MIN_LIMIT, max: 256 }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('subject').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND),
        body('reason').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND),
        body('message').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND),
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            
            const ipAddress: string = (req as any).ipAddress;
            if(requestHandler.hasValidationError(req,next))
            {
                return;
            }  

            let input = requestHandler.getBody(req) as FCN.CreateContactRequest;

            let r = await contactController.contactUs(input, ipAddress);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    }
)


router.post('/subscribeWithUs',
    [
        body('emailId').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isEmail().withMessage(REGISTRATION_VALIDATION.EMAIL_ID).isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.EMAILID_MIN_LIMIT, max: 256 }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            
            const ipAddress: string = (req as any).ipAddress;
            if(requestHandler.hasValidationError(req,next))
            {
                return;
            }  

            let input = requestHandler.getBody(req) as FCN.CreateSubscribeWithUsRequest;

            let r = await contactController.subscribeWithUs(input, ipAddress);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    }
)
router.get('/getAllContactUs/',
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {

            let input = requestHandler.getQuery(req) as FCN.GetAllContactUsDetailsRequest;
            let record = parseInt(input.record);
            if (record == 0) {
                let resError: NodeJS.ApiErrorType = { status: ERROR_CODES.INVALID_REQUEST, message: CONTACT_VALIDATION.NO_RECORDS_FOUND };
                next(resError);
                return;
            }
            let r = await contactController.getContactWithUsDetails(input);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    })



router.get('/getAllSubscribeWithUs/',
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {

            let input = requestHandler.getQuery(req) as FCN.GetAllSubscribeWithUsDetailsRequest;
            let record = parseInt(input.record);
            if (record == 0) {
                let resError: NodeJS.ApiErrorType = { status: ERROR_CODES.INVALID_REQUEST, message: CONTACT_VALIDATION.NO_RECORDS_FOUND };
                next(resError);
                return;
            }
            let r = await contactController.getSubscribeWithUsDetails(input);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    })

export { router as contact };


