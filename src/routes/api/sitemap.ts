import * as express from 'express';
import * as requestHandler from '../../init/requestHandler';
import { checkAccessTokenMiddleware } from './Auth/accessManager';
import * as sitemapController from '../../controllers/siteMapController';


let router = express.Router();

router.get('/sitemap.xml',
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let xml = await sitemapController.pushAllDataToXml();
            res.send(xml);
        }
        catch (e) {
            next(e);
        }
    });

export { router as siteMapRouter };