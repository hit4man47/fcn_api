import * as express from 'express';
import * as aboutController from '../../controllers/aboutController';
import { check, body, validationResult, query, param } from 'express-validator';
import fs from 'fs';
import { ERROR_CODES, ImageResizeConstants } from "../../utils/constants";
import * as constants from "../../utils/constants";
import * as responseHandler from '../../init/responseHandler';
import * as requestHandler from '../../init/requestHandler';
import { middle } from '../../utils/fileUpload';
import {FILE_SIZE, SAVE_TO_FOLDER} from './utils/validationConstant'
import * as path from 'path'
import mongoose from 'mongoose';
import { moduleConstants, permissonLevelsConstants } from '../../utils/permissionManager';
import { uploadingMiddleWare, deletefiles, getImagesMimeTypes } from '../../utils/fileUpload';
import { compareSync } from 'bcryptjs';

import { checkAccessTokenMiddleware } from './Auth/accessManager';

const router = express.Router();

//route to get all categories
router.get('/', aboutController.getAbout);

//route to get all categories
router.get('/country', aboutController.getCountries);

//put
router.post('/', body('content').exists().withMessage('Is missing'),
    aboutController.updateAboutUs)

//post 
router.post('/team',
    body(['name', 'designation', 'message']).exists().withMessage('Is missing')
    , aboutController.addTeamMember);

// //route to post category
// router.post('/',
//     body(['category', 'addedBy']).exists().withMessage('One or more parameters is missing in the request')
//     , categoryController.createCategory);





router.post('/addTeam', (req: express.Request, res: express.Response, next: Function) => {
    uploadingMiddleWare(req as any, res, next, { folderName: SAVE_TO_FOLDER.TEAM, maxFileCount: 1, fileTypeConfig: [{ allowedMineTypes: getImagesMimeTypes(), maxFileCount: 1, maxSizeOfOneFile: FILE_SIZE.TEAM_IMG_MAX_SIZE }] }, [ImageResizeConstants.ABOUT_TEAM.SMALL, ImageResizeConstants.ABOUT_TEAM.MEDIUM]);
},
[
        body('name').exists().withMessage('Parameters (name) are missing'),
        body('designation').exists().withMessage('Parameters (designation) are missing'),
        body('message').exists().withMessage('Parameters (message) are missing'),

],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        
        if (requestHandler.hasValidationError(req, next))
        {
            return;
        }

        let image = requestHandler.uploaddata(req);

        console.log("image==", image);

        req.body.profiles = req.body.profiles?JSON.parse(req.body.profiles):"";

        let input = requestHandler.getBody(req) as Team.ITeamRequest;
        let result = await aboutController.addTeam(input, image);
        responseHandler.sendSuccessResponse(res, result);
    }
);


router.put('/deleteTeam/:id',
[
    param('id').exists().withMessage(constants.COMMON_ROUTES_VALIDATION.ID_NOT_FOUND),
],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try{

            if (requestHandler.hasValidationError(req, next)) {
                return;
            }
            let teamId=requestHandler.getParams(req) as Team.id;
         
            if (!mongoose.Types.ObjectId.isValid(teamId.id)) {
                responseHandler.sendErrorResponse(res, { status: ERROR_CODES.Invalid_Mongoose_ObjectId, message: constants.COMMON_ROUTES_VALIDATION.ID_NOT_FOUND});
                return
            }

            let deleteteam= await aboutController.deleteTeam(teamId.id);
            if(deleteteam){
                responseHandler.sendSuccessResponse(res, deleteteam);
            }
        }
        catch(error){

            next(error)
        }
    }
    );

router.put('/updateTeam/:id',(req: express.Request, res: express.Response, next: Function) => {
    uploadingMiddleWare(req as any, res, next, { folderName: SAVE_TO_FOLDER.TEAM, maxFileCount: 1, fileTypeConfig: [{ allowedMineTypes: getImagesMimeTypes(), maxFileCount: 1, maxSizeOfOneFile: FILE_SIZE.TEAM_IMG_MAX_SIZE }] }, [ImageResizeConstants.ABOUT_TEAM.SMALL, ImageResizeConstants.ABOUT_TEAM.MEDIUM]);
},
[
    body('name').exists().withMessage('Parameters (name) are missing'),
    body('designation').exists().withMessage('Parameters (designation) are missing'),
    body('message').exists().withMessage('Parameters (message) are missing')
],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
       try{
        if (requestHandler.hasValidationError(req, next))
        {
            return;
        }

        let image = requestHandler.uploaddata(req);
        req.body.profiles = req.body.profiles?JSON.parse(req.body.profiles):"";
        let input = requestHandler.getBody(req) as Team.ITeamRequest;


        let result = await aboutController.updateTeam(input, req.params.id, image);
        responseHandler.sendSuccessResponse(res, result);
    }
    catch(error){

        next(error)
    }
    }
);

router.get('/getTeamById/:teamId',
    // param(['teamId']).exists().withMessage('Is missing'),
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        if (requestHandler.hasValidationError(req, next)) {
            return;
        }
        let input = req.params.teamId;
   
        let result = await aboutController.getTeamByTeamId(input);
        responseHandler.sendSuccessResponse(res, result);
    }
);

router.get('/getTeam',
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
      
        let result = await aboutController.getTeam();
        responseHandler.sendSuccessResponse(res, result);
    }
);
router.post('/team/get/all', aboutController.getDataTable);
export { router as about };


