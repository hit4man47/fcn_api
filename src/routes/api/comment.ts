import * as express from 'express'
import {body} from 'express-validator'
import * as responseHandler from '../../init/responseHandler';
import * as requestHandler from '../../init/requestHandler'
import * as requestResponseHandler from '../../init/requestResponseHandler'
import {addComment,getComment, DeleteComment,updateCommentStatus, subComment, getNewsComment} from '../../controllers/comment'
import { ERROR_CODES, COMMON_ROUTES_VALIDATION } from "../../utils/constants";
import * as constants  from "../../utils/constants";
var ObjectId= require('mongoose').Types.ObjectId;

let router=express.Router()


router.post('/:newsId',
[
    body('name').exists().withMessage("please enter your name").isString().withMessage("please enter a valid name").trim(),
    body('emailId').exists().withMessage("please enter your email Id").isEmail().withMessage("please enter a valid emailId"),
    body("comment").exists().withMessage("please leave your comment")
],
async(req:express.Request,res:express.Response,next:express.NextFunction)=>
{
    try
    {
        if (requestHandler.hasValidationError(req, next)) {
            return;
        }

        let input = requestHandler.getBody(req) as COMMENT.QUERYDATA;
        let news = requestHandler.getParams(req) as COMMENT.addComment;

        if (!ObjectId.isValid(news.newsId)) {
            requestResponseHandler.sendErrorResponse(res, { status: ERROR_CODES.Invalid_Mongoose_ObjectId, message: "NEWS"+constants.COMMON_ROUTES_VALIDATION.ID_NOT_FOUND});
            return
        }
        let r = await addComment(input,news.newsId);
        responseHandler.sendSuccessResponse(res, r);
    } 
    catch (error)
    {
        next(error)
    }
})


router.post('/subComment/:commentId/:newsId',
[
    body('name').exists().withMessage("please enter your name").isString().withMessage("please enter a valid name").trim(),
    body('emailId').exists().withMessage("please enter your email Id").isEmail().withMessage("please enter a valid emailId"),
    body("comment").exists().withMessage("please leave your comment")
],
async(req:express.Request,res:express.Response,next:express.NextFunction)=>
{
    try
    {
        if (requestHandler.hasValidationError(req, next)) {
            return;
        }
        let input= requestHandler.getBody(req) as COMMENT.QUERYDATA
        let commentId=requestHandler.getParams(req) as COMMENT.QUERYNEWSID        
        let { ipAddress } = (res as any);
        if (!(ObjectId.isValid(commentId.newsId) && ObjectId.isValid(commentId.commentId))) {
            requestResponseHandler.sendErrorResponse(res, { status: ERROR_CODES.Invalid_Mongoose_ObjectId, message: "NEWS and COMMENT"+constants.COMMON_ROUTES_VALIDATION.ID_NOT_FOUND});
            return
        } 
       let r = await subComment(input,commentId);
       responseHandler.sendSuccessResponse(res, r);
    } 
    catch (error)
    {
        next(error)
    }
})


router.get('/',
async(req:express.Request,res:express.Response,next:express.NextFunction)=>
{
    try
    {
        let query= requestHandler.getQuery(req) as COMMENT.NEWSQUERYID
     
        if(query.newsQueryId.length>0)
        {
            if (!ObjectId.isValid(query.newsQueryId)) {
                next()
                return
            }      
        }
        let r= await getComment(query);
        responseHandler.sendSuccessResponse(res, r);
    } 
    catch (error)
    {
        next(error)
    }
})



router.get('/getComment',
async(req:express.Request,res:express.Response,next:express.NextFunction)=>
{
    try
    {
        let query= requestHandler.getQuery(req) as COMMENT.newsComment
        console.log("query :",query)
        if(query.newsId.length>0)
        {
            console.log("inside if of routes ",query.newsId.length)
            if (!ObjectId.isValid(query.newsId)) {
                requestResponseHandler.sendErrorResponse(res, { status: ERROR_CODES.Invalid_Mongoose_ObjectId, message: "NEWS"+constants.COMMON_ROUTES_VALIDATION.ID_NOT_FOUND});
                return
            }      
        }
        let r= await getNewsComment(query);
        responseHandler.sendSuccessResponse(res, r);
    } 
    catch (error)
    {
        next(error)
    }
})


router.put('/updateComment/:id',[
    body('status').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND)
],
async(req:express.Request,res:express.Response,next:express.NextFunction)=>
{
    try
    {
        let param= requestHandler.getParams(req) as COMMENT.CommentUpdateId
        let status=requestHandler.getBody(req) as COMMENT.CommentUpdateStatus
      
            if (!ObjectId.isValid(param.id)) {
                requestResponseHandler.sendErrorResponse(res, {status: ERROR_CODES.OBJECT_ID_NOT_FOUND_ERROR, message: constants.COMMON_ROUTES_VALIDATION.ID_NOT_FOUND});
                return;
            }    
         if(!(status.status && status.status == constants.COMMENT_STATUS.APPROVED || status.status == constants.COMMENT_STATUS.PENDING ||status.status == constants.COMMENT_STATUS.REJECT ))
         {
            requestResponseHandler.sendErrorResponse(res, {status: ERROR_CODES.INVALID_STATUS, message: constants.COMMANT.INVALID_STATUS});
            return;
         }
              let updateCommentReq={
                  id:param.id,
                  status:status.status.toUpperCase()
              }
              
        
        let r= await updateCommentStatus(updateCommentReq);

        responseHandler.sendSuccessResponse(res, r);
    } 
    catch (error)
    {
        next(error)
    }
})


router.delete('/deleteComment/:id',
async(req:express.Request,res:express.Response,next:express.NextFunction)=>
{
    try
    {
        let param= requestHandler.getParams(req) as COMMENT.deleteComent
            if (!ObjectId.isValid(param.id)) {
                requestResponseHandler.sendErrorResponse(res, {status: ERROR_CODES.OBJECT_ID_NOT_FOUND_ERROR, message: constants.COMMON_ROUTES_VALIDATION.ID_NOT_FOUND});
                return
            }    
        let r= await DeleteComment(param);

        responseHandler.sendSuccessResponse(res, r);
    } 
    catch (error)
    {
        next(error)
    }
})

export {router as newsComment}