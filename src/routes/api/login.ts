import * as express from 'express';
import { body, header } from 'express-validator';
import { ERROR_CODES, COMMON_ROUTES_VALIDATION } from "../../utils/constants";
import * as responseHandler from '../../init/responseHandler';
import * as login from '../../controllers/login';
import * as requestHandler from '../../init/requestHandler';
import * as register from '../../controllers/register';
import { initialize, session } from 'passport';
import * as  googleAuth from '../../config/googleAuth';
import * as  facebookAuth from '../../config/facebookAuth';
import { LOGIN_VALIDATION, REGISTRATION_VALIDATION_MIN_MAX_LIMIT } from './utils/validationConstant'
import passport = require('passport');

const moment = require('moment')

let router = express.Router();

router.use(initialize());
router.use(session());

router.post('/loginLocal',
    [
        body('userNameoremailId').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(LOGIN_VALIDATION.USER_NAME_OR_EMAILID).isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.EMAILID_MIN_LIMIT, max: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.EMAILID_MAX_LIMIT }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH),
        body('password').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(LOGIN_VALIDATION.PASSWORD).isLength({ min: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.PASSWORD_MIN_LIMIT, max: REGISTRATION_VALIDATION_MIN_MAX_LIMIT.PASSWORD_MAX_LIMIT }).withMessage(COMMON_ROUTES_VALIDATION.INVALID_LENGTH)
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {

            const ipAddress: string = (req as any).ipAddress;
            console.log(requestHandler.hasValidationError(req, next))
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let input = requestHandler.getBody(req) as LoginAuthentication.LoginRequest;
            let r = await login.login(input, ipAddress);

            res.set('access_token', r.data.access_token);
            res.set('refresh_token', r.data.refresh_token);

            responseHandler.sendSuccessResponse(res, r);
        }
        catch (error) {
            next(error)
        }
    })

//************* this is for creating new refresh token ******************** */

router.post('/loginLocalrefresh',
    [
        header('refresh_token').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND).isString().withMessage(COMMON_ROUTES_VALIDATION.INVALID_TOKEN)
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const ipAddress: string = (req as any).ipAddress;
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }
            let time = Date.now();
            console.log("time :",time)
            console.log("token :",requestHandler.getRefreshToken(req))
            console.log("ipAddress :",ipAddress)
            let r = await login.refreshTokenValidator(requestHandler.getRefreshToken(req) as string, ipAddress, time);
            res.set('access_token', r.data.access_token)
            res.set('refresh_token', r.data.refresh_token)

            responseHandler.sendSuccessResponse(res, r);

        }
        catch (error) {
            next(error);
        }
    })


// this is the end point*************************************

//Login with google
googleAuth.doAuth(passport);

//login with facebook
facebookAuth.doAuth(passport);




router.get('/google', passport.authenticate('google', {
    scope: ['email', 'profile'], prompt: 'consent', passReqToCallback: true
})
);


router.get('/google/callback', passport.authenticate('google'),
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            let x = (req as any).user;
            let { ipAddress } = req as any;
            let input: Register.CreateUserOAuthRequest = {
                name: x.profile._json.name,
                emailId: x.profile._json.email,
                ipAddress: ipAddress
            }
            let data = await register.createUserByGoogle(input, ipAddress);
            responseHandler.sendSuccessResponse(res, data);
        } catch (error) {
            next(error)
        }

    });

//Login with facebook
router.get('/facebook', passport.authenticate('facebook', {
    scope: ['email', 'public_profile']
}));

router.get('/facebook/callback', passport.authenticate('facebook'),
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {

        try {
            let x = (req as any).user;
            console.log(x.profile._json);
            // if Facebook not return Email then thow error
            if (x.profile._json.email === null || x.profile._json === undefined) {
                let resError: NodeJS.ApiErrorType =
                    { status: ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, message: "Email not exits!" };
                responseHandler.sendErrorResponse(res, resError);
            }
            let { ipAddress } = req as any;
            let input: Register.CreateUserOAuthRequest = {
                name: x.profile._json.name,
                emailId: x.profile._json.email,
                ipAddress: ipAddress
            }
            let data = await register.createUserByFaceBook(input, ipAddress);
            responseHandler.sendSuccessResponse(res, data);
        } catch (error) {
            next(error);
        }
    });



export { router as login };