import * as express from 'express';
import * as responseHandler from '../../init/responseHandler';
import { registerRouter } from './register';
import { checkAccessTokenMiddleware } from './Auth/accessManager';
import { admin } from './admin';
import { changePassword } from '../../routes/api/changePassword';
import { login } from './login';
import { user } from './user';
import { forgetPasswordRouter } from './forgetPassword';
import { terms } from './terms';
import { privacy } from './privacy';
import { contact } from './contact';
import { about } from './about';
import { disclaimer } from './disclaimer';
import { logout } from './logout';
import { verifyAccountRouter } from './verifyAccount';
import { categoryRouter } from './categoryRouter'
import { addUpdateRouter } from './addupdatedata'
import { tages } from './tags'
import { PitchYourWork } from './pitch'
import { newsComment } from './comment';
import { siteMapRouter } from './sitemap';
import {awsS3} from './aws';




let apiRoutes = express.Router();
//IP ADDRESS BINDER
apiRoutes.use((req, res, next) => {
    console.log("binding ip");
    (req as any).ipAddress = getIpAddress(req);
    next();
})

// routes path
apiRoutes.use('/admin', admin);
apiRoutes.use('/about', about);
apiRoutes.use('/contact', contact);
apiRoutes.use('/disclaimer', disclaimer);
apiRoutes.use('/login', login);
apiRoutes.use('/logout', checkAccessTokenMiddleware, logout);
apiRoutes.use('/privacy', privacy);
apiRoutes.use('/register', registerRouter);
apiRoutes.use('/terms', terms);
apiRoutes.use('/user', checkAccessTokenMiddleware, user);
apiRoutes.use('/userForget', forgetPasswordRouter);
apiRoutes.use('/users', checkAccessTokenMiddleware, changePassword);
apiRoutes.use('/verifyAccount', verifyAccountRouter);
apiRoutes.use('/categoryRouter', categoryRouter);
apiRoutes.use('/addUpdateData', addUpdateRouter);
apiRoutes.use('/tages', tages);
apiRoutes.use('/pitch', PitchYourWork);
apiRoutes.use('/newsComment', newsComment);
apiRoutes.use('/siteMap', siteMapRouter);
apiRoutes.use('/awsS3', awsS3);


//EXPRESS ERROR HANDLER
apiRoutes.use((error: NodeJS.ApiErrorType, req: express.Request, res: express.Response, next: Function) => {
    responseHandler.sendErrorResponse(res, error);
});

export { apiRoutes };

export function getIpAddress(req: any) {
    return req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection.socket ? req.connection.socket.remoteAddress : null);
}



