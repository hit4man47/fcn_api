import * as express from 'express';
import * as disclaimerController from '../../controllers/disclaimerController';

import { check, body, validationResult, query, param } from 'express-validator';
import * as requestResponseHandler from '../../init/requestResponseHandler';
import { CONSTANTS } from '../../utils/constants';
const router = express.Router();


router.get('/', disclaimerController.getDisclaimer);
router.post('/',
    body('content').exists().withMessage('Is missing')
    , disclaimerController.updateDisclaimer);


export { router as disclaimer };


