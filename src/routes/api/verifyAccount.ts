import * as express from 'express';
import * as verifyAccount from '../../controllers/verifyAccount';
import * as authUtils from './Auth';

import { check, body, validationResult, query, param } from 'express-validator';

import { ERROR_CODES } from "../../utils/constants";

import * as responseHandler from '../../init/responseHandler';
import * as requestHandler from '../../init/requestHandler';

let router = express.Router();

router.get('/checkToken',
    [
        query('token').exists().withMessage('Not found').isString().withMessage('Invalid')
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const errors = validationResult(req);
            const ipAddress: string = (req as any).ipAddress;
            if (!errors.isEmpty()) {
                let resError: NodeJS.ApiErrorType = { status: ERROR_CODES.INVALID_REQUEST, extraError: errors.array() };
                next(resError);
                return;
            }

            let input = requestHandler.getQuery(req) as VerifyAccount.CheckTokenRequest;
            
            let { errorMessage, hasError, payload, errCode } = await authUtils.verifyJWTToken(input.token, authUtils.TOKEN_TYPE.VERIFY_ACCOUNT);

            if (hasError) {
                let resError: NodeJS.ApiErrorType = { status: errCode, message: errorMessage as string };
                next(resError);
                return;
            }
            else {
                let r = await verifyAccount.checkToken(payload as VerifyAccount.TokenInterface, ipAddress);
                responseHandler.sendSuccessResponse(res, r);
            }
        }
        catch (e) {
            next(e);
        }
    }
)




/*
interface VerifyTokenRequest
    {
        token:string,
        password:string
    }
*/



router.post('/verifyToken',
    [
        body('token').exists().withMessage("not found").isString().withMessage("not a string"),
        body('password').exists().withMessage("not found").isString().withMessage("not a string"),

    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const errors = validationResult(req);
            const ipAddress: string = (req as any).ipAddress;
            if (!errors.isEmpty()) {
                let resError: NodeJS.ApiErrorType = { status: ERROR_CODES.INVALID_REQUEST, extraError: errors.array() };
                next(resError);
                return;
            }


            let input = requestHandler.getBody(req) as VerifyAccount.VerifyTokenRequest;

            let {errorMessage,hasError,payload,errCode} = await authUtils.verifyJWTToken(input.token,authUtils.TOKEN_TYPE.VERIFY_ACCOUNT);
            console.log("payload   ::::::",payload)
            if(hasError)
            {
                let resError: NodeJS.ApiErrorType = { status:errCode, message:errorMessage as string};
                next(resError);
                return;
            }
            else
            {
                let r = await verifyAccount.verifyToken(payload as VerifyAccount.TokenInterface,  input.password , ipAddress);
                res.set('access_token',r.data.access_token)
                res.set('refresh_token', r.data.refresh_token)
                responseHandler.sendSuccessResponse(res, r);
            }
        }
        catch (e) {
            next(e);
        }
    }
)


export { router as verifyAccountRouter };
