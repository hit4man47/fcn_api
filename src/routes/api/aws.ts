import * as express from 'express';
import { body} from 'express-validator';
import {COMMON_ROUTES_VALIDATION } from "../../utils/constants";
import * as requestHandler from '../../init/requestHandler';
import {UploadFolderRequest} from '../../../src/types/customTypes'
import * as awsS3 from '../../controllers/awsS3';
import * as responseHandler from '../../init/responseHandler';
import {deleteAWSKey} from '../../utils/fileUpload';

let router = express.Router();

router.post('/uploadFolder',  
[
    body('folderFullPath').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND),
    body('folderName').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND),
],
async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    try 
    {
        if (requestHandler.hasValidationError(req, next)) {
            return;
        }

        let input = requestHandler.getBody(req) as UploadFolderRequest;    
        let r = await awsS3.uploadFolder(input);
        responseHandler.sendSuccessResponse(res, r);
    }
    catch (e) 
    {
        next(e);
    }
});


router.post('/deleteFileFromS3',  
    [
        body('Key').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND),
        body('Bucket').exists().withMessage(COMMON_ROUTES_VALIDATION.NOT_FOUND)
    ],
   async function (req: express.Request, res: express.Response, next: express.NextFunction) {
       try 
       {
           if (requestHandler.hasValidationError(req, next)) 
           {
               return;
           }
           let input = requestHandler.getBody(req) as AWS.S3.DeleteObjectRequest;
           console.log("input===", input);
           let r = await deleteAWSKey(input);
           responseHandler.sendSuccessResponse(res, r);   
       }
       catch (e) 
       {
           next(e);
       }
   });




export { router as awsS3 };
