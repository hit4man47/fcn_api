import * as express from 'express';
import { moduleConstants, permissonLevelsConstants } from '../../utils/permissionManager';
import { body, validationResult } from 'express-validator';
import { USER_ROLES } from "../../utils/constants";
import * as responseHandler from '../../init/responseHandler';
import * as changePasswords from '../../controllers/changePassword';
import * as requestHandler from '../../init/requestHandler';
import { rejects } from 'assert';
import { checkAccessTokenMiddleware } from './Auth/accessManager';

//import {checkAuthentication} from '../../init/middlewares'

let router = express.Router();

router.post('/changePassword',
    [
        body('currentPassword').exists().withMessage("not found").isString().withMessage("not a string").isLength({ min: 8, max: 16 }).withMessage("invalid length"),
        body('newPassword').exists().withMessage("not found").isString().withMessage("not a string").isLength({ min: 8, max: 16 }).withMessage("invalid length")
    ],checkAccessTokenMiddleware,
    async function (req: express.Request, res: express.Response, next: express.NextFunction) {

        try {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }
            var passwords = requestHandler.getBody(req) as ChangePassword.PasswordRequest;
            let payload =await requestHandler.getPayload(req)  
            // if (requestHandler.isAccessDenied(req, next, moduleConstants.USERS, permissonLevelsConstants.UPDATE,[USER_ROLES.CLIENT,USER_ROLES.BUSINESS_MANAGER])) {
            //     return;
            // }          
            var result = await changePasswords.changeUserPassword(payload, passwords);
            res.set('access_token',result.data.access_token)
            res.set('refresh_token', result.data.refresh_token)
            responseHandler.sendSuccessResponse(res, result);
        }
        catch (e) {
            next(e);
        }
    });


router.post('/adminChangePassword',
    [
        body('id').exists().withMessage("not found").isString().withMessage("not a string").isLength({ min: 8, max: 16 }).withMessage("invalid length"),
        body('newPassword').exists().withMessage("not found").isString().withMessage("not a string").isLength({ min: 8, max: 16 }).withMessage("invalid length")
    ],checkAccessTokenMiddleware,
    async function (req: express.Request, res: express.Response, next: express.NextFunction) {

        try {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }
            var passwords = requestHandler.getBody(req) as ChangePassword.AdminChangePassword;

            let {hasError,errorMessage,payload,errCode} = requestHandler.getPayload(req)            


            var result = await changePasswords.changeAdminPassword(payload, passwords);
            responseHandler.sendSuccessResponse(res, result);
        }
        catch (e) {
            next(e);
        }
    });


export { router as changePassword }
