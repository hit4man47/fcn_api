import * as express from 'express';
import { sendSuccessResponse } from '../../init/responseHandler';
import * as requestHandler from '../../init/requestHandler';
import * as responseHandler from '../../init/responseHandler';
import { logout } from '../../controllers/logout';
import { checkAccessTokenMiddleware } from './Auth/accessManager';


let router = express.Router();

router.post('/logoutLocal',
    [],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            const ipAddress: string = (req as any).ipAddress;


            let r = await logout(requestHandler.getPayload(req), ipAddress);

            responseHandler.sendSuccessResponse(res, r);
        }
        catch (error) {
            next(error)
        }
    })

export { router as logout };