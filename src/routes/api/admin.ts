import * as express from 'express';
import { body } from 'express-validator';
import * as responseHandler from '../../init/responseHandler';
import * as admin from '../../controllers/admin';
import * as requestHandler from '../../init/requestHandler';
import { PERMISSION } from '../../utils/constants'
import * as clientuser from '../../controllers/clientUser';
import { FILE_SIZE, SAVE_TO_FOLDER } from './utils/validationConstant';
import { uploadingMiddleWare, deletefiles, getImagesMimeTypes } from '../../utils/fileUpload';
import { checkAccessTokenMiddleware } from './Auth/accessManager';

let router = express.Router();


router.post('/login',
    [
        body('name').exists().withMessage("not found").isString().withMessage("invalid name"),
        body('password').exists().withMessage("not found").isAlphanumeric().withMessage("invalid password"),
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            console.log((req as any).ipAddress);
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let reqObj = requestHandler.getBody(req) as Admin.LoginRequest;
            let r = await admin.login(reqObj);
            responseHandler.sendSuccessResponse(res, r);

        }
        catch (e) {
            next(e);
        }
    });


router.post('/imageurl', (req: express.Request, res: express.Response, next: Function) => {
    uploadingMiddleWare(req as any, res, next, { folderName: SAVE_TO_FOLDER.CONTENT, maxFileCount: 15, fileTypeConfig: [{ allowedMineTypes: getImagesMimeTypes(), maxFileCount: 15, maxSizeOfOneFile: FILE_SIZE.CONTENT_IMG_SIZE }] });
}, [],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            console.log((req as any).ipAddress);
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let image = requestHandler.uploaddata(req);
            let r = await admin.saveImageUrl(image);
            responseHandler.sendSuccessResponse(res, r);

        }
        catch (e) {
            next(e);
        }
    });





export { router as admin };