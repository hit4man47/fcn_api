
import * as express from 'express';
import { body, ValidationChain, param } from 'express-validator';
import * as responseHandler from '../../init/responseHandler';
import * as requestHandler from '../../init/requestHandler';
import { checkAccessTokenMiddleware } from './Auth/accessManager';
import * as category from '../../controllers/Category';
import { CATEGORY_VALIDATION_MIN_MAX_LIMIT } from './utils/validationConstant'
import { ApiCategory } from '../../types/Category';
import { jwtTypes } from './Auth';
import mongoose, { Mongoose } from 'mongoose';

let router = express.Router();

function basicInfoValidations():never
{
    let v:ValidationChain[]=
    [
        body('description').exists().withMessage("NOT_FOUND").isString().withMessage("").trim().isLength(
        {
            min: CATEGORY_VALIDATION_MIN_MAX_LIMIT.CATEGORY_DESC_MIN_LIMIT,
            max: CATEGORY_VALIDATION_MIN_MAX_LIMIT.CATEGORY_DESC_MAX_LIMIT
        })
            .withMessage("INVALID_LENGTH"),
        body('name').exists().withMessage("NOT_FOUND").isString().withMessage("").trim().isLength(
            {
                min: CATEGORY_VALIDATION_MIN_MAX_LIMIT.CATEGORY_NAME_MIN_LIMIT,
                max: CATEGORY_VALIDATION_MIN_MAX_LIMIT.CATEGORY_NAME_MAX_LIMIT
            })
            .withMessage("INVALID_LENGTH"),
        body('parentId').exists().withMessage("NOT_FOUND").isString().withMessage("NOT_NUMBER").trim(),
        body('order').exists().withMessage("NOT_FOUND").isNumeric().withMessage("NOT_NUMBER").trim(),
        body('label').exists().withMessage("NOT_FOUND")
    ];

    return v as never;
}

function paginationValidations():never
{
    let v:ValidationChain[]=
    [
        body('sortFilters').exists().withMessage("Not exist").custom((sortFilters)=>
        {
            let ok=typeof sortFilters === "object";
            for(let i in sortFilters)
            {
                if(sortFilters.hasOwnProperty(i))
                {
                    let v=(sortFilters as any)[i];
                    ok= ok && (v==0 || v==1 || v==-1); 
                }
            }
            return ok;
        }).withMessage("Invalid"),
        body('whereFilters').exists().withMessage("Not exist").custom((whereFilters)=>
        {
            let ok=typeof whereFilters === "object";
            return ok;
        }).withMessage("Invalid"),

        body('pageNumber').exists().withMessage("Not exist").isNumeric().withMessage('Not a Number').isInt({min:1}),
        body('pageSize').exists().withMessage("Not exist").isNumeric().withMessage('Not a Number').isInt({min:0,max:200})
    ];

    return v as never;
}

router.post('/',
    [
       body("_id").custom((_id)=>
       {
           if(_id)
           {
            return mongoose.Types.ObjectId.isValid(_id);
           }
           else
           {
            return true;
           }
       }).withMessage("INVALID")
    ].concat(basicInfoValidations()),
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try 
        {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }

            let input = requestHandler.getBody(req) as ApiCategory.CreateOrUpdateCategoryRequest;
            //let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);
           // payload.userName="sanjay"
            let r = await category.createOrUpdateCategory(input,"Admin");
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });

router.post('/getAll',paginationValidations(),
    async (req: express.Request, res: express.Response, next: express.NextFunction) =>
    {
        try
        {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }

            let input = requestHandler.getBody(req) as ApiCategory.GetAllCategoryRequest;
            let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

            let r = await category.getAllCategory(input, payload);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e)
        {
            next(e);
        }
    });

router.post('/getById',
    [
        body("_id").exists().withMessage("Not exist").isString().withMessage("").trim().isMongoId().withMessage("Invalid"),
      
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) =>
    {
        try
        {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }

            let input = requestHandler.getBody(req) as ApiCategory.GetCategoryByIdRequest;
            let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

            let r = await category.getCategoryById(input, payload);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e)
        {
            next(e);
        }
    });
    


router.post('/getLabel',
[
    body("categoryId").exists().withMessage("Not exist").isString().withMessage("").trim().isMongoId().withMessage("Invalid"),
  
],
async (req: express.Request, res: express.Response, next: express.NextFunction) =>
{
    try
    {
        if (requestHandler.hasValidationError(req, next))
        {
            return;
        }

        let input = requestHandler.getBody(req) as ApiCategory.GetLabelByCategoryId;
        let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

        let r = await category.getLabelByCategoryId(input, payload);
        responseHandler.sendSuccessResponse(res, r);
    }
    catch (e)
    {
        next(e);
    }
});

    router.post('/addLabel',
    [
        body("name").exists().withMessage("Not exist"),
        body("categoryId").exists().withMessage("Not exist").isString().withMessage("").trim().isMongoId().withMessage("Invalid id"),
      
        
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) =>
    {
        try
        {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }
    
            let input = requestHandler.getBody(req) as ApiCategory.AddLabel;
            let r = await category.addLabel(input);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e)
        {
            next(e);
        }
    });

    // router.delete('/:categoryId',
    // [
    //     param("categoryId").exists().withMessage("Not exist").isString().withMessage("").trim().isMongoId().withMessage("Invalid"),
      
    // ],
    // async (req: express.Request, res: express.Response, next: express.NextFunction) =>
    // {
    //     try
    //     {
    //         if (requestHandler.hasValidationError(req, next))
    //         {
    //             return;
    //         }

    //         let input = requestHandler.getParams(req) as ApiCategory.GetCategoryByIdRequest;
    //         let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

    //         let r = await category.deleteCategory(input, payload);
    //         responseHandler.sendSuccessResponse(res, r);
    //     }
    //     catch (e)
    //     {
    //         next(e);
    //     }
    // });


export { router as categoryRouter };


