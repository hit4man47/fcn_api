
import * as express from 'express';
import { body, ValidationChain, query } from 'express-validator';
import * as responseHandler from '../../init/responseHandler';
import * as requestHandler from '../../init/requestHandler';
import { checkAccessTokenMiddleware } from './Auth/accessManager';
import * as addUpdateData from '../../controllers/addUpdateData';
import { DATA_VALIDATION_MIN_MAX_LIMIT } from './utils/validationConstant'
import { ApiAddUpdateData } from '../../types/addUpdateData';
import { jwtTypes } from './Auth';
import mongoose, { Mongoose } from 'mongoose';
import { middle, uploadingMiddleWare, deletefiles, getImagesMimeTypes, getPdfMimeTypes, getVideoMimeTypes } from '../../utils/fileUpload';

import { FILE_SIZE, SAVE_TO_FOLDER } from './utils/validationConstant';
import * as addupdateModel from '../../models/AddUpdateData';
import {ImageResizeConstants, IMAGE_URL_STATUS} from '../../utils/constants';



let router = express.Router();


function basicInfoValidations(): never {
    let v: ValidationChain[] =
        [
            body('description').exists().withMessage("NOT_FOUND").isString().withMessage("").trim().isLength(
                {
                    min: DATA_VALIDATION_MIN_MAX_LIMIT.DATA_DESC_MIN_LIMIT,
                    max: DATA_VALIDATION_MIN_MAX_LIMIT.DATA_DESC_MAX_LIMIT
                })
                .withMessage("INVALID_LENGTH"),
            body('title').exists().withMessage("NOT_FOUND").isString().withMessage("").trim().isLength(
                {
                    min: DATA_VALIDATION_MIN_MAX_LIMIT.DATA_TITLE_MIN_LIMIT,
                    max: DATA_VALIDATION_MIN_MAX_LIMIT.DATA_TITLE_MAX_LIMIT
                })
                .withMessage("INVALID_LENGTH"),
            body('shortDescription').exists().withMessage("NOT_FOUND").isString().withMessage("").trim().isLength(
                {
                    min: DATA_VALIDATION_MIN_MAX_LIMIT.DATA_SHORT_DESC_MIN_LIMIT,
                    max: DATA_VALIDATION_MIN_MAX_LIMIT.DATA_SHORT_DESC_MAX_LIMIT
                })
                .withMessage("INVALID_LENGTH"),
            body('category').exists().withMessage("NOT_FOUND").isMongoId().withMessage("INVALID_CATEGORY_ID").trim(),
            body('subCategory').exists().withMessage("NOT_FOUND").isMongoId().withMessage("INVALID_SUBCATEGORY_ID").trim(),

        ];
    return v as never;
}

function paginationValidations(): never {
    let v: ValidationChain[] =
        [

            body('sortFilters').exists().withMessage("Not exist").custom((sortFilters) => {
                let ok = typeof sortFilters === "object";
                for (let i in sortFilters) {
                    if (sortFilters.hasOwnProperty(i)) {
                        let v = (sortFilters as any)[i];
                        ok = ok && (v == 0 || v == 1 || v == -1);
                    }
                }
                return ok;
            }).withMessage("Invalid"),
            body('whereFilters').exists().withMessage("Not exist").custom((whereFilters) => {
                let ok = typeof whereFilters === "object";
                return ok;
            }).withMessage("Invalid"),

            body('pageNumber').exists().withMessage("Not exist").isNumeric().withMessage('Not a Number').isInt({ min: 1 }),
            body('pageSize').exists().withMessage("Not exist").isNumeric().withMessage('Not a Number').isInt({ min: 0, max: 200 })
        ];

    return v as never;
}

// ADD_OR_UPDATE.EDITOR_CHOICE, ImageResizeConstants.ADD_OR_UPDATE.RELATED_POST
router.post('/createOrUpdateData', (req: express.Request, res: express.Response, next: Function) => {
    uploadingMiddleWare(req as any, res, next, { folderName: req.body.type, maxFileCount: 2, fileTypeConfig: [{ allowedMineTypes: getImagesMimeTypes(), maxFileCount: 1, maxSizeOfOneFile: FILE_SIZE.FCN_NEWS_IMAGE_MAX_SIZE }, { allowedMineTypes: getPdfMimeTypes(), maxFileCount: 1, maxSizeOfOneFile: FILE_SIZE.FCN_NEWS_PDF_MAX_SIZE }] }, [ImageResizeConstants.NEWS_IMAGE.SMALL, ImageResizeConstants.NEWS_IMAGE.SMALL1, ImageResizeConstants.NEWS_IMAGE.SMALL2, ImageResizeConstants.NEWS_IMAGE.SMALL3, ImageResizeConstants.NEWS_IMAGE.MEDIUM, ImageResizeConstants.NEWS_IMAGE.LARGE1, ImageResizeConstants.NEWS_IMAGE.LARGE2]);
},
    [
        body("_id").custom((_id) => {
            if (_id) {
                return mongoose.Types.ObjectId.isValid(_id);
            }
            else {
                return true;
            }
        }).withMessage("INVALID")

    ].concat(basicInfoValidations()),
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let input = requestHandler.getBody(req) as ApiAddUpdateData.CreateOrUpdateDataRequest;
            console.log("request type ::", req.body.type)
            let uploadFile = requestHandler.uploaddata(req);


            console.log("uploadFile===::::1:::::", uploadFile);


            if (uploadFile && uploadFile.length) {
                for (let i = 0; i < uploadFile.length; i++) {
                    console.log(uploadFile[i])
                    if (uploadFile[i].tag === "file") {
                        input.file = uploadFile[i].dbpath
                    }
                    if (uploadFile[i].tag === "image") {
                        input.thumbnail = uploadFile[i].dbpath;
                    }
                }
            }

            console.log("input file path ::", input.file)
            console.log("inputinput.thumbnail path ::", input.thumbnail)
            let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

            input.seo = typeof input.seo === 'string' ? JSON.parse(input.seo) : input.seo;
            input.doc = typeof input.doc === 'string' ? JSON.parse(input.doc) : input.doc;
            input.tags = typeof input.tags === 'string' ? JSON.parse(input.tags) : input.tags;
            input.label = typeof input.label === 'string' ? JSON.parse(input.label) : input.label;
            input.section = typeof input.section === 'string' ? JSON.parse(input.section) : input.section;


            let r = await addUpdateData.createOrUpdateData(input, payload);

            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });



router.post('/getAll', [
    body('type').exists().withMessage("NOT_FOUND").isString().withMessage("NOT_String").trim()
].concat(paginationValidations()),
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let input = requestHandler.getBody(req) as ApiAddUpdateData.GetAllDataRequest;
            let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

            let r = await addUpdateData.getAllData(input);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });



router.post('/getById',
    [
        body("_id").exists().withMessage("Not exist").isString().withMessage("").trim().isMongoId().withMessage("Invalid"),
        body("type").exists().withMessage("Not exist").isString().withMessage("NOT_String").trim(),


    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let input = requestHandler.getBody(req) as ApiAddUpdateData.GetDataByIdRequest
            let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);
            const ipAddress: string = (req as any).ipAddress;
            let r = await addUpdateData.getDataById(input, payload, ipAddress);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });



router.post('/getNewsDetailById',
    [
        body("_id").exists().withMessage("Not exist").isString().withMessage("").trim().isMongoId().withMessage("Invalid"),
        // body("type").exists().withMessage("Not exist").isString().withMessage("NOT_String").trim(),


    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let input = requestHandler.getBody(req) as ApiAddUpdateData.GetDataByIdRequest
            let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

            const ipAddress: string = (req as any).ipAddress;
            let type
            if (!input.type)
                type = await addUpdateData.getDataType(input._id);

            if (type)
                input.type = type as any;
            let r = await addUpdateData.getNewsDetailById(input, payload, ipAddress);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });



router.post('/getNewsDetailByUniqueUrl',
    [
        body('url').exists().withMessage("Url Not exist").isString().withMessage("please enter url"),
        body('pageData').exists().withMessage("Page Data Not exist")

    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let input = requestHandler.getBody(req) as ApiAddUpdateData.Url;
            const ipAddress: string = (req as any).ipAddress;
            // let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);
            let type;
            if (!input.type)
                type = await addUpdateData.getDataTypeUsingUrl(input.url);

            if (type)
                input.type = type as any;
            let r = await addUpdateData.getUniqueUrlData(input, ipAddress);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });

router.post('/changeStatus',
    [
        body("id").exists().withMessage("Not exist").isString().withMessage("").trim().isMongoId().withMessage("Invalid id"),
        body("type").exists().withMessage("Not exist").isString().withMessage("NOT_String").trim(),
        body("status").exists().withMessage("Not exist").isNumeric().withMessage("NOT_Number").trim()

    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let input = requestHandler.getBody(req) as ApiAddUpdateData.ChangeStatus;
            let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

            let r = await addUpdateData.changeStatus(input, payload);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });

router.post('/getRelatedCategory', paginationValidations(),
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let input = requestHandler.getBody(req) as ApiAddUpdateData.GetRelatedCategory;
            let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

            let r = await addUpdateData.getRelatedCategory(input);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });

router.get('/getPostData',
    [
        query("per_page").exists().withMessage("not found"),
        query("page").exists().withMessage("not found")
    ], async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }
            let inputParameter = requestHandler.getQuery(req) as ApiAddUpdateData.queryParameter;
            let result = await addUpdateData.getPostData(inputParameter)
            responseHandler.sendSuccessResponse(res, result)
            return
        }
        catch (error) {
            next(error)
        }
    }
)


router.post('/beautifulSoup',
    [
        body("pageSize").exists().withMessage("not found"),
        // query("page").exists().withMessage("not found")
    ], async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }
            let input = requestHandler.getBody(req);
            let result = await addUpdateData.beautifulSoup(input.pageSize);
            responseHandler.sendSuccessResponse(res, result)
            return
        }
        catch (error) {
            next(error)
        }
    }
)


router.post('/categoryData',
    [
        body("pageSize").exists().withMessage("not found"),
        // query("page").exists().withMessage("not found")
    ], async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }
            let input = requestHandler.getBody(req);
            let result = await addUpdateData.getCategory(input.pageSize);
            responseHandler.sendSuccessResponse(res, result)
            return
        }
        catch (error) {
            next(error)
        }
    }
)
router.post('/tags',
    [
        body("pageSize").exists().withMessage("not found"),
        // query("page").exists().withMessage("not found")
    ], async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }
            let input = requestHandler.getBody(req);
            let result = await addUpdateData.getAndUpdateTags(input.pageSize);
            responseHandler.sendSuccessResponse(res, result)
            return
        }
        catch (error) {
            next(error)
        }
    }
)



//api for copy database
interface addfield {
    type: string
}

router.post('/addfield',
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }
            let input = requestHandler.getBody(req) as addfield;

            let r = await addupdateModel.copyCollection(input.type);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });

export { router as addUpdateRouter };




