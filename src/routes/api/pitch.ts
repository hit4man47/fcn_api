
import * as express from 'express';
import { body, ValidationChain, param, } from 'express-validator';
import * as responseHandler from '../../init/responseHandler';
import * as requestHandler from '../../init/requestHandler';
import * as pitch from '../../controllers/pitch';
import { DATA_VALIDATION_MIN_MAX_LIMIT } from './utils/validationConstant'
import { ApiPitch } from '../../types/pitch';
import { middle, uploadingMiddleWare, deletefiles, getImagesMimeTypes, getPdfMimeTypes, getVideoMimeTypes } from '../../utils/fileUpload';
import mongoose, { Mongoose } from 'mongoose';
import { FILE_SIZE, SAVE_TO_FOLDER } from './utils/validationConstant';


let router = express.Router();

function basicInfoValidations(): never {
    let v: ValidationChain[] =
        [
            body('type').exists().withMessage("NOT_FOUND").isString().withMessage("NOT_string").trim(),
            body('description').exists().withMessage("NOT_FOUND").isString().withMessage("").trim().isLength(
                {
                    min: DATA_VALIDATION_MIN_MAX_LIMIT.DATA_DESC_MIN_LIMIT,
                    max: DATA_VALIDATION_MIN_MAX_LIMIT.DATA_DESC_MAX_LIMIT
                })
                .withMessage("INVALID_LENGTH"),
            body('metaDescription').exists().withMessage("NOT_FOUND").isString().withMessage("").trim().isLength(
                {
                    min: DATA_VALIDATION_MIN_MAX_LIMIT.DATA_DESC_MIN_LIMIT,
                    max: DATA_VALIDATION_MIN_MAX_LIMIT.DATA_DESC_MAX_LIMIT
                })
                .withMessage("INVALID_LENGTH"),

            body('title').exists().withMessage("NOT_FOUND").isString().withMessage("NOT_string").trim(),
            body('releaseData').exists().withMessage("NOT_FOUND").isNumeric().withMessage("NOT_number").trim(),
            body('summary').exists().withMessage("NOT_FOUND").isString().withMessage("NOT_string").trim(),
            body('subCategory').exists().withMessage("NOT_FOUND").isString().withMessage("NOT_string").trim(),
            body('name').exists().withMessage("NOT_FOUND").isString().withMessage("NOT_string").trim(),
            body('contactNumber').exists().withMessage("NOT_FOUND").isString().withMessage("NOT_string").trim(),
            body('companyName').exists().withMessage("NOT_FOUND").isString().withMessage("NOT_string").trim(),
            body('message').exists().withMessage("NOT_FOUND").isString().withMessage("NOT_string").trim(),

        ];
    return v as never;
}


function paginationValidations():never
{
    let v:ValidationChain[]=
    [
        body('sortFilters').exists().withMessage("Not exist").custom((sortFilters)=>
        {
            let ok=typeof sortFilters === "object";
            for(let i in sortFilters)
            {
                if(sortFilters.hasOwnProperty(i))
                {
                    let v=(sortFilters as any)[i];
                    ok= ok && (v==0 || v==1 || v==-1); 
                }
            }
            return ok;
        }).withMessage("Invalid"),
        body('whereFilters').exists().withMessage("Not exist").custom((whereFilters)=>
        {
            let ok=typeof whereFilters === "object";
            return ok;
        }).withMessage("Invalid"),

        body('pageNumber').exists().withMessage("Not exist").isNumeric().withMessage('Not a Number').isInt({min:1}),
        body('pageSize').exists().withMessage("Not exist").isNumeric().withMessage('Not a Number').isInt({min:1,max:200})
    ];

    return v as never;
}


router.post('/addPitch',
    //uploadingMiddleWare(req, res, next, { folderName: SAVE_TO_FOLDER.FCN, maxFileCount: 1, fileTypeConfig: [{ allowedMineTypes: getImagesMimeTypes(), maxFileCount: 1, maxSizeOfOneFile: FILE_SIZE.FCN_NEWS_IMAGE_MAX_SIZE}, { allowedMineTypes: getPdfMimeTypes(), maxFileCount: 1, maxSizeOfOneFile:FILE_SIZE.FCN_NEWS_PDF_MAX_SIZE}]});

    [].concat(basicInfoValidations()),
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            if (requestHandler.hasValidationError(req, next)) {
                return;
            }

            let input = requestHandler.getBody(req) as ApiPitch.CreatePitchRequest;

            // let uploadFile = requestHandler.uploaddata(req);
            // if (uploadFile && uploadFile.length) 
            // {
            //     input.file  = uploadFile[0].dbpath;
            // }

            let r = await pitch.createPitch(input);

            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });




    router.post('/getAll',paginationValidations(),
    async (req: express.Request, res: express.Response, next: express.NextFunction) =>
    {
        try
        {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }

            let input = requestHandler.getBody(req) as ApiPitch.GetAllPitchRequest;
          //  let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);

            let r = await pitch.getAllPitch(input);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e)
        {
            next(e);
        }
    });


    router.put('/:_id',
    [
       param("_id").custom((_id)=>
       {
           if(_id)
           {
            return mongoose.Types.ObjectId.isValid(_id);
           }
           else
           {
            return true;
           }
       }).withMessage("INVALID"),
       body("status").exists().withMessage("NOT_FOUND").isString().withMessage("NOT_string").trim(),
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try 
        {
            if (requestHandler.hasValidationError(req, next))
            {
                return;
            }
            let _id = requestHandler.getParams(req) as string;
            let status = requestHandler.getBody(req) as string
            //let payload: jwtTypes.LOGIN_PAYLOAD = await requestHandler.getPayload(req);
          
            let input:ApiPitch.UpdatePitchRequest={
                _id:_id,
                status:status
            }
            let r = await pitch.DeletePitchWork(input,"Admin");
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    });



    export { router as PitchYourWork };



