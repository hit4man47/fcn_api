import * as express from 'express';
import * as forgetPassword from '../../controllers/forgetPassword';

import { check, body, validationResult, query, param } from 'express-validator';

import { ERROR_CODES } from "../../utils/constants";
import * as responseHandler from '../../init/responseHandler';
import * as requestHandler from '../../init/requestHandler';

let router = express.Router();
router.post('/forgetPassword',
    [
        body('userNameOrEmailId').exists().withMessage("not found").isLength({ min: 6, max: 256 }).withMessage("length not right").isString().withMessage("invalid EmailId or UserName")
    ],
    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            console.log("Forget Route");
            const ipAddress: string = (req as any).ipAddress;
            if(requestHandler.hasValidationError(req,next))
            {
                return;
            }  

            let input = requestHandler.getBody(req) as ForgetPassword.ForgetPasswordRequest;

            let r = await forgetPassword.forgetPassword(input, ipAddress);
            responseHandler.sendSuccessResponse(res, r);
        }
        catch (e) {
            next(e);
        }
    }
)

// router.get('/verifyUser/:tempToken',
//     [
//         param('tempToken').exists().withMessage("not found").isString().withMessage('invalid token')
//     ],
//     async (req: express.Request, res: express.Response, next: express.NextFunction) => {
//         try {
//             if(requestHandler.hasValidationError(req,next))
//             {
//                 return;
//             }  

//             let input = requestHandler.getParams(req) as ForgetPassword.VerifyUserRequest;
//             let payload =await requestHandler.getPayload(req)            

//             let r = await forgetPassword.verifyUser(input,payload);
//             responseHandler.sendSuccessResponse(res, r);
//         }

//         catch (error) {
//             next(error);
//         }
//     })

// router.put('/resetPassword/:tempToken',
//     [
//         body('password').exists().withMessage("not found").isLength({ min: 8, max: 20 }).withMessage("length not right").isString().withMessage("invalid password"),
//         param('tempToken').exists().withMessage("not found").isString().withMessage('invalid token')
//     ],
//     async (req: express.Request, res: express.Response, next: express.NextFunction) => {
//         try {
//             if(requestHandler.hasValidationError(req,next))
//             {
//                 return;
//             }  

//             let input: ForgetPassword.ResetPasswordRequest = {
//                 tempToken: requestHandler.getParams(req).tempToken,
//                 password: requestHandler.getBody(req).password
//             }
//             let payload =await requestHandler.getPayload(req)            

//             let r = await forgetPassword.resetPassword(input,payload);
//             responseHandler.sendSuccessResponse(res, r);
//         }
//         catch (e) {
//             next(e);
//         }
//     }
// )

export { router as forgetPasswordRouter };
