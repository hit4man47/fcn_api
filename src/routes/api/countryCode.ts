
import * as express from 'express';
import * as path from 'path'

const router = express.Router();

router.get('/',

    async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try
        {
            let location = path.join(__dirname ,"/../../../src/utils/countryCode.json");
            res.sendFile(location);    
        }
        catch (error) 
        {
            next(error);
        }
    }
);

export {router as countryCode}