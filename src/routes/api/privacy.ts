
import * as express from 'express';
import * as privacyController from '../../controllers/privacyController';
import { check, body, validationResult, query, param } from 'express-validator';


const router = express.Router();

// router.get('*', (req, res)=>{
//     console.log(uuidv3('hello.example.com', uuidv3.DNS));
//     res.send(uuidv3('hello.example.com', uuidv3.DNS))
// })
//route to get all questions
router.get('/', privacyController.getPrivacy);

//route to get question by id
router.post('/',
    body('content').exists().withMessage('Please provide content')
    , privacyController.updatePrivacy);

export { router as privacy };


