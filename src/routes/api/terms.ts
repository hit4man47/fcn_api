
import * as express from 'express';
import * as termsController from '../../controllers/termsController';
import { check, body, validationResult, query, param } from 'express-validator';

const uuidv3 = require('uuid/v3');

const router = express.Router();

router.get('/', termsController.getTermsOfUse);

router.post('/',
    body('content').exists().withMessage('Please provide content')
    , termsController.updateTermsOfUse);

export { router as terms };


