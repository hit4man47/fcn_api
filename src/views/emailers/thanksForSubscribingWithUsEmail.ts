import {getTemplate,getSalutation} from './common';


export function getHtml(subject:string,input:FCN.ThanksForSubscribing)
{
    return getTemplate(getEmailBody(input),subject);
}

function getEmailBody(input: FCN.ThanksForSubscribing)
{
  return `
  <table cellspacing="0" cellpadding="0">
  <tr>
    <td style="padding-left: 20px;"><p style="color:#676767; margin-bottom: 40px; margin-top: 30px;">Dear ${input.emailId},</p>
      <p style="color:#676767; margin-bottom: 30px;">Thank you for Subscribing to Feelium E-Contract Newsletter. </p>
      <p style="color:#676767; margin-bottom: 30px;">Now staying updated with the latest E-contracting and legal happenings is that much easier! </p>
      <p style="color:#676767; margin-bottom: 30px; line-height:40px;">We have recently updated our Knowledge Centre with Blogs and latest news on the Essentials of an E-Contract and its Legal Framework.  </p>
      ${getSalutation()}
        
      <p style="color:#676767; margin-bottom: 40px;">Note: To unsubscribe <a href="" style="color:#676767" >click here</a></p></td>
      
  </tr>
</table>
  `;
}