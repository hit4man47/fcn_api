import {getTemplate,getSalutation} from './common';

interface SendOTPOnMail
{
  clientName:string,
  otp: string
}

export function getHtml(subject:string,input:SendOTPOnMail)
{
    return getTemplate(getEmailBody(input),subject);
}


function getEmailBody(input: SendOTPOnMail)
{
return `
    table cellspacing="0" cellpadding="0" style="width: 100%; background: #0c588a;padding: 20px;">
        <tr>
          <td><a href="#"><img src="http://www.feeliumecontract.com/lib/images/final-logo.png" alt="" style="width:120px; height: 120px;" /></a></td>
          <td style="text-align: right;"><h3 style="color:#fff; font-size: 28px; margin-bottom: 15px;">OTP for Signature Verification  </h3></td>
        </tr>
      </table>
      <!--end table-->
      
      <table cellspacing="0" cellpadding="0">
        <tr>
          <td style="padding-left: 20px;"><p style="color:#676767; margin-bottom: 40px; margin-top: 30px;">Dear ${input.clientName}</p>
            <p style="color:#676767; margin-bottom: 30px; line-height:40px;">Thank you for uploading your signature. Please enter the below OTP i.e. One Time Password on our website to authenticate the same </p>
            <p style="color:#676767; margin-bottom: 30px;">${input.otp} </p>
		
            <p style="color:#676767; margin-bottom: 30px;">You will receive a different code on your registered mobile no. for authentication purposes.


 </p>

        ${getSalutation()}
    
        </td>
  </tr>
</table>  
  `;

}

