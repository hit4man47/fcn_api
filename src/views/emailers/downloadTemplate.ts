import {getTemplate,getSalutation} from './common';

interface DownloadTemplateEmail 
{
    clientName : string
}

export function getHtml(subject:string,input: DownloadTemplateEmail)
{
    return getTemplate(getEmailBody(input),subject);
}


function getEmailBody(input: DownloadTemplateEmail)
{
  return `
  <table cellspacing="0" cellpadding="0">
  <tr>
    <td style="padding-left: 20px;"><p style="color:#676767; margin-bottom: 40px; margin-top: 30px;">Dear ${input.clientName},</p>
      <p style="color:#676767; margin-bottom: 30px; ">Please download your  attachments.</p>
      <p style="color:#676767; margin-bottom: 30px; line-height:40px">Our platform was conceptualized to provide individuals and businesses with a digital medium of contracting. We offer an extensive library <a href="link" target="_blank"  style="color:#0c588a;text-decoration: underline;">link</a>
      that caters to all your personal and business contracting needs.</p>
       <p style="color:#676767; margin-bottom: 30px; line-height:40px;">
While our team is getting in touch with you, check out our Knowledge Centre <a href="link" target="_blank"  style="color:#0c588a;text-decoration: underline;">link</a>
to learn more about E-Contracts. 
</p>
        <p style="color:#676767; margin-bottom: 30px;">Our Team will get in touch with you at the earliest.</p>
        ${getSalutation()}

      </td>
  </tr>
</table>
  `;
}
