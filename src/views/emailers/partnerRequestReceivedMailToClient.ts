import {getTemplate,getSalutation} from './common';



export function getHtml(subject:string,input: string)
{
    return getTemplate(getEmailBody(input),subject);
}

function getEmailBody(input: string)
{
    return `
     
    <table cellspacing="0" cellpadding="0">
    <tr>
      <td style="padding-left: 20px;"><p style="color:#676767; margin-bottom: 40px; margin-top: 30px;">Dear ${input}</p>
        <p style="color:#676767; margin-bottom: 30px; line-height:40px;">Thank you showing interest in Partnering with Feelium E-contracts as (selected category).  </p>
        <p style="color:#676767; margin-bottom: 30px;">We care about our Partners and offer them multiple benefits of associating with us: </p>
         <p style="color:#676767; margin-bottom: 30px; margin-left:15px;">.	Commission based earning<br /><br />
.	Work from Home<br /><br />
.	Branding and technical support<br /><br />
.	Choice to work on your expertise area<br /><br />
.	Promote E-contract and earn returns
</p>
          <p style="color:#676767; margin-bottom: 30px;">Our Team will get in touch with you at the earliest.</p>
            ${getSalutation()}
       
        
        </td>
    </tr>
  </table>
    `
}