import {getTemplate,getSalutation} from './common';

interface SuccessMail
{
  clientName:string,
}

export function getHtml(subject:string,input:SuccessMail)
{
    return getTemplate(getEmailBody(input),subject);
}

function getEmailBody(input: SuccessMail)
{
  return `
  <table cellspacing="0" cellpadding="0">
  <tr>
    <td style="padding-left: 20px;"><p style="color:#676767; margin-bottom:30px; margin-top: 30px;">Dear ${input.clientName},  Thank you for Signing up!</p>
 <p style="color:#676767; margin-bottom: 30px;">Welcome Aboard!<p>
      <p style="color:#676767; margin-bottom: 30px;"> Feelium Econtracts is the One-Stop-Shop for all Contracting needs!</p>
      <p style="color:#676767; margin-bottom: 30px;line-height:40px;	">We are so happy to see you progressing towards a 21st century method of contracting. Our platform offers you an extensive library of legal documents for all your contracting needs. 

</p>
      <p style="color:#676767; margin-bottom: 15px;">With Feelium E-Contracts you can:</p>
      <p style="color:#676767; margin-bottom: 30px; margin-left: 15px;">·  Create a contract using our templates<br />
        <br />
        ·       Upload your KYC documents<br />
        <br />
        ·       Digitally Sign the Contract <br />
        <br />
        ·      Share and Chat with the other Parties for discussion </p>
      <p style="color:#676767; margin-bottom: 30px;">We look forward to helping you adapt to the Digital Contracting World.</p>
 <p style="color:#676767; margin-bottom: 30px;">Watch our explainer video and start creating legally binding contracts in minutes.</p>
    
     
      ${getSalutation()}
     
 </td>
  </tr>
</table>  
  `;
}

