interface SocialMedia {
    src: string,
    href: string
}

interface EmailerPath {
    econLogo: SocialMedia,
    facebook: SocialMedia,
    instagram: SocialMedia,
    linkedin: SocialMedia,
    twitter: SocialMedia
}

/*
https://img.icons8.com/officel/16/000000/facebook-new.png
https://img.icons8.com/cute-clipart/50/000000/instagram-new.png
http://www.feeliumecontract.com/lib/images/final-logo.png
*/

const pathList: EmailerPath =
{
    econLogo: { src: "http://www.feeliumecontract.com/lib/images/final-logo.png", href: 'http://econ.panaeshacapital.co.in/' },
    facebook:
    {
        src: "https://mpng.pngfly.com/20180330/dqe/kisspng-social-media-facebook-blog-computer-icons-linkedin-facebook-5abe0679295037.2876115315224029371692.jpg",
        href: "https://www.facebook.com/feeliumcontract/"
    },
    instagram:
    {
        src: "https://p7.hiclipart.com/preview/477/609/118/logo-computer-icons-clip-art-instagram-logo-thumbnail.jpg",
        href: "https://www.instagram.com/feeliumecontract/"
    },

    linkedin:
    {
        src: "https://carlisletheacarlisletheatre.org/images/linkedin-logo-png-circle-2.jpg",
        href: "https://www.linkedin.com/company/feelium-e-contract/"
    },

    twitter:
    {
        src: "https://png.pngtree.com/png-clipart/20190516/original/pngtree-twitter-logo-icon-png-image_3570310.jpg",
        href: "https://twitter.com/feeliumcontract"
    }
}



export function getHeader(title: string) {
    return `
    
    <table cellspacing="0" cellpadding="0" class="em-tbl-header">
        <tr>
          <td><a href="#"><img src="${pathList.econLogo}" alt="" class="em-logo" /></a></td>
          <td style="text-align: right;"><h3 class="em-title">${title}
		
</h3></td>
        </tr>
      </table>
    `
}


export function getHeaderWithCompanyDetails(input: any) {
    return `
    <td style="padding: 20px;"><table cellspacing="0" cellpadding="0" style="width: 100%">
        <tr>
          <td><a href="${pathList.econLogo.href}"><img src="${pathList.econLogo.src}" alt="" style="width:120px; height: 120px;" /></a></td>
          <td style="text-align: right;"><span style="font-size:12px; color:#676767;">Original For Recipient</span>
            <h3 style="color:#0c588a; font-size: 28px; margin-bottom: 15px;">Panaesha Capital Pvt Ltd</h3>
            <p style="font-size: 15px; color: #2b2b2b;">D-16/3, D Block, Connaught Place <br />
              New Delhi, 110001</p>
            <p style="font-size: 15px; color: #2b2b2b;">GSTIN:07AAJCP7392J1ZS</p>
            <p style="font-size: 15px; color: #2b2b2b; margin-bottom: 10px;"><a href="#" style="color:#0c588a;">www.feeliumecontract.com</a></p>
            <p style="font-size: 15px; color: #2b2b2b;">INVOICE NO. #PCPL/001</p>
            <p style="font-size: 15px; color: #2b2b2b;">Date:16/10/2019</p></td>
        </tr>
      </table>
    `
}

export function getFooter() {
    const x = `
    <table cellspacing="0" cellpadding="0" class="em-tbl-footer">
        <tr>
          <td style="text-align:center;"><p class="copyright">Copyright @ Feelium E-contract All Right Reserved.</p>
            <p class="links"><a href="#" class="linktag">Contact Us</a> I <a href="#" class="linktag">Terms Of Use</a> I <a href="#" class="linktag">Privacy Policy</a></p>
            <ul class="footer-list">
              <li class="footer-list-item"><a href="${pathList.instagram.href}" target="_blank"><img src="${pathList.instagram.src}" class="img-icon" alt=""></a> </li>
              <li class="footer-list-item"><a href="${pathList.facebook.href}" target="_blank"><img src="${pathList.facebook.src}" class="img-icon" alt=""></a> </li>
              <li class="footer-list-item"><a href="${pathList.linkedin.href}" target="_blank"><img src="${pathList.linkedin.src}" class="img-icon" alt=""></a> </li>
              <li style=""><a href="${pathList.twitter.href}" target="_blank"><img src="${pathList.twitter.src}" class="img-icon" alt=""></a> </li>
            </ul></td>
        </tr>
      </table>
    `;
    // console.log('\n\n\n\n\n\nx: ', x);
    return x;
}






export function getStyles() {
    return `
    * {
        padding: 0px;
        margin: 0px;
    }
    @font-face {
        font-family: GothamRounded-Book;
        src: url(./fonts/GothamRounded-Book.otf);
        font-weight: 400;
    }
    body {
        font-family: GothamRounded-Book;
        font-size: 15px;
    }
    .em-table{
        width:900px; margin:auto;
    }
    .em-logo{
        width:120px; height: 120px;
    }
    .em-title{
        color:#fff; font-size: 28px; margin-bottom: 15px;
        line-height: 40px;
    }
    .em-tbl-header{
        width: 100%; background: #0c588a;padding: 20px;
    }
    
    .em-para{
        color:#676767;
    }
    .em-pl{
        padding-left: 20px;
    }
    .em-ml{
        margin-left: 15px;
    }
    .em-mt{
        margin-top: 30px;
    }
    .em-mb{
        margin-bottom: 30px;
    }
    .em-mr{
        margin-right: 30px;
    }
    .em-linehight{
        line-height: 40px;
    }
    .em-link{
        color:#0c588a;    text-decoration: underline;
    }
    .em-btn{
        background: #00c1f8;color: #fff; padding: 10px; border-radius: 10px;text-decoration: none;
    }
    .em-tbl-footer{
        width: 100%; background: #0c588a;padding: 20px;
    }
    .em-tbl-footer .copyright{
        color:#fff;font-size: 12px
    }
    .em-tbl-footer .links{
        color:#fff; margin-top: 10px; font-size: 10px;
    }
    .em-tbl-footer .links .linktag{
        color:#fff; text-decoration: none;
    }
    .footer-list{
        list-style-type: none;display: inline-flex; margin-top: 10px;
    }
    .footer-list-item{
        margin-right: 15px;
    }
    .img-icon{
        width: 20px;
        height: 20px
    }
    `;
}

export function getSalutation() {
    return `
            <p class="em-para em-mb">Thanks & Regards,<br /><br  />
              Feelium E-Contracts Team </p>
    `;
}



export function getTemplate(body: string, titleName: string) {
    return `
        <!doctype html>
        <html>
        <head>
        <meta charset="utf-8">
        <style>
        ${getStyles()}
        </style>
        </head>

        <body>
        <table cellspacing="0" cellpadding="0" border="0"  class="em-table">
        <tr>
            <td>
            ${getHeader(titleName)}
            ${body}
            ${getFooter()}
            </td>
            <!--end main td--> 
        </tr>
        </table>
        </body>
        </html>`
}


export function getBillTemplate(companyHeader: any, body: string) {
    return `
        <!doctype html>
        <html>
        <head>
        <meta charset="utf-8">
        <style>
        ${getStyles()}
        </style>
        </head>

        <body>
        <table cellspacing="0" cellpadding="0" border="0" style="width:900px; margin:auto;">
        <tr>
            <td>
            ${getHeaderWithCompanyDetails(companyHeader)}
            ${body}
            </td>
            <!--end main td--> 
        </tr>
        </table>
        </body>
        </html>`
}



