import {getTemplate,getSalutation} from './common';


interface ESignatureSuccessResponse
{
    clientName: string
}

export function getHtml(subject:string,input:ESignatureSuccessResponse)
{
    return getTemplate(getEmailBody(input),subject);
}



function getEmailBody(input: ESignatureSuccessResponse)
{
return `
<td><table cellspacing="0" cellpadding="0" style="width: 100%; background: #0c588a;padding: 20px;">
<tr>
  <td><a href="#"><img src="images/feelium-econ-logo.svg" alt="" style="width:120px; height: 120px;" /></a></td>
  <td style="text-align: right;"><h3 style="color:#fff; font-size: 28px; margin-bottom: 15px;">E-Signature Successfully Uploaded Document Name.  </h3></td>
</tr>
</table>
<!--end table-->

<table cellspacing="0" cellpadding="0">
<tr>
  <td style="padding-left: 20px;"><p style="color:#676767; margin-bottom: 40px; margin-top: 30px;">Dear Member,</p>
    <p style="color:#676767; margin-bottom: 30px;">Your digital signature has been uploaded successfully to Document Name.  </p>
    
    <p style="color:#676767; margin-bottom: 30px;"> Thank you for using the most preferred contracting platform. </p>
    ${getSalutation()}
        </td>
  </tr>
</table>  
  `;

}


