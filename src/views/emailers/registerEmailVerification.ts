import { getTemplate, getSalutation } from './common';



export function getHtml(subject: string, input: ClientUser.EmailVerificationEmail) {
  return getTemplate(getEmailBody(input), subject);
}

function getEmailBody(input: ClientUser.EmailVerificationEmail) {
  return `
  <table cellspacing="0" cellpadding="0">
        <tr>
          <td class="em-pl"><p class="em-para em-mb em-mt">Dear ${input.clientName}</p>
		   <p class="em-para em-mb">Thank you for registering with Feelium E-Contracts. </p>
            <p class="em-para em-mb">Experience the ultimate contracting on our platform. </p>
            <p class="em-para em-mb">Just click on the below link to confirm your email and voila! You can start your Digital Contracting journey today! 

</p>
            <p class="em-para em-mb"><a href="${input.emailVerficationUrl}" target="_blank" style="color:#0c588a;text-decoration: underline;">${input.emailVerficationUrl}</a></p>
            <p class="em-para em-mb">Click on the button below to verify your mail</p>
            <p class="em-para em-mb"><a href="${input.emailVerficationUrl}" class="em-btn">Click here to Verify</a></p>

           
            ${getSalutation()}
       </td>
        </tr>
      </table>`;
}

