import * as nodemailer from 'nodemailer';
import { serverConfig } from '../../init/config';
import * as registerEmailVerification from './registerEmailVerification';
import * as registerSuccessEmail from './registerSuccessEmail';
import * as forgetPassword from './forgetPassword';
import * as thanksForPartneringWithUsEmail from './thanksForPartneringWithUsEmail';
import * as thanksForSubscribingWithUsEmail from './thanksForSubscribingWithUsEmail';
import * as thanksForContactingEmail from './thanksForContactingEmail';
import * as downloadTemplate from './downloadTemplate'
import * as sendOTPMail from './sendOTPMail'
import * as esignatureSuccessfullyUploaded from './esignatureSuccessfullyUploaded'

import * as  partnerRequestReceivedMailToClient from './partnerRequestReceivedMailToClient'




var transporter = nodemailer.createTransport({
  host: serverConfig.smtpServer.host,
  port: serverConfig.smtpServer.port,
  secure: false, // true for 465, false for other ports
  auth:
  {
    user: serverConfig.smtpServer.user, // your domain email address
    pass: serverConfig.smtpServer.password // your password
  }
});

/***
 * resolve(success:bool)
 * never reject
 */
export function sendHtml(to: string, subject: string, message: any):Promise<boolean>
{
    return new Promise(function(resolve,reject)
    {
        var mailOptions =
        {
            from: serverConfig.smtpServer.sender.admin,
            to: to,
            subject: subject,
            html: message

        };
        transporter.sendMail(mailOptions, function (error, info)
        {
            
            if (error)
            {
                console.log("Email ERROR", error);
                resolve(false);
            }
            else
            {
                console.log('Email sent: ' + info.response + " to:" + to);
                resolve(true);
            }

        });
    })
  
}


// ==for attachment===========
export function sendHtmlWithAttachment(to: string, subject: string, message: any, filePath: string, filename: string):Promise<boolean>
{
    return new Promise(function(resolve,reject)
    {
        var mailOptions =
        {
            from: serverConfig.smtpServer.sender.admin,
            to: to,
            subject: subject,
            html: message,

            attachments: [
                {
                    filename: filename,
                    path: filePath
                }
            ]

        };
        transporter.sendMail(mailOptions, function (error, info)
        {
            
            if (error)
            {
                console.log("Email ERROR", error);
                resolve(false);
                return
            }
            else
            {
                console.log('Email sent: ' + info.response + " to:" + to);
                resolve(true);
                return
            }

        });
    })
  
}





export let SUBJECTS=
{
    REGISTER_EMAIL_VERIFICATION:"Verify your Email to Start Contracting at Feelium E-contracts",
    REGISTER_SUCCESS_EMAIL: "Welcome to Feelium E-Contract",
    KYC_APPROVED: "KYC Approved at Feelium Econtracts!",
    KYC_DISAPPROVED: "KYC DISAPPROVED at Feelium Econtracts!",
    FORGET_PASSWORD_EMAIL: "Reset Password to access your Feelium E-Contract Account!",
    INVOICE: "INVOICE",
    THANKS_FOR_CONTACTING_EMAIL: "Thank you for contacting Feelium E-contracts!",
    THANKS_FOR_PARTNERING_WITH_US_EMAIL:"Thank you for partnering with Feelium E-Contracts",
    THANKS_FOR_SUBSCRIBING_WITH_US_EMAIL:"Thank you for Subscribing with Feelium E-Contracts",
    DOWNLOAD_TEMPLATE_EMAIL: "Download Your E-contract Pdf Now",
    OTP_FOR_SIGNATURE_VERIFICATION:"OTP for Signature Verification",
    ESIGNATURE_SUCCESSFULLY_UPLOADED: "E-Signature Successfully Uploaded Document Name",
    REQUEST_FOR_LEGAL_ADVICE_SUCCESS_MAIL_TO_CLIENT: "Get Expert Legal Advice on Feelium E-contracts",
    REQUEST_FOR_E_SIGNATURE_FROM_ANOTHER_PARTY: "Request for E-Signature : ",
    CUSTOMIZATION_REQUEST_FOR_DOCUMENT_TO_USER: "Customization Request for ",
    CUSTOMIZATION_REQUEST_FOR_DOCUMENT_TO_ADMIN: "Customization Request for ", 
    REQUEST_FOR_BECOME_PARTNER:"Partnering with Feelium E-contract was never more Easy!",
    REQUEST_FOR_ADVICE_FROM_ADVISIOR: "Request for your Advice for my Feelium E-contract"
}



export {registerEmailVerification};
export {registerSuccessEmail};
export {forgetPassword}
export {thanksForPartneringWithUsEmail};
export {thanksForSubscribingWithUsEmail};
export {thanksForContactingEmail};
export {downloadTemplate};
export {sendOTPMail};
export {esignatureSuccessfullyUploaded};
export {partnerRequestReceivedMailToClient};