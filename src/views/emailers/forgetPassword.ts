import {getTemplate,getSalutation} from './common';



export function getHtml(subject:string,input:ClientUser.ForgetPasswordEmail)
{
    return getTemplate(getEmailBody(input),subject);
}

function getEmailBody(input:ClientUser.ForgetPasswordEmail)
{
  return `
  <table cellspacing="0" cellpadding="0">
  <tr>
    <td style="padding-left: 20px;"><p style="color:#676767; margin-bottom: 40px; margin-top: 30px;">Dear ${input.clientName},</p>
      <p style="color:#676767; margin-bottom: 30px;">We see you are trying to reset your account password. Click on the link below to reset your password.  </p>
      <p style="color:#676767; margin-bottom: 30px;"><a href="${input.forgetPasswordLink}" target="_blank"  style="color:#0c588a;text-decoration: underline;">${input.forgetPasswordLink}</a></p>
         <p style="color:#676767; margin-bottom: 40px;">For any other related issue contact us at: <a href="${input.contactUsLink}" target="_blank"  style="color:#0c588a;    text-decoration: underline;">${input.contactUsLink}</a></p>
         ${getSalutation()}
   </td>
  </tr>
</table>
  `;
}

