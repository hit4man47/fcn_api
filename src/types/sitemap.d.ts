declare namespace SiteMap {
    interface UrlData {
        loc: string,
        lastmod: number,
        changefreq: string
    }
}