declare namespace COMMENT {

    //post api input body
        export interface QUERYDATA {
            name: string,
            emailId: string,
            comment: string
        }
        export interface SubComment {
           
            comment: string
        }
        //post api input param
        export interface QUERYNEWSID {
            newsId: string,
            commentId:string
        }
        interface addComment{
            newsId:string,
        }
        export interface AddNewsQuery {
            name: string,
            newsId: string,
            emailId: string,
            comment: string,
            subComment?:string,
           // readStatus:string
    
        }
     
        export interface AddSubcomment extends AddNewsQuery {
            commentType:number
           
    
        }
        //get api parmas with access token of admin
        export interface NEWSQUERYID
        {
            newsQueryId:string,
            record:string,
            offset:string
        }
    
        export interface newsComment
        {
            newsId:string,
            record:string,
            offset:string
        }
    
        interface deleteComent{
            id:string
        }
        interface CommentData{
            comment:any,
            count:number
        }
        interface CommentUpdateId{
            id:string,
        }
        interface CommentUpdateStatus{
            status:string
        }
        interface CommentUpdate{
            id:string,
            status:string
        }
      
    }