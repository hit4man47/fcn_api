

declare namespace ChangePassword {

    export interface ChangePasswordRequest {
        payload: string,
        currentPassword: string,
        newPassword: string

    }


    export interface PasswordRequest {
        currentPassword: string,
        newPassword: string
    }
    export interface AdminChangePassword {
        id: string,
        newPassword: string
    }

    export interface CreateTokenRequest {
        userName: string,
        emailId: string,
        userId: number,
        source: number,

    }

    //changePassword
    interface currentPasswordData {
        passwordHash: string
    }
    //changePassword
    interface UpdatePasswordInput {
        newPassword: string,
        userName?: string,
        emailId: string
    }

    interface BlackList {
        userId: string,
        currentTimeStamp: number
    }




}