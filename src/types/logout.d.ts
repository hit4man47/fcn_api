declare namespace Logout {
    interface LogoutInput {
        loginStatus: boolean,
        logoutStatus: boolean,
        logoutTimeStamp: number,
        logoutIpAddress: string
    }

    interface BlacklistToken {
        loginHistoryId: string
        userId: string,
        logoutTimeStamp: number
    }
}