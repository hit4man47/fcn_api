import { Pagination } from "./types";
import { GetAllCategorySortFilters, Category, AddCategoryInput, GetAllCategoryWhereFilters, GetAllCategoryOutput, AddLabelInput } from "models/Category";

export declare namespace ApiCategory
{
   
    interface CreateOrUpdateCategoryRequest extends AddCategoryInput
    {
        _id?:string,
        userCommitMessage?:string
    }
    interface PublichCategory {
        categoryId:string,
    }
    interface CreateOrUpdateCategoryResponse extends Category.CategoryExposedSchema
    {
    }

    interface GetCategoryByIdResponse extends Category.CategoryExposedSchema
    {}
    interface GetCategoryByIdRequest
    {
        categoryId:string,
        
    }
    interface GetCategoryByParentIdRequest
    {
        parentId:string,
        
    }

    interface GetAllCategoryRequest extends Pagination
    {
        whereFilters: GetAllCategoryWhereFilters,
        sortFilters: GetAllCategorySortFilters
    }

    interface GetAllCategoryResponse extends GetAllCategoryOutput,Pagination
    {}
    interface GetCategoryResponseByParentId extends Omit<GetAllCategoryOutput, "hasMore">
    {}

    interface AddLabel extends AddLabelInput{ }
    interface GetLabelByCategoryId
    {
        categoryId:string,
        
    }
}