declare namespace Team {
    interface ITeamRequest {
        name: string,
        photoUrl: string,
        designation: string,
        message: string,
        profiles: Array<IProfileData>
    }

    interface IProfileData {
        profile: string,
        url: string
    }
    interface TeamUpdateRequest {
        teamId: string,
        name: string,
        designation: string,
        message: string,
    }
    interface id{
        id:string
    }

}