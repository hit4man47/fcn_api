

declare namespace VerifyAccount
{
    interface CheckTokenRequest
    {
        token:string
    }

    interface VerifyTokenRequest
    {
        token:string,
        password:string
    }

    interface TokenInterface
    {
        emailId:string,
        resetPwdId?:string,
        tempUserId?:string,
        type:string,
        isPasswordSet:boolean,
        title:string,
        infoMessage:string
    }


    interface CheckTokenResponse    
    {
        canProceed: boolean,
        isPasswordSet: boolean,
        title: string,
        errorMessage ?: string,
        infoMessage : string 
    }
    

    interface VerifyTokenResponse    
    {
       access_token:string,
       refresh_token:string
    }

}