import * as express from 'express';


export interface U {
    name: string,
    fullPath: string,
    tag: string,
    typeOfFileUploaded:string,
    dbpath: string
}

export interface myRes {
    name : string,
    fullPath: string,
    tag: string,
    dbpath : string,
    resizedImage ?: Array<string>
}

export interface MyExpressRequest extends express.Request {
    // uploadData?: U[]
    extendedData:
    {
        uploadData ?: myRes[],
        ipAddress:string,
        incomingTime:number,
        payload:any,
    }    
}

export interface UploadFolderRequest
{
    folderFullPath: string
    folderName: string,
}
