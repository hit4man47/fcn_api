declare namespace LoginAuthentication {

    interface LoginRequest {
        userNameoremailId: string,
        password: string
    }

    interface RefreshTokenRequest {
        refreshToken: string
    }




    // Permission of users
    interface InputPermissions {
        legalDoc: string,
        newsRoom: string,
        lawBook: string,
        users: string
        memberships: string,
        testimonial: string,
        team: string,
        faq: string,
        packages: string,
        category: string,
        kyc: string,
        blog: string
    }
    // Insert data of Client User


    //local logi

    interface LoginHistoryInput {
        userId: string,
        loginIpAddress: string,
        loginStatus: boolean,
        logoutStatus: boolean,
        loginTimeStamp: number,
        logoutTimeStamp: number,


    }

    interface LoginHistoryData {
        _id: string,
        userId: string,
        loginIpAddress: string,
        loginStatus: boolean,
        logoutStatus: boolean,
        loginTimeStamp: number,
        logoutTimeStamp: number

    }
    export interface UserObjectData {
        userName: string;
        emailId: string;
        source: number;
        userId: string;
        name: string;
        role: string,
        ipAdrres: string,
        permission: InputPermissions
    }
    export interface JWT_LOGIN_VERIFICATION_PAYLOAD {
        userId: string; //(10^7)
        userName: string,
        name: string,
        source: number,
        emailId: string,
        ipaddress: string,
        role: string,
        permission: string,
        phoneNumber: string,
        loginHistoryId: string
    }

}



