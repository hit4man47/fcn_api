declare namespace Register {
    //createUser
    interface CreateUserRequest {
        name: string,
        userName: string,
        emailId: string,
        countryCode: string,
        phoneNumber: string,
        password: string
    }

    interface VerifyEmailRequest {
        tempToken: string
    }

    interface CreateUserOAuthRequest {
        name: string,
        emailId: string,
        ipAddress: string
    }

}