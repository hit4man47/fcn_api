declare namespace ClientUser {

//adminRegister,createuser
  
    //client user, adminRegister
    interface ClientUserDataRequest {
        name: string,
        countryCode: string,
        emailId: string,
        phoneNumber: string,
        designation?: string,
        userName?: string,
        password: string,
        avatar?: string,
        role: string,
        notification?: boolean,
        permission?: string
    }
    interface AdminUserDataRequest
    {
        name: string,
     //   countryCode: string,
        emailId: string,
        phoneNumber: string,
      //  designation?: string,
        userName?: string,
        password?: string,
        avatar?: string,
        role: string,
      //  notification?: boolean,
        permission?: string
    }
    interface JWT_EMAIL_VERIFICATION_PAYLOAD 
    {
        TempClientUserId: string
       // userName:string
    }
    interface JWT_FORGET_PASSWORD_VERIFICATION_PAYLOAD
    {
        isPasswordSet:string
         userName:string
    }
  
//verifyEmail
    interface VerifyEmailRequest {
        token: string
    }


    interface InputClientUserData {
        userName: string,
        name: string,
        countryCode: string,
        phoneNumber: string,
        passwordHash?: string|null,
        emailId: string,
        emailIdStatus: number,
        createTimeStamp: number,
        updateTimeStamp: number,
        phoneNumberStatus: number,
        source: number,
        business_Id?: String,
        role: string,
        designation?: string,
       // type: string,
        createBy: string | null,
        status: number,
        refreshToken: string,
        refreshTokenExpiry: number,
        allowGoogle: boolean,
        allowFacebook: boolean,
        ipAddrress: string,
        avatar?: string,
        permission:  string
    }


//admin
    interface UpdateClientUserData {
        userName: string,
        name: string,
        countryCode: string,
        phoneNumber: string,
       // passwordHash?: string,
        emailId: string,
     //   emailIdStatus: number,
        createTimeStamp: number,
        updateTimeStamp: number,
        phoneNumberStatus: number,
        source: number,
        business_Id?: String,
        role: string,
        designation?: string,
     //   type: string,
        createBy: string | null,
        status: number,
     //   refreshToken: string,
      //  refreshTokenExpiry: number,
       // allowGoogle: boolean,
        //allowFacebook: boolean,
        ipAddrress: string,
        avatar?: string,
       // permission: string
    }
  export interface GETUSERIDPARAM
  {
      id:string
  }

    interface UpdateUserDetails {
       
        name: string,
        emailId:string,
        phoneNumber: string,
       
    }
    interface updateUserData extends UpdateUserDetails{
        updateTimeStamp: number,
        emailIdStatus: number,
        phoneNumberStatus: number
    }

    
    export interface ClientUserPayloadId
    {
        userId:string;
    }
    interface OutputUserRoleData {
        _id: number,
        userName?: string,
        name: string,
        countryCode: string,
        phoneNumber: string,
        emailId: string,
        emailIdStatus: number,
        phoneNumberStatus: number,
        source: number,
        business_Id?: String,
        role: string,
        designation?: string,
     //   type: string,
        createBy: string | null,
        status: number,
        allowGoogle: boolean,
        allowFacebook: boolean,
        avatar?: string,
        permission: string

    }



    interface UpdateAdminPermission {
        _id: string,
        permission: string
    }
    
    interface UpdateUserPermission {
        userId: string,
        newPermission:string,
       // userRole:string,
    }

    //interface for datatables
    interface IGetUserDataTable{
        draw: number,
        findQuery: object,
        sort: object,
        limit: number,
        skip: number
    }

    interface IGetUserDT{
        count: number,
        data: object
    }


    interface  TemplateEmailVerificationLink
    {
        verficationUrl: any,
        name: string,
    }

    interface ForgetPasswordEmail
    {
        clientName: string,
        forgetPasswordLink: string, 
        contactUsLink: string
    } 

    interface EmailVerificationEmail
    {
        emailVerficationUrl: string,
        clientName: string,
    }
    export interface CHECKUNIQUESNESS
    {
        emailId:string,
        role:string,
        newUserName:string,
        phoneNumber:string
    }
    export  interface OutputUserIdData {
        _id: number,
        userName: string,
        name: string,
        countryCode: string,
        phoneNumber: string,
        emailId: string,
        phoneNumberStatus: number,
        source: number,
        role: string,
        status: number,
        avatar?: string,
        permission: string

    }
}