declare namespace ForgetPassword
{
    interface ForgetPasswordRequest
    {
        userNameOrEmailId:string
    }


    interface GetUserDetailsByEmailOrUserNameResponse 
    {
        userId:number
        emailId:string,
        phoneNumber:string,
        emailIdStatus:number,
        phoneNumberStatus:number
    }

    interface VerifyUserRequest
    {
        tempToken:string
    }

    interface ResetPasswordRequest
    {
        tempToken:string,
        password:string
    }



//forgetPassword
 interface FindEmailOrUserNameInput {
    userNameOrEmailId: string
}


//forgetPassword
interface GetUserDetailsByEmailOrUserNameData {
    userId: string,
    emailId: string,
    userName: string,
    phoneNumber: string,
    emailIdStatus: number,
    phoneNumberStatus: number,
    role: string
}

//forgetPassword
 interface GetUserDetailsByIdData {
    userId: string,
    userName: string,

}



interface TemplateforgetPasswordObj{
    name: string,
    forgetPasswordUrl: string
}

}