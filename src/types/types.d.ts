export type SortType = -1 | 1 | 0
export interface Pagination {
    pageNumber: number,
    pageSize: number
}