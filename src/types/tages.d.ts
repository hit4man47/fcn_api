import {  Tages, AddTagesInput, GetAllTagesWhereFilters, GetAllTagesSortFilters, GetAllTagesOutput  } from "../models/tags";
import { Pagination } from "./types";
export declare namespace ApiTages
{
   
    interface CreateOrUpdateTagesRequest extends AddTagesInput
    {
        _id?:string,
       
    }

    interface GetAllTagesRequest extends Pagination
    {
        whereFilters: GetAllTagesWhereFilters,
        sortFilters: GetAllTagesSortFilters,
        
    }
    interface GetAllTagesResponse extends GetAllTagesOutput,Pagination
    {}
    interface DeleteTags
    {
        tagId:string,
        
    }
  
}