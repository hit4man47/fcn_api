import { Pagination } from "./types";

import { GetAllDataSortFilters, AddUpdateData, AddTDataInput, GetAllDataWhereFilters, GetAllDataOutput ,AddImpressionInput,UpdateDataInfo} from "models/AddUpdateData";
import { GetAllCategoryWhereFilters, GetAllCategorySortFilters } from "models/Category";

export declare namespace ApiAddUpdateData
{
   
    interface CreateOrUpdateDataRequest extends AddTDataInput 
    {
        id?:string,
        type:string,
       
    }
    interface ChangeStatus {
        id:string,
        type:string,
        status:number
    
    }
    interface CreateOrUpdateDataResponse extends AddUpdateData.AddUpdateDataExposedSchema
    {
    }

    interface GetDataByIdResponse extends AddUpdateData.AddUpdateDataExposedSchema
    {}
 interface GetDataByIdRequest
    {
        _id:string,
        type:string,
        pageData:1|0
    }
    interface GetAllDataRequest extends Pagination {
        whereFilters: GetAllDataWhereFilters,
        sortFilters: GetAllDataSortFilters,
        type: string
    }
    interface GetRelatedCategory extends Pagination {
        whereFilters: GetAllCategoryWhereFilters,
        sortFilters: GetAllCategorySortFilters
      
    }

    interface GetAllDataResponse extends GetAllDataOutput,Pagination
    {}
    interface GetRelatedCategoryResponse extends Omit<GetAllDataOutput ,"count">
    {
        category:string
    }


    interface AddImpressionRequest extends AddImpressionInput {}
  

    interface viewUpdate 
    {
        noOfView:number
    }
  
    interface queryParameter
    {
        per_page:number,
        page:number
    }
    interface Url
    {
        url:string,
        type:string,
        pageData:1|0
    }
}