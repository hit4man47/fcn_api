

declare namespace User
{
    interface CheckUserNameRequest
    {
        userName:string

    }


    interface CheckEmailIdRequest
    {
        emailId:string,

    }
  export interface GET_USER_INPUT
    {
        record: string,
        offset: string,
        sort?: string,
        order?: string,
        filter?: string
    }
    
}