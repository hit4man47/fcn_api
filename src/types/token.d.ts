declare namespace TempraryToken
{
    export interface BlockedToken
    {
        access_token:string
    }
    export interface TokenPayload
    {
        userId:number,
        userName:string,
        source:number,
        emailId:string,
        ipaddress:string,
        iat:number,
        exp:number,
        role:string,
        permission:string
    }
    export interface RoleBasedArray
    {
        role:string
    }
}