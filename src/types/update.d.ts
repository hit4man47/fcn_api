declare namespace UPDATE
{
   export interface UPDATE_RESPONSE
   {
       //client:any,
       access_token:string
   }
   export interface USER_CHANGE_LOG
   {
    oldEmailId:string,
    newEmailId: string,
    ipAdrres: string,
   }
}