import { Pitch, AddPitchInput, GetAllPitchWhereFilters, GetAllPitchSortFilters, GetAllPitchOutput } from "models/pitch";
import { Pagination } from "./types";


export declare namespace ApiPitch
{
   
    interface CreatePitchRequest extends AddPitchInput
    {}
    interface UpdatePitchRequest  {
    _id:string,
    status:string
    }

    interface GetAllPitchRequest extends Pagination
    {
        whereFilters: GetAllPitchWhereFilters,
        sortFilters: GetAllPitchSortFilters
    }
    interface GetAllPitchResponse extends GetAllPitchOutput,Pagination
    {}

}