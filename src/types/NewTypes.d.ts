
export interface ImageDimen
{
    height:number,
    width:number
}


export interface ImageResize
{
    ABOUT_TEAM:
    {
        SMALL:ImageDimen,
        MEDIUM:ImageDimen 
    },

    // ADD_OR_UPDATE:
    // {
    //     EDITOR_CHOICE: ImageDimen,
    //     RELATED_POST:ImageDimen
    // },

     // HomePage Most recent first: 350x150 // fcn focus
     NEWS_IMAGE: 
     {
        SMALL1: ImageDimen,
        SMALL2: ImageDimen,
        SMALL3: ImageDimen,
        SMALL: ImageDimen,
        MEDIUM:ImageDimen,
        LARGE1:ImageDimen,
        LARGE2:ImageDimen,
     }
    
    // NEWS_ROOM:
    // {
    //     MEDIUM:ImageDimen,
    //     LARGE:ImageDimen,
    //     SMALL:ImageDimen
    // },
    // TESTIMONIAL:
    // {
    //     SMALL:ImageDimen
    // }
}