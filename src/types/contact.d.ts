
declare namespace FCN {
    interface IContactRequest {
        categoryId?: string
    }

    interface CreateContactRequest {
        name: string,
        emailId: string,
        subject: string,
        reason: string,
        message: string,
    }
    interface CreatePartnerWithUsRequest {
        name: string,
        emailId: string,
        contactNumber: string,
        associationType: string,
        message: string,
    }

    interface CreateContactInput {
        name: string,
        emailId: string,
        subject: string,
        message: string,
        reason: string,
        ipAddress: string,
        addedOn: number
    }

    interface CreateContactData extends CreateContactInput {
        _id: string
    }

    interface CreatePartnerWithUsInput {
        name: string,
        emailId: String,
        contactNumber: String,
        associationType: String,
        message: string,
        ipAddress: string,
        addedOn: number
    }

    interface CreatePartnerWithUsData extends CreatePartnerWithUsInput {
        _id: string
    }

    interface CreateSubscribeWithUsRequest {
        emailId: string
    }

    interface CreateSubscribeWithUsInput {
        emailId: string,
        ipAddress: string,
        addedOn: number,
        subscriptionStatus: number
    }

    interface CreateLawbookSubscriperInput {
        emailId: string,
        ipAddress: string,
        subscriptionStatus: number
    }
    interface CreateLawbookSubscriperInputWithData extends CreateLawbookSubscriperInput{
        _id: string;
    }

    interface CreateSubscribeWithUsData extends CreateSubscribeWithUsInput {
        _id: string
    }

    interface GetAllContactUsDetailsRequest {
        record: string,
        offset: string
    }

    interface GetAllPartnerWithUsDetailsRequest extends GetAllContactUsDetailsRequest {

    }

    interface GetAllSubscribeWithUsDetailsRequest extends GetAllContactUsDetailsRequest {

    }

    interface ThanksForContacting extends CreateContactInput {

    }

    interface ThanksForPartnering extends CreatePartnerWithUsInput {

    }

    interface ThanksForSubscribing extends CreateSubscribeWithUsInput {

    }

    interface GetAllPartnerWithUsDetailsData {
        result: CreatePartnerWithUsData,
        count: number
    }

    interface GetAllSubscribeWithUsDetailsData {
        result: CreateSubscribeWithUsData,
        count: number
    }

    interface GetAllContactUsDetailsData {
        result: CreateContactData,
        count: number
    }
}
