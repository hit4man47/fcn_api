import mongoose, { Schema } from 'mongoose';
//add order 


const disclaimerSchema: Schema = new Schema({
    disclaimerId: { type: String, required: true, index: true, unique: true, default: 1 },
    content: { type: String, required: true },
    //addedBy: { type: String, required: true },
    //addedOn: { type: Date, default: Date.now },
    //editedBy: { type: String,required:true},
    //editedOn: { type: String, default: Date.now }
});


export default mongoose.model('Disclaimer', disclaimerSchema);
