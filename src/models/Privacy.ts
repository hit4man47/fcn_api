import mongoose, { Schema } from 'mongoose';
//add order 


const privacySchema: Schema = new Schema({
    privacyId: { type: Number, required: true, index: true, unique: true, default: 1 },
    content: { type: String, required: true },
    addedBy: { type: String, required: true },
    addedOn: { type: Date, default: Date.now },
    editedBy: { type: String },
    editedOn: { type: String, default: Date.now }
});

// Export the model and return your IUser interface
export default mongoose.model('Privacy', privacySchema);

