const mongoose = require('mongoose');

import * as bcrypt from '../utils/bcryptUtils';
import { CONSTANTS } from '../utils/constants';
//changePassword
export interface UpdatePasswordInput {
    newPassword: string,
    userName: string,
    emailId: string
}

//changePassword
export interface currentPasswordData {
    passwordHash: string
}

//forgetPassword
export interface FindEmailOrUserNameInput {
    userNameOrEmailId: string
}

//forgetPassword
export interface GetUserDetailsByEmailOrUserNameData {
    userId: number,
    emailId: string,
    userName: string,
    phoneNumber: string,
    emailIdStatus: number,
    phoneNumberStatus: number,
    role: string
}

//forgetPassword
export interface GetUserDetailsByIdData {
    userId: number,
    userName: string,
    emailId: string,
    phoneNumber: string,
    emailIdStatus: number,
    phoneNumberStatus: number,
    role: string
}

//local login
export interface UserObjectData {
    userName: string,
    emailId: string,
    passwordHash: string,
    source: number,
    userId: number,
    name: string
}

//register
export interface InsertTempUserData {
    timeStamp: number,
    userName: string,
    name: string,
    countryCode: string,
    phoneNumber: string,
    passwordHash: string,
    emailId: string,
}

//forgetPassword
export interface TempUserData {
    userName: string,
    name: string,
    countryCode: string,
    phoneNumber: string,
    passwordHash: string,
    emailId: string,
}

//register
export interface InsertUserData extends TempUserData {
    emailIdStatus: number,
    timeStamp: number,
    phoneNumberStatus: number,
    source: number,
    ipAddress: string,
    refreshToken: string,
    refreshTokenExpiry: number
}

export const TempUserSchema = new mongoose.Schema({
    userName: { type: String },
    name: { type: String },
    countryCode: { type: String },
    phoneNumber: { type: String },
    passwordHash: { type: String },
    emailId: { type: String },
})

export const UserSchema = new mongoose.Schema({
    userName: { type: String },
    name: { type: String },
    countryCode: { type: String },
    phoneNumber: { type: String },
    passwordHash: { type: String },
    emailId: { type: String },
    emailIdStatus: { type: String },
    timeStamp: { type: Date },
    phoneNumberStatus: { type: Boolean },
    source: { type: String },
    ipAddress: { type: String }
})
const tempUserModel = mongoose.model("TempUser", TempUserSchema);
const userModel = mongoose.model("User", UserSchema);

export function checkEmailExistDB(email: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        let exist = false;
        try {
            userModel.findOne({ 'emailId': email }, (err: any, user: any) => {
                if (err) {
                    reject(err);
                }
                console.log('user========>>>>>>>> ', user)
                if (user) {
                    exist = true;
                }
                resolve(exist);


            })

        }

        catch (e) {
            reject(e);
        }
    });
}
export function checkUserNameExistDB(userName: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        let exist = false;
        try {
            userModel.findOne({ 'userName': userName }, (err: any, user: any) => {
                if (err) {
                    reject(err);
                }
                if (user) {
                    exist = true;
                }

                resolve(exist);

            })
        }

        catch (e) {
            reject(e);
        }
    });
}
export function insertUserTempDB(userObj: InsertTempUserData): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let user = new tempUserModel(userObj)
            user.save((err: any) => {
                if (err)
                    reject(err)
                else
                    resolve(user);
            })
        }
        catch (e) {
            reject(e);
        }
    });
}
export function getTempUserByIdDB(tempUserId: string): Promise<TempUserData | null> {
    return new Promise(async (resolve, reject) => {
        try {
            tempUserModel.findById(tempUserId, (err: any, user: any) => {
                if (err)
                    reject(err)
                else {
                    resolve(user)
                }
            })
        }

        catch (error) {
            reject(error);
        }
    })
}
export function insertIntoUsersDB(input: InsertUserData): Promise<number> {
    return new Promise(async (resolve, reject) => {
        try {
            let user = new userModel(input)
            user.save((err: any) => {
                if (err)
                    reject(err)
                else
                    resolve(user);


            })
        }
        catch (e) {
            reject(e);
        }
    });
}
export function changePasswordDB(input: UpdatePasswordInput): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            // console.log("inputdata", input)

            var userNameExist = false;

            userModel.update({ 'emailId': input.emailId }, { 'passwordHash': input.newPassword }, (err: any) => {
                if (err)
                    reject(err)
                else {
                    userNameExist = true;
                }
                resolve(userNameExist);
            })

        }

        catch (e) {
            reject(e);
        }


    });
}

export function getUserPasswordDB(emailId: string): Promise<currentPasswordData | null> {
    return new Promise(async (resolve, reject) => {
        try {
            userModel.findOne({ 'emailId': emailId }, (err: any, doc: any) => {
                if (err)
                    reject(err)
                else {
                    resolve(doc)
                }
            })
        }

        catch (error) {
            reject(error)
        }
    })
}



export function getUserDatailsByEmailOrUserNameDB(input: FindEmailOrUserNameInput): Promise<GetUserDetailsByEmailOrUserNameData | null> {
    return new Promise(async (resolve, reject) => {
        try {
            userModel.findOne({ '$or': [{ 'userName': input.userNameOrEmailId }, { 'emailId': input.userNameOrEmailId }] }, (err: any, doc: any) => {
                if (err)
                    reject(err)
                else {
                    resolve(doc)
                }

            })
        }

        catch (e) {
            reject(e);
        }
    })
}



export function getUserByIdDB(userId: any): Promise<GetUserDetailsByIdData | null> {
    return new Promise(async (resolve, reject) => {
        try {
            userModel.findById(userId, (err: any, doc: any) => {
                if (err)
                    reject(err)
                else {
                    resolve(doc)
                }
            })
        }
        catch (e) {
            reject(e);
        }
    })
}

export function updatePasswordDB(password: string, userId: any): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let passwordHash = await bcrypt.getHash(password);
            userModel.findOneAndUpdate({ _id: userId }, { 'passwordHash': passwordHash }, (err: any) => {
                if (err)
                    reject(err)
                else
                    resolve(true)
            })
        }

        catch (e) {
            reject(e);
        }
    })
}


export function getUserDB(userNameOrEmail: string): Promise<UserObjectData | null> {
    return new Promise(async (resolve, reject) => {
        try {
            userModel.findOne({ '$or': [{ 'userName': userNameOrEmail }, { 'emailId': userNameOrEmail }] }, (err: any, doc: any) => {


                //userModel.findOne({'userName':userNameOrEmail},(err:any,doc:UserObjectData)=>{

                let userdata: UserObjectData;
                if (err) {
                    console.log("error = ", err);
                    reject(err);
                }

                else if (doc) {
                    userdata = doc;
                    resolve(userdata);
                }
                else {
                    resolve(null);
                }

            })

        }

        catch (e) {
            reject(e)
        }
    })
}
//********************************* */

export function validateRefreshTokenDB(input: string, time: number): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            userModel.findOne({ 'refreshToken': input }, async (err: any, doc: any) => {

                let exist = false;
                if (doc) {
                    console.log("inside")
                    let result = await validateRefreshTokenTimeDB(input, time);
                    exist = result;
                }
                console.log("outside")
                resolve(exist)
            })
        }
        catch (error) {
            reject(error);
        }

    })
}

// **************** INTERFACE FOR EXPIRY TIME OF REFRESH TOKEN **************
export interface RefreshExpiry {
    refreshTokenExpiry: number
}

// ****************  VALIDATION OF TIME VALIDITY OF REFRESH TOKEN  *************
// **************** CALLING THIS FUNCTION FROM VALIDATE REFRESH TOKEN DB ***********

export function validateRefreshTokenTimeDB(input: string, time: number): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            userModel.findOne({ 'refreshToken': input }, (err: any, doc: any) => {
                let exist = false;
                if (doc) {
                    console.log("inside time validation")
                    let { refreshTokenExpiry } = doc.refreshTokenExpiry;
                    let result: RefreshExpiry = { refreshTokenExpiry }

                    if (doc.refreshTokenExpiry >= time) {
                        console.log("inside time validation true conditon")
                        exist = true
                    }

                }
                resolve(exist)
            })
        }
        catch (e) {
            reject(e)
        }
    })
}

//******************* RETRIVE DATA FROM DATABASE FOR ACCESS TOKEN PAYLOAD IF REFRESH TOKEN VALIDATES ******** */


export function getUserDataDB(input: string): Promise<UserObjectData | null> {
    return new Promise(async (resolve, reject) => {
        try {
            userModel.findOne({ 'refreshToken': input }, (err: any, doc: any) => {
                if (doc) {
                    let { userName, emailId, passwordHash, source, userId, name } =
                    {
                        userName: doc.userName,
                        emailId: doc.emailId,
                        passwordHash: doc.passwordHash,
                        source: doc.source,
                        userId: doc._id,
                        name: doc.name
                    }

                    let result: UserObjectData =
                    {
                        userName,
                        emailId,
                        passwordHash,
                        source,
                        userId,
                        name
                    }
                    resolve(result);
                }
                else {
                    resolve(null);
                }
            })
        }
        catch (e) {
            reject(e)
        }
    })
}

//************ UPDATING REFRESH TOKEN AFTER VALIATION INTO THE DATABASE *********** */

export function updateRefreshTokenDB(input: string, userId: number): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let result = false;
            console.log(Date.now() + CONSTANTS.JWT_REFRESH_TOKEN_EXPIRY_SEC)

            userModel.findOneAndUpdate({ _id: userId }, { 'refreshToken': input, 'refreshTokenExpiry': Date.now() + CONSTANTS.JWT_REFRESH_TOKEN_EXPIRY_SEC * 1000 }, (err: any, doc: any) => {
                if (err) {
                    reject(err)
                }
                if (doc) {
                    result = true
                }
                resolve(true)
            })
        }
        catch (e) {
            reject(e)
        }
    })
}

export default userModel;