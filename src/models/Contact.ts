import mongoose, { Schema, mongo } from 'mongoose';

const contactUsSchema: Schema = new Schema({
    name: { type: String, required: true },
    emailId: { type: String, required: true },
    subject: { type: String, required: true },
    reason: { type: String, required: true },
    message: { type: String, required: true },
    ipAddress: { type: String, required: true }
}, {
    timestamps: true
});

const partnerWithUsSchema: Schema = new Schema({
    name: { type: String, required: true },
    emailId: { type: String, required: true },
    contactNumber: { type: String, required: true },
    associationType: { type: String, required: true },
    message: { type: String, required: true },
    ipAddress: { type: String, required: true }
}, {
    timestamps: true
});

const subscribeWithUsSchema: Schema = new Schema({
    emailId: { type: String, required: true, unique: true },
    ipAddress: { type: String, required: true },
    subscriptionStatus: { type: Number, required: true }
}, {
    timestamps: true
});

const lawBookScubscriptionSchema: Schema = new Schema({
    emailId: { type: String, required: true, unique: true, index: true },
    ipAddress: { type: String, required: true },
    status: { type: Boolean, default: true }
}, {
    timestamps: true
})

export const partnerWithUsModel = mongoose.model('PartnerWithUs', partnerWithUsSchema);
export const contactUsModel = mongoose.model('Contact', contactUsSchema);
export const subscribeWithUsModel = mongoose.model('SubscribeWithUs', subscribeWithUsSchema);
const lawBookScubscriptionModel = mongoose.model('LawbookSubscription', lawBookScubscriptionSchema);

export function createContactUsModel(input: FCN.CreateContactInput): Promise<FCN.CreateContactData> {
    return new Promise(async (resolve, reject) => {
        try {
            let contactUsData = new contactUsModel(input);
            resolve( await contactUsData.save() as any)
        }
         catch (error) 
        {
            reject(error);
        }
    })
}

export function createPartnerWithUsModel(input: FCN.CreatePartnerWithUsInput): Promise<FCN.CreatePartnerWithUsData> {
    return new Promise(async (resolve, reject) => {
        try {
            let partnerWithUsData = new partnerWithUsModel(input);

            resolve( await partnerWithUsData.save() as any)

        } catch (error) {
            reject(error);
        }
    })
}

export function createLawbookSubscriptionModel(input: FCN.CreateLawbookSubscriperInput): Promise<FCN.CreateLawbookSubscriperInputWithData> {
    return new Promise(async (resolve, reject) => {
        try {
            let sunscribeWithUsData = new subscribeWithUsModel(input);
           
            resolve ( await sunscribeWithUsData.save()  as any)

        } catch (error) {
            reject(error);
        }
    })
}

export function createSubscribeWithUsModel(input: FCN.CreateSubscribeWithUsInput): Promise<FCN.CreateSubscribeWithUsData> {
    return new Promise(async (resolve, reject) => {
        try {
            let sunscribeWithUsData = new subscribeWithUsModel(input);

            resolve( await sunscribeWithUsData.save() as any)

        } catch (error) {
            reject(error);
        }
    })
}

export function getContactWithUsDetailsModel(input: FCN.GetAllContactUsDetailsRequest): Promise<FCN.GetAllContactUsDetailsData> {
    return new Promise(async (resolve, reject) => {
        try {
            let record = parseInt(input.record);
            let offset = parseInt(input.offset);
            let count = await contactUsModel.countDocuments();
            let result: FCN.CreateContactData= contactUsModel.find().limit(record).skip(offset) as any
                let res = {
                    count,
                    result
                }
                resolve(res);
        }
        catch (e) {
            reject(e);
        }
    });
}

export function getPartnerWithUsDetailsModel(input: FCN.GetAllPartnerWithUsDetailsRequest): Promise<FCN.GetAllPartnerWithUsDetailsData> {
    return new Promise(async (resolve, reject) => {
        try {
            let record = parseInt(input.record);
            let offset = parseInt(input.offset);
            let count = await partnerWithUsModel.countDocuments();
            let result: FCN.CreatePartnerWithUsData= partnerWithUsModel.find().limit(record).skip(offset)as any;
            let res = {
                count,
                result
            }
            resolve(res);

        }
        catch (e) {
            reject(e);
        }
    });
}

export function getSubscribeWithUsDetailsModel(input: FCN.GetAllSubscribeWithUsDetailsRequest): Promise<FCN.GetAllSubscribeWithUsDetailsData> {
    return new Promise(async (resolve, reject) => {
        try {
            let record = parseInt(input.record);
            let offset = parseInt(input.offset);
            let count = await subscribeWithUsModel.countDocuments();
             subscribeWithUsModel.find().limit(record).skip(offset).exec((err: any, result: FCN.CreateSubscribeWithUsData) => {
                if (err) {
                    reject(err)
                }

                let res = {
                    count,
                    result
                }
                resolve(res);
            });

        }
        catch (e) {
            reject(e);
        }
    });
}

export function checkIfEmailIdExistsModel(emailId: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let exist = false;
            subscribeWithUsModel.findOne({ 'emailId': emailId }, (err: any, user: any) => {
                if (err) {
                    reject(err);
                }
                if (user) {
                    exist = true;
                    resolve(exist);
                }
                else {
                    resolve(exist);

                }
            })
        }
        catch (error) {
            reject(error);
        }
    })
}
export function checkIfEmailIdExistsLawbookSubscribeModel(emailId: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let exist = false;
            lawBookScubscriptionModel.findOne({ 'emailId': emailId }, (err: any, user: any) => {
                if (err) {
                    reject(err);
                }
                if (user) {
                    exist = true;
                    resolve(exist);
                }
                else {
                    resolve(exist);
                }
            })
        }
        catch (error) {
            reject(error);
        }
    })
}

