
import mongoose, { Schema } from 'mongoose';
import { COMMENT_STATUS,COMMENT_TYPES } from '../utils/constants';

var ObjectId = require('mongoose').Types.ObjectId;
const newsCommentSchema = new Schema(
    {
        newsId: { type: Schema.Types.ObjectId, ref: 'news' },
        name: { type: String, required: true },
        emailId: { type: String, required: true,lowercase:true },
        comment: { type: String, required: true },
        readStatus: { type: String, default: "UNREAD" },
        status: { type: String, default: "PENDING" },
        createdDate: { type: Number, default: Date.now() },
        commentType: { type: Number, default: 0 },
        updateDate: { type: Number },
        subComment: [{ type: Schema.Types.ObjectId, ref: 'comments', default: null }]
    },
    {
        timestamps: true
    }
)

export const CommentModel = mongoose.model('comments', newsCommentSchema)



export function addComment(input: COMMENT.AddNewsQuery): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let CommentData = new CommentModel(input);
            await CommentData.save((err: any, doc: any) => {
                if (err) {
                    reject(err)
                }

                resolve(doc)
            })
        }
        catch (error) {
            reject(error)
        }
    })
}

export function addSubcomment(input: COMMENT.AddSubcomment): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let CommentData = new CommentModel(input);
            await CommentData.save((err: any, doc: any) => {
                if (err) {
                    reject(err)
                }

                resolve(doc)
            })
        }
        catch (error) {
            reject(error)
        }
    })
}

export function updateSubcomment(commentId: any, subCommentId: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {

            await CommentModel.findOneAndUpdate({ _id: commentId }, { $push: { 'subComment': subCommentId } }, { new: true }).exec((err: any, result: any) => {
                if (err) {
                    reject(err)
                }
                resolve(result);

            });
        }
        catch (e) {
            reject(e);
        }
    });
}
interface GetNewsByIdInputData {
    newsId: string,
 
}

export function getCommentByNewsId(input:GetNewsByIdInputData): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {

            let count=await CommentModel.countDocuments({$and:[{'newsId':input.newsId},{'commentType':COMMENT_TYPES.COMMENT}]});

            let result = await CommentModel.find({'$and': [{ 'newsId': input.newsId }, { 'status': COMMENT_STATUS.APPROVED },{commentType:COMMENT_TYPES.COMMENT}]}).sort({ '_id': -1 }).limit(10).populate({path:'subComment',match:{ 'status': COMMENT_STATUS.APPROVED }}).lean().exec();
            
            resolve({comment:result,count:count});
        }
        catch (error) {

        }
    })
}


export function getCommentById(commentId:any): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            const doc = CommentModel.findById({ '_id': commentId}).populate('subComment._id').lean().exec();

            resolve(doc);

        }
        catch (error) {

        }
    })
}



export function getNewsComment(input: COMMENT.newsComment): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let count=await CommentModel.countDocuments({$and:[{'newsId':input.newsId},{'commentType':COMMENT_TYPES.COMMENT}]});

                await CommentModel.find({$and:[{'newsId':input.newsId},{'commentType':COMMENT_TYPES.COMMENT}]}).sort({ _id: -1}).limit(parseInt(input.record)).skip(parseInt(input.offset)).populate('subComment').exec((err: any, doc: any) => {
                    if (err) {
                        reject(err);
                    }
                 
                        resolve({comment:doc,count:count});
                    
                });
            
        
        }
        catch (error) {

        }
    })
}



export function getNewsQuery(input: COMMENT.NEWSQUERYID): Promise<COMMENT.CommentData|any> {
    return new Promise(async (resolve, reject) => {
        try {
         
            let count = await CommentModel.estimatedDocumentCount();
          
            if (input.newsQueryId == null || input.newsQueryId == undefined || input.newsQueryId == "") {
            
                let validate = { _id: -1 }
                await CommentModel.find().populate('newsId').sort(validate).limit(parseInt(input.record)).skip(parseInt(input.offset)).exec((err: any, doc: any) => {
                    if (err) {
                        reject(err);
                    }
                    let commentResponse: COMMENT.CommentData = {
                        comment: doc,
                        count: count
                    }
                    resolve(commentResponse);

                });
            }
            else {
                await CommentModel.findOneAndUpdate({ '_id': input.newsQueryId }, { readStatus: "READ" }, { upsert: false, new: true }).populate('newsId').exec((err: any, doc: any) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(doc);
                    }
                });
            }
        }
        catch (error) {

        }
    })
}


export function deleteNewsComment(id: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
         
          await CommentModel.findByIdAndDelete({ _id: id }).exec((err: any, result: any) => {
                if (err) {
                    reject(err)
                }
         
                resolve(result);
            });

        }
        catch (e) {
            reject(e);
        }
    });
}

export function deleteNewsCommentById(commentId: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            await CommentModel.deleteMany({_id:commentId}).exec((err: any, result: any) => {
                if (err) {
                    reject(err)
                }
               
                resolve(result);
            });
        }
        catch (e) {
            reject(e);
        }
    });
}


export function deleteNewsCommentByNewsId(newsId: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            await CommentModel.deleteMany({ "newsId": newsId }).exec((err: any, result: any) => {
                if (err) {
                    reject(err)
                }
             
                resolve(result);
            });
        }
        catch (e) {
            reject(e);
        }
    });
}



export function updateCommentStatus(input: COMMENT.CommentUpdate): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {

            await CommentModel.findOneAndUpdate({ _id: input.id }, { 'status': input.status }, { new: true, upsert: false }, (err: any, result: any) => {
                if (err) {
                    reject(err)
                }

                resolve(result);


            });
        }
        catch (e) {
            reject(e);
        }
    });
}
