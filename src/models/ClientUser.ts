import * as mongoose from "mongoose";
import { CONSTANTS, USER_ROLES } from "../utils/constants";
import * as bcrypt from '../utils/bcryptUtils';


import { ObjectId, ObjectID, Timestamp } from 'bson';
import { resolve } from "dns";
import { UpdateManyOptions } from "mongodb";

export enum UserTableType {
    MAIN = "ClientUser",
    TEMP = "TempClientUser"
}

const userSchemaDefination: mongoose.SchemaDefinition =
{
    userName: { type: String },
    name: { type: String },
    countryCode: { type: String },
    phoneNumber: { type: String },
    passwordHash: { type: String },
    emailId: { type: String },
    emailIdStatus: { type: Number },
    createTimeStamp: { type: Number },
    upadteTimeStamp: { type: Number },
    phoneNumberStatus: { type: Number },
    source: { type: Number },
    business_Id: { type: String },
    role: { type: String },
    designation: { type: String },
    //type: { type: String },
    createBy: { type: ObjectId },
    status: { type: Number },
    refreshToken: { type: String },
    refreshTokenExpiry: { type: Number },
    allowGoogle: { type: Boolean },
    allowFacebook: { type: Boolean },
    ipAdrres: { type: String },
    avatar: { type: String },
    permission: { type: String }
};

const userChangelog = new mongoose.Schema({
    oldEmailId: { type: String },
    newEmailId: { type: String },
    ipAdrres: { type: String },
    changeRequestTimeStamp: { type: Number, default: Date.now() },
    updateRequestTimeStamp: { type: Number },
    changleEmailStatus: { type: Number, default: 0 }
})



const TempClientUserSchema = new mongoose.Schema(userSchemaDefination);
const ClientUserSchema = new mongoose.Schema(userSchemaDefination);

export const TempClientUserModel = mongoose.model(UserTableType.TEMP, TempClientUserSchema);
export const ClientUserModel = mongoose.model(UserTableType.MAIN, ClientUserSchema);


const PasswordChange = new mongoose.Schema({
    requestTime: { type: String, default: Date.now() },
    ipAdrres: { type: String },
    userId: { type: String },
    verificationTimeStamp: { type: String },
    verificationIpAddress: { type: String },
    status: { type: String },
    type: { type: String }
})
export const PasswordChangeModel = mongoose.model("PasswordChange", PasswordChange);
export const UserChangelogModel = mongoose.model("UserChangeLog", userChangelog)


export function insertIntoUserChangeLog(input: UPDATE.USER_CHANGE_LOG): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let userChangelog = new UserChangelogModel(input);
            await userChangelog.save(err => {
                if (err) {
                    reject(err)
                }
                resolve(userChangelog)
            })
        } catch (error) {
            reject(error)
        }
    })
}

export function updateChangeEmailStatus(newEmailId: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            await UserChangelogModel.findOneAndUpdate({ 'newEmailId': newEmailId }, { 'changleEmailStatus': 1 }, (err: any) => {
                if (err)
                    reject(err)
                else
                    resolve(true)
            })
        }
        catch (error) {
            reject(error)
        }
    })
}
export function getUserLogData(_id: string): Promise<string | null> {
    return new Promise(async (resolve, reject) => {
        try {
            UserChangelogModel.findOne({ _id }, { "status": 1 }, (err: any, doc: any) => {
                if (err) {
                    reject(err)
                }
                else {
                    if (doc && doc.status) {
                        resolve(doc);
                    }
                    else {
                        resolve(null);
                    }
                }
            })
        }
        catch (e) {
            reject(e);
        }
    })

}

export function insertIntoPasswordChange(input: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let PasswordChange = new PasswordChangeModel(input);
            await PasswordChange.save(err => {
                if (err) {
                    reject(err)
                }
                resolve(PasswordChange);
            })
        } catch (error) {
            reject(error)
        }
    })
}

export function getResetPasswordRequestStatus(_id: string): Promise<string | null> {
    return new Promise(async (resolve, reject) => {
        try {
            PasswordChangeModel.findOne({ _id }, { "status": 1 }, (err: any, doc: any) => {
                if (err) {
                    reject(err)
                }
                else {
                    if (doc && doc.status) {
                        resolve(doc.status);
                    }
                    else {
                        resolve(null);
                    }
                }
            })
        }
        catch (e) {
            reject(e);
        }
    })

}


export function updateResetPasswordRequestStatus(_id: string, status: string, verificationIpAddress: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            await PasswordChangeModel.findOneAndUpdate({ _id }, { verificationTimeStamp: Date.now(), status, verificationIpAddress }, (err: any) => {
                if (err)
                    reject(err)
                else
                    resolve(true)
            })
        }
        catch (e) {
            reject(e);
        }
    })
}
export function updateEmailId(id: string, emailId: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {

        try {
            await ClientUserModel.findByIdAndUpdate({ '_id': id }, { 'emailId': emailId, 'upadteTimeStamp': Date.now() }, (err: any) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(true)
                }
            })
        }
        catch (error) {
            reject(error)
        }
    })
}

const LoginHistorySchema = new mongoose.Schema({
    userId: { type: String },
    loginIpAddress: { type: String },
    logoutIpAddress: { type: String },
    loginStatus: { type: Boolean },
    logoutStatus: { type: Boolean },
    loginTimeStamp: { type: Number },
    logoutTimeStamp: { type: Number }

});

export const LoginHistoryModel = mongoose.model("LoginHistory", LoginHistorySchema);




export function insertIntoTempClientUserData(input: ClientUser.InputClientUserData): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let TempClientUser = new TempClientUserModel(input);
            await TempClientUser.save(err => {
                if (err) {
                    reject(err)
                }
                resolve(TempClientUser);
            })
        }
        catch (error) {
            reject(error)
        }
    })
}

export function getTempClientDataDB(input: string): Promise<any | null> {
    return new Promise(async (resolve, reject) => {
        try {
            TempClientUserModel.findOne({ _id: input }, (err: any, doc: any) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(doc)
                }
            })
        }
        catch (e) {
            reject(e);
        }
    });
}



export function getTempClientDataByEmailId(emailId: string): Promise<any | null> {
    return new Promise(async (resolve, reject) => {
        try {
            TempClientUserModel.findOne({ emailId: emailId }, (err: any, doc: any) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(doc)
                }
            })
        } catch (e) {
            reject(e);
        }
    });
}


// Insert Client User data in DB
export function insertClientUserData(input: ClientUser.InputClientUserData): Promise<any> {

    return new Promise(async (resolve, reject) => {
        try {
            // Create document of ClientUser Model
            let ClientUser = new ClientUserModel(input);

            await ClientUser.save(err => {
                if (err) {
                    reject(err);
                }
                resolve(ClientUser);
            });
        } catch (error) {
            reject(error);
        }
    });
}

export function updateUserPermission(input: ClientUser.UpdateUserPermission, role: USER_ROLES): Promise<any> {

    return new Promise(async (resolve, reject) => {
        try {
            console.log("id :", input.userId)
            console.log("permssion :", input.newPermission)
            //   {$and:[{'userId':userId},{'status':status}]}
            await ClientUserModel.findOneAndUpdate({ $and: [{ '_id': input.userId }, { 'role': role }] }, { 'permission': input.newPermission }, { upsert: false, new: true }, (err, result) => {

                if (err) {
                    console.log("updatePermission db Error:", err)
                    reject(err);
                    return;
                }
                else {
                    resolve(result);
                    return
                }
            });


        } catch (error) {
            console.log("Error in update Permission catch", error)
            reject(error);
        }
    });
}

//update clientDetails by Admin
export function updateClientUserData(userId: string, input: ClientUser.UpdateClientUserData): Promise<any> {

    return new Promise(async (resolve, reject) => {
        try {
            await ClientUserModel.findOneAndUpdate({ '_id': userId }, input, { upsert: false, new: true }, (err, result) => {

                if (err) {
                    reject(err)
                }
                else {
                    resolve(result)
                }
            });

        } catch (error) {
            reject(error);
        }
    });
}

//update userDetails
export function updateUserData(_id: string, input: ClientUser.UpdateUserDetails): Promise<any> {

    return new Promise(async (resolve, reject) => {
        try {
            await ClientUserModel.findOneAndUpdate({ '_id': _id }, input, { upsert: false, new: true }, (err, result) => {

                if (err) { reject(err) }
                else { resolve(result) }

            });

        } catch (error) {
            reject(error);
        }
    });
}

// get Client User By userName
export function getClientUserBYUserName(userName: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            await ClientUserModel.findOne({ 'userName': userName }, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        } catch (error) {
            reject(error);
        }
    });
}


export function checkUserNameExistDB(userName: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        let exist = false;
        try {
            ClientUserModel.findOne({ 'userName': userName }, (err: any, user: any) => {
                if (err) {
                    reject(err);
                }
                if (user) {
                    exist = true;
                    resolve(exist);
                }
                else {
                    resolve(exist);

                }
            })

        }

        catch (e) {
            reject(e);
        }
    });
}


// get Client User By emailId
export function getClientUserBYEmailId(emailId: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            await ClientUserModel.findOne({ 'emailId': emailId },  (err: any, result: any) => {
                if (err) {
                    reject(err);
                } 
                    resolve(result);
                
            });
        } catch (error) {
            reject(error);
        }
    });
}



export function checkEmailExistDB(email: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        let exist = false;
        try {
            ClientUserModel.findOne({ 'emailId': email }, (err: any, user: any) => {
                if (err) {
                    reject(err);
                }

                if (user) {
                    exist = true;
                    resolve(exist);
                }
                else {
                    resolve(exist);

                }
            })
        }
        catch (e) {
            reject(e);
        }
    });
}



// get Client User By phoneNumber
export function getClientUserBYPhoneNumber(phoneNumber: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            await ClientUserModel.findOne({ 'phoneNumber': phoneNumber }, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            }
            );
        } catch (error) {
            reject(error);
        }
    });
}


//***********************/
//Change password

export function getUserPasswordDB(emailId: string): Promise<any | null> {
    return new Promise(async (resolve, reject) => {
        try {

            await ClientUserModel.findOne({ 'emailId': emailId }, (err: any, doc: any) => {
                if (err)
                    reject(err)
                else {
                    resolve(doc)
                }
            })
        }

        catch (error) {
            reject(error)
        }
    })
}


export function changePasswordDB(input: ChangePassword.UpdatePasswordInput): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {

            var userNameExist = false;

            await ClientUserModel.findOneAndUpdate({ 'emailId': input.emailId }, { 'passwordHash': input.newPassword }, (err: any) => {
                if (err) {
                    reject(err)
                }
                else {
                    userNameExist = true;
                }
                resolve(userNameExist);
            })

        }

        catch (e) {
            reject(e);
        }


    });
}




export function getUserDatailsByEmailOrUserNameDB(userNameOrEmailId: string): Promise<any | null> {
    return new Promise(async (resolve, reject) => {
        try {
            await ClientUserModel.findOne({ '$or': [{ 'userName': userNameOrEmailId }, { 'emailId': userNameOrEmailId }] }, (err: any, doc: any) => {

                if (err)
                    reject(err)
                else {
                    resolve(doc)
                }
            })
        }
        catch (e) {
            reject(e);
        }
    })
}


export function getUserByIdDB(userId: any): Promise<any | null> {
    return new Promise(async (resolve, reject) => {
        try {
            ClientUserModel.findById({ _id: userId },(err: any, doc: any) => {
                if (err)
                    reject(err)

                resolve(doc)

            })
        }
        catch (e) {
            reject(e);
        }
    })
}


// /************TO RETRIVE DATA USING GIVEN USERID THROUGH PAYLOAD */
// export function getDataDB(input: number): Promise<ClientUser.InputClientUserData|null> {
//     return new Promise(async (resolve, reject) => {
//         try {
//             ClientUserModel.findOne({ _id: input }, (err: any, doc: any) => {
//                 if(err)
//                 {
//                     reject(err)
//                 }
//                 else
//                 {
//                     resolve(doc)
//                 }
//             })
//         } catch (e) {
//             reject(e);
//         }
//     });
// }


export function updatePasswordDB(password: string, userId: any): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let passwordHash = await bcrypt.getHash(password);
            await ClientUserModel.findOneAndUpdate({ _id: userId }, { 'passwordHash': passwordHash }, (err: any) => {
                if (err)
                    reject(err)
                else
                    resolve(true)
            })
        }
        catch (e) {
            reject(e);
        }
    })
}

//get user by email or userName
export function getUserDB(userNameOrEmail: string): Promise<any | null> {
    return new Promise(async (resolve, reject) => {
        try {
            ClientUserModel.findOne({ $or: [{ userName: userNameOrEmail }, { emailId: userNameOrEmail }] }, (err: any, doc: any) => {
                let userdata: ClientUser.InputClientUserData;
                if (err) {
                    reject(err);
                }
                else if (doc) {
                    userdata = doc;
                    resolve(userdata);
                } else {
                    resolve(null);
                }
            }

            );
        } catch (e) {
            reject(e);
        }
    });
}

//        mohit---------------------
export function validateRefreshTokenDB(input: string, time: number): Promise<any | null> {
    return new Promise(async (resolve, reject) => {
        try {
            ClientUserModel.findOne({ 'refreshToken': input }, (err: any, doc: any) => {
                if (err) {
                    reject(err);
                }
                //console.log("data = ", doc);
                if (doc) {
                    if (doc.refreshTokenExpiry >= time) {
                        //console.log("Refresh token expired");

                        resolve(doc);
                    }
                }
                resolve(null);
            });
        } catch (error) {
            reject(error);
        }
    });
}


//REFRESH TOKEN
//********************************* */
// export function validateRefreshTokenDB(input: string): Promise<boolean> {
//     return new Promise(async (resolve, reject) => {
//         try {
//             ClientUserModel.findOne({ refreshToken: input }, async (err: any, doc: any) => {
//                 let exist = false;                
//                 if (doc) {
//                     let result = await validateRefreshTokenTimeDB(input);
//                     exist = result;
//                 }
//                 resolve(exist);
//             });
//         } catch (error) {
//             reject(error);
//         }
//     });
// }

// **************** INTERFACE FOR EXPIRY TIME OF REFRESH TOKEN **************
export interface RefreshExpiry {
    refreshTokenExpiry: number;
}

// // ****************  VALIDATION OF TIME VALIDITY OF REFRESH TOKEN  *************
// // **************** CALLING THIS FUNCTION FROM VALIDATE REFRESH TOKEN DB ***********

// export function validateRefreshTokenTimeDB(input: string): Promise<boolean> {
//     return new Promise(async (resolve, reject) => {
//         try {
//             ClientUserModel.findOne({ refreshToken: input }, (err: any, doc: any) => {
//                 let exist = false;
//                 if (doc) {
//                     let { refreshTokenExpiry } = doc.refreshTokenExpiry;
//                     let result: RefreshExpiry = { refreshTokenExpiry };
//                     if (
//                         result.refreshTokenExpiry >= CONSTANTS.JWT_REFRESH_TOKEN_EXPIRY_SEC
//                     ) {
//                         exist = true;
//                     }
//                 }
//                 resolve(exist);
//             });
//         } catch (e) {
//             reject(e);
//         }
//     });
// }



// //******************* RETRIVE DATA FROM DATABASE FOR ACCESS TOKEN PAYLOAD IF REFRESH TOKEN VALIDATES ******** */

// export function getUserDataDB(input: string): Promise<UserObjectData | null> {
//     return new Promise(async (resolve, reject) => {
//         try {
//             ClientUserModel.findOne({ refreshToken: input }, (err: any, doc: any) => {
//                 if (doc) {

//                     let result: UserObjectData = {
//                         userName: doc.userName,
//                         emailId: doc.emailId,
//                         passwordHash: doc.passwordHash,
//                         source: doc.source,
//                         userId: doc._id,
//                         name: doc.name,
//                         role:doc.role,
//                         permission : doc.permission,
//                         ipAdrres :doc.ipAdrres
//                     };
//                     resolve(result);
//                 } else {
//                     resolve(null);
//                 }
//             });
//         } catch (e) {
//             reject(e);
//         }
//     });
// }

//************ UPDATING REFRESH TOKEN AFTER VALIATION INTO THE DATABASE *********** */

export function updateRefreshTokenDB(input: string, userId: string, refreshTokenExpiry: number): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let result = false;

            ClientUserModel.findOneAndUpdate({ _id: userId }, { refreshToken: input, refreshTokenExpiry: refreshTokenExpiry + CONSTANTS.JWT_REFRESH_TOKEN_EXPIRY_SEC * 1000 }, (err: any, doc: any) => {

                if (err) {
                    reject(err);
                }
                if (doc) {
                    result = true;
                }
                resolve(true);
            }
            );
        } catch (e) {
            reject(e);
        }
    });

}
export function updateEmailStatus(clientUserId: string, emailIdStatus: number): Promise<any> {
    return new Promise(async (resolve, reject) => {

        try {
            await ClientUserModel.findOneAndUpdate({ '_id': clientUserId }, { 'emailIdStatus': emailIdStatus }, { upsert: false, new: true }, (err, result) => {

                if (err) { reject(err) }
                else { resolve(result) }

            });

        } catch (error) {

            reject(error);

        }

    });
}

export interface GetUserDetailsByIdData {
    //******************* RETRIVE DATA FROM DATABASE FOR ACCESS TOKEN PAYLOAD IF REFRESH TOKEN VALIDATES ******** */

    userId: string,
    userName: string,
    emailId: string,
    phoneNumber: string,
    // **************** INTERFACE FOR EXPIRY TIME OF REFRESH TOKEN **************

    emailIdStatus: number,
    phoneNumberStatus: number
}


export interface GetUserDetailsByEmailOrUserNameData {
    userId: string,
    emailId: string,
    userName: string,
    phoneNumber: string,
    emailIdStatus: number,
    phoneNumberStatus: number
}



export function getUserByRoleDB(input: string): Promise<ClientUser.OutputUserRoleData[]> {
    return new Promise(async (resolve, reject) => {
        try {
            ClientUserModel.find({ 'role': input }, (err: any, doc: ClientUser.OutputUserRoleData[]) => {
                if (err) {
                    reject(err)
                }
                else if (doc) {
                    resolve(doc)
                }
            })
        }
        catch (error) {
            reject(error)
        }
    })
}



export function getAdminUsers(param: User.GET_USER_INPUT): Promise<ClientUser.IGetUserDT> {
    return new Promise(async (resolve, reject) => {
        try {
            //console.log("######",param.findQuery)
            let sort: any = {
                'createTimeStamp': -1
            };
            // console.log("sort from controller :",sort)
            const filter = param.filter;
            // console.log({ filter });
            //  console.log("FROM PARAM :",param.sort)
            let findQuery = {};
            if (filter && filter !== 'null')
                findQuery = { 'role': { '$regex': filter, '$options': 'i' } };
            // console.log("1",param.sort ==null)
            // console.log("2",param.sort ==undefined, typeof param.sort) 
            if (param.sort && param.sort.length > 0 && param.sort !== "undefined" && param.sort !== "null") {
                sort = { [param.sort]: param.order }
            }
            console.log("sort from model :", sort)
            const count = await ClientUserModel.countDocuments(findQuery).exec();
            const res: any = await ClientUserModel.find(findQuery).sort(sort).limit(parseInt(param.record)).skip(parseInt(param.offset)).lean().exec();
            const output: ClientUser.IGetUserDT = {
                count: count,
                data: res
            }
            resolve(output);
        }
        catch (e) {
            reject(e)
        }
    })
}

export function checkUserExist(emailId: string, tableType: UserTableType): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let model = tableType === UserTableType.MAIN ? ClientUserModel : TempClientUserModel;
            model.findOne({ emailId: emailId }, (err: any, doc: any) => {
                if (err) {
                    reject(err)
                }
                else {
                    console.log("vishwas", doc);
                    if (doc && doc.emailId) {
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                }
            }).select({ "emailId": 1 })
        }
        catch (e) {
            reject(e);
        }
    });
}

export function getUserById(_id: string, tableType: UserTableType): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let model = tableType === UserTableType.MAIN ? ClientUserModel : TempClientUserModel;
            model.findOne({ _id }, (err: any, doc: any) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(doc)
                }
            })
        }
        catch (e) {
            reject(e);
        }
    });
}

export function getUserDetailsById(userId:string):Promise<any>{
    return new Promise(async(resolve,reject)=>
    {
        try {
            ClientUserModel.findById({ '_id':userId }, (err: any, doc:any) => {
                if (err) {
                    reject(err)
                }
                else if (doc) {
                    resolve(doc)
                }
            })
        } catch (error) {
            reject(error);
        }
    })
}


export function getUserByEmailId(emailId: string, tableType: UserTableType): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let model = tableType === UserTableType.MAIN ? ClientUserModel : TempClientUserModel;
            model.findOne({ emailId: emailId }, (err: any, doc: any) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(doc)
                }
            })
        }
        catch (e) {
            reject(e);
        }
    });
}







export function getUserPasswordHash(emailId: string, tableType: UserTableType): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let model = tableType === UserTableType.MAIN ? ClientUserModel : TempClientUserModel;
            model.findOne({ emailId: emailId }, (err: any, doc: any) => {
                if (err) {
                    reject(err)
                }
                else {
                    console.log("vishwas", doc);
                    if (doc && doc.emailId) {
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                }
            }).select({ "password": 1 })
        }
        catch (e) {
            reject(e);
        }
    });
}


export function createLoginHistoryModel(input: LoginAuthentication.LoginHistoryInput): Promise<LoginAuthentication.LoginHistoryData> {
    return new Promise(async (resolve, reject) => {
        try {
            let clientUserloginHistoryData = new LoginHistoryModel(input);

            await clientUserloginHistoryData.save((err: any, doc: any) => {

                if (err) {
                    reject(err);
                }
                resolve(doc);
            })
        } catch (error) {
            reject(error);
        }
    })
}

export function logoutModel(id: string, input: Logout.LogoutInput): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let result = false;
            await LoginHistoryModel.findOneAndUpdate({ 'loginHistoryId': id }, { 'loginStatus': input.loginStatus, 'logoutStatus': input.logoutStatus, 'logoutTimeStamp': input.logoutTimeStamp, 'logoutIpAddress': input.logoutIpAddress }, (err: any, doc: any) => {
                if (err)
                    reject(err)
                if (doc) {
                    result = true;
                }
                resolve(true);
            })
        }
        catch (e) {
            reject(e);
        }
    })
}



// insertTempDataToMain(payload.emailId, userModel.UserTableType.MAIN, userModel.UserTableType.TEMP);
// (payload.emailId, userModel.UserTableType.MAIN, userModel.UserTableType.TEMP);

// export function insertTempDataToMain(emailId: string, fromTableType: UserTableType , toTableType : UserTableType)
// {
//     return new Promise(async (resolve, reject) =>
//     {
//         try
//         {
//             let tmpDoc = await TempClientUserModel.getTempClientDataByEmailId
//         }
//         catch(e)
//         {
//             reject(e);
//         }


//     })
// }



