import mongoose, { Schema } from 'mongoose';
import { CONSTANTS, CategoryStatus } from '../utils/constants';
import { SortType } from '../types/types';
import { dbRejector, isDuplicateError } from '../utils/dbUtils';
import { ObjectId } from 'bson';



export declare namespace Category {
    export interface CategorySchema {
        _id: string,
        name: string,
        status: CategoryStatus,
        description: string,
        parentId: string,
        order: number,
        label: Array<string>,
        addedBy: string,
        timeStamp: number,
    }

    export interface CategoryExposedSchema extends Omit<CategorySchema, "addedBy"> { }


    export interface LabelSchema {
        _id: string,
        name: Array<string>,
        categoryId: string,
       
    }
    export interface LabelExposedSchema extends LabelSchema { }
}

const CategoryFields =
{
    _id: "_id",
    name: "name",
    status: "status",
    description: "description",
    parentId: "parentId",
    order: "order",
    label: "label",
    addedBy: "addedBy",
    timeStamp: "timeStamp"
}

const categorySchema = new Schema(
    {
        name: { type: String },
        status: { type: Number, required: true, default: CategoryStatus.ACTIVE },
        description: { type: String, required: true },
        parentId: { type: Object, required: true },
        order: { type: Number, required: true, default: 1 },
        label: { type: [] },
        addedBy: { type: String, required: true },
        timeStamp: { type: Number, required: true }
    });


const CategoryModel = mongoose.model('category', categorySchema);


const labelFields =
{
    _id: "_id",
    name: "name",
    categoryId: "categoryId",
   
}

const labelSchema = new Schema(
    {
        name: { type:[] },
        categoryId:{type:String}
       
    });
const labelModel = mongoose.model('label', labelSchema);



export function getCategory(categoryId: string): Promise<Category.CategoryExposedSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let selectFilter: any = {};
            selectFilter[CategoryFields.addedBy] = 0;

            resolve(await CategoryModel.findById(categoryId).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}


export interface GetCategoryWhereFilters {
    categoryName: string | null,
    parentId: string | null
}
export function getCategoryByName( whereFilters: GetCategoryWhereFilters,): Promise<any | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let selectFilter: any = {};
          //  selectFilter[CategoryFields._id] = 1;
          selectFilter[CategoryFields.addedBy] = 0;
            let { categoryName, parentId } = whereFilters;
            let whereF: any = {};
            if (true) {
                if (categoryName) {
                    whereF[CategoryFields.name] = categoryName.toUpperCase()   
                }
                if (parentId) {
                    whereF[CategoryFields.parentId] = parentId
                }
            }
            resolve(await CategoryModel.find(whereF).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}


export interface AddCategoryInput extends UpdateCategoryInfo { }

export function addCategory(addCategoryeInput: AddCategoryInput, addedBy: string): Promise<Category.CategorySchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let { description, parentId, name, order, label } = addCategoryeInput;

            let u: any = {};

            u[CategoryFields.timeStamp] = Date.now();

            if (description)
                u[CategoryFields.description] = description;

            if (parentId)
                u[CategoryFields.parentId] = parentId;

            if (name)
                u[CategoryFields.name] = name.toUpperCase();

            if (order)
                u[CategoryFields.order] = order;

            if (addedBy)
                u[CategoryFields.addedBy] = addedBy;
            if (label)
                u[CategoryFields.label] = label.length > 0 ? label.map(item => String(item).toUpperCase() ) : []

            const data = new CategoryModel(u);
            resolve(await data.save() as any);
        }
        catch (error) {
            if (isDuplicateError(error)) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "This Category name already exist");
            }
            else {
                dbRejector(reject, error);
            }
        }
    });
}


export function getLabelByCategoryId(categoryId: string): Promise<any | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let selectFilter: any = {};
           // selectFilter[labelFields._id] = 0;
    
            resolve(await labelModel.findOne({"categoryId":categoryId}).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}

export interface AddLabelInput extends Omit<Category.LabelExposedSchema, "_id"> { }
export function AddLabel(labelInput: AddLabelInput): Promise<Category.CategorySchema | null> {
    return new Promise(async (resolve, reject) => {
        try {

            let u: any = {};
            u[labelFields.name] =  labelInput.name.length > 0 ? labelInput.name.map(item => String(item).toUpperCase() ) : [];
            u[labelFields.categoryId] = labelInput.categoryId;

            const data = new labelModel(u);
            resolve(await data.save() as any);
        }
        catch (error) {
            if (isDuplicateError(error)) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "This Category name already exist");
            }
            else {
                dbRejector(reject, error);
            }
        }
    });
}

export interface UpdateCategoryInfo extends Omit<Category.CategorySchema, "_id" | "status" | "addedBy" | "timeStamp"> { }

export function updateCategoryInfo(categoryId: string, updateCategoryInfo: UpdateCategoryInfo): Promise<Category.CategoryExposedSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let u: any = {};
            let { description, parentId, name, order, label } = updateCategoryInfo;

            if (description)
                u[CategoryFields.description] = description;

            if (parentId)
                u[CategoryFields.parentId] = parentId;

            if (name)
                u[CategoryFields.name] = name;

            if (order)
                u[CategoryFields.order] = order;

            if (label)
                u[CategoryFields.label] = label.length > 0 ? label.map(item => String(item).toUpperCase() ) : [] ;


            let selectFilter: any = {};
            selectFilter[CategoryFields.addedBy] = 0;

            resolve(await CategoryModel.findByIdAndUpdate(categoryId, u, { new: true }).select(selectFilter) as any);
        }
        catch (error) {
            if (isDuplicateError(error)) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "This Category name already exist");
            }
            else {
                dbRejector(reject, error);
            }
        }
    });
}

export function updateCategoryStatus(categoryId: string, status: CategoryStatus): Promise<Category.CategoryExposedSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let updateObj: any = {};
            updateObj[CategoryFields.status] = status;

            let selectFilter: any = {};
            selectFilter[CategoryFields.addedBy] = 0;
            //     let data=await CategoryModel.find({"parentId":new ObjectId(categoryId)});
            //     console.log("data",data);

            //     if(data)
            //     await CategoryModel.findByIdAndUpdate({"parentId":categoryId}, updateObj, { new: true }).select(selectFilter) as any;

            //    // await CategoryModel.findByIdAndUpdate({"parentId":categoryId}, updateObj, { new: true }).select(selectFilter) as any;
            resolve(await CategoryModel.findByIdAndUpdate(categoryId, updateObj, { new: true }).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}

export interface GetAllCategoryWhereFilters {
    _id: string | null,
    parentId: string | null,
    status: CategoryStatus | 0,
    name: string | null,
    categoryName: string | null,
  
}

export interface GetAllCategorySortFilters {
    _id: SortType,
    parentId: SortType,
    status: SortType,
    time: SortType,
    name: SortType,
    order: SortType
}

export interface GetAllCategoryOutput {
    categoryList: Array<Category.CategoryExposedSchema>,
    count: number
}

export function getAllCategory(pageNumber: number, pageSize: number, whereFilters: GetAllCategoryWhereFilters, sortFilter: GetAllCategorySortFilters): Promise<GetAllCategoryOutput> {
    return new Promise(async (resolve, reject) => {
        try {
            let offset = (pageNumber - 1) * pageSize;
            let whereF: any = {};
            if (true) {
                let { status, name, parentId, _id } = whereFilters;
                if (!status) {
                    whereF[CategoryFields.status] = CategoryStatus.ACTIVE
                }
                if (parentId) {
                    whereF[CategoryFields.parentId] = parentId
                }
                if (status) {
                    whereF[CategoryFields.status] = status
                }
                if (name)
                    whereF[CategoryFields.name] =name.toUpperCase()
                if (_id) {
                    whereF[CategoryFields._id] = _id
                }
            }
         
            let sortF: any = {};
            if (true) {
                let { parentId, status, name, time, _id, order } = sortFilter;
                if (!order)
                    sortF[CategoryFields.order] = order

                if (order) {
                    sortF[CategoryFields.order] = order
                }
                if (parentId) {
                    sortF[CategoryFields.parentId] = parentId
                }
                if (status) {
                    sortF[CategoryFields.status] = status
                }
                if (name) {
                    sortF[CategoryFields.name] = name
                }
                if (time) {
                    sortF[CategoryFields.timeStamp] = time
                }
                if (_id) {
                    sortF[CategoryFields._id] = _id
                }

            }
            let selectFilter: any = {};
            selectFilter[CategoryFields.addedBy] = 0;
            const count = await CategoryModel.count(whereF);
            let arr: Category.CategoryExposedSchema[];
            if (pageSize == 0) {
                arr = await CategoryModel.find(whereF).select(selectFilter).sort(sortF).skip(offset) as any;

            } else {
                arr = await CategoryModel.find(whereF).select(selectFilter).sort(sortF).skip(offset).limit(pageSize + 1) as any;
            }


            // let hasMore = false;
            // if (arr.length == pageSize + 1) {
            //     arr.splice(pageSize, 1);
            //     hasMore = true
            // }
            let output: GetAllCategoryOutput =
            {
                categoryList: arr,
                count
            }

            resolve(output);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}

export function getDefaultCategory(): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let selectFilter: any = {};
            selectFilter[CategoryFields._id] = 1;
            selectFilter[CategoryFields.name] = 1;
            resolve(await CategoryModel.find({"parentId":"0"}).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}