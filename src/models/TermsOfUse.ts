import mongoose, { Schema } from 'mongoose';
//add order 


const termsSchema: Schema = new Schema({
    termsId: { type: Number, required: true, index: true, unique: true, default: 1 },
    content: { type: String, required: true },
    addedBy: { type: String, required: true },
    editedBy: { type: String },
}, {
    timestamps: true
});

// Export the model and return your IUser interface
export default mongoose.model('TermsOfUse', termsSchema);

