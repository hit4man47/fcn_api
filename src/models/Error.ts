import mongoose, { Schema } from 'mongoose';


const errorSchema = new Schema({
    errorId: { type: String, index: true, unique: true, required: true },
    error: { type: Object },
    user: { type: String, default: 'Admin' }
}, {
    timestamps: true
});


export default mongoose.model('error', errorSchema);

// export const CountriesModel = mongoose.model('country', countrySchema);