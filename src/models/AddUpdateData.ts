import mongoose, { Schema } from 'mongoose';
import { CONSTANTS, AddUpdateStatus, DATATYPE, CategoryStatus, DocStatus } from '../utils/constants';
import { SortType } from '../types/types';
import { dbRejector, isDuplicateError } from '../utils/dbUtils';
import { createUniqueUrl } from '../utils/commonUtils';

export declare namespace AddUpdateData {

    export interface SEO {
        title: string,
        metaDescription: string,
        canonical: string,
        metaTag: string
    }

    export interface DOC {
        visibility: string,
        publish: number,
    }
    export interface AddUpdateDataSchema {
        _id: string,
        title: string,
        status: AddUpdateStatus,
        description: string,
        shortDescription: string
        seo: SEO,
        doc: DOC,
        category: string,
        subCategory: string,
        label: Array<string>,
        tags: Array<string>,
        section: Array<string>,
        file: string,
        thumbnail: string
        video: string,
        type: string,
        url: string,
        createdDate: number,
        updatedDate: number,
        noOfView: number,
        timeSpend: number,
        addedBy: string,
        author: string

    }
    export interface AddDataInNewsSchema {
        mySqlId: number,
        title: string,
        status: AddUpdateStatus,
        description: string,
        shortDescription: string
        seo: SEO,
        doc: DOC,
        category: string,
        subCategory: string,
        label: Array<string>,
        tags: Array<string>,
        section: Array<string>,
        file?: string,
        thumbnail: string
        video: string,
        createdDate: number,
        updatedDate: number,
        noOfView: number,
        timeSpend: number,
        addedBy: string,
        type: string,
        url: string,
        author: string
    }

    export interface AddUpdateDataExposedSchema extends Omit<AddUpdateDataSchema, "addedBy" | "createdDate" | "updatedDate" | "__v"> { }
}


// const PRICE_DECIMAL_COUNT = 2;
// const PRICE_DEFAULT = 5000;

const AddUpdateDataFields =
{
    _id: "_id",
    title: "title",
    status: "status",
    description: "description",
    seo: "seo",
    doc: "doc",
    category: "category",
    shortDescription: "shortDescription",
    subCategory: "subCategory",
    label: "label",
    tags: "tags",
    section: "section",
    file: "file",
    thumbnail: "thumbnail",
    video: "video",
    type: "type",
    url: "url",
    createdDate: "createdDate",
    updatedDate: "updatedDate",
    noOfView: "noOfView",
    timeSpend: "timeSpend",
    addedBy: "addedBy",
    author: "author"

}

// function getDecimalNumber(val: number) { return (val / (10 ^ PRICE_DECIMAL_COUNT)); }
// function setDecimalNumber(val: number) { return (val * (10 ^ PRICE_DECIMAL_COUNT)); }

const newsSchema = new Schema(
    {
        mySqlId: { type: Number, default: 0 },
        title: { type: String, required: true },
        status: { type: Number, required: true, default: AddUpdateStatus.PUBLIC },
        description: { type: String, required: true },
        shortDescription: { type: String, required: true },
        seo: {
            title: { type: String },
            metaDescription: { type: String },
            canonical: { type: String },
            metaTag: { type: String }
        },
        doc: {
            visibility: { type: String, default: DocStatus.PUBLIC },
            publish: { type: Number, default: Date.now() },
        },
        category: { type: String, ref: "category" },
        subCategory: { type: String, ref: "category" },
        label: { type: [], uppercase: true },
        tags: { type: [] },
        section: { type: [] },
        file: { type: String },
        thumbnail: { type: String, default: null },
        video: { type: String },
        type: { type: String },
        url: { type: String, required: true, index: { unique: true } },
        noOfView: { type: Number },
        addedBy: { type: String, required: true },
        timeSpend: { type: Number },
        createdDate: { type: Number, required: true },
        updatedDate: { type: Number, required: true },
        author: { type: String, ref: "author" }

    });
export const newsModel = mongoose.model('news', newsSchema);

export const oldnewsModel = mongoose.model('oldnews', newsSchema);


export declare namespace NewsImpression {
    export interface ImpressionSchema {
        _id: string,
        ipAddress: string,
        newsId: string,
        type: string,
        noOfView: number,
        device: string,
        viewDate: number

    }

    export interface ImpressionExposedSchema extends Omit<ImpressionSchema, "__v"> { }
}


const ImpressionFields =
{
    _id: "_id",
    ipAddress: "ipAddress",
    newsId: "newsId",
    type: "type",
    noOfView: "noOfView",
    device: "device",
    viewDate: "viewDate",

}

const impressionSchema = new Schema(
    {
        ipAddress: { type: String },
        newsId: { type: String },
        type: { type: String },
        noOfView: { type: Number },
        device: { type: String },
        viewDate: { type: Number },

    });
const impressionModel = mongoose.model('NewsImpression', impressionSchema);


export function getData(id: string, type: string): Promise<AddUpdateData.AddUpdateDataExposedSchema> {
    return new Promise(async (resolve, reject) => {
        try {

            let selectFilter: any = {};
            selectFilter[AddUpdateDataFields.addedBy] = 0;
            selectFilter[AddUpdateDataFields.createdDate] = 0;
            selectFilter[AddUpdateDataFields.updatedDate] = 0;

            let whereF: any = {};
            if (true) {
                whereF[AddUpdateDataFields._id] = id
                whereF[AddUpdateDataFields.type] = type
            }
            var data: AddUpdateData.AddUpdateDataExposedSchema;
            data = await newsModel.findById(whereF).populate("category", "name").populate("subCategory", "name").populate("author").select(selectFilter) as any

            resolve(data);

        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}
export function checkUniqueUrl(input: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {

        try {
            let data = await newsModel.findOne({ 'url': input });
            console.log("data :;", data)
            if (data) {
                resolve(true);
            }
            else {
                resolve(false);
            }
        }
        catch (error) {
            reject(error)
        }
    })
}

export interface AddTDataInput extends UpdateDataInfo { }

export function addData(addInput: AddTDataInput, addedBy: string, type: string): Promise<AddUpdateData.AddUpdateDataSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let { title, description, seo, doc, category, subCategory, label, tags, section, file, video, noOfView, timeSpend, status, url, author, shortDescription, thumbnail } = addInput;

            let u: any = {};

            u[AddUpdateDataFields.createdDate] = Date.now();
            u[AddUpdateDataFields.updatedDate] = Date.now();

            if (description)
                u[AddUpdateDataFields.description] = description;

            if (title)
                u[AddUpdateDataFields.title] = title;

            if (seo)
                u[AddUpdateDataFields.seo] = seo;

            if (doc)
                u[AddUpdateDataFields.doc] = doc;

            if (category)
                u[AddUpdateDataFields.category] = category;

            if (subCategory)
                u[AddUpdateDataFields.subCategory] = subCategory;

            if (addedBy)
                u[AddUpdateDataFields.addedBy] = addedBy;

            if (label)
                u[AddUpdateDataFields.label] = label;

            if (tags)
                u[AddUpdateDataFields.tags] = tags;

            if (section)
                u[AddUpdateDataFields.section] = section;

            if (file)
                u[AddUpdateDataFields.file] = file;

            if (video)
                u[AddUpdateDataFields.video] = video;

            if (noOfView)
                u[AddUpdateDataFields.noOfView] = noOfView;

            if (timeSpend)
                u[AddUpdateDataFields.timeSpend] = timeSpend;

            if (status)
                u[AddUpdateDataFields.status] = status;
            if (type)
                u[AddUpdateDataFields.type] = type;

            if (url) {
                let w: whrF = {
                    url: url,
                    _id: ""
                }
                let uniqueUrl = await findUrlExist(w);
                if (uniqueUrl) {
                    global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "This Data already exist");
                    return;
                }
                u[AddUpdateDataFields.url] = url;
            }
            if (author) {
                u[AddUpdateDataFields.author] = author

            }
            else {
                let author = await getDefaultAuthor();
                u[AddUpdateDataFields.author] = author[0]._id;


            }
            // console.log("author :: after",author)
            // let lenght=description.length

            if (!shortDescription) {
                u[AddUpdateDataFields.shortDescription] = description;
            }
            else {
                u[AddUpdateDataFields.shortDescription] = shortDescription;
            }
            if (thumbnail) {
                u[AddUpdateDataFields.thumbnail] = thumbnail;
            }
            let data = new newsModel(u);
            resolve(await data.save() as any);
        }
        catch (error) {
            console.log("Error", error)
            if (isDuplicateError(error)) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, `This ${type} already exist`);
            }
            else {
                dbRejector(reject, error);
            }
        }
    });
}

interface MigData extends Omit<AddUpdateData.AddUpdateDataSchema, "seo" | "doc" | "section" | "timeSpend" | "addedBy"> {
    doc?: AddUpdateData.DOC
}
export function addMigrationData(input: MigData, publishTimeStamp: number): Promise<void> {
    return new Promise(async (resolve, reject) => {
        try {
            input.doc = {
                visibility: DocStatus.PUBLIC,
                publish: publishTimeStamp
            }

            const data = new newsModel(input);
            resolve(await data.save() as any);
        } catch (error) {

        }
    });
}

export interface UpdateDataInfo extends Omit<AddUpdateData.AddUpdateDataExposedSchema, "_id" | "addedBy" | "createdDate" | "updatedDate"> { }

export function updateDataInfo(id: string, updateInfo: UpdateDataInfo, type: string): Promise<AddUpdateData.AddUpdateDataExposedSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {

            let { title, description, seo, doc, category, subCategory, label, tags, section, file, video, noOfView, timeSpend, status, url, shortDescription, thumbnail } = updateInfo;

            let whereF: any = {};
            if (true) {
                whereF[AddUpdateDataFields._id] = id
                whereF[AddUpdateDataFields.type] = type
            }

            let u: any = {};
            u[AddUpdateDataFields.updatedDate] = Date.now();

            if (description)
                u[AddUpdateDataFields.description] = description;

            if (title)
                u[AddUpdateDataFields.title] = title;

            if (seo)
                u[AddUpdateDataFields.seo] = seo;

            if (doc)
                u[AddUpdateDataFields.doc] = doc;

            if (category)
                u[AddUpdateDataFields.category] = category;

            if (subCategory)
                u[AddUpdateDataFields.subCategory] = subCategory;

            if (label)
                u[AddUpdateDataFields.label] = label;

            if (tags)
                u[AddUpdateDataFields.tags] = tags;

            if (section)
                u[AddUpdateDataFields.section] = section;

            if (file)
                u[AddUpdateDataFields.file] = file;

            if (video)
                u[AddUpdateDataFields.video] = video;

            if (noOfView)
                u[AddUpdateDataFields.noOfView] = noOfView;

            if (timeSpend)
                u[AddUpdateDataFields.timeSpend] = timeSpend;

            if (status)
                u[AddUpdateDataFields.status] = status;

            if (url) {
                let w: whrF = {
                    url: url,
                    _id: id
                }
                let uniqueUrl = await findUrlExist(w);
                if (uniqueUrl) {
                    global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "This Data already exist");
                    return;
                }
                u[AddUpdateDataFields.url] = url;

                if (shortDescription)
                    u[AddUpdateDataFields.shortDescription] = shortDescription;
                if (thumbnail)
                    u[AddUpdateDataFields.thumbnail] = thumbnail;

            }
            let selectFilter: any = {};
            selectFilter[AddUpdateDataFields.addedBy] = 0;

            resolve(await newsModel.findByIdAndUpdate(whereF, u, { new: true }).select(selectFilter) as any);
        }
        catch (error) {
            console.log("Error", error);
            if (isDuplicateError(error)) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "This Data already exist");
            }
            else {
                dbRejector(reject, error);
            }
        }
    });
}

export function updateStatus(id: string, status: AddUpdateStatus, type: string): Promise<AddUpdateData.AddUpdateDataExposedSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let whereF: any = {};
            if (true) {
                whereF[AddUpdateDataFields._id] = id
                whereF[AddUpdateDataFields.type] = type
            }

            let updateObj: any = {};
            updateObj[AddUpdateDataFields.status] = status;

            let selectFilter: any = {};
            selectFilter[AddUpdateDataFields.addedBy] = 0;
            resolve(await newsModel.findByIdAndUpdate(whereF, updateObj, { new: true }).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}




export interface GetAllDataWhereFilters {
    category: string | null,
    status: AddUpdateStatus | 0,
    title: string | null,
    subCategory: string | null,
    label: Array<string>,
    section: Array<string>,
    _id: string | null,
    categoryName: string | null,
    subCategoryName: string | null,
    url: string | null
}

export interface GetAllDataSortFilters {
    title: SortType,
    status: SortType,
    timeSpend: SortType,
    createdDate: SortType,
    category: SortType,
    subCategory: SortType,
    noOfView: SortType,
    publish?: SortType
}

export interface GetAllDataOutput {
    dataList: Array<AddUpdateData.AddUpdateDataExposedSchema>,
    count: number;
}

export function getAllData(pageNumber: number, pageSize: number, whereFilters: GetAllDataWhereFilters, sortFilter: GetAllDataSortFilters, type: string): Promise<GetAllDataOutput> {
    return new Promise(async (resolve, reject) => {
        try {
            let offset = (pageNumber - 1) * pageSize;
            let whereF: any = {};
            if (true) {
                let { category, status, title, subCategory, label, _id, section, url } = whereFilters;
                whereF['doc.publish'] = { '$lte': Date.now() }
                whereF['doc.visibility'] = DocStatus.PUBLIC
                if (!status) {
                    whereF[AddUpdateDataFields.status] = AddUpdateStatus.PUBLIC;
                }
                if (category) {
                    whereF[AddUpdateDataFields.category] = category
                }
                if (status) {
                    whereF[AddUpdateDataFields.status] = status
                }
                if (title && typeof title === "string")
                    whereF[AddUpdateDataFields.title] = { $regex: '.*' + String(title).trim() + '.*', $options: 'i' }
                if (subCategory)
                    whereF[AddUpdateDataFields.subCategory] = subCategory
                if (label && label.length > 0)
                    whereF[AddUpdateDataFields.label] = { '$in': label.map((item: string) => item.toUpperCase()) }
                if (section && section.length > 0)
                    whereF[AddUpdateDataFields.section] = { '$in': section.map((item: string) => item.toUpperCase()) }
                if (_id)
                    whereF[AddUpdateDataFields._id] = _id
                if (type) {
                    whereF[AddUpdateDataFields.type] = type
                }
                if (url) {
                    whereF[AddUpdateDataFields.url] = url
                }
            }
            console.log("realated post", whereF)
            let sortF: any = {};
            if (true) {
                let { category, status, title, subCategory, timeSpend, createdDate, noOfView, publish } = sortFilter;
                //sortF[AddUpdateDataFields.doc] = title
                if (!noOfView) {
                    sortF['doc.publish'] = -1;
                    sortF[AddUpdateDataFields.createdDate] = -1;
                }

                if (noOfView) {
                    sortF[AddUpdateDataFields.noOfView] = noOfView;
                    sortF['doc.publish'] = -1;
                    sortF[AddUpdateDataFields.createdDate] = -1;

                }
                if (title) {
                    sortF[AddUpdateDataFields.title] = title
                }
                if (status) {
                    sortF[AddUpdateDataFields.status] = status
                }
                if (category) {
                    sortF[AddUpdateDataFields.category] = category
                }
                if (subCategory) {
                    sortF[AddUpdateDataFields.subCategory] = subCategory
                }
            }
            console.log("sortF", sortF);

            // 'name': { '$regex': req.body.search.value.trim()
            let selectFilter: any = {};
            selectFilter[AddUpdateDataFields.addedBy] = 0;
            selectFilter[AddUpdateDataFields.seo] = 0;
            selectFilter[AddUpdateDataFields.createdDate] = 0;
            selectFilter[AddUpdateDataFields.updatedDate] = 0;

            var arr: AddUpdateData.AddUpdateDataExposedSchema[] = [];
            var count = await newsModel.countDocuments(whereF);

            arr = await newsModel.find(whereF).populate("category", "name").populate("subCategory", "name").populate("author").select(selectFilter).sort(sortF).skip(offset).limit(pageSize + 1) as any;
            // let hasMore = false;
            // if (arr.length == pageSize + 1) {
            //     arr.splice(pageSize, 1);
            //     hasMore = true
            // }

            arr.map((item: any) => {
                item.shortDescription = item.shortDescription.substr(0, 100)
            })
          //  console.log("444444444444", arr);


            let output: GetAllDataOutput =
            {
                dataList: arr,
                count
            }

            resolve(output);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}


export function getImpression(id: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let selectFilter: any = {};
            resolve(await impressionModel.findById(id).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}

export interface AddImpressionInput extends Omit<NewsImpression.ImpressionExposedSchema, "_id"> { }

export function addImpression(addInput: AddImpressionInput): Promise<NewsImpression.ImpressionSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {

            let { ipAddress, newsId, type, noOfView, device, viewDate } = addInput;

            let u: any = {};

            u[ImpressionFields.viewDate] = Date.now();

            if (ipAddress)
                u[ImpressionFields.ipAddress] = ipAddress;

            if (newsId)
                u[ImpressionFields.newsId] = newsId;

            if (type)
                u[ImpressionFields.type] = type;

            if (device)
                u[ImpressionFields.device] = device;

            if (viewDate)
                u[ImpressionFields.viewDate] = viewDate;

            if (noOfView)
                u[AddUpdateDataFields.noOfView] = noOfView;


            const data = new impressionModel(u);
            resolve(await data.save() as any);
        }
        catch (error) {

            dbRejector(reject, error);

        }
    });
}
export function getDataType(_id: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let type = ""
            const arr = await newsModel.findById(_id) as any;
            if (arr) {
                type = arr.type;
            }
            resolve(type);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}

export function getDataTypeUsingUrl(url: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let type = ""
            const arr = await newsModel.findOne({ 'url': url }) as any;
            if (arr) {
                type = arr.type;
            }
            resolve(type);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}

interface whrF {
    url: string,
    _id: string,

}

export function findUrlExist(input: whrF): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
        try {
            let { _id, url } = input;
            let whereF: any = {};
            if (true) {
                if (_id) {
                    whereF[AddUpdateDataFields._id] = { '$ne': _id }
                }
                whereF[AddUpdateDataFields.url] = url
            }
            let urlExist = false;
            let doc = await newsModel.find(whereF) as any;
            if (doc.length > 0) {
                urlExist = true;
            }
            resolve(urlExist);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}
export function copyCollection(type: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
           
            const d = await newsModel.find({})
            //d.forEach(function(d){ });
            // db.getSiblingDB('target_database')['target_collection'].insert(d);
           // console.log("all data", d);
            newsModel.insertMany(d);
            resolve(d);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}

export function updateUrl(type: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let result: any;
            (await newsModel.find({})).forEach(async function (p: any) {

                let title = await createUniqueUrl(p.title)
                result = await newsModel.update({ _id: p._id }, { $set: { "url": title } })

            });
            resolve(result);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}

export function addInNews(input: Array<AddUpdateData.AddDataInNewsSchema>): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            //    let data=new newsModel(input)
            //    let result=await data.save();
            resolve(await newsModel.insertMany(input));
            // resolve(await newsModel.update({"author":"5e0bfd9a9956775e26bc551a"},input,{multi:true}));  ///update author
            // resolve(await newsModel.remove({"addedBy": "Fintech Crypto News"}));   //remove migrate data only
        } catch (error) {
            reject(error)
        }
    })
}

export function getUniqueUrlData(input: string): Promise<any> {
    return new Promise(async (resolve, reject) => {

        try {
            let data = await newsModel.findOne({ 'url': input }).populate("category", "name").populate("subCategory", "name").populate("author");

            if (data) {
                resolve(data);
            }
        }
        catch (error) {
            reject(error)
        }
    })
}

/**
 * 
 * 
 * Author schema */

export declare namespace Author {
    export interface authorSchema {
        _id: string
        name: string,
        createdDate: number
        aboutAuthor: string,
        image: string
    }
}

const authorSchema = new Schema({

    name: { type: String, required: true },
    createdDate: { type: Number, default: Date.now },
    aboutAuthor: { type: String },
    image: { type: String }
});
const authorModel = mongoose.model('author', authorSchema);

export interface addAuthor extends Omit<Author.authorSchema, "createdDate" | "_id"> { }

export function AddAuthor(input: addAuthor): Promise<any> {
    return new Promise(async (reject, resolve) => {
        try {

            let authorData = new authorModel(input)
            resolve(await authorData.save() as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }

    })
}
//let d:addAuthor={
//         name:"Admin",
//         aboutAuthor:"I am Admin Author",
//         image:"http://feeliumcontract.com/fcn/images/logo.png"
//     }
//       let result=await AddAuthor(d);
//    if(result){
//    console.log("add author",result)
//    }



export function getDefaultAuthor(): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {

            let author = await authorModel.find() as any;
            resolve(author)
            return
        } catch (error) {
            dbRejector(reject, error);
        }
    })
}


