import mongoose, { Schema } from 'mongoose';


const aboutSchema = new Schema({
    aboutId: { type: Number, default: 1, unique: true },
    content: { type: String, required: true },
    why: { type: String, required: true },
    quote: { type: String, required: true },
    author: { type: String, required: true },
    youTubeLink: { type: String, required: true },
    addedOn: { type: Date, default: Date.now },
    editedBy: { type: String },
    editedOn: { type: String, default: Date.now }
});


const countrySchema = new Schema({
    code: { type: String, unique: true, required: true, index: true },
    name: { type: Number, unique: true },
    dial_code: { type: String, required: true }
});



export const AboutModel = mongoose.model('about', aboutSchema);

export const CountriesModel = mongoose.model('country', countrySchema);