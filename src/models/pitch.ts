import mongoose, { Schema } from 'mongoose';
import { CONSTANTS, PitchStatus } from '../utils/constants';
import { SortType } from '../types/types';
import { dbRejector, isDuplicateError } from '../utils/dbUtils';
import { ApiTages } from 'types/tages';
import { ApiPitch } from 'types/pitch';



export declare namespace Pitch {

    export interface PitchSchema {
        _id: string,
        type:string,
        title:string,
        description: string,
        releaseData:number,
        metaDescription:number,
        bodyAndimage:string,
        summary:string,
        url:string,
        subCategory:string,
        name: string,
        emailId: string,
        contactNumber: string,
        companyName: string,
        message: string,
        status:string,
        timeStamp: number,
    }

    export interface PitchExposedSchema extends Omit<PitchSchema, "addedBy"> { }
}

const PitchFields =
{

        _id: "_id",
        type:  "type"    ,
        title: "title"     ,
        description:"description"     ,
        releaseData:"releaseData",
        metaDescription:"metaDescription",
        bodyAndimage:"bodyAndimage"     ,
        summary: "summary"    ,
        url: "url"    ,
        subCategory: "subCategory"    ,
        name:  "name"    ,
        emailId:"emailId"     ,
        contactNumber:  "contactNumber"   ,
        companyName: "companyName"    ,
        message:  "message"   ,
        status:"status",
        timeStamp:"timeStamp" ,
}

const pitchSchema = new Schema(
    {
        type:{ type: String},   
        title:{ type: String}    ,
        description:{ type: String}     ,
        releaseData:{ type: Number },
        metaDescription:{ type: String},
        bodyAndimage:{ type: String}    ,
        summary:{ type: String}    ,
        url:{ type: String}  ,
        subCategory: { type: String}   ,
        name:  { type: String}   ,
        emailId:{ type: String},
        contactNumber:  { type: String},
        companyName: { type: String} ,
        message: { type: String} ,
        status:{type:String,default:PitchStatus.PENDING},
        timeStamp:{ type: Number, required: true },

    });


const PitchModel = mongoose.model('pitch', pitchSchema);


export function getPich(pichId: string): Promise<Pitch.PitchExposedSchema | null> {

    return new Promise(async (resolve, reject) => {
        try {
           let selectFilter: any = {};
           selectFilter[PitchFields.timeStamp] = 0;

            resolve(await PitchModel.findById(pichId).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}


export interface AddPitchInput extends  Omit<Pitch.PitchSchema, "_id" | "timeStamp" | "status"> { }

export function addPitch(AddPitchInput: AddPitchInput): Promise<Pitch.PitchSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let { type,title,description,releaseData,metaDescription,bodyAndimage , summary,url ,subCategory,name,emailId,contactNumber , companyName , message} = AddPitchInput;

            let u: any = {};

            u[PitchFields.timeStamp] = Date.now();

            if (type)
                u[PitchFields.type] = type;
                if (title)
                u[PitchFields.title] = title;
                if (description)
                u[PitchFields.description] = description;
                if (releaseData)
                u[PitchFields.releaseData] = releaseData;
                if (metaDescription)
                u[PitchFields.metaDescription] = metaDescription;
                if (bodyAndimage)
                u[PitchFields.bodyAndimage] = bodyAndimage;
                if (summary)
                u[PitchFields.summary] = summary;
                if (url)
                u[PitchFields.url] = url;
                if (subCategory)
                u[PitchFields.subCategory] = subCategory;
                if (name)
                u[PitchFields.name] = name;
                if (emailId)
                u[PitchFields.emailId] = emailId;
                if (contactNumber)
                u[PitchFields.contactNumber] = contactNumber;
                if (companyName)
                u[PitchFields.companyName] = companyName;
                if (message)
                u[PitchFields.message] = message;

            const data = new PitchModel(u);
            resolve(await data.save() as any);
        }
        catch (error) {
            if (isDuplicateError(error)) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "This pitch data name already exist");
            }
            else {
                dbRejector(reject, error);
            }
        }
    });
}


export interface GetAllPitchWhereFilters {
    type: string | null,
    status: PitchStatus | 0,
    name: string | null,
    subCategory:string|null
}

export interface GetAllPitchSortFilters {
    type: SortType,
    status: SortType,
    releaseData: SortType,
 
}

export interface GetAllPitchOutput {
    PitchList: Array<Pitch.PitchExposedSchema>,
    hasMore: boolean
}

export function getAllPitch(pageNumber: number, pageSize: number, whereFilters: GetAllPitchWhereFilters, sortFilter: GetAllPitchSortFilters): Promise<GetAllPitchOutput> {
    return new Promise(async (resolve, reject) => {
        try {
            let offset = (pageNumber - 1) * pageSize;
            let whereF: any = {};
            if (true) {
                let { type, status, name ,subCategory} = whereFilters;
       

                if (type) {
                    whereF[PitchFields.type] = type
                }
                if (status) {
                    whereF[PitchFields.status] = status
                }
                if (name){
                    whereF[PitchFields.name] = { '$regex': name.trim(), $options: 'i' }
                }
              if (subCategory) {
                        whereF[PitchFields.subCategory] = subCategory
                    }
            }
            let sortF: any = {};
            if (true) {
                let { type, status,releaseData } = sortFilter;
                if (!releaseData)
                releaseData = -1;

                if (type) {
                    sortF[PitchFields.type] = type
                }
                if (status) {
                    sortF[PitchFields.status] = status
                }
                if (releaseData) {
                    sortF[PitchFields.releaseData] = releaseData
                }
            }

            // 'name': { '$regex': req.body.search.value.trim()
            console.log({ whereF });

            let selectFilter: any = {};
            selectFilter[PitchFields.timeStamp] = 0;

            const arr: Pitch.PitchExposedSchema[] = await PitchModel.find(whereF).select(selectFilter).sort(sortF).skip(offset).limit(pageSize + 1) as any;
         
            let hasMore = false;
            if (arr.length == pageSize + 1) {
                arr.splice(pageSize, 1);
                hasMore = true
            }
            let output: GetAllPitchOutput =
            {
                PitchList: arr,
                hasMore
            }

            resolve(output);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}



export function updatePitchWork( updateCategoryInfo: ApiPitch.UpdatePitchRequest): Promise<Pitch.PitchExposedSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            
            resolve(await PitchModel.findByIdAndUpdate(updateCategoryInfo._id, updateCategoryInfo.status, { new: true }) as any);
        }
        catch (error) {
            if (isDuplicateError(error)) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "This pitch name already exist");
            }
            else {
                dbRejector(reject, error);
            }
        }
    });
}
