import mongoose, { Schema } from 'mongoose';
import { CONSTANTS } from '../utils/constants';
import { TagsStatus } from '../utils/constants';
import { SortType } from '../types/types';
import { dbRejector, isDuplicateError } from '../utils/dbUtils';



export declare namespace Tages {

    export interface TagesSchema {
        _id: string,
        name: string,
        sqlId:string,
        count: number,
        addedBy: string,
        timeStamp: number,
    }

    export interface TagesExposedSchema extends Omit<TagesSchema, "addedBy"> { }
}

const TagesFields =
{
    _id: "_id",
    name: "name",
    sqlId:'sqlId',
    count: "count",
    addedBy: "addedBy",
    timeStamp: "timeStamp",
    status:"status"
}

const tagsSchema = new Schema(
    {
        name        :   { type: String,unique:true },
        count       :   { type: Number, required: true, default:0 },
        sqlId       :   {type:Number},
        addedBy     :   { type: String, required: true },
        timeStamp   :   { type: Number, required: true },
        status      :   { type:String , required:true, default:TagsStatus.ACTIVE}
    });


export const TagsModel = mongoose.model('tags', tagsSchema);



export interface AddTagesInput extends  Omit<Tages.TagesSchema, "_id" | "addedBy" | "timeStamp"> { }

export function addTags(addTagesInput: AddTagesInput, addedBy: string): Promise<Tages.TagesSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let { name,count} = addTagesInput;

            let u: any = {};

            u[TagesFields.timeStamp] = Date.now();

            if (name)
                u[TagesFields.name] = name;

            if (count)
                u[TagesFields.count] = count;
            
                if (addedBy)
                u[TagesFields.addedBy] = addedBy;
            
            const data = new TagsModel(u);
            resolve(await data.save() as any);
        }
        catch (error) {
            if (isDuplicateError(error)) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "This tages name already exist");
            }
            else {
                dbRejector(reject, error);
            }
        }
    });
}



export interface UpdateTagesInfo extends Omit<Tages.TagesSchema, "_id" | "addedBy" | "timeStamp"> { }

export function updateTagsInfo(tagesId: string, UpdateTagesInfo: UpdateTagesInfo): Promise<Tages.TagesExposedSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let u: any = {};
            let {name,count} = UpdateTagesInfo;

            if (name)
                u[TagesFields.name] = name;

            if (count)
                u[TagesFields.count] = count;

            let selectFilter: any = {};
            selectFilter[TagesFields.addedBy] = 0;

            resolve(await TagsModel.findByIdAndUpdate(tagesId, u, { new: true }).select(selectFilter) as any);
        }
        catch (error) {
            if (isDuplicateError(error)) {
                global.rejector(reject, CONSTANTS.ERROR_UNKNOWN_SHOW_TO_USER, "This tages name already exist");
            }
            else {
                dbRejector(reject, error);
            }
        }
    });
}


export function getTags(tagesId: string): Promise<Tages.TagesExposedSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let selectFilter: any = {};
            selectFilter[TagesFields.addedBy] = 0;

            resolve(await TagsModel.findById(tagesId).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}

export function updateTagsCount(tagId: string, count: Number): Promise<Tages.TagesExposedSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let updateObj: any = {};
            updateObj[TagesFields.count] = count;

            let selectFilter: any = {};
            selectFilter[TagesFields.addedBy] = 0;

            resolve(await TagsModel.findByIdAndUpdate(tagId, updateObj, { new: true }).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}


export interface GetAllTagesWhereFilters {
    _id:string | null,
    name: string | null,
    count: string | null,
    addedBy: string | null,
    status:string| null
    
}

export interface GetAllTagesSortFilters {
    _id:SortType,
    name: SortType,
    count: SortType,
    timeSpend: SortType,
    status:SortType
   
}

export interface GetAllTagesOutput {
    dataList: Array<Tages.TagesExposedSchema>,
    count: number
}

export function getAllTages(pageNumber: number, pageSize: number, whereFilters: GetAllTagesWhereFilters, sortFilter: GetAllTagesSortFilters): Promise<GetAllTagesOutput> {
    return new Promise(async (resolve, reject) => {
        try {
            let offset = (pageNumber - 1) * pageSize;
            let whereF: any = {};
            if (true) {
                let { name, count, addedBy,_id,status} = whereFilters;
                if(!status)
                whereF[TagesFields.status]=TagsStatus.ACTIVE;

                if (name) {
                    whereF[TagesFields.name] = name
                }
                if (count) {
                    whereF[TagesFields.count] = count
                }
                if (status) {
                    whereF[TagesFields.status] = status
                }
                if (addedBy)
                    whereF[TagesFields.addedBy] = addedBy
                    if (_id)
                    whereF[TagesFields._id] = _id
            }
            let sortF: any = {};
            if (true) {
                let { name, count, timeSpend,_id,status } = sortFilter;
                if (!timeSpend)
                    timeSpend = -1;

                if (name) {
                    sortF[TagesFields.name] = name
                }
                if (count) {
                    sortF[TagesFields.count] = count
                }
                if (_id) {
                    sortF[TagesFields._id] = _id
                }
                if (status) {
                    sortF[TagesFields.status] = status
                }
           
            }
            // 'name': { '$regex': req.body.search.value.trim()
            console.log({ whereF });

            let selectFilter: any = {};
            selectFilter[TagesFields.addedBy] = 0;
            var arr: Tages.TagesExposedSchema[] = [];
            let count:number;
            if(pageSize == 0){
                count= await TagsModel.count(whereF);
                arr = await TagsModel.find(whereF).select(selectFilter).sort(sortF).skip(offset) as any;

            }else{
                count= await TagsModel.count(whereF);
                arr = await TagsModel.find(whereF).select(selectFilter).sort(sortF).skip(offset).limit(pageSize + 1) as any;      
            }
                    let output: GetAllTagesOutput =
                    {
                        dataList: arr,
                        count
                    }

                    resolve(output);
            }
        catch (error) {
                dbRejector(reject, error);
            }
        });
}
export function updateTagStatus(tagId: string, status: TagsStatus): Promise<Tages.TagesExposedSchema | null> {
    return new Promise(async (resolve, reject) => {
        try {
            let updateObj: any = {};
            updateObj[TagesFields.status] = status;

            let selectFilter: any = {};
            selectFilter[TagesFields.addedBy] = 0;
            resolve(await TagsModel.findByIdAndUpdate(tagId, updateObj, { new: true }).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}


export function getTagsBysqlId(tagesId: Array<number>): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let selectFilter: any = {};
            selectFilter[TagesFields._id] = 1;
console.log("taginput",tagesId)
            resolve(await TagsModel.find({"sqlId":{ '$in':tagesId}}).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}
export function getAllTags(): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let selectFilter: any = {};
            selectFilter[TagesFields._id] = 1;
            selectFilter[TagesFields.name] = 1;
            selectFilter[TagesFields.sqlId] = 1;

            resolve(await TagsModel.find({}).select(selectFilter) as any);
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}