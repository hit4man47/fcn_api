import mongoose, { Schema } from 'mongoose';


const testimonialSchema = new Schema({
    comment: { type: String, required: true },
    name: { type: String, required: true },
    type: { type: String, required: true, default: 'Client' },
    rating: { type: Number, required: true },
    photoURL: { type: String, required: true },
    addedBy: { type: String, required: true },
    editedBy: { type: String }
}, {
    timestamps: true
});


// Export the model and return your IUser interface
export default mongoose.model('Testimonial', testimonialSchema);

