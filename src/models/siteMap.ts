import mongoose, { Schema, Types } from 'mongoose';
import { ERROR_CODES } from '../utils/constants';
import { newsModel } from '../models/AddUpdateData';
import { dbRejector } from '../utils/dbUtils';


export function getAllData(): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await newsModel.find().select('url').select('updatedDate').lean().exec();

            if (data) {
                resolve(data);
            } else {
                global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, 'News details not found');
                return;
            }
        }
        catch (error) {
            dbRejector(reject, error);
        }
    });
}
