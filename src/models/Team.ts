import mongoose, { Schema } from 'mongoose';
import * as constants from '../utils/constants';
//add order 

const TeamSchema: Schema = new Schema({
    name:{type:String,required:true},
    designation: { type: String, required: true },
    profiles: { type: Array },
    message: { type: String, required: true },
    photoUrl: { type: String },
    status:{type:String,default:constants.TEAM_STATUS.ACTIVE}
    
}, {
    timestamps: true
});

// Export the model and return your IUser interface
export default mongoose.model('Team', TeamSchema);

