import { ImageResize } from "../types/NewTypes";

export type SortType = -1 | 1 | 0
export interface Pagination {
    pageNumber: number,
    pageSize: number
}

export enum ERROR_CODES {
    DATABASE_ERROR = 402,
    DATABASE_DUPLICATE_ERROR_CODE = 1062,
    SESSION_ERRORR = 406,
    ERROR_UNKNOWN_HIDDEN_FROM_USER = 101,
    ERROR_UNKNOWN_SHOW_TO_USER = 102,
    INVALID_REQUEST = 103,
    INVALID_KYC_ID = 404,
    INVALID_MONGOOSE_OBJECTID = 406,
    JWT_ERROR = 404,
    INVALID_JWT_ERROR = 511,
    EXPIRED_JWT_ERROR = 599,
    Invalid_KYC_ID = 404,
    ERROR_CANNOT_ACCESS_SMS_GATEWAY = 408,
    ERROR_CANNOT_FULLFILL_REQUEST = 417,
    PDF_GENERATION_ERROR = 418,
    Invalid_Mongoose_ObjectId = 404,
    INVALID_USER = 106,
    OBJECT_ID_NOT_FOUND_ERROR = 400,
    USER_UNAUTHORISE_ERROR = 104,
    USER_DOESNOT_EXIST = 401,
    USER_INVALID_PASSWORD = 401,
    INVALID_CONTRACT_ID = 403,
    TEMPLATE_NOT_FOUND = 405,
    USER_NOT_FOUND = 106,
    INVALID_COST = 150,
    ORDER_REQUEST_FAILED = 155,
    PAYMENT_REQUEST_FAILED = 156,
    PAYMENT_REQUEST_INSERT_ERROR = 157,
    PAYMENT_REQUEST_UPDATE_ERROR = 158,
    UPDATE_PARTIES_PAYMENT_ERROR = 159,
    UPDATE_ORDER_PAYMENT_ERROR = 160,
    UPDATE_CONTRACT_PAYMENT_ERROR = 161,
    USER_TEMPLATE_UPDATE_ERROR = 162,
    ORDER_DATA_NOT_FOUND = 154,
    CONTRACT_STATUS_UPDATE_ERROR = 166,
    ADD_PARTIES_ERROR = 170,
    ADD_INVITEE_ERROR = 171,
    ACCESS_DENIED = 172,
    INVALID_SIGNATURE_ID = 175,
    INVALID_UPLOADING = 1103,
    KYC_ID_NOT_EXIST = 409,
    INVALID_STATUS = 110,
    SUBCOMMENT_CREATED = 116,
    FAIL_UPDATE_DB=161,
    BUCKET_ERR0R = 509

}




let CONSTANTS = Object.freeze({

    JWT_VERIFICATION_EXPIRY_SEC: 3600,
    BASE_URL_VERIFICATION_PK: 9876540,
    JWT_ACCESS_TOKEN_EXPIRY_SEC: 2 * 3600,
    JWT_REFRESH_TOKEN_EXPIRY_SEC: 3 * 30 * 24 * 3600,
    OTP_VERIFICATION_EXPIRY_MINS: 10,
    FETCH_QUESTION_MIN_LENGTH: 3,
    FETCH_QUESTION_MAX_LENGTH: 500,
    AUTO_QUESTION_SUGGESTION_MAX_LENGTH: 20,
    MOST_POPULAR_COUNT: 3,
    ERROR_DATABASE_ERROR: 101,
    ERROR_UNKNOWN: 102,
    ERROR_NO_ERROR: 103,
    ERROR_LIMIT_REACHED: 109,
    ERROR_SESSION_ERROR: 111,
    ERROR_UNKNOWN_SHOW_TO_USER: 112,
    ERROR_UNKNOWN_HIDDEN_FROM_USER: 113,
    ERROR_MISSING_POST_PARAMS: 114,
    ERROR_PERMISSION_DENIED: 115,
    ERROR_INPUT_ERROR: 116,
    ERROR_INVALID_TOKEN: 404,
    ERROR_EXPIRED_TOKEN: 405,
    NO_OF_MAXIMUM_RESEND: 3,
    NO_OF_MAXIMUM_ATTEMPTS: 3,


    MINIMUM_SEARCH_LENGTH: 5,
    MAX_PAGE_SIZE: 100,
    MAX_DOC_COUNT: 2,

    DEFAULT_PAGINATION: {
        LIMIT: 6
    },

    REGEX_EMAIL_VALIDATE: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    REGEX_URL: /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi,
    DATABASE_DUPLICATE_ERROR: 1062,
    SKIP_STATUS_STATUS: -1,
    CATEGORY_RESPONSE:
    {
        UPDATE_QUERY: "Category updated successfully",
        CREATE_QUERY: "Category Saved Successfully",
        GET_QUERY: "Data fetched successfully"
    },
    MAILING_AGENTS:
    {
        AWS_SES: "AWS",
        SEND_GRID: "SENDGRID"
    },

    USER_STATUS:
    {
        DELETED: 0,
        ACTIVE: 1
    },

    SUBSCRIPTION_STATUS:
    {
        SUBSCRIBED: 1,
        UNSUBSCRIBED: 0
    },
    ACTION_TAKEN:
    {
        NEW_USER: 1,
        GROUPS_UPDATED: 2,
        USER_REENTRY: 3
    },

    SENDER_STATUS:
    {
        DELETED: 0,
        ACTIVE: 1
    },
    IGNORE_FILTER: -1,
    TEMPLATE_EXPITY_TIME_IN_MILLIS: 10 * 60 * 1000,
    SESSION_TIME_IN_MILLIS: 5 * 60 * 1000,
    BASE_URL : "http://18.140.120.37:5005/",

    TOKEN_DELIMITER: '####@@@@####',//no special char so that no issue in regex,
});
export var role_array: Array<TempraryToken.RoleBasedArray> = [{ role: "ADMIN" }, { role: "SENIOR LAWYER" }, { role: "CONTENT WRITER" }, { role: "LAWYER" }, { role: "PERSONAL" }];

export var BlockedTokenArray: Array<TempraryToken.BlockedToken> = new Array();
export enum STATUS {
    ACTIVE = 1,
    NOT_ACTIVE = 0
}
export enum ACCOUNT_SOURCE {
    LOCAL = 1,
    FACEBOOK = 2,
    GOOGLE = 3
}
export enum PHONE_STATUSES {
    VERIFIED = 1,
    NOT_VERIFIED = 0
}

export enum VERIFICATION_STATUS {
    VERIFIED = 1,
    NOT_VERIFIED = 0
}

export enum SUBSCRIPTION_STATUS {
    VERIFIED = 1,
    NOT_VERIFIED = 0
}
export enum EMAIL_STATUSES {
    VERIFIED = 1,
    NOT_VERIFIED = 0
}

export enum IMAGE_URL_STATUS{
    NOT_UPLOADED = 0,
    UPLOADED=1
}


export enum BUCKET
  {
    BUCKET_NAME =  "fcnnewsnew", // "seeeeeeeeee", //"fcnnewsnew", //"feeliumcryptonews",
    BUCKET_REGION =  "ap-south-1",
  }

export enum MIGRATE_DATA_QUALITY_PERCENT
{
    QUALITY = 70
}



export enum USER_ROLES {
    ADMIN = "ADMIN",
    CLIENT = "CLIENT",
    CONTENT_WRITER = "CONTENT_WRITER",
    LAWYER = "LAWYER",
    SENIOR_MANAGER = "SENIOR_MANAGER",
    MANAGER = "MANAGER",
    BUSINESS_MANAGER = "BUSINESS_MANAGER",
    BUSINESS_EXECUTIVE = "BUSINESS_EXECUTIVE",

}

export enum TYPES {
    BUSINESS = "BUSINESS",
    PERSONAL = "PERSONAL"
}

export enum TEAM_STATUS {

    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE"
}

export enum PERMISSION {
    DEFALUT = "READ",
    WRITE = "WRITE",
    READ = "READ"
}
export let COMMENT_STATUS = {
    APPROVED: "APPROVED",
    REJECT: "REJECT",
    PENDING: "PENDING"
}
export enum COMMENT_TYPES {
    COMMENT = 0,
    SUBCOMMENT = 1
}

export enum CATEGORY_ID {

    "5de0139387d28c19b9836d18"=8||10||4,      //   PARENT CATEGORY INDUSTRY for id 8,10,4
    "5de0137687d28c19b9836d17"=7||5||9,           //  PARENT CATEGORY BLOCKCHAIN for id 7,5,9
   "5de802ec8841061450e7bc44" =6,             //PARENT CATEGORY ANALYSIS for id 6   
   "5dee938a11ba5c59e0488f2d"=42           // PARENT CATEGORY  Watch & Learn for id 42
}
export enum SUB_CATEGORY_ID
{
    CRYPTOCURRENCY= "5de0196f87d28c19b9836d20",//SUB CATEGORY CRYPTOCURRENCY   ALSO  USED FOR BITCOIN    8, 4
    TECHNOLOGY="5de018ef87d28c19b9836d1b",   // SUB CATEGORY  TECHNOLOGY USED FOR CATEGORY NAME BLOCKCHAIN TECHNOLOGY  7
    CRYPTO_EXCHANGE_COMPARISON="5de803cc8841061450e7bc46", // SUB CATEGORY CRYPTO EXCHANGE COMPARISON USED FOR CATEGORY NAME CURRENCY EXCHANGE 6
    NEWS="5de018fc87d28c19b9836d1c",         //SUB CATEGORY NEWS IN BLOCKCHAIN CATEGORY USED FOR CATEGORY NAME FINTECH  9
    FINANCE="5de0195987d28c19b9836d1e",  // SUB CATEGORY FINANCE USED FOR CATEGORY NAME MOBILE BANKING  10
    USE_CASE="5de0190b87d28c19b9836d1d", //SUB CATEGORY USER CASE USED CATEGORY NAME GUIDES   5
    PRESS_RELEASE="5dee944011ba5c59e0488f31" // SUB CATEGORY PRESS RELEASES  42

}

// export let BUCKET = {
//     BUCKET_NAME: "feeliumcryptonews",
//     BUCKET_REGION: "ap-south-1",
// }

export let AwsS3 = {
    BUCKET_ACCESS_FORBIDDEN: "You are not having the required permission or bucket belongs to someone else",
    BUCKET_ACCESS_BAD_REQUEST: "You are inputing an invalid bucket name or bucket is migrating or something is missing in your request",
    CHECK_BUCKET_EXIST_ERROR: "Some error occured when try to check bucket exist or not"
}

export let COMMON_ROUTES_VALIDATION = {

    NOT_FOUND: "NOT FOUND",
    ID_NOT_FOUND: "ID NOT FOUND",
    INVALID_LENGTH: "INVALID LENGTH",
    INVALID_TOKEN: "INVALID TOKEN",
    INVALID_EMAIL: "Invalid Email",
    INVALID_PHONE_NUMBER: "Invalid Phone Number",

    MIN_LENGTH: 3,
    MAX_LENGTH: 512,
    PHONE_NUMBER_MIN_LENGTH: 6,
    PHONE_NUMBER_MAX_LENGTH: 16,
    PHONE_NUMBER_LENGTH_ERROR: "invalid phone number length",
    PHONE_NUMBER_TYPE_ERROR: "invalid phone number type",
    MAIL_SEND_SUCCESS: "Mail Sent Successfully",
    MAIL_SEND_FAIL: "Issue in sending mail",
    INVALID_LENGTH_OF_ADDRESS: "INVALID LENGTH OF ADDRESS",
    INVALID_LENGTH_OF_COUNTRY: "INVALID LENGTH OF COUNTRY",
    INVALID_LENGTH_OF_CITY: "INVALID LENGTH OF CITY"
}


export let REGISTRATION_ERROR = {
    EMAIL_EXISTS_ERROR: "THIS EMAIL ALREADY EXISTS",
    USERNAME_EXISTS_ERROR: "THIS USERNAME ALREADY EXISTS",
    PHONE_NUMBR_EXISTS: "THIS PHONE NUMBER ALREADY EXISTS",
    NO_USERDATA_IN_DB: "NO SUCH USER EXISTS IN TEMP DB",
    REQUEST_CANNOT_BE_ACHIEVED: "INTERNAL ERROR",
    CANNOT_SEND_EMAIL: "EMAIL CANNOT BE SEND DUE TO INTERNAL ISSUES",
    DUPLICASY_ISSUE_ERROR: "EMAIL CANNOT BE SENT DUE TO SOME DUPLICACY ISSUES"
}


export let COMMON_DATA_BASE_ERROR = {
    INTERNAL_DATABASE_ERROR: "INTRNAL DATABASE ERROR",
    CAN_NOT_BE_ADDED_INTO_DATABASE: "CAN NOT ADD THIS DATA INTO DATABASE",
    NO_SUCH_USER_EXISTS: "NO SUCH USER EXISTS WITH THIS USER ID"
}

export let NOT_VERIFIED = {
    PHONE_NOT_VERIFIED: "PHONE NUMBER IS NOT VERIFIED",
    EMAIL_ID_VERIFIED: "EMAIL ID IS NOT VERIFIED"
}

export let LOGIN_ERROR = {
    EMAILID_USERNAME_NOT_FOUND: "THIS EMAIL ID OR USERNAME DOES NOT EXISTS",
    INCORRECT_PASSWORD: "THIS PASSWORD IS INCORRECT",
    EMAILID_OR_PHONE_NUMBER_NOT_VERIFIED: "PLEASE VERIFY YOURN EMAIL ID OR PHONE NUMBER FIRST !",
    INVALID_USER_ID: "Invalid userId"
}
export let COMMON_TOKEN_MESSAGE = {
    INVALID_TOKEN: "THIS TOKEN IS INVALID",
    TOKEN_CREATED: "NEW TOKEN HAS BEEN CREATED",
    CANNOT_UPDATE: "CAN NOT UPDATE THIS TOKEN"
}
export let JWT_ERROR = {
    PAYLOAD_NULL: "NO DATA FOUND IN PAYLOAD",
    INVALID_JSONWEB_TOKEN: "UNAUTHERIZED JWT ACCESS",
    TOKEN_EXPIRED: "SESSION TIMEOUT"
}

export let FILE_SEND_BY_EMAIL = {
    TEMPLATE_PDF: "template.pdf"
}


export let NEWSROOM = {
    INVALID_CATEGORY: "INVALID CATEGORY",
    NEWS_TITLE_ERROR: "No News exist for this title",
    CREATE_SUCCESS: "NewsRoom created successfully",
    DETAILS_FETCH_ERROR: "news details cannot be fetched",
    DETAILS_FETCH_SUCCESS: "news details fetched successfully",
    UPDATE_ERROR: "news room updation error",
    UPDATE_SUCCESS: "updated successfully",
    VIEW_UPDATE_ERROR: "view update error",
    VIEW_UPDATE_SUCCESS: "view update success",
    DELETE_ERROR: "delete error",
    DELETE_SUCCESS: "delete successfully",
    ALREADY_DELETE: "Data already deleted",
    FETCH_DATA_SUCCESS: "news room fetched successfully",
    NEWS_QUERY_ERROR: "YOUR QUERY CANNOT BE SEND DUE TO INTERNAL SERVER ISSUE"
}
export let COMMENT = {
    SUBCOMMENT_CREATED: "subcomment created successfully...",
}
export let PANAESHA_COMPANY_DETAILS = {
    COMPANY_NAME: "Panaesha Capital Pvt Ltd",
    COMPANY_ADDRESS: "D-16/3, D Block, Connaught Place",
    COMPANY_LOCATION: "New Delhi, 110001",
    COMPANY_GST_NUMBER: "GSTIN:07AAJCP7392J1ZS",
    COMPANY_WEBSITE: "www.feeliumecontract.com"
}
export const CONTACT_CODE_MESSAGES = {
    MAIL_SENDING_ERROR: "ERROR IN SENDING MAIL",
    THANKYOU_FOR_CONTACTING_US: "THANK YOU FOR CONTACTING FEELIUM CRYPTO NEWS",
    THANKYOU_FOR_PARTNERING_WITH_US: "THANK YOU FOR PARTNERING WITH FEELIUM CRYPTO NEWS",
    ALREADY_SUBSCRIBED: "ALREADY SUBSCRIBED",
    THANKYOU_FOR_SUBSCRIBING_WITH_US: "THANK YOU FOR SUBSCRIBING WITH FEELIUM CRYPTO NEWS",
    SUCCESS: "SUCCESS"
}

export const ADD_UPDATE_DATA_MESSAGES = {
    FAIL_UPDATE_DB: "Failed to update Data in DB",
   
    SUCCESS: "SUCCESS"
}

export const LOGOUT = {
    SUCCESSFULL_LOGOUT: "LOGOUT SUCCESSFULLY"
}

export let EMAIL = {
    EMAIL_SEND_ERROR: "mail can not be send"
}

export let COMMANT = {
    INVALID_STATUS: "invalid commant status"

}

export enum CategoryStatus    
    {
        ACTIVE=1,
        INACTIVE=2,
        DELETE=3
    }

export enum TagsStatus    
{
    ACTIVE=1,
    INACTIVE=2,
    DELETE=3
}  
 export enum AddUpdateStatus
{
    PUBLIC=1,
    PRIVATE=2,
    DELETED=3
}

export enum DATATYPE {
    NEWS="news",
    INFOGRAPHIC="infographic",
    RESEARCH_PAPER="researchPaper",
    PRESS_RELEASE="pressRelease",
    VIDEO="video"

}
export enum PitchStatus    
{
    PENDING="PENDING",
    APPROVED="APPROVED",
    DISAPPROVED="DISAPPROVED"
}

export enum DocStatus
{
    PUBLIC="PUBLIC",
    PRIVATE="PRIVATE",
  
}



export const ImageResizeConstants:ImageResize=
{
    
    ABOUT_TEAM:
    {
        SMALL:{width: 100,height:100},
        MEDIUM:{width:200, height: 200}
    },

    // ADD_OR_UPDATE: // HOME_PAGE -> WATCH AND LEARN  
    // {   
    //     EDITOR_CHOICE:{width: 130,height:65}, 
    //     RELATED_POST :{width: 130,height:75} //In HomPage, Label in Most Recent, lABEL RESARCH pAPER, Insight,  Industry ka relatd category 
    // },
  
    NEWS_IMAGE: 
    {
        SMALL1: {width: 130,height:65}, // ADD_OR_UPDATE EDITOR_CHOICE
        SMALL2: {width: 130,height:75}, // ADD_OR_UPDATE RELATED_POST
        SMALL3: {width: 100,height:100},
        SMALL: {width: 100,height:100},
        MEDIUM:{width:340, height: 200},
        LARGE1: {width: 350, height: 150},  // HomePage Most recent first: 350x150 // fcn focus
        LARGE2:{width:350, height: 175}, //HomePage: PRESS RELEASE, WATCH_AND_LEARN, the big fcn,  : 350x175 // Relate category news first
    }
}


export let SIZES_FOR_MIGRATE_IMAGES = [ImageResizeConstants.NEWS_IMAGE.SMALL, ImageResizeConstants.NEWS_IMAGE.SMALL1, ImageResizeConstants.NEWS_IMAGE.SMALL2, ImageResizeConstants.NEWS_IMAGE.SMALL3, ImageResizeConstants.NEWS_IMAGE.MEDIUM, ImageResizeConstants.NEWS_IMAGE.LARGE1, ImageResizeConstants.NEWS_IMAGE.LARGE2]


export { CONSTANTS };


