import { CONSTANTS } from "./constants";

var LOGIN_LOGOUT_MAP = new Map<string, Logout.BlacklistToken>();

export function getLoginLogoutMap(loginHistoryId: string): Logout.BlacklistToken | null {
    let user = LOGIN_LOGOUT_MAP.get(loginHistoryId);
    if (user) {
        return user;
    }
    else {
        return null;
    }
}

export function enterUserloginIdInBlackListMap(loginHistoryId: string, userId: string, logoutTimeStamp: number) {
    var blackListUser = {
        loginHistoryId,
        userId,
        logoutTimeStamp
    }
    LOGIN_LOGOUT_MAP.set(loginHistoryId, blackListUser);
}

export function checkIfUserExistsInBlackList(loginHistoryId: string, userId: string): boolean {
    for (var [key, value] of LOGIN_LOGOUT_MAP) {
        if (value.loginHistoryId == loginHistoryId && value.userId == userId) {
            return true;
        }
    }
    return false;
}

export function showLoginLogoutMap() {
    console.log("size is : ", LOGIN_LOGOUT_MAP.size);
    for (var [key, value] of LOGIN_LOGOUT_MAP) {
        console.log("KEY:" + key);
        console.log("VALUE:" + value);
    }
}

export function deleteUserFromBlackList(loginHistoryId: string) {
    let latestTimeStamp = Date.now();

    for (var [key, value] of LOGIN_LOGOUT_MAP) {
        if (value.logoutTimeStamp + CONSTANTS.JWT_ACCESS_TOKEN_EXPIRY_SEC > latestTimeStamp) {
            LOGIN_LOGOUT_MAP.delete(loginHistoryId);
        }
    }
}


var CHANGE_PASSWORD_MAP = new Map<string, ChangePassword.BlackList>();

export function enterChangePassInBlackListMap(userId: string, currentTimeStamp: number) {
    let changePasswordBlackList = {
        userId,
        currentTimeStamp

    }
    CHANGE_PASSWORD_MAP.set(userId, changePasswordBlackList);
}

export function checkIfUserExistsChangePasswordList(userId: string, JWTCreatedTimeStamp: number): boolean {
    for (var [key, value] of CHANGE_PASSWORD_MAP) {
        if (value.currentTimeStamp > JWTCreatedTimeStamp && value.userId == userId) {
            return true;
        }
    }
    return false;
}


