import * as randomstring from 'randomstring'
import { CONSTANTS, ERROR_CODES } from './constants';
import cryptoRandomString from 'crypto-random-string';

export function genrateRefreshToken(): Promise<string> {
    return new Promise(async (resolve, reject) => {
        const str = randomstring.generate()
        if (!str) {
            global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "Refresh token can't be genrated ");
            return;
        }
        else {
            resolve(str)
        }

    })
}

export function genrateRandomString(length: number): Promise<string> {
    return new Promise(async (resolve, reject) => {
        const str = randomstring.generate(length);
        if (!str) {
            global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, "Random string can't be genrated ");
            return;
        }
        else {
            resolve(str)
        }

    })
}

export function generateOTP(length: number, characters: string): string {

    let generatedOtp = cryptoRandomString({ length: 6, characters: '1234567890' });
    console.log("otpgenRater    ", generatedOtp);
    if (!generatedOtp) {
        return "";
    }
    else {
        return generatedOtp;
    }


}