import * as jwt from 'jsonwebtoken';
import { CONSTANTS, ERROR_CODES, USER_ROLES, JWT_ERROR } from './constants';


export function createJWTToken(payload: any, expiresInSecs: number, secretKey: string): Promise<string> {
    return new Promise((resolve, reject) => {
        jwt.sign(payload, secretKey, { expiresIn: expiresInSecs }, function (err, token) {
            if (err) {
                console.log("111111111111", err);
                global.rejector(reject, ERROR_CODES.JWT_ERROR, "JWT ERROR");
                return;
            }
            else {
                resolve(token);
            }
        })
    });
}

/**
 * return payload if token is valid
 * @param token 
 * @param secretKey 
 */
export function verifyJWTToken(token: string, secretKey: string): Promise<any> {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secretKey, function (err: jwt.VerifyErrors, payload) {
            if (err) {
                let errMessage = "";
                // if (err.name === "JsonWebTokenError")
                if (err.name === "TokenExpiredError") {
                    errMessage = "token is expired";
                    global.rejector(reject, ERROR_CODES.EXPIRED_JWT_ERROR, JWT_ERROR.TOKEN_EXPIRED);
                    return
                }
                errMessage = err.name;
                global.rejector(reject, ERROR_CODES.INVALID_JWT_ERROR, JWT_ERROR.INVALID_JSONWEB_TOKEN);
                return
            }
            else {
                resolve(payload)
                return
            }
        });
    });
}