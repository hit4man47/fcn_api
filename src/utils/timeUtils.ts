// const moment = require('moment')
const moment = require('moment');
const momenttz = require('moment-timezone');

const timeZone = 'Asia/Calcutta';

export const now = () => {
    return moment.utc().format()
};

let getLocalTime = () => {
    return moment().tz(timeZone).format()
};

let convertToLocalTime = (time: number) => {
    return momenttz.tz(time, timeZone).format('LLLL')
};