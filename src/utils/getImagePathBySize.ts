import {getImagesMimeTypes} from '../utils/fileUpload';
import * as mime from 'mime';
import {ImageResizeConstants} from "../utils/constants"
import { ImageDimen } from "types/NewTypes";



export function changePathFromS3(arrObj:any,imgsize: ImageDimen, filePathStr: string):Promise<any>
{
    return new Promise((resolve, reject) =>
    {
        try
        {
            for (const data of arrObj) {
                const imageMimeTypes = getImagesMimeTypes();
                let mimeType = mime.getType(data.photoUrl) ;
                if(imageMimeTypes.indexOf(mimeType || '')>=0)
                 {
                    const u: string = data[filePathStr]; //data.photoUrl

                    console.log("u===", u);
                    const lastIndexOfDot = u.lastIndexOf('.');
                    // const choosenDimen = ImageResizeConstants.ABOUT_TEAM.SMALL; // ImageResizeConstants.ADD_OR_UPDATE.RELATED_POST;
                    const newUrl = `${u.substr(0,lastIndexOfDot)}_${imgsize.width}x${imgsize.height}${u.substr(lastIndexOfDot)}`;
                    data[filePathStr] = newUrl;
                }
            }
            resolve(arrObj);
            return;
        }
        catch(e)
        {
            reject(e);
            return;
        }
    })
}