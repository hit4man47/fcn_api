
import * as path from 'path';
import * as fs from 'fs';


export function saveFileFromTempToMain(upload: any, docPath: Array<string> , tmpFolder:string, mainFolder: string, dbFolderPath: string):Promise<Array<string>>
{
    return new Promise(async (resolve, reject) => {
        try
        {
            //Save uploaded files path
            if (upload && upload.length > 0) 
            {
                for (let i = 0; i < upload[0].uniqueFilesNamesList.length; i++) {
                
                    let oldPath = path.join(__dirname + tmpFolder + upload[0].uniqueFilesNamesList[i]);
                    let newPath = path.join(__dirname + mainFolder + upload[0].uniqueFilesNamesList[i]);
                    let pathToBeSave = dbFolderPath + upload[0].uniqueFilesNamesList[i];

                    fs.copyFile(oldPath, newPath, (err) => {
                        if (err) 
                        {
                            reject(err);
                        }
                        console.log(`${upload[0].uniqueFilesNamesList[i]} is successsfully saved!!!!`);
                    });
                    docPath.push(pathToBeSave);
                }
                resolve(docPath);
            }
        }
        catch(e)
        {
            reject(e);
        }
    })    
}