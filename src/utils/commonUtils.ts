// import CategoryModel from "../models/Category";


// export const checkCategoryExist = (category: string): Promise<boolean> => {
//     return new Promise(async (resolve, reject) => {
//         try {
//             const checkCat = await CategoryModel.findOne({ 'categoryId': category });
//             if (checkCat) {
//                 resolve(true);
//             }
//             else {
//                 resolve(false);
//             }
//             console.log("resolved::::::::");
//         }
//         catch (e) {
//             reject(e);
//         }
//     });
// }


export const createUniqueUrl = (title: string): Promise<string> => {
    return new Promise(async (resolve, reject) => {
        try {

             const specialCharacterRegEx = /[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~”“‘’]/g;
             const replaceSpaceWithDash = (text: string) => {
                if (text && text !== undefined && text !== null) {
                    return text.replace(/\s{1,}/g, '-');
                }
                return '';
            }

            const textWithoutSpecialCharacters = title.replace(specialCharacterRegEx, '');
            resolve(replaceSpaceWithDash(textWithoutSpecialCharacters).toLocaleLowerCase());
    
        }
        catch (e) {
            reject(e);
        }
    });
}

export function delay(secs:number):Promise<void>
{
    return new Promise((resolve, reject) =>
    {
        setTimeout(()=>
        {
            resolve();
        },secs*1000);    
    });
}


