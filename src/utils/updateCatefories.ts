import * as request from 'request';
import { ERROR_CODES } from './constants';
import { json } from 'body-parser';

export function getData(per_page:number,page:number): Promise<any> {
    return new Promise(async (resolve, reject) => {

        try {
            request.get(
                {
                    url: `https://www.fintechcryptonews.com/wp-json/wp/v2/posts?per_page=${per_page}&page=${page}&_embed`
                },
                function(error: any, response: any, body: any)
                {
                    console.log(per_page,page,error);
                    if(error)
                    {
                        console.log("Api post type Error:",error);
                        global.rejector(reject,ERROR_CODES.ERROR_CANNOT_ACCESS_SMS_GATEWAY,"Unable to get through the api");
        
                    }
                    else 
                    {
                        let result=JSON.parse(body);
                        if(!result.length)
                        resolve([]);
                        else
                        resolve(result);
                    }
                    
                })
        }
        catch (error)
        {
            reject(error)
        }

    })
}



export function getCategoryData(per_page:number,page:number): Promise<any> {
    return new Promise(async (resolve, reject) => {

        try {
            request.get(
                {
                    url:`https://www.fintechcryptonews.com/wp-json/wp/v2/categories?per_page=${per_page}&page=${page}&_embed`
                    
                },
                function(error: any, response: any, body: any)
                {
                    console.log(per_page,page,error);
                    if(error)
                    {
                        console.log("Api post type Error:",error);
                        global.rejector(reject,ERROR_CODES.ERROR_CANNOT_ACCESS_SMS_GATEWAY,"Unable to get through the api");
        
                    }
                    else 
                    {
                        let result=JSON.parse(body);
                        if(!result.length)
                        resolve([]);
                        else
                        resolve(result);
                    }
                    
                })
        }
        catch (error)
        {
            reject(error)
        }

    })
}



export function getTags(per_page:number,page:number): Promise<any> {
    return new Promise(async (resolve, reject) => {

        try {
            request.get(
                {
                    url:`https://www.fintechcryptonews.com/wp-json/wp/v2/tags?per_page=${per_page}&page=${page}&_embed`
                    
                },
                function(error: any, response: any, body: any)
                {
                    console.log(per_page,page,error);
                    if(error)
                    {
                        console.log("Api post type Error:",error);
                        global.rejector(reject,ERROR_CODES.ERROR_CANNOT_ACCESS_SMS_GATEWAY,"Unable to get through the api");
        
                    }
                    else 
                    {
                        let result=JSON.parse(body);
                        if(!result.length)
                        resolve([]);
                        else
                        resolve(result);
                    }
                    
                })
        }
        catch (error)
        {
            reject(error)
        }

    })
}