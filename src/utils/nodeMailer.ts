import * as nodemailer from 'nodemailer';
import { serverConfig } from '../init/config';
// import * as ejs from 'ejs'
//import * as sgTransport from 'nodemailer-sendgrid-transport';
import * as express from 'express';
import { rejects } from 'assert';
import { EJSON } from 'bson';
import { request } from 'http';
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
// import { getTemplate } from '../views/mailOtp'

// import {getEmailVerificationLink} from '../views/newVerifyEmailLink' 

var transporter = nodemailer.createTransport({
  host: serverConfig.smtpServer.host,
  port: serverConfig.smtpServer.port,
  secure: false, // true for 465, false for other ports
  auth:
  {
    user: serverConfig.smtpServer.user, // your domain email address
    pass: serverConfig.smtpServer.password // your password
  }
});


export function sendHtml(to: string, subject: string, message: any) {

  var mailOptions =
  {
    from: serverConfig.smtpServer.sender.admin,
    to: to,
    subject: subject,
    html: message
  };
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log("Email ERROR", error);
    } else {
      console.log('Email sent: ' + info.response + " to:" + to);
    }
  });
}

//================================================================
export function sendEmailCommon(senderEmailId: string, data: any, message: string, ejsFilePath: string): Promise<boolean> {

  return new Promise(async function (resolve, reject) {
    try {
      // newSignUpSuccess.ejs

      console.log("ejsFilePath========", ejsFilePath);
      const str = fs.readFileSync(path.join(__dirname, `../views/${ejsFilePath}`), 'utf8');
      var messageHtml = ejs.render(str, { data: data });
      sendHtml(senderEmailId, message, messageHtml);
      resolve(true);
    }
    catch (e) {
      console.log("signUp Template error:", e);
      reject(e);
    }
  })
}

export function sendEmailSignUpSuccess(senderEmailId: string, data: string, message: string): Promise<boolean> {
  return new Promise(async function (resolve, reject) {
    try {
      // newSignUpSuccess.ejs
      const str = fs.readFileSync(path.join(__dirname, '../views/newSignUpSuccess.ejs'), 'utf8');
      var messageHtml = ejs.render(str, { data: data });
      sendHtml(senderEmailId, "Welcome to Feelium E-Contracts", messageHtml);
      resolve(true);
    }
    catch (e) {
      console.log("signUp Template error:", e);
      reject(e);
    }
  })
}


export function sendEmailVerificationMail(senderEmailId: string, data: string, message: string): Promise<boolean> {
  return new Promise(async function (resolve, reject) {
    try {
      var template =
        `<h2>Pancap Email Verification</h2>
        <br>
        <b>`+ message + `
        <b>
        <br>
        `+ data + `
        <br>`;

      sendHtml(senderEmailId, "Email Verification", template);
      resolve(true);
    }
    catch (e) {
      console.log("sendEmailVerificationMail ERROR:", e);
      reject(e);
    }
  })
}



export function sendContactUsMail(senderEmailId: string, data: FCN.CreateContactInput): Promise<boolean> {
  return new Promise(async function (resolve, reject) {
    try {
      const filePath = path.join(__dirname, '../views/sendDefaultResponse.ejs');
      console.log("============>>>>>>>>>>>>>>>>its a filepath with dirname", filePath)
      const str = fs.readFileSync(filePath, 'utf8');
      var messageHtml = ejs.render(str, { data: data });
      let template: string = messageHtml.toString();
      sendHtml(senderEmailId, template, "Thank you for contacting Feelium E-contracts!");
      resolve(true);
    }
    catch (e) {
      console.log("sendContactUsMail ERROR:", e);
      reject(e);
    }
  })
}

export function sendPartnerWithUsMail(senderEmailId: string, data: FCN.CreatePartnerWithUsInput): Promise<boolean> {
  return new Promise(async function (resolve, reject) {
    try {
      const filePath = path.join(__dirname, '../views/sendDefaultResponse.ejs');
      console.log("============>>>>>>>>>>>>>>>>its a filepath with dirname", filePath)
      const str = fs.readFileSync(filePath, 'utf8');
      var messageHtml = ejs.render(str, { data: data });
      let template: string = messageHtml.toString();
      sendHtml(senderEmailId, template, "Thank you for partnering with Feelium E-contracts!");
      resolve(true);
    }
    catch (e) {
      console.log("sendPartnerWithUsMail ERROR:", e);
      reject(e);
    }
  })
}

export function sendSubscribeWithUsMail(senderEmailId: string, data:FCN.CreateSubscribeWithUsRequest): Promise<boolean> {
  return new Promise(async function (resolve, reject) {
    try {
      const filePath = path.join(__dirname, '../views/sendDefaultResponse.ejs');
      console.log("============>>>>>>>>>>>>>>>>its a filepath with dirname", filePath)
      const str = fs.readFileSync(filePath, 'utf8');
      var messageHtml = ejs.render(str, { data: data });
      let template: string = messageHtml.toString();
      sendHtml(senderEmailId, template, "Thank you for subscribing with Feelium E-contracts!");
      resolve(true);
    }
    catch (e) {
      console.log("sendSubscribeWithUsMail ERROR:", e);
      reject(e);
    }
  })
}