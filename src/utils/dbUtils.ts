import { MongoError } from "mongodb";
import { CONSTANTS } from "./constants";

export function dbRejector(reject:Function,error:string)
{
    global.rejector(reject,CONSTANTS.ERROR_DATABASE_ERROR,error,error);
}

export function isDuplicateError(error:MongoError|null):boolean
{
    if(error && error.name === 'MongoError' &&  error.code === 11000)
    {
        return true;
    }
    else
    {
        return false;
    }
}