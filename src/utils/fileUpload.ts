import * as fs from 'fs';
import * as path from 'path';
import {  ERROR_CODES, BUCKET } from '../utils/constants';
import * as express from 'express';
import { inspect } from 'util';
var Busboy = require('./busboy/lib/main');
import { delay } from './commonUtils';
import * as responseHandler from '../init/responseHandler';
import { MyExpressRequest, U, myRes } from '../types/customTypes';
import * as AWS from 'aws-sdk';  
import { AwsS3} from '../utils/constants' // BUCKET,
import { ImageDimen } from '../types/NewTypes';
import * as mime from 'mime';
import * as jimp from 'jimp';


import e = require('express');



// create s3 instance
const s3 = new AWS.S3()

const checkBucketParams : AWS.S3.HeadBucketRequest = {
    Bucket:BUCKET.BUCKET_NAME
};


//configuring the AWS environment
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: BUCKET.BUCKET_REGION,
    apiVersion: '2006-03-01'  // This is API version of AWS, (not ours). Don't change it, unless not using new version of the AWS API  //  2006-03-01

});


const createBucketParams : AWS.S3.CreateBucketRequest= {
    Bucket: BUCKET.BUCKET_NAME,
    ACL: "private",// 'public-read',
    CreateBucketConfiguration: 
    {    
        LocationConstraint: BUCKET.BUCKET_REGION,// BUCKET.BUCKET_REGION 
    }    
}


export function checkBucketExists(checkBucketParams: AWS.S3.HeadBucketRequest) : Promise<boolean>
{
    return new Promise(async (resolve, reject) =>
    {
        try
        {

            s3.headBucket( checkBucketParams,( (err: AWS.AWSError, data) => {
                if(err)
                {
                console.log("err====", err);
                    switch(err.statusCode)
                    {
                        case 403: //err.code == 'Forbidden'
                        {
                            console.log("nndndnd");
                            global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_HIDDEN_FROM_USER, AwsS3.BUCKET_ACCESS_FORBIDDEN)
                            return;
                        }
                        case 404: //  err.code == 'NotFound' => Can create new bucket and given bucket name available
                        {   
                            resolve(false);
                            return;
                        }   
                        case 400: // err.code: 'BadRequest'
                        {
                            global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_HIDDEN_FROM_USER, AwsS3.BUCKET_ACCESS_BAD_REQUEST)
                            return;
                        }
                        default:
                        {
                            global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_HIDDEN_FROM_USER, AwsS3.CHECK_BUCKET_EXIST_ERROR)
                            return;
                        }
                    }
                }
                resolve(true);
                return;
            }) );
           
        }
        catch(e)
        {
            reject(e);
        }
    });
}

export function createBucketUtils(createBucketParams: AWS.S3.CreateBucketRequest) : Promise<AWS.S3.CreateBucketOutput | null>
{
    return new Promise(async (resolve, reject) =>
    {
        try
        {
            s3.createBucket( (createBucketParams), ((err: AWS.AWSError, data: AWS.S3.CreateBucketOutput)=>{

                if(err)
                {
                    console.log("err===", err);
                    switch(err.code)
                    {

                        case 'BucketAlreadyOwnedByYou':// statusCode:409
                        case 'BucketAlreadyExists':// statusCode:409
                        case 'InvalidBucketName': // statusCode: 400 ERROR_UNKNOWN_SHOW_TO_USER
                        {
                            global.rejector(reject, ERROR_CODES.BUCKET_ERR0R,err.message);
                            return;
                        }
                        default:
                        {
                            global.rejector(reject, ERROR_CODES.ERROR_UNKNOWN_SHOW_TO_USER, err.message);
                            return;
                        }
                    }
                }                
                resolve(data);
                return;
            })
            ); 
        }
        catch(e)
        {
            reject(e);
        }
    });
}



export function keyUploadingUtils(params: AWS.S3.PutObjectRequest) : Promise<any>
{
    return new Promise(async (resolve, reject) =>
    {
        try
        {
            s3.upload( params, (err: Error, data: AWS.S3.ManagedUpload.SendData)=>{
                if(err)
                {
                    reject(err);
                    return;
                }
                resolve(data.Location);    
            });

        }
        catch(e)
        {
           
            reject(e);
        }
    });
}


export function middle(req: express.Request, mimeLimits: any): Promise<any> {
    console.log("*********************", req.headers['content-length']);


    return new Promise((resolve, reject) => {
        var busboy = new Busboy({ headers: req.headers, mimeLimits });
        let uploadedFilePathList: Array<string> = [];
        let anyError = false;



        let uploadedFiles: Array<{ tag: string, uniqueFilesNamesList: Array<string> }> = [];

        busboy.on('file', function (fieldname: string, file: any, filename: string, encoding: string, mimetype: string) {
            let uniqueName = `${Date.now()}_${filename.replace(/[^a-z0-9._]+/gi, "").toLowerCase()}`;
            let saveTo = path.join(__dirname + "/../../src/temp", uniqueName);

            let success = true;
            //console.log('File [' + fieldname + ']: filename: ' + filename + ', encoding: ' + encoding + ', mimetype: ' + mimetype);
            file.on('data', function (data: any) {
                //console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
            });

            file.on('end', function () {
                console.log('File [' + filename + '] Finished');
                if (success) {
                    let uniqueFilesNamesList: Array<string> = [];
                    for (let a of uploadedFiles) {
                        if (a.tag == fieldname) {
                            uniqueFilesNamesList = a.uniqueFilesNamesList;
                            break;
                        }
                    }
                    if (!uniqueFilesNamesList) {
                        uniqueFilesNamesList = [];
                    }
                    let isFirst = uniqueFilesNamesList.length == 0;
                    uniqueFilesNamesList.push(uniqueName);

                    if (isFirst)
                        uploadedFiles.push({ tag: fieldname, uniqueFilesNamesList });
                }
                uploadedFilePathList.push(saveTo);

            });

            const uploadError = function (eventName: string) {
                success = false;
                anyError = true;
                console.log(`Upload error ${eventName}`);
            }

            file.on('limit', function () {
                uploadError('limit');
            });

            file.on('partsLimit', function () {
                uploadError("null");
            });

            file.on('filesLimit', function () {
                uploadError("null");
            });

            file.on('fieldsLimit', function () {
                uploadError("null");
            });

            file.on('error', function () {
                uploadError("null");
            });


            file.pipe(fs.createWriteStream(saveTo));
        });

        busboy.on('field', function (fieldname: string, val: string, fieldnameTruncated: boolean, valTruncated: boolean, encoding: string, mimetype: string) {
            if (!req.body) {
                req.body = {};
            }
            req.body[`${fieldname.trim()}`] = val.trim();
            //console.log('Field [' + fieldname + ']: value: ' + inspect(val));
        });

        busboy.on('finish', function () {
            if (anyError) {
                for (let saveTo of uploadedFilePathList) {
                    console.log("unlinking", saveTo);
                    fs.unlink(saveTo, () => { });
                }

                reject("Failed to upload");
            }
            else {
                // setTimeout(resolve(uploadedFiles), ()=>{})
                setTimeout(() => {
                    resolve(uploadedFiles);

                }, 1000);
            }
        });

        req.pipe(busboy);
    });
}

export interface UploadConfig {
    folderName: string,
    maxFileCount: number,
    fileTypeConfig: FileTypeConfig[]
}

export interface FileTypeConfig {
    allowedMineTypes: string[],
    maxSizeOfOneFile: number,
    maxFileCount: number
}


export function uploadingMiddleWare(req: MyExpressRequest, res: express.Response, next: Function, mimeLimits: UploadConfig, imageResizerDimensList?:ImageDimen[],imageCompressionQualityPercent?:number)
{
    var busboy = new Busboy({ headers: req.headers, mimeLimits });
    let uploadedFilePathList: Array<string> = [];
    let anyError = false;

    let errorMessage = "Uploading Error";
    let uploadData: U[] = [];
    let success = true;
    const uploadError = function (eventName: string) {

        success = false;
        anyError = true;
        errorMessage = `Uploading Error ${eventName}`;
        console.log(`Upload error ${eventName}`);
    }
    busboy.on('file', function (fieldname: string, file: any, filename: string, encoding: string, mimetype: string) {
        let uniqueName = `${Date.now()}_${filename.replace(/[^a-z0-9._]+/gi, "").toLowerCase()}`;

        console.log("*********************************************************")
        console.log("mimetype ::",mimetype)
        console.log("*********************************************************")


        if (!fs.existsSync(path.join(__dirname + "../../../temp"))) {
            fs.mkdirSync(path.join(__dirname + "../../../temp"));
        }
        let saveTo = path.join(__dirname + "../../../temp", uniqueName);

        console.log("saveTo===", saveTo);

        file.on('data', function (data: any) {
        });
        
        file.on('end', function () {
         setTimeout(() => {
            if (success) {
                if(!mimeLimits.folderName){
                    mimeLimits.folderName=req.body.type;
                }
                console.log("mimeLimits.folderName2",mimeLimits.folderName)
        
                let dbpath = `assets/${mimeLimits.folderName}/${uniqueName}`;
        
                console.log("dbpath===", dbpath);
        
                let fileData: U =
                {
                    name: uniqueName,
                    fullPath: saveTo,
                    tag: fieldname,
                    dbpath: dbpath,
                    typeOfFileUploaded:mimetype,
                }
                uploadData.push(fileData);                
            }

            uploadedFilePathList.push(saveTo); 

         }, 1000);

        });
        file.on('limit', function () {
            uploadError('limit');
        });

        file.on('partsLimit', function () {
            uploadError("partsLimit");
        });

        file.on('fieldsLimit', function () {
            uploadError("file size or type is not correct");
        });

        file.on('error', function () {
            uploadError("unknown error");
        });


        file.pipe(fs.createWriteStream(saveTo));
    });

    busboy.on('field', function (fieldname: string, val: string, fieldnameTruncated: boolean, valTruncated: boolean, encoding: string, mimetype: string) {
        if (!req.body) {
            req.body = {};
        }
        req.body[`${fieldname.trim()}`] = val.trim();
    });

    busboy.on('finish', async function () {
        try
        {
                // if (anyError) {
                //     for (let saveTo of uploadedFilePathList) {
                //         console.log("unlinking", saveTo);
                //         fs.unlink(saveTo, () => { });
                //     }

                //     let resError: NodeJS.ApiErrorType = { status: ERROR_CODES.INVALID_UPLOADING, message: errorMessage };
                //     responseHandler.sendErrorResponse(res, resError);
                // }
            if(anyError)
            {
                throw new Error("Failed to upload file(s)");      
            }
            else 
            {

                await delay(1);

                // const imageMimeTypes = getImagesMimeTypes();
                const imageMimeTypes  = getImagesMimeCustom();
                // check bucket exist or not
                let doesBucketExist = await checkBucketExists(checkBucketParams);

                // Creating a new bucket, if doesn't exists
                if(!doesBucketExist) 
                {
                    let createBucketRes: AWS.S3.CreateBucketOutput | null = await createBucketUtils(createBucketParams);
                    if(createBucketRes)
                    {
                        console.log("Bucket Successfully created!!!===:::0", createBucketRes.Location);
                    }
                }

                let resData : myRes[] = [];

                if(uploadData)
                {
                    for(let data of uploadData)
                    {

                        // console.log("\n\n\n\ndata===", data);
                        let myFile = fs.readFileSync(data.fullPath);

                        let uploadObjectParams :AWS.S3.PutObjectRequest =  {
                            Bucket: BUCKET.BUCKET_NAME,
                            Key: data.dbpath,
                            Body: myFile,
                            ACL: "public-read",
                            ContentType: data.typeOfFileUploaded
                        }

                        let s3Path = await keyUploadingUtils(uploadObjectParams);

                        console.log("s3Path===", s3Path);

                        const mimeType= mime.getType(data.fullPath);

                    
                        let resizeImagePaths: string[] = [];

                        if( imageResizerDimensList && imageMimeTypes.indexOf(mimeType || '')>=0)
                        {
                            const quality= (imageCompressionQualityPercent && imageCompressionQualityPercent>0 && imageCompressionQualityPercent<=100)?imageCompressionQualityPercent:60;
                            resizeImagePaths = await imageResizer(data.fullPath,imageResizerDimensList,quality, mimeLimits.folderName);
                        }

                        // console.log("resizeImagePaths===", resizeImagePaths);

                        let fileData: myRes = {
                            name: data.name,
                            fullPath: data.fullPath,
                            tag: data.tag,
                            dbpath: data.dbpath,
                            resizedImage: resizeImagePaths
                        }
                        // console.log("fileData==", fileData);
        
                        resData.push(fileData)
                    }
                }
                req.extendedData.uploadData = resData; 
                console.log("req.extendedData.uploadData===", req.extendedData.uploadData);
                next();

              
                
                    // setTimeout(() => {
                    //     if (uploadData) {
                    //         uploadData.forEach(imageData => {
                    //             let source = imageData.fullPath;
                    //             if(!mimeLimits.folderName){
                    //                 mimeLimits.folderName=req.body.type;
                    //             }
                    //             console.log("mimeLimits.folderName1",mimeLimits.folderName)
                    //             let destination = path.join(__dirname, '../../assets/', mimeLimits.folderName, imageData.name);
                    //             console.log("dest PATH", path.join(__dirname, '../../assets/', mimeLimits.folderName));

                    //             if (!fs.existsSync(path.join(__dirname, '../../assets/', mimeLimits.folderName))) {
                    //                 fs.mkdirSync(path.join(path.join(__dirname, '../../assets/', mimeLimits.folderName)));
                    //             }
                    //             fs.copyFile(source, destination, (err) => {
                    //                 if (err) {
                    //                     uploadError("File copy error");
                    //                 }
                    //             })
                    //         });
                    //     }
                    //     req.uploadData = uploadData;
                    //     next();
                    // }, 1000);
            }
        }
        catch(e)
        {
            console.log("err===", e);

            for (let saveTo of uploadedFilePathList) {
                console.log("unlinking", saveTo);
                fs.unlink(saveTo, () => { });
            }

            // for(let data of uploadData)
            // {
            //     // console.log("data===", data);
            //     let val:AWS.S3.DeleteObjectRequest = {
            //         Bucket: BUCKET.BUCKET_NAME,
            //         Key: data.dbpath
            //     }
            //     deleteAWSKey(val)
                
            //     // let destination = path.join(__dirname, '../../assets/', mimeLimits.folderName, data.name);
            //     // fs.unlink(destination, () => { });
            // }

            let resError: NodeJS.ApiErrorType = { status: ERROR_CODES.INVALID_UPLOADING, message: errorMessage };
            responseHandler.sendErrorResponse(res, resError);
    

        }
        
    });

    busboy.on('filesLimit', function () {
        uploadError("filesLimit");
    });

    

    //rough size validation
    let requestSizeInBytes= req.headers['content-length'] || 0;
    requestSizeInBytes=parseInt(requestSizeInBytes as string);
    
    let maxSizeAllowed= 0;
    for(let fc of mimeLimits.fileTypeConfig)
    {
        maxSizeAllowed+=(fc.maxFileCount*fc.maxSizeOfOneFile);
    }

    maxSizeAllowed+=5*1024*1024;
    console.log("***",{requestSizeInBytes},{maxSizeAllowed});
    if(requestSizeInBytes>maxSizeAllowed)
    {
        let resError: NodeJS.ApiErrorType = { status: ERROR_CODES.INVALID_UPLOADING, message: "Invalid Upload Size!!" };
        responseHandler.sendErrorResponse(res, resError);    
    }
    else
    {
        req.pipe(busboy);
    }
}

export function getImagesMimeTypes(): string[] {
    return ["image/png", "image/jpeg", "image/gif", "image/jpg", "image/x-icon"];
}

export function getImagesMimeCustom(): string[] {
    return ["image/png", "image/jpeg", "image/jpg"];
}

export function getPdfMimeTypes(): string[] {
    return ["application/pdf"];
}

export function getVideoMimeTypes(): string[] {
    return ["video/mp4", "video/x-flv"];
}




export function deletefiles(imageData: U[]): boolean {
    if (imageData) {
        imageData.forEach(image => {
            let filepath = path.join(__dirname, '../../', image.dbpath);
            fs.unlink(filepath, (err) => { })
        })

    }

    return true;
}



export function imageResizer(src:string,imageResizerDimensList:ImageDimen[],quality:number, folderName: string) : Promise<Array<string>>
{
    return new Promise(async (resolve, reject) =>
    {
        try
        {
            //console.log("src==", src);
            
           // console.log(quality);

            let res= [];
        
            let i = 0;

            // if (!fs.existsSync(path.join(__dirname + `../../../assets/${folderName}`))) {
            //     fs.mkdirSync(path.join(__dirname + `../../../assets/${folderName}`));
            // }
           
            for(let dimen of imageResizerDimensList)
            {
                const image= await jimp.read(src);
                
                const newImageName=`${path.basename(src,path.extname(src))}_${dimen.width}x${dimen.height}${path.extname(src)}`;

                const dest=path.resolve(path.dirname(src),newImageName);
                
                await image.resize(dimen.width,dimen.height).quality(quality).write(dest); 
                res.push(newImageName);
            }

            // console.log("res===", res);

            let compressedFile = []

            for(let img of res)
            {
                let compressedImageSrc = path.resolve( path.dirname(src), img);    
               //  console.log("compressedImageSrc==", compressedImageSrc);
                let key = `assets/${folderName}/${img}`;                
                // console.log("key===", key);
                
                let readFileContent = fs.readFileSync(compressedImageSrc);
                
                let uploadObjectParams :AWS.S3.PutObjectRequest =  {
                    Bucket: BUCKET.BUCKET_NAME,
                    Key: key,
                    Body: readFileContent,
                    ACL: "public-read",
                    ContentType: mime.getType(src) as string
                }
                let s3Path = await keyUploadingUtils(uploadObjectParams);
               //  console.log("s3Path", s3Path);
                // compressedFile.push(s3Path)
                compressedFile.push(key);
            }
            console.log("\n");
            resolve(compressedFile);
        }
        catch(e)
        {
            reject(e);
        }
    });
}




export function deleteAWSKey(input: AWS.S3.DeleteObjectRequest) : Promise<NodeJS.ApiResponseType>
{
    return new Promise(async (resolve, reject) =>
    {
        try
        {
           
            // console.log("key==", key);

            s3.deleteObject({Bucket: input.Bucket, Key: input.Key}, (err: AWS.AWSError, data: AWS.S3.DeleteObjectOutput) =>{
                if(err)
                {
                    console.log("Coming in err");
                    reject(err);
                    return;
                }
                resolve({ message: `File deleted successfully with path ${input.Key}`,data:{}, status: 200 });
                return;    
            });
        }
        catch(e)
        {
            reject(e);
        }
    });
}