import { USER_ROLES } from "./constants";

export interface LOGIN_PAYLOAD {
    userId: string; //(10^7)
    userName: string,
    name: string,
    source: number,
    emailId: string,
    ipaddress: string,
    role: USER_ROLES,
    permission: string,
    phoneNumber: string,
    loginHistoryId: string
}

export interface SIGNATURE_VERIFICATION_PAYLOAD {
    name: string,
    emailId: string,
    userId: string,
    index: number,
    contractId: string
}
