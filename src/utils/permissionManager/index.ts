import { permissonLevelsConstants, moduleConstants, hasPermission, validateNewPermissions, getDefaultPermissions, getAllPossiblePermissions, updatePermission } from './lib';


export { permissonLevelsConstants, moduleConstants, hasPermission, validateNewPermissions, getDefaultPermissions, getAllPossiblePermissions, updatePermission};

/***
 * HOW TO
 * set getDefaultPermissions and initialiseMaxPermissionMap very CAREFULLY
 *
 * WHENEVER adding/Removing any Module DONT FORGOT TO CHANGE initialiseMaxPermissionMap and CAREFULLY
 */