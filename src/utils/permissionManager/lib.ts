import { USER_ROLES } from '../constants';

let MAX_PERMS_LENGTH = 16;

interface PermissonLevel {
    NONE: 0,
    READ: 1,
    UPDATE: 3,
    DELETE: 5,
    ADD: 7
}

export const permissonLevelsConstants: PermissonLevel =
{
    NONE: 0,
    READ: 1,
    UPDATE: 3,
    DELETE: 5,
    ADD: 7
}

interface Module {
    ABOUT_US: 0,
    PRIVACY_POLICY: 1,
    TERMS_CONDITION: 2,
    DISCLAIMER: 3,
    NEWSROOM: 4,
    LAWBOOK: 5,
    USERS: 6,
    TEAM: 7,
    FAQ: 8,
    CATEGORY: 9,
    EXPERT_LEGAL_ADVICE_REQUESTS: 10
}

export const moduleConstants: Module =
{
    ABOUT_US: 0,
    PRIVACY_POLICY: 1,
    TERMS_CONDITION: 2,
    DISCLAIMER: 3,
    NEWSROOM: 4,
    LAWBOOK: 5,
    USERS: 6,
    TEAM: 7,
    FAQ: 8,
    CATEGORY: 9,
    EXPERT_LEGAL_ADVICE_REQUESTS: 10
}

class PermissionScope {
    private module: number;
    public maxPermissionConstant: number = permissonLevelsConstants.NONE;
    public upMap: Map<USER_ROLES, number>;
    public defPermMap: Map<USER_ROLES, number[]>;

    constructor(module: number, maxPermissionsList: number[]) {
        this.module = module;
        this.upMap = new Map();
        this.defPermMap = new Map();

        if (maxPermissionsList && maxPermissionsList.length > 0) {
            this.maxPermissionConstant = 1;
            for (let s of maxPermissionsList) {
                this.maxPermissionConstant *= s;
            }
        }
        if (this.maxPermissionConstant === permissonLevelsConstants.NONE) {
            throw new Error("maxPermissionConstant cannot be of NONE scope");
        }

        PERMISSION_SCOPE_MAP.set(module, this);
    }

    private getTotalPermissionResult(permissionLevels: Array<number>) {
        let result: number = (permissionLevels.length == 0) ? 0 : 1;
        for (let pl of permissionLevels) {
            if (this.maxPermissionConstant % pl === 0) {
                result *= pl;
            }
            else {
                throw new Error("getTotalPermissionResult cannot accept permissionLevels outside the scope of maxPermissionConstant");
            }
        }

        return result;
    }

    public setPermissionLevelForRole(role: USER_ROLES, permissionLevels: Array<number>, defaultPermissionLevels: Array<number>) {
        this.upMap.set(role, this.getTotalPermissionResult(permissionLevels));
        this.defPermMap.set(role, defaultPermissionLevels);
    }
}



let PERMISSION_SCOPE_MAP: Map<number, PermissionScope> = new Map();



function validateMaxPermission(): boolean {
    let ok = true;
    for (let moduleName in moduleConstants) {
        if (moduleConstants.hasOwnProperty(moduleName)) {
            let moduleValue: number = (moduleConstants as any)[moduleName];

            let userRolesFound: Map<string, boolean> = new Map();
            //initializing no user role found
            for (let role in USER_ROLES) {
                if (USER_ROLES.hasOwnProperty(role)) {
                    let roleValue: string = (USER_ROLES as any)[role];
                    userRolesFound.set(roleValue, false);
                }
            }



            let upmap = (PERMISSION_SCOPE_MAP.get(moduleValue) as PermissionScope).upMap;
            if (upmap) {
                for (let [role, permissionResult] of upmap) {
                    userRolesFound.set(role, true);
                }
            }

            for (let [role, found] of userRolesFound) {
                if (!found) {
                    ok = false;
                    console.log(`PERMISSION NOT SET FOR ROLE:${role} for module:${moduleName}`)
                }
            }
        }
    }

    return ok;
}

function getAllPermission(): number[] {
    let r: number[] = [];

    for (let permissionName in permissonLevelsConstants) {
        if (permissonLevelsConstants.hasOwnProperty(permissionName)) {
            if ((permissonLevelsConstants as any)[permissionName] != permissonLevelsConstants.NONE)
                r.push((permissonLevelsConstants as any)[permissionName]);
        }
    }

    return r;
}

function initialiseMaxPermissionMap() {
    let { ADD, UPDATE, DELETE, READ } = permissonLevelsConstants;
    let {
        ABOUT_US,
        PRIVACY_POLICY,
        TERMS_CONDITION,
        DISCLAIMER,
        NEWSROOM,
        LAWBOOK,
        USERS,
        TEAM,
        FAQ,
        CATEGORY,
        EXPERT_LEGAL_ADVICE_REQUESTS
    } = moduleConstants;

    let { ADMIN, CLIENT, CONTENT_WRITER, LAWYER, SENIOR_MANAGER, MANAGER, BUSINESS_MANAGER, BUSINESS_EXECUTIVE } = USER_ROLES;

    let permissionScope = new PermissionScope(ABOUT_US, getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], [READ]);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], [READ]);

    permissionScope = new PermissionScope(PRIVACY_POLICY, getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], [READ]);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], [READ]);

    permissionScope = new PermissionScope(TERMS_CONDITION, getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], [READ]);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], [READ]);

    permissionScope = new PermissionScope(DISCLAIMER, getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], [READ]);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], [READ]);

    permissionScope = new PermissionScope(NEWSROOM, getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], [READ]);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], [READ]);

    permissionScope = new PermissionScope(LAWBOOK, getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], [READ]);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], [READ]);

    permissionScope = new PermissionScope(USERS, getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], [READ]);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], [READ]);

    permissionScope = new PermissionScope(TEAM, getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], [READ]);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], [READ]);

    permissionScope = new PermissionScope(FAQ, getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], [READ]);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], [READ]);

    permissionScope = new PermissionScope(CATEGORY, getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(MANAGER, [UPDATE, READ], [UPDATE, READ]);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], [READ]);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], [READ]);

    permissionScope = new PermissionScope(EXPERT_LEGAL_ADVICE_REQUESTS,getAllPermission());
    permissionScope.setPermissionLevelForRole(ADMIN, getAllPermission(), getAllPermission());
    permissionScope.setPermissionLevelForRole(CONTENT_WRITER, [], []);
    permissionScope.setPermissionLevelForRole(SENIOR_MANAGER, [], []);
    permissionScope.setPermissionLevelForRole(BUSINESS_MANAGER, [], []);
    permissionScope.setPermissionLevelForRole(BUSINESS_EXECUTIVE, [], []);
    permissionScope.setPermissionLevelForRole(MANAGER, [READ], []);
    permissionScope.setPermissionLevelForRole(CLIENT, [READ], []);
    permissionScope.setPermissionLevelForRole(LAWYER, [READ], []);




    if (!validateMaxPermission()) {
        console.log("initialiseMaxPermissionMap FAILED");
    }
    else {
        console.log("MaxPermissions are VALID")
    }
}

function sanitizePerm(s: string): string[] {
    if (!s) {
        s = "";
    }
    s = s.replace(/ /g, '');//REPLACE ALL SPACES
    s = s.replace(/[^0-9-]/g, "");;//REPLACE ALL NON-NUMERIC CHARS except -


    let temp: string[] = s.split("-");

    let ps = [];
    for (let p of temp) {
        if (p)
            ps.push(p);
    }

    while (ps.length < MAX_PERMS_LENGTH) {
        ps.push("0");
    }

    let extra = ps.length - MAX_PERMS_LENGTH;

    if (extra > 0) {
        ps.splice(MAX_PERMS_LENGTH, extra);
    }

    return ps;
}

function convertPermissionArrayToString(ps: string[]): string {
    let permission = "";
    let isFirst = true;
    for (let i = 0; i < ps.length; i++) {
        if (!isFirst) {
            permission += "-";
        }
        permission += ps[i];
        isFirst = false;
    }
    return permission;
}

function validateModuleIndex(moduleIndex: number | string): boolean {
    let ok = false;
    moduleIndex = parseInt(moduleIndex as any);
    for (let moduleName in moduleConstants) {
        if (moduleConstants.hasOwnProperty(moduleName)) {
            if (moduleIndex === (moduleConstants as any)[moduleName]) {
                ok = true;
                break;
            }
        }
    }

    if (!ok) {
        throw new Error("Invalid Module Index");
    }

    return ok;
}

function validatePermissionLevel(moduleIndex: number | string, permissionIndex: number | string): boolean {
    let ok = false;
    moduleIndex = parseInt(moduleIndex as any);
    permissionIndex = parseInt(permissionIndex as any);

    let maxPermissionConstant = (PERMISSION_SCOPE_MAP.get(moduleIndex) as PermissionScope).maxPermissionConstant;
    if (maxPermissionConstant && permissionIndex != permissonLevelsConstants.NONE) {
        ok = (maxPermissionConstant % permissionIndex === 0);
    }

    if (permissionIndex == permissonLevelsConstants.NONE) {
        ok = true;
    }
    if (!ok) {
        throw new Error("Invalid Permission Level,It may be out of max scope");
    }

    return ok;
}

function validateUserRole(role: string): boolean {
    let ok = false;
    for (let key in USER_ROLES) {
        if (USER_ROLES.hasOwnProperty(key)) {
            if (role === (USER_ROLES as any)[key]) {
                ok = true;
                break;
            }
        }
    }
    if (!ok) {
        throw new Error("Invalid User Role");
    }

    return ok;
}

/***
 * NODE,ANGULAR
 */
export function hasPermission(moduleIndex: number, permissionLevel: number, permissionStr: string): boolean {
    validateModuleIndex(moduleIndex);
    validatePermissionLevel(moduleIndex, permissionLevel);

    let permisisonsList: string[] = sanitizePerm(permissionStr);

    let currentPermissionResult: string | number = permisisonsList[moduleIndex];

    if (!currentPermissionResult) {
        currentPermissionResult = "0";
    }

    currentPermissionResult = parseInt(currentPermissionResult);

    return permissionLevel === 0 || (currentPermissionResult > 0 && currentPermissionResult % permissionLevel === 0);
}

/***
 * NODE
 */
export function validateNewPermissions(permissionStr: string, role: USER_ROLES): boolean {
    validateUserRole(role);
    let permissionsList: string[] = sanitizePerm(permissionStr);

    let userPermissionResult: number | string;
    let moduleValue: number, permissionLevelValue;

    for (let moduleName in moduleConstants) {
        if (moduleConstants.hasOwnProperty(moduleName)) {
            moduleValue = (moduleConstants as any)[moduleName];

            let maxPermissionResult: string | number | undefined = 0;
            let upmap = (PERMISSION_SCOPE_MAP.get(moduleValue) as PermissionScope).upMap;
            if (upmap) {
                maxPermissionResult = upmap.get(role);
                if (maxPermissionResult === undefined) {
                    return false;
                }
            }
            else {
                return false;
            }
            maxPermissionResult = parseInt(maxPermissionResult as any);
            if (!maxPermissionResult) {
                maxPermissionResult = 0;
            }

            userPermissionResult = permissionsList[moduleValue];
            userPermissionResult = parseInt(userPermissionResult);

            for (let permissionName in permissonLevelsConstants) {
                if (permissonLevelsConstants.hasOwnProperty(permissionName)) {
                    permissionLevelValue = (permissonLevelsConstants as any)[permissionName];

                    if (!maxPermissionResult || !userPermissionResult) {
                        if (!userPermissionResult) {
                            //ok ,asking for NONE PERMISSION
                        }
                        else {
                            //max permisssion allowed is NONE and user is asking more
                            return false;
                        }
                    }
                    else {
                        if (userPermissionResult % permissionLevelValue == 0 && maxPermissionResult % permissionLevelValue != 0) {
                            return false;
                        }
                    }
                }
            }
        }
    }
    return true;
}

/***
 * NODE
 */
export function getDefaultPermissions(role: USER_ROLES): string {
    validateUserRole(role);
    let permissionStr = "";

    let moduleIndex;
    let defPerm: number[];

    for (let moduleName in moduleConstants) {
        if (moduleConstants.hasOwnProperty(moduleName)) {
            moduleIndex = (moduleConstants as any)[moduleName];
            defPerm = ((PERMISSION_SCOPE_MAP.get(moduleIndex) as PermissionScope).defPermMap.get(role)) as number[];
            permissionStr = updatePermission(permissionStr, moduleIndex, defPerm, role);
        }
    }

    return convertPermissionArrayToString(sanitizePerm(permissionStr));
}

/***
 * ANGULAR
 *  returns array of {moduleName, moduleIndex, checked}
 */
export function getAllPossiblePermissions(role: USER_ROLES, moduleIndex: number, permissionStr: string): Array<{ name: string, value: number, checked: boolean }> {
    validateUserRole(role);
    validateModuleIndex(moduleIndex);

    let r: Array<{ name: string, value: number, checked: boolean }> = [];
    r.push({ name: "NONE", value: permissonLevelsConstants.NONE, checked: false });

    let a = (PERMISSION_SCOPE_MAP.get(moduleIndex) as PermissionScope).upMap;

    if (a) {
        let maxPermissionResult = a.get(role);
        if (!maxPermissionResult) {
            maxPermissionResult = 0;
        }

        let permissionLevelValue: number;

        for (let pName in permissonLevelsConstants) {
            if (permissonLevelsConstants.hasOwnProperty(pName)) {
                permissionLevelValue = (permissonLevelsConstants as any)[pName];

                if (maxPermissionResult && permissionLevelValue && maxPermissionResult % permissionLevelValue == 0) {
                    r.push({ name: pName, value: permissionLevelValue, checked: false });
                }
            }
        }
    }


    let permisisonsList: string[] = sanitizePerm(permissionStr);

    let currentPermissionResult: string | number = permisisonsList[moduleIndex];

    if (!currentPermissionResult) {
        currentPermissionResult = "0";
    }

    currentPermissionResult = parseInt(currentPermissionResult);

    for (let a of r) {
        if (!currentPermissionResult) {
            if (a.value === permissonLevelsConstants.NONE) {
                a.checked = true;
            }
        }
        else {
            if (a.value !== permissonLevelsConstants.NONE) {
                a.checked = currentPermissionResult % a.value == 0;
            }
        }
    }


    return r;
}

/***
 * ANGULAR
 */
export function updatePermission(permissionStr: string, moduleIndex: number, permissionLevels: Array<number | string>, role: USER_ROLES): string {
    validateModuleIndex(moduleIndex);
    validateUserRole(role);
    console.log("Updating Permission", permissionStr, moduleIndex, permissionLevels, role);
    let permissionsList: string[] = sanitizePerm(permissionStr);

    if (permissionLevels.length == 0) {
        permissionLevels.push(permissonLevelsConstants.NONE);
    }

    let maxPermissionResult: string | number | undefined = 0;
    let upmap = (PERMISSION_SCOPE_MAP.get(moduleIndex) as PermissionScope).upMap;
    if (upmap) {
        maxPermissionResult = upmap.get(role);
        if (maxPermissionResult === undefined) {
            throw new Error("User Permission Result not found for this module");
        }
    }
    else {
        throw new Error("User Permission Map not found for this module");
    }

    maxPermissionResult = parseInt(maxPermissionResult as any);
    if (!maxPermissionResult) {
        maxPermissionResult = 0;
    }

    let newPermissionResult: number = 1;
    for (let newPermissionLevel of permissionLevels) {
        validatePermissionLevel(moduleIndex, newPermissionLevel);
        newPermissionLevel = parseInt(newPermissionLevel as any);

        if (!newPermissionLevel) {
            newPermissionLevel = 0;
        }

        if (newPermissionLevel) {
            if (!maxPermissionResult || maxPermissionResult % newPermissionLevel != 0) {
                console.log(newPermissionLevel, maxPermissionResult);

                throw new Error("Invalid Permission---");
            }

            newPermissionResult *= newPermissionLevel;
        }
        else {
            newPermissionResult = 0;
        }
    }

    permissionsList[moduleIndex] = newPermissionResult.toString();

    return convertPermissionArrayToString(permissionsList);
}

initialiseMaxPermissionMap();


console.log("Get all")
//console.log("IndexMap:",MAX_PERMISSIONS_MAP);

//console.log("#",updatePermission("-10",0,[7,5],USER_ROLES.ADMIN));

//console.log(getDefaultPermissions(USER_ROLES.CLIENT));

// console.log(getAllPossiblePermissions(USER_ROLES.SENIOR_MANAGER,5));
// // console.log({});


// hasPermission(permissionsModulesAndIndexes.NEWSROOM, Permissions.READ_UPDATE_DELETE_ADD, "");