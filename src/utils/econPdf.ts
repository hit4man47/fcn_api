const pdf = require('html-pdf');
import * as path from 'path';
import { ERROR_CODES } from './constants';

export function createPdf(htmlTemplate: string,outputDirPath:string): Promise<string>
{
    return new Promise(async (resolve, reject) =>
    {
        let fileName=`${Date.now()}.pdf`;
        const options = { format: 'A4', orientation: 'portrait'};
        pdf.create(htmlTemplate, options).toFile(path.resolve(outputDirPath,fileName), function (err: any, res: any)
        {
            if (err)
            {
                global.rejector(reject,ERROR_CODES.PDF_GENERATION_ERROR);
            }
            else
            {
                console.log("PDF Generated",path.resolve(outputDirPath,fileName));
                resolve(fileName);
            }
        });
    });
}
